/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Oluwasegun.Idowu
 * @param <T>
 */
public abstract class AbstractFacade<T extends Serializable> implements AbstractFacadeLocal<T>, Serializable {

    private Class<T> t;

    protected abstract EntityManager getEntityManager();
    protected Logger log;
    protected String entityNamePrefix = "";

    public AbstractFacade(Class<T> classT) {
        this.t = classT;
        log = Logger.getLogger(t.getName());
    }

    @Override
    public T create(T entity) throws Exception {
        return getEntityManager().merge(entity);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    protected abstract T edit(T entity) throws Exception;

    @Override
    public T find(Long id) throws Exception {
        return getEntityManager().find(t, id);
    }

    @Override
    public T find(BigInteger id) throws Exception {
        return getEntityManager().find(t, id);
    }

    @Override
    public T find(String id) throws Exception {
        return getEntityManager().find(t, id);
    }

    @Override
    public List<T> findAll() throws Exception {
        String c = entityIdentifier();
        StringBuilder builder = new StringBuilder();
        builder.append("select ").append(c).append(" from ").append(t.getSimpleName()).append(" ").append(c);
        String query = builder.toString();
//        return getEntityManager().createQuery().getResultList();
                return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    public List<T> findAll(int min, int max) throws Exception {
        String c = entityIdentifier();
                StringBuilder builder = new StringBuilder();
        builder.append("select ").append(c).append(" from ").append(t.getSimpleName()).append(" ").append(c);
        String query = builder.toString();
//        return getEntityManager().createQuery("select " + c + " from " + t.getSimpleName() + " " + c).setFirstResult(min).setMaxResults(max).getResultList();
       return getEntityManager().createQuery(query).setFirstResult(min).setMaxResults(max).getResultList();
    }

    private Query getQuery(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception {
        String c = entityIdentifier();
        String orderClause = "";
        if (orderFields.length > 0) {
            int i = 0;
            orderClause += " order by " + c + ".";
            for (String orderField : orderFields) {
                i++;
                if (orderField == null) {
                    orderClause = "";
                }
                orderClause += i == orderFields.length ? orderField + " DESC": orderField + " DESC,";
            }
        }
        String filterWhere = "";
        if (filters != null) {
            int i = 0;
            filterWhere += filters.isEmpty() ? "" : " where ";
            for (Map.Entry<String, Object> entry : filters.entrySet()) {
                i++;
                String f;
                if (entry.getValue() != null) {
                    if (entry.getValue() instanceof String) {
                        String val = (String) entry.getValue();
                        if (val.isEmpty()) {
                            continue;
                        }
                        f = c + "." + entry.getKey() + " like '%" + entry.getValue() + "%'";
                    } else {
                        f = c + "." + entry.getKey() + "=:" + entry.getKey().replaceAll("\\.", "");
                    }
                } else {
                    f = entry.getKey();
                }
                filterWhere += i == filters.size() ? f : f + " and ";
            }
        }
        filterWhere += activeOnly ? filterWhere.contains("where") ? (" and " + c + ".active=" + Boolean.TRUE) : (" where " + c + ".active=" + Boolean.TRUE) : "";
        String flagStatusClause = "";
        if (flagStatuses != null && flagStatuses.length > 0) {
            int i = 0;
            flagStatusClause += filterWhere.contains("where") ? " and " : " where ";
            flagStatusClause += c + ".flagStatus in ('";
            for (String flagStatus : flagStatuses) {
                i++;
                flagStatusClause += i == flagStatuses.length ? flagStatus + "')" : flagStatus + "','";
            }
        }

//
        //       log.log(Level.INFO, "filterWhere: {0}", filterWhere);
        //   log.log(Level.INFO, "flagStatusClause: {0}", flagStatusClause);
        //   log.log(Level.INFO, "orderClause: {0}", orderClause);
        // log.log(Level.INFO, "entityNamePrefix:::: {0}", entityNamePrefix);
        String query = "select " + c + " from " + entityNamePrefix + t.getSimpleName() + " " + c + filterWhere + flagStatusClause + (orderClause.equals("null") ? "" : orderClause);
         //          log.log(Level.INFO, "query:::: {0}", query);
         if(t.getSimpleName().equals("Settlement") || t.getSimpleName().equals("Transaction") || t.getSimpleName().contains("Payment") || t.getSimpleName().contains("HoldingAccount")) {
        log.log(Level.INFO, "query:::: {0}", query);
        }
        return getEntityManager().createQuery(query);
    }

    private Query getQuery(Map<String, Object> filters, String[] flagStatuses, String... orderFields) throws Exception {
        String c = entityIdentifier();
        String orderClause = "";
        if (orderFields.length > 0) {
            int i = 0;
            orderClause += " order by " + c + ".";
            for (String orderField : orderFields) {
                i++;
                if (orderField == null) {
                    orderClause = "";
                }
                orderClause += i == orderFields.length ? orderField + " DESC": orderField + " DESC,";
            }
        }
        String filterWhere = "";
        if (filters != null) {
            int i = 0;
            filterWhere += filters.isEmpty() ? "" : " where ";
            for (Map.Entry<String, Object> entry : filters.entrySet()) {
                i++;
                String f;
                if (entry.getValue() != null) {
                    if (entry.getValue() instanceof String) {
                        String val = (String) entry.getValue();
                        if (val.isEmpty()) {
                            continue;
                        }
                        f = c + "." + entry.getKey() + " like '%" + entry.getValue() + "%'";
                    } else {
                        f = c + "." + entry.getKey() + "=:" + entry.getKey().replaceAll("\\.", "");
                    }
                } else {
                    f = entry.getKey();
                }
                filterWhere += i == filters.size() ? f : f + " and ";
            }
        }
        //filterWhere += activeOnly ? filterWhere.contains("where") ? (" and " + c + ".active=" + Boolean.TRUE) : (" where " + c + ".active=" + Boolean.TRUE) : "";
        String flagStatusClause = "";
        if (flagStatuses != null && flagStatuses.length > 0) {
            int i = 0;
            flagStatusClause += filterWhere.contains("where") ? " and " : " where ";
            flagStatusClause += c + ".flagStatus in ('";
            for (String flagStatus : flagStatuses) {
                i++;
                flagStatusClause += i == flagStatuses.length ? flagStatus + "')" : flagStatus + "','";
            }
        }

//
        //       log.log(Level.INFO, "filterWhere: {0}", filterWhere);
        //   log.log(Level.INFO, "flagStatusClause: {0}", flagStatusClause);
        //   log.log(Level.INFO, "orderClause: {0}", orderClause);
        // log.log(Level.INFO, "entityNamePrefix:::: {0}", entityNamePrefix);
        String query = "select " + c + " from " + entityNamePrefix + t.getSimpleName() + " " + c + filterWhere + flagStatusClause + (orderClause.equals("null") ? "" : orderClause);
                if(t.getSimpleName().equals("Settlement") || t.getSimpleName().equals("Transaction")  || t.getSimpleName().contains("Payment") || t.getSimpleName().contains("HoldingAccount")) {
        log.log(Level.INFO, "query:::: {0}", query);
        }
// log.log(Level.INFO, "query:::: {0}", query);
        return getEntityManager().createQuery(query);
    }

    private Query getQuery(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        String c = entityIdentifier();
        String orderClause = "";
        if (orderFields.length > 0) {
            int i = 0;
            orderClause += " order by " + c + ".";
            for (String orderField : orderFields) {
                i++;
                if (orderField == null) {
                    orderClause = "";
                }
                orderClause += i == orderFields.length ? orderField + " DESC": orderField + " DESC,";
            }
        }
        String filterWhere = "";
        if (filters != null) {
            int i = 0;
            filterWhere += filters.isEmpty() ? "" : " where ";
            for (Map.Entry<String, Object> entry : filters.entrySet()) {
                i++;
                String f;
                if (entry.getValue() != null) {
                    if (entry.getValue() instanceof String) {
                        String val = (String) entry.getValue();
                        if (val.isEmpty()) {
                            continue;
                        }
                        f = c + "." + entry.getKey() + " like '%" + entry.getValue() + "%'";
                    } else {
                        f = c + "." + entry.getKey() + "=:" + entry.getKey().replaceAll("\\.", "");
                    }
                } else {
                    f = entry.getKey();
                }
                filterWhere += i == filters.size() ? f : f + " and ";
            }
        }
        filterWhere += activeOnly ? filterWhere.contains("where") ? (" and " + c + ".active=" + Boolean.TRUE) : (" where " + c + ".active=" + Boolean.TRUE) : "";
        String fromDateStr = null;
        String toDateStr = null;
        if (fromDate == null && toDate != null) {
            fromDate = new Date();
            Calendar from = Calendar.getInstance();
            from.setTime(fromDate);
//            fromDateStr = from.get(Calendar.YEAR) + "-" + (from.get(Calendar.MONTH) + 1) + "-" + from.get(Calendar.DAY_OF_MONTH) + " " + from.get(Calendar.HOUR_OF_DAY) + ":" + from.get(Calendar.MINUTE) + ":" + from.get(Calendar.SECOND);;
                  fromDateStr = getDateString(from);
            Calendar to = Calendar.getInstance();
            to.setTime(toDate);
              if(fromDate.equals(toDate)) {
            to.set(Calendar.DAY_OF_MONTH, to.get(Calendar.DAY_OF_MONTH) + 1);
              }
//            toDateStr = to.get(Calendar.YEAR) + "-" + (to.get(Calendar.MONTH) + 1) + "-" + to.get(Calendar.DAY_OF_MONTH) + " " + to.get(Calendar.HOUR_OF_DAY) + ":" + to.get(Calendar.MINUTE) + ":" + to.get(Calendar.SECOND);;
             toDateStr = getDateString(to);
            filterWhere += filterWhere.contains("where") ? (" and " + c + "." + dateField + " <= '" + toDateStr + "'") : (" where " + c + "." + dateField + " <= '" + toDateStr + "'");
        } else if (fromDate != null && toDate == null) {
            Calendar from = Calendar.getInstance();
            from.setTime(fromDate);
            from.set(Calendar.DAY_OF_MONTH, from.get(Calendar.DAY_OF_MONTH));
//            fromDateStr = from.get(Calendar.YEAR) + "-" + (from.get(Calendar.MONTH) + 1) + "-" + from.get(Calendar.DAY_OF_MONTH)  + " " + from.get(Calendar.HOUR_OF_DAY) + ":" + from.get(Calendar.MINUTE) + ":" + from.get(Calendar.SECOND);
              fromDateStr = getDateString(from);
            toDate = new Date();
            Calendar to = Calendar.getInstance();
            to.setTime(toDate);
              if(fromDate.equals(toDate)) {
            to.set(Calendar.DAY_OF_MONTH, to.get(Calendar.DAY_OF_MONTH) + 1);
              }
//            toDateStr = to.get(Calendar.YEAR) + "-" + (to.get(Calendar.MONTH) + 1) + "-" + to.get(Calendar.DAY_OF_MONTH) + " " + to.get(Calendar.HOUR_OF_DAY) + ":" + to.get(Calendar.MINUTE) + ":" + to.get(Calendar.SECOND);
             toDateStr = getDateString(to);
            filterWhere += filterWhere.contains("where") ? (" and " + c + "." + dateField + " >='" + fromDateStr
                    + "'") : (" where " + c + "." + dateField + " >='" + fromDateStr
                    + "'");
        } else if (fromDate != null && toDate != null) {
            Calendar from = Calendar.getInstance();
            from.setTime(fromDate);
//            fromDateStr = from.get(Calendar.YEAR) + "-" + (from.get(Calendar.MONTH) + 1) + "-" + from.get(Calendar.DAY_OF_MONTH) + " " + from.get(Calendar.HOUR_OF_DAY) + ":" + from.get(Calendar.MINUTE) + ":" + from.get(Calendar.SECOND);
                        fromDateStr = getDateString(from);
            Calendar to = Calendar.getInstance();
            to.setTime(toDate);
            if(fromDate.equals(toDate)) {
            to.set(Calendar.DAY_OF_MONTH, to.get(Calendar.DAY_OF_MONTH) + 1);
            }
//            if (sameDaySearch) {
//                to.set(Calendar.DAY_OF_YEAR, to.get(Calendar.DAY_OF_YEAR) + 1);
//                to.set(Calendar.HOUR_OF_DAY, 0);
//                to.set(Calendar.MINUTE, 0);
//                to.set(Calendar.SECOND, 0);
//                to.set(Calendar.MILLISECOND, 0);
//            }
//            toDateStr = to.get(Calendar.YEAR) + "-" + (to.get(Calendar.MONTH) + 1) + "-" + to.get(Calendar.DAY_OF_MONTH) + " " + to.get(Calendar.HOUR_OF_DAY) + ":" + to.get(Calendar.MINUTE) + ":" + to.get(Calendar.SECOND);
                       toDateStr = getDateString(to);
            filterWhere += filterWhere.contains("where") ? (" and " + c + "." + dateField + " >='" + fromDateStr
                    + "' and " + c + "." + dateField + " <= '" + toDateStr + "'") : (" where " + c + "." + dateField + " >='" + fromDateStr
                    + "' and " + c + "." + dateField + " <= '" + toDateStr + "'");
        }
  //      log.log(Level.INFO, "fromDateee: {0}", fromDate);
   //     log.log(Level.INFO, "toDateee: {0}", toDate);

        String flagStatusClause = "";
        if (flagStatuses != null && flagStatuses.length > 0) {
            int i = 0;
            flagStatusClause += filterWhere.contains("where") ? " and " : " where ";
            flagStatusClause += c + ".flagStatus in ('";
            for (String flagStatus : flagStatuses) {
                i++;
                flagStatusClause += i == flagStatuses.length ? flagStatus + "')" : flagStatus + "','";
            }
        }

        String query = "select " + c + " from " + entityNamePrefix + t.getSimpleName() + " " + c + filterWhere + flagStatusClause + (orderClause.equals("null") ? "" : orderClause);
        if(t.getSimpleName().equals("Settlement") || t.getSimpleName().equals("Transaction")  || t.getSimpleName().contains("Payment")  || t.getSimpleName().contains("AuditTable") || t.getSimpleName().contains("HoldingAccount")) {
        log.log(Level.INFO, "query:::: {0}", query);
        }
        return getEntityManager().createQuery(query);
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, null, orderFields);

        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, false, null, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, flagStatuses, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null, fromDate, toDate, dateField, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, flagStatuses, fromDate, toDate, dateField, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public long findAllCount(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll().size();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, flagStatuses, fromDate, toDate, dateField, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.getResultList().size();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, flagStatuses, orderFields);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

    @Override
    public List<T> findAll(Map<String, Object> filters, boolean activeOnly) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.getResultList();
    }

    @Override
    public List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly) throws Exception {
        if (filters == null) {
            return findAll();
        }
        Set<Map.Entry<String, Object>> entrySet = filters.entrySet();
        Query query = getQuery(filters, activeOnly, null);
        for (Map.Entry<String, Object> entry : entrySet) {
            if (entry.getValue() != null && !(entry.getValue() instanceof String)) {
                query.setParameter(entry.getKey().replaceAll("\\.", ""), entry.getValue());
            }
        }
        return query.setFirstResult(min).setMaxResults(max).getResultList();
    }

//    protected abstract T approve(T entity) throws Exception;
//
//    protected abstract T reject(T entity) throws Exception;
    private String entityIdentifier() {
        return String.valueOf(t.getSimpleName().charAt(0)).toLowerCase();
    }

    @Override
    public void setEntityNamePrefix(String prefix) {
        entityNamePrefix = prefix;
    }



    /**
     * @return the bankUser
     */
    public boolean isBankUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return loggedInUser instanceof BankUser;
    }

    /**
     * @return the bankUser
     */
    public Bank getLoggedBank() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (!(loggedInUser instanceof BankUser)) {
            return null;
        } else {
            return ((BankUser) loggedInUser).getBank();
        }
    }

    /**
     * @return the bankUser
     */
    public User getLoggedUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return loggedInUser;
    }

    /**
     * @return the bankUser
     */
    public Corporate getLoggedCorporate() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (!(loggedInUser instanceof CorporateUser)) {
            return null;
        } else {
            return ((CorporateUser) loggedInUser).getCorporate();
        }
    }

    /**
     * @return the corporateUser
     */
    public boolean isCorporateUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return loggedInUser instanceof CorporateUser;
    }

    /**
     * @return the etzUser
     */
    public boolean isEtzUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return !(loggedInUser instanceof BankUser || loggedInUser instanceof CorporateUser);
    }

    public static void main(String[] args) {
        Calendar to = Calendar.getInstance();

        String month = String.valueOf((to.get(Calendar.MONTH) + 1)).length() < 2?"0" + String.valueOf((to.get(Calendar.MONTH) + 1)):String.valueOf((to.get(Calendar.MONTH) + 1));
        String dayOfMonth = String.valueOf(to.get(Calendar.DAY_OF_MONTH)).length() < 2?"0" + String.valueOf(to.get(Calendar.DAY_OF_MONTH)):String.valueOf(to.get(Calendar.DAY_OF_MONTH));
        String hour = String.valueOf(to.get(Calendar.HOUR_OF_DAY)).length() < 2?"0" + String.valueOf(to.get(Calendar.HOUR_OF_DAY)):String.valueOf(to.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(to.get(Calendar.MINUTE)).length() < 2?"0" + String.valueOf(to.get(Calendar.MINUTE)):String.valueOf(to.get(Calendar.MINUTE));
        String second = String.valueOf(to.get(Calendar.SECOND)).length() < 2?"0" + String.valueOf(to.get(Calendar.SECOND)):String.valueOf(to.get(Calendar.SECOND));
        String ss= to.get(Calendar.YEAR) + "-" + month + "-" + dayOfMonth + " " + hour + ":" + minute + ":" + second;
        System.out.println("ss: " + ss);
        java.sql.Date valueOf = java.sql.Date.valueOf(ss);
        System.out.println("valueOf: " + valueOf);
    }
    
    private String getDateString(Calendar to) {
        
        String month = String.valueOf((to.get(Calendar.MONTH) + 1)).length() < 2?"0" + String.valueOf((to.get(Calendar.MONTH) + 1)):String.valueOf((to.get(Calendar.MONTH) + 1));
        String dayOfMonth = String.valueOf(to.get(Calendar.DAY_OF_MONTH)).length() < 2?"0" + String.valueOf(to.get(Calendar.DAY_OF_MONTH)):String.valueOf(to.get(Calendar.DAY_OF_MONTH));
        String hour = String.valueOf(to.get(Calendar.HOUR_OF_DAY)).length() < 2?"0" + String.valueOf(to.get(Calendar.HOUR_OF_DAY)):String.valueOf(to.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(to.get(Calendar.MINUTE)).length() < 2?"0" + String.valueOf(to.get(Calendar.MINUTE)):String.valueOf(to.get(Calendar.MINUTE));
        String second = String.valueOf(to.get(Calendar.SECOND)).length() < 2?"0" + String.valueOf(to.get(Calendar.SECOND)):String.valueOf(to.get(Calendar.SECOND));
        String ss= to.get(Calendar.YEAR) + "-" + month + "-" + dayOfMonth + " " + hour + ":" + minute + ":" + second;
        return ss;
    }

}
