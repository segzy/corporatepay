/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.payroll.facade.PayItemFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.PayrollPeriodFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.StatutoryDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.StatutoryPayComponentFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryGroupStepFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryAdditionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.model.LoanDeduction;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.PayItem;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.RecurringAddition;
import com.etranzact.corporatepay.payroll.model.RecurringDeduction;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersAddition;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.StatutoryDeduction;
import com.etranzact.corporatepay.payroll.model.StatutoryPayComponent;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkerSalaryItem;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroupStep;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryAddition;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "payrollPeriodBean")
@SessionScoped
public class PayrollPeriodBean extends AbstractBean<PayrollPeriod> {

    @EJB
    private PayrollPeriodFacadeLocal payrollPeriodFacadeLocal;
    @EJB
    private WorkersCategoryFacadeLocal workersCategoryFacadeLocal;
//    @EJB
//    private WorkersCategoryGroupStepFacadeLocal workersCategoryGroupStepFacadeLocal;
    @EJB
    private StatutoryPayComponentFacadeLocal statutoryPayComponentFacadeLocal;
    @EJB
    private WorkersFacadeLocal workersFacadeLocal;
    @EJB
    private PayItemFacadeLocal payItemFacadeLocal;
    @EJB
    private StatutoryDeductionFacadeLocal statutoryDeductionFacadeLocal;
    @EJB
    private WorkersSalaryAdditionFacadeLocal workersSalaryAdditionFacadeLocal;
    @EJB
    private WorkersSalaryDeductionFacadeLocal workersSalaryDeductionFacadeLocal;
    private List<WorkersCategory> workersCategories;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;

    @Override
    protected PayrollPeriodFacadeLocal getFacade() {
        return payrollPeriodFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

//    @Override
//    public void onRowSelect(SelectEvent selectEvent) {
//        super.onRowSelect(selectEvent);
//        try {
//            selectedItem = payrollPeriodFacadeLocal.find(selectedItem.getTag());
//        } catch (Exception ex) {
//            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    public PayrollPeriodBean() {
        super(PayrollPeriod.class);
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            Map<String, Object> map;
            if (selectedItem.getFlagStatus() == null) {
                map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("tag", selectedItem.getTag());
                List<PayrollPeriod> payrollPeriods = payrollPeriodFacadeLocal.findAll(map, true);
                if (!payrollPeriods.isEmpty()) {
                    throw new Exception("Payroll Period already exist");
                }
            }
            selectedItem.setCorporate(getLoggedCorporate());
            selectedItem.setFlagStatus(AppConstants.PAYROLL_IN_PROGRESS);
            Calendar effectivedate = Calendar.getInstance();
            effectivedate.setTime(selectedItem.getEffectiveDate());
            Calendar enddate = Calendar.getInstance();
            enddate.setTime(selectedItem.getEndDate());
            if (effectivedate.getTimeInMillis() >= enddate.getTimeInMillis()) {
                throw new Exception("Effective Date must be earlier than End Date");
            }

            if (selectedItem.getTag() == null) {
                map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("flagStatus", AppConstants.PAYROLL_IN_PROGRESS);
                List<PayrollPeriod> payrollInProgressPeriods = getFacade().findAll(map, true);
                if (!payrollInProgressPeriods.isEmpty()) {
                    throw new Exception("Cannot create another Period until salaries of existing ones have been processed");
                }
                getFacade().create(selectedItem);
            } else {
                getFacade().create(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payroll Period Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new PayrollPeriod();
        }

    }

    /**
     * @return the workersCategories
     */
    public List<WorkersCategory> getWorkersCategories() {
        if (workersCategories == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            try {
                workersCategories = workersCategoryFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
                workersCategories = new ArrayList<>();
            }
        }
        return workersCategories;
    }

    public void computeSalaries() {
        FacesMessage m = new FacesMessage();
        try {
            List<WorkersCategory> workersCategories1 = selectedItem.getWorkersCategories();
            if (workersCategories1.isEmpty()) {
                throw new Exception("At least 1 Category must be included in the Payroll");
            }
            for (WorkersCategory workersCategory : workersCategories1) {
                if (!workersCategory.getFlagStatus().equals(AppConstants.SETUP_COMPLETED)) {
                    throw new Exception(workersCategory.getId() + " setup has not been completed");
                }
            }
            Preferences corporatePreferences = preferenceFacadeLocal.getCorporatePreferences(getLoggedCorporate());
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            map.put("basic", Boolean.TRUE);
            List<PayItem> payItems = payItemFacadeLocal.findAll(map, true);
            if (payItems.isEmpty() || payItems.size() > 1) {
                throw new Exception("Error getting Monthly Basic Default PayItem");
            }
            PayItem basicPayItem = payItems.get(0);
            map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            List<StatutoryDeduction> statutoryDeductions = statutoryDeductionFacadeLocal.findAll(map, true);
            if (statutoryDeductions.isEmpty()) {
                throw new Exception("No Statutory Deduction SetUp");
            }
            Map<String, Worker> exceptionMap = new HashMap<>();
            for (WorkersCategory workersCategory : workersCategories1) {
                map = new HashMap<>();
                map.put("corporateBeneficiary.corporate", getLoggedCorporate());
                map.put("workersCategory", workersCategory);
                List<Worker> categoryWorkers = workersFacadeLocal.findAll(map, true);
                worker:
                for (Worker worker : categoryWorkers) {
                    List<WorkerSalaryItem> salaryItems = new ArrayList<>();
                    log.log(Level.INFO, "Worker:: {0} .......................................", worker.getStaffId());
                    WorkersCategoryGroupStep workersCategoryGroupStep = worker.getWorkersCategoryGroupStep();
                    if (workersCategoryGroupStep == null) {
                        exceptionMap.put(AppConstants.NO_SALARY_POISTION, worker);
                        continue;
                    } else if (workersCategoryGroupStep.getFlagStatus() == null || workersCategoryGroupStep.getFlagStatus().equals(AppConstants.SET_UP_IN_PROGRESS)) {
                        exceptionMap.put(AppConstants.SALARY_POISTION_NOT_SETUP, worker);
                        continue;
                    }

                    Set<StatutoryPayComponent> statutoryPayComponents = workersCategoryGroupStep.getStatutoryPayComponents();
                    log.info("processing Statutory Addition.... ");
                    map = new HashMap<>();
                    map.put("workersCategoryGroupStep", workersCategoryGroupStep);
                    map.put("payItem", basicPayItem);
                    List<StatutoryPayComponent> stepStatutoryPayComponents = statutoryPayComponentFacadeLocal.findAll(map, true);
                    StatutoryPayComponent basicPayComponent = stepStatutoryPayComponents.get(0);
                    BigDecimal netPay = BigDecimal.ZERO;
                    if (corporatePreferences.getPayrollMode().equals(AppConstants.PAYROLL_LITE)) {
                        if (worker.getGrossSalary() == null) {
                            exceptionMap.put(AppConstants.GROSS_SALARY_NOT_FOUND, worker);
                            continue;
                        }
                        netPay = netPay.add(worker.getGrossSalary());
                    } else {
                        for (StatutoryPayComponent statutoryPayComponent : statutoryPayComponents) {
                            WorkerSalaryItem workerSalaryItem = new WorkerSalaryItem();
                            BigDecimal actualStatutoryAdditionAmount = getActualStatutoryAdditionAmount(basicPayComponent, statutoryPayComponent);

                            log.log(Level.INFO, "amount for statutory addition ({0}) ::: {1}", new Object[]{statutoryPayComponent.getPayItem().getPayItemname(), actualStatutoryAdditionAmount});
                            netPay = netPay.add(actualStatutoryAdditionAmount);
                            workerSalaryItem.setAmount(actualStatutoryAdditionAmount);
                            workerSalaryItem.setEffect(AppConstants.ADDITIVE_PAYCOMPONENT);
                            workerSalaryItem.setItemId(statutoryPayComponent.getId());
                            workerSalaryItem.setPayrollPeriod(selectedItem);
                            workerSalaryItem.setItemType(AppConstants.STATUTORY_ADDITION);
                            workerSalaryItem.setWorker(worker);
                            salaryItems.add(workerSalaryItem);
                        }
                    }
                    log.info("processing Statutory Deduction.... ");
                    for (StatutoryDeduction statutoryDeduction : statutoryDeductions) {
                        WorkerSalaryItem workerSalaryItem = new WorkerSalaryItem();
                        BigDecimal actualStatutoryDeductionAmount;
                        if (corporatePreferences.equals(AppConstants.PAYROLL_LITE)) {
                            if (worker.getGrossSalary() == null) {
                                exceptionMap.put(AppConstants.GROSS_SALARY_NOT_FOUND, worker);
                                continue worker;
                            }
                            actualStatutoryDeductionAmount = getActualStatutoryDeductionAmount(worker.getGrossSalary(), statutoryDeduction);
                        } else {
                            actualStatutoryDeductionAmount = getActualStatutoryDeductionAmount(basicPayComponent, statutoryDeduction);
                        }
                        log.log(Level.INFO, "amount for statutory deduction ({0}) ::: {1}", new Object[]{statutoryDeduction.getDeductionName(), actualStatutoryDeductionAmount});
                        netPay = netPay.subtract(actualStatutoryDeductionAmount);
                        workerSalaryItem.setAmount(actualStatutoryDeductionAmount);
                        workerSalaryItem.setEffect(AppConstants.DEDUCTIVE_PAYCOMPONENT);
                        workerSalaryItem.setItemId(statutoryDeduction.getId());
                        workerSalaryItem.setPayrollPeriod(selectedItem);
                        workerSalaryItem.setItemType(AppConstants.STATUTORY_DEDUCTION);
                        workerSalaryItem.setWorker(worker);
                        salaryItems.add(workerSalaryItem);
                    }
                    map = new HashMap<>();
                    map.put("worker", worker);
                    List<WorkersSalaryAddition> workersSalaryAdditions = workersSalaryAdditionFacadeLocal.findAll(map, true);
                    log.info("processing Other Additions.... ");
                    for (WorkersSalaryAddition workersSalaryAddition : workersSalaryAdditions) {
                        WorkerSalaryItem workerSalaryItem = new WorkerSalaryItem();
                        BigDecimal actualStatutoryDeductionAmount;
                        if (corporatePreferences.equals(AppConstants.PAYROLL_LITE)) {
                            if (worker.getGrossSalary() == null) {
                                exceptionMap.put(AppConstants.GROSS_SALARY_NOT_FOUND, worker);
                                continue worker;
                            }
                            actualStatutoryDeductionAmount = getActualOtherAdditionAmount(worker.getGrossSalary(), workersCategoryGroupStep, workersSalaryAddition);
                        } else {
                            actualStatutoryDeductionAmount = getActualOtherAdditionAmount(basicPayComponent, workersCategoryGroupStep, workersSalaryAddition);
                        }
                        log.log(Level.INFO, "amount for other addition ({0}) ::: {1}", new Object[]{workersSalaryAddition.getOtherAddition().getAdditionName(), actualStatutoryDeductionAmount});

                        netPay = netPay.add(actualStatutoryDeductionAmount);
                        workerSalaryItem.setAmount(actualStatutoryDeductionAmount);
                        workerSalaryItem.setEffect(AppConstants.ADDITIVE_PAYCOMPONENT);
                        workerSalaryItem.setItemId(workersSalaryAddition.getId());
                        workerSalaryItem.setPayrollPeriod(selectedItem);
                        workerSalaryItem.setItemType(workersSalaryAddition.getOtherAddition() instanceof OneTimeAddition ? AppConstants.ONE_TIME_ADDITION
                                : workersSalaryAddition.getOtherAddition() instanceof RecurringAddition ? AppConstants.RECURRING_ADDITION : "");
                        workerSalaryItem.setWorker(worker);
                        salaryItems.add(workerSalaryItem);
                    }
                    map = new HashMap<>();
                    map.put("worker", worker);
                    List<WorkersSalaryDeduction> workersSalaryDeductions = workersSalaryDeductionFacadeLocal.findAll(map, true);
                    log.info("processing Other Deduction.... ");
                    for (WorkersSalaryDeduction workersSalaryDeduction : workersSalaryDeductions) {
                        WorkerSalaryItem workerSalaryItem = new WorkerSalaryItem();
                        BigDecimal actualStatutoryDeductionAmount;

                        if (corporatePreferences.equals(AppConstants.PAYROLL_LITE)) {
                            if (worker.getGrossSalary() == null) {
                                exceptionMap.put(AppConstants.GROSS_SALARY_NOT_FOUND, worker);
                                continue worker;
                            }
                            actualStatutoryDeductionAmount = getActualOtherDeductionAmount(worker.getGrossSalary(), workersCategoryGroupStep, workersSalaryDeduction);
                        } else {
                            actualStatutoryDeductionAmount = getActualOtherDeductionAmount(basicPayComponent, workersCategoryGroupStep, workersSalaryDeduction);
                        }
                        log.log(Level.INFO, "amount for other deduction ({0}) ::: {1}", new Object[]{workersSalaryDeduction.getOtherDeduction().getDeductionName(), actualStatutoryDeductionAmount});
                        netPay = netPay.subtract(actualStatutoryDeductionAmount);
                        workerSalaryItem.setAmount(actualStatutoryDeductionAmount);
                        workerSalaryItem.setEffect(AppConstants.DEDUCTIVE_PAYCOMPONENT);
                        workerSalaryItem.setItemId(workersSalaryDeduction.getId());
                        workerSalaryItem.setPayrollPeriod(selectedItem);
                        workerSalaryItem.setItemType(workersSalaryDeduction.getOtherDeduction() instanceof OneTimeDeduction ? AppConstants.ONE_TIME_DEDUCTION
                                : workersSalaryDeduction.getOtherDeduction() instanceof RecurringDeduction ? AppConstants.RECURRING_DEDUCTION
                                        : workersSalaryDeduction.getOtherDeduction() instanceof LoanDeduction ? AppConstants.LOAN : "");
                        workerSalaryItem.setWorker(worker);
                        salaryItems.add(workerSalaryItem);
                    }
                    log.info("netpay:: " + netPay);
                    if (netPay.compareTo(BigDecimal.ONE) <= 0) {
                        exceptionMap.put(AppConstants.NEGATIVE_OR_ZERO_NET_SALARY, worker);
                        continue;
                    }
                    if (corporatePreferences.getPayrollMode().equals(AppConstants.PAYROLL_LITE)) {
                        WorkerSalaryItem workerSalaryItem = new WorkerSalaryItem();
                        workerSalaryItem.setAmount(netPay);
                        workerSalaryItem.setEffect(AppConstants.ADDITIVE_PAYCOMPONENT);
                        workerSalaryItem.setItemId(-1l);
                        workerSalaryItem.setPayrollPeriod(selectedItem);
                        workerSalaryItem.setItemType(AppConstants.STATUTORY_ADDITION);
                        workerSalaryItem.setWorker(worker);
                        salaryItems.add(workerSalaryItem);
                    }
                    log.info("exception count::: " + exceptionMap.size());
                    log.info("salary item " + salaryItems.size());

                }
            }

            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payroll Computed Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception e) {
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    private BigDecimal getActualStatutoryAdditionAmount(StatutoryPayComponent basicPayComponent, StatutoryPayComponent statutoryPayComponent) {
        try {
            if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                return statutoryPayComponent.getAmount();
            } else if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                return (statutoryPayComponent.getAmount().divide(new BigDecimal("100"))).multiply(basicPayComponent.getAmount());
            }
            return BigDecimal.ZERO;
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal getActualStatutoryDeductionAmount(StatutoryPayComponent basicPayComponent, StatutoryDeduction statutoryDeduction) {
        try {
            if (statutoryDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                return statutoryDeduction.getAmount();
            } else if (statutoryDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                return (statutoryDeduction.getAmount().divide(new BigDecimal("100"))).multiply(basicPayComponent.getAmount());
            }
            return BigDecimal.ZERO;
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal getActualStatutoryDeductionAmount(BigDecimal grossPay, StatutoryDeduction statutoryDeduction) {
        try {
            if (statutoryDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                return statutoryDeduction.getAmount();
            } else if (statutoryDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                return (statutoryDeduction.getAmount().divide(new BigDecimal("100"))).multiply(grossPay);
            }
            return BigDecimal.ZERO;
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal getActualOtherAdditionAmount(StatutoryPayComponent basicPayComponent, WorkersCategoryGroupStep workersCategoryGroupStep, WorkersSalaryAddition workersSalaryAddition) {
        try {
            List<PayItem> refStatutoryPayItems = workersSalaryAddition.getOtherAddition().getRefStatutoryPayItems();
            BigDecimal totalRef = BigDecimal.ZERO;
            for (PayItem payItem : refStatutoryPayItems) {
                if (payItem.getBasic()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("workersCategoryGroupStep", workersCategoryGroupStep);
                    map.put("payItem", payItem);
                    List<StatutoryPayComponent> refStatutoryPayComponents = statutoryPayComponentFacadeLocal.findAll(map, true);
                    if (refStatutoryPayComponents.isEmpty() || refStatutoryPayComponents.size() > 1) {
                        return totalRef;
                    }
                    StatutoryPayComponent statutoryPayComponent = refStatutoryPayComponents.get(0);
                    if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                        totalRef = totalRef.add(statutoryPayComponent.getAmount());
                    } else if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                        totalRef = totalRef.add((statutoryPayComponent.getAmount().divide(new BigDecimal(100))).multiply(basicPayComponent.getAmount()));
                    }
                } else {
                    totalRef = totalRef.add(basicPayComponent.getAmount());
                }
            }
            log.log(Level.INFO, "totalRef gotten:: {0}", totalRef);
            if (workersSalaryAddition instanceof RecurringWorkersAddition) {
                RecurringWorkersAddition recurringWorkersAddition = (RecurringWorkersAddition) workersSalaryAddition;
                if (recurringWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return recurringWorkersAddition.getAmount();
                } else if (recurringWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (recurringWorkersAddition.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            } else if (workersSalaryAddition instanceof OneTimeWorkersAddition) {
                OneTimeWorkersAddition oneTimeWorkersAddition = (OneTimeWorkersAddition) workersSalaryAddition;
                if (oneTimeWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return oneTimeWorkersAddition.getAmount();
                } else if (oneTimeWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (oneTimeWorkersAddition.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            }
            return BigDecimal.ZERO;
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal getActualOtherAdditionAmount(BigDecimal grossSalary, WorkersCategoryGroupStep workersCategoryGroupStep, WorkersSalaryAddition workersSalaryAddition) {
        try {
            List<PayItem> refStatutoryPayItems = workersSalaryAddition.getOtherAddition().getRefStatutoryPayItems();
            BigDecimal totalRef = BigDecimal.ZERO;
            for (PayItem payItem : refStatutoryPayItems) {
                if (payItem.getBasic()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("workersCategoryGroupStep", workersCategoryGroupStep);
                    map.put("payItem", payItem);
                    List<StatutoryPayComponent> refStatutoryPayComponents = statutoryPayComponentFacadeLocal.findAll(map, true);
                    if (refStatutoryPayComponents.isEmpty() || refStatutoryPayComponents.size() > 1) {
                        return totalRef;
                    }
                    StatutoryPayComponent statutoryPayComponent = refStatutoryPayComponents.get(0);
                    if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                        totalRef = totalRef.add(statutoryPayComponent.getAmount());
                    } else if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                        totalRef = totalRef.add((statutoryPayComponent.getAmount().divide(new BigDecimal(100))).multiply(grossSalary));
                    }
                } else {
                    totalRef = totalRef.add(grossSalary);
                }
            }
            log.log(Level.INFO, "totalRef gotten:: {0}", totalRef);
            if (workersSalaryAddition instanceof RecurringWorkersAddition) {
                RecurringWorkersAddition recurringWorkersAddition = (RecurringWorkersAddition) workersSalaryAddition;
                if (recurringWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return recurringWorkersAddition.getAmount();
                } else if (recurringWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (recurringWorkersAddition.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            } else if (workersSalaryAddition instanceof OneTimeWorkersAddition) {
                OneTimeWorkersAddition oneTimeWorkersAddition = (OneTimeWorkersAddition) workersSalaryAddition;
                if (oneTimeWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return oneTimeWorkersAddition.getAmount();
                } else if (oneTimeWorkersAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (oneTimeWorkersAddition.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            }
            return BigDecimal.ZERO;
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal getActualOtherDeductionAmount(StatutoryPayComponent basicPayComponent, WorkersCategoryGroupStep workersCategoryGroupStep, WorkersSalaryDeduction workersSalaryDeduction) {
        try {
            List<PayItem> refStatutoryPayItems = workersSalaryDeduction.getOtherDeduction().getRefStatutoryPayItems();
            BigDecimal totalRef = BigDecimal.ZERO;
            for (PayItem payItem : refStatutoryPayItems) {
                if (payItem.getBasic()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("workersCategoryGroupStep", workersCategoryGroupStep);
                    map.put("payItem", payItem);
                    List<StatutoryPayComponent> refStatutoryPayComponents = statutoryPayComponentFacadeLocal.findAll(map, true);
                    if (refStatutoryPayComponents.isEmpty() || refStatutoryPayComponents.size() > 1) {
                        return totalRef;
                    }
                    StatutoryPayComponent statutoryPayComponent = refStatutoryPayComponents.get(0);
                    if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                        totalRef = totalRef.add(statutoryPayComponent.getAmount());
                    } else if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                        totalRef = totalRef.add((statutoryPayComponent.getAmount().divide(new BigDecimal(100))).multiply(basicPayComponent.getAmount()));
                    }
                } else {
                    totalRef = totalRef.add(basicPayComponent.getAmount());
                }
            }
            log.log(Level.INFO, "totalRef gotten:: {0}", totalRef);
            if (workersSalaryDeduction instanceof RecurringWorkersDeduction) {
                RecurringWorkersDeduction recurringWorkersDeduction = (RecurringWorkersDeduction) workersSalaryDeduction;
                if (recurringWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return recurringWorkersDeduction.getAmount();
                } else if (recurringWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (recurringWorkersDeduction.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            } else if (workersSalaryDeduction instanceof OneTimeWorkersDeduction) {
                OneTimeWorkersDeduction oneTimeWorkersDeduction = (OneTimeWorkersDeduction) workersSalaryDeduction;
                if (oneTimeWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return oneTimeWorkersDeduction.getAmount();
                } else if (oneTimeWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (oneTimeWorkersDeduction.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            } else if (workersSalaryDeduction instanceof LoanWorkersDeduction) {
                LoanWorkersDeduction loanWorkersDeduction = (LoanWorkersDeduction) workersSalaryDeduction;
                BigDecimal balance = loanWorkersDeduction.getAmount().subtract(loanWorkersDeduction.getPaidAmount());
                if (balance.compareTo(loanWorkersDeduction.getDeductionAmount()) <= 0) {
                    return balance;
                } else {
                    return loanWorkersDeduction.getDeductionAmount();
                }
            }
            return BigDecimal.ZERO;
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return BigDecimal.ZERO;
        }
    }

    private BigDecimal getActualOtherDeductionAmount(BigDecimal grossSalary, WorkersCategoryGroupStep workersCategoryGroupStep, WorkersSalaryDeduction workersSalaryDeduction) {
        try {
            List<PayItem> refStatutoryPayItems = workersSalaryDeduction.getOtherDeduction().getRefStatutoryPayItems();
            BigDecimal totalRef = BigDecimal.ZERO;
            for (PayItem payItem : refStatutoryPayItems) {
                if (payItem.getBasic()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("workersCategoryGroupStep", workersCategoryGroupStep);
                    map.put("payItem", payItem);
                    List<StatutoryPayComponent> refStatutoryPayComponents = statutoryPayComponentFacadeLocal.findAll(map, true);
                    if (refStatutoryPayComponents.isEmpty() || refStatutoryPayComponents.size() > 1) {
                        return totalRef;
                    }
                    StatutoryPayComponent statutoryPayComponent = refStatutoryPayComponents.get(0);
                    if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                        totalRef = totalRef.add(statutoryPayComponent.getAmount());
                    } else if (statutoryPayComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                        totalRef = totalRef.add((statutoryPayComponent.getAmount().divide(new BigDecimal(100))).multiply(grossSalary));
                    }
                } else {
                    totalRef = totalRef.add(grossSalary);
                }
            }
            log.log(Level.INFO, "totalRef gotten:: {0}", totalRef);
            if (workersSalaryDeduction instanceof RecurringWorkersDeduction) {
                RecurringWorkersDeduction recurringWorkersDeduction = (RecurringWorkersDeduction) workersSalaryDeduction;
                if (recurringWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return recurringWorkersDeduction.getAmount();
                } else if (recurringWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (recurringWorkersDeduction.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            } else if (workersSalaryDeduction instanceof OneTimeWorkersDeduction) {
                OneTimeWorkersDeduction oneTimeWorkersDeduction = (OneTimeWorkersDeduction) workersSalaryDeduction;
                if (oneTimeWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    return oneTimeWorkersDeduction.getAmount();
                } else if (oneTimeWorkersDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    return (oneTimeWorkersDeduction.getAmount().divide(new BigDecimal("100"))).multiply(totalRef);
                }
            } else if (workersSalaryDeduction instanceof LoanWorkersDeduction) {
                LoanWorkersDeduction loanWorkersDeduction = (LoanWorkersDeduction) workersSalaryDeduction;
                BigDecimal balance = loanWorkersDeduction.getAmount().subtract(loanWorkersDeduction.getPaidAmount());
                if (balance.compareTo(loanWorkersDeduction.getDeductionAmount()) <= 0) {
                    return balance;
                } else {
                    return loanWorkersDeduction.getDeductionAmount();
                }
            }
            return BigDecimal.ZERO;
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return BigDecimal.ZERO;
        }
    }

    public void rollbackSalaries() {
        FacesMessage m = new FacesMessage();
        m.setSeverity(FacesMessage.SEVERITY_INFO);
        m.setSummary("Payroll Computation rolled back Successfully");
        FacesContext.getCurrentInstance().addMessage(null, m);
    }

    public void requestSalariesApproval() {
        FacesMessage m = new FacesMessage();
        m.setSeverity(FacesMessage.SEVERITY_INFO);
        m.setSummary("Payroll Approval Request has been Sent");
        FacesContext.getCurrentInstance().addMessage(null, m);
    }

    private PayrollPeriod getCurrentPayrollPeriod() {
        try {
            Map m = new HashMap();
            m.put("corporate", getLoggedCorporate());
            m.put("flagStatus", "PIP");
            List payrollInProgressPeriods = this.payrollPeriodFacadeLocal.findAll(m, new String[0]);
            if (payrollInProgressPeriods.isEmpty()) {
                return null;
            }
            if (payrollInProgressPeriods.size() > 1) {
                return null;
            }
            return (PayrollPeriod) payrollInProgressPeriods.get(0);
        } catch (Exception ex) {
            Logger.getLogger(PayrollPeriodBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
