/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_TRANSACTION")
@Audited
public class Transaction implements Serializable {

    @ManyToOne
    private Payment payment;

    @ManyToOne
    private PaymentScheduler schedule;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TRANS_GEN")
    @GenericGenerator(name = "TRANS_GEN", strategy = "enhanced-table", parameters = {
        @org.hibernate.annotations.Parameter(name = "table_name", value = "hibernate_sequence_trans")
    })
    @Column(name = "TRANSACTION_ID")
    private BigInteger transactionId;
    @ManyToOne
    @JoinColumn(name = "CORPORATE_ID")
    private Corporate corporate = new Corporate();

    @ManyToOne
    @JoinColumn(name = "BENEFICIARY_ID")
    private Beneficiary beneficiary;

    @Column(name = "TRANSACTION_TYPE")
    private String transactionType;
    @Column(name = "TRANSACTION_STATUS")
    private String transactionStatus;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PAYMENT_DATE")
    private Date paymentDate;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Transient
    private String amountf;
    @ManyToOne
    @JoinColumn(name = "DEST_ACCOUNT")
    private Account account;
    @Column(name = "NARRATION")
    private String narration;
    @Column(name = "TRANSACTION_REFERENCE")
    private String transactionReference;
    @Column(name = "S_BATCHID")
    private String sbatchId;
    @Transient
    private String dateCreatedStr;
    @Transient
    private String paymentDateStr;
    @Transient
    private String status;
    @Column(name = "RESPONSE_CODE")
    private String responseCode;
    @Column(name = "FEE_AMOUNT")
    private BigDecimal etzFeeAmount;
    @Column(name = "B_FEE_AMOUNT")
    private BigDecimal bankFeeAmount;
    @Column(name = "T_FEE_AMOUNT")
    private BigDecimal thirdpartyFeeAmount;
    @Column(name = "T_FEE_AMOUNT2")
    private BigDecimal thirdpartyFeeAmount2;
    @JoinColumn(name = "PARENT_TRANS_ID")
    @ManyToOne
    private Transaction parentTransId;
    @JoinColumn(name = "MFB_BANK_ID")
    @ManyToOne
    private Bank mfbBank;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REV_DATE")
    private Date revDate;
    @Column(name = "REVERSAL_RESPONSE")
    private String revResponse;
    @Column(name = "REVERSAL_REFERENCE")
    private String revReference;
    @Transient
    private String dateReversedStr;
    @Transient
    private String beneficaryBank;
     @Transient
    private String paymentBank;
    @Transient
    private String beneficaryName;
    @Transient
    private String beneficaryAccount;
    @Transient
    private String initiatingBank;
    @Transient
    private String paymentReference;
    @Transient
    private String authDate;
    @Transient
    private String authBy;
        @Transient
    private boolean statusUnknown;
                @Transient
    private String settlementUniqueId;

    /**
     * @return the status
     */
    public String getStatus() {
        status = transactionStatus != null ? transactionStatus.equals(AppConstants.TRANSACTION_CREATED_OK) ? AppConstants.TRANSACTION_CREATED_OK_TEXT
                : transactionStatus.equals(AppConstants.TRANSACTION_CREATED_WITH_ERROR) ? AppConstants.TRANSACTION_CREATED_WITH_ERROR_TEXT : transactionStatus.equals(AppConstants.TRANSACTION_PAID)
                                ? AppConstants.TRANSACTION_PAID_TEXT : transactionStatus.equals(AppConstants.TRANSACTION_PENDING_APPROVAL) ? AppConstants.TRANSACTION_PENDING_APPROVAL_TEXT
                                        : transactionStatus.equals(AppConstants.TRANSACTION_PENDING_PAYMENT) ? AppConstants.TRANSACTION_PENDING_PAYMENT_TEXT : transactionStatus.equals(AppConstants.TRANSACTION_PAID) ? AppConstants.TRANSACTION_PAID_TEXT
                                                        : transactionStatus.equals(AppConstants.TRANSACTION_FAILED) ? AppConstants.TRANSACTION_FAILED_TEXT
                                                                : transactionStatus.equals(AppConstants.TRANSACTION_REVERSED) ? AppConstants.TRANSACTION_REVERSED_TEXT : transactionStatus.equals(AppConstants.TRANSACTION_WAITING) ? AppConstants.TRANSACTION_PENDING_PAYMENT_TEXT :transactionStatus.equals(AppConstants.TRANSACTION_STATUS_UNKNOWN)?AppConstants.TRANSACTION_STATUSUNKNOWN_TEXT  : transactionStatus.equals(AppConstants.TRANSACTION_PROCESSING) ? AppConstants.TRANSACTION_PENDING_PAYMENT_TEXT : "" : "";
        return status;
    }

    /**
     * @return the transactionId
     */
    public BigInteger getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(BigInteger transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the beneficiary
     */
    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    /**
     * @param beneficiary the beneficiary to set
     */
    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }

    /**
     * @return the trsnsactionType
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the trsnsactionType to set
     */
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    /**
     * @return the trsnsactionStatus
     */
    public String getTrsnsactionStatus() {
        return transactionStatus;
    }

    /**
     * @param trsnsactionStatus the trsnsactionStatus to set
     */
    public void setTrsnsactionStatus(String trsnsactionStatus) {
        this.transactionStatus = trsnsactionStatus;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the account
     */
    public Account getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * @return the payment
     */
    public Payment getPayment() {
        return payment;
    }

    /**
     * @param payment the payment to set
     */
    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
//       // if (narration == null) {
//           System.out.println("trans id::: " + transactionId.toString());
//            if (this.payment instanceof UploadPayment) {
//                narration =  narration.startsWith("08")?narration:"08" + transactionId.toString() + "; " + narration;
//            } else if (this.payment instanceof SingleBeneficiaryPayment) {
//                narration =  transactionId.toString().startsWith("08")?transactionId.toString():"08" + transactionId.toString() + "; " + narration;
//            }
//      //  }
//              System.out.println("narration::: " + narration);
        return narration.startsWith("10")?"08" + narration:narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the transactionReference
     */
    public String getTransactionReference() {
        return transactionId.toString().startsWith("08")?transactionId.toString():"08" + transactionId.toString();
    }

    /**
     * @param transactionReference the transactionReference to set
     */
    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    /**
     * @return the paymentDate
     */
    public Date getPaymentDate() {
        paymentDate = paymentDate == null ? dateCreated : paymentDate;
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the schedule
     */
    public PaymentScheduler getSchedule() {
        return schedule;
    }

    /**
     * @param schedule the schedule to set
     */
    public void setSchedule(PaymentScheduler schedule) {
        this.schedule = schedule;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.transactionId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaction other = (Transaction) obj;
        if (!Objects.equals(this.transactionId, other.transactionId)) {
            return false;
        }
        return true;
    }

    /**
     * @return the dateCreatedStr
     */
    public String getDateCreatedStr() {
        dateCreatedStr = AppUtil.formatDate(dateCreated == null ? new Date() : dateCreated, "EEE, d MMM yyyy HH:mm:ss");
        return dateCreatedStr;
    }

    /**
     * @return the amountf
     */
    public String getAmountf() {
        amountf = AppUtil.formatAsAmount(amount.toString());
        return amountf;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the etzFeeAmount
     */
    public BigDecimal getEtzFeeAmount() {
        return etzFeeAmount;
    }

    /**
     * @param etzFeeAmount the etzFeeAmount to set
     */
    public void setEtzFeeAmount(BigDecimal etzFeeAmount) {
        this.etzFeeAmount = etzFeeAmount;
    }

    /**
     * @return the bankFeeAmount
     */
    public BigDecimal getBankFeeAmount() {
        return bankFeeAmount;
    }

    /**
     * @param bankFeeAmount the bankFeeAmount to set
     */
    public void setBankFeeAmount(BigDecimal bankFeeAmount) {
        this.bankFeeAmount = bankFeeAmount;
    }

//    public static void main(String[] args) {
//        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hmm a");
//        try {
//            Date d = formatter.parse("26-Aug-2015 115 PM");
//            System.out.println("d:: " + d);
//        } catch (ParseException ex) {
//            Logger.getLogger(Transaction.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    /**
     * @return the thirdpartyFeeAmount
     */
    public BigDecimal getThirdpartyFeeAmount() {
        return thirdpartyFeeAmount;
    }

    /**
     * @param thirdpartyFeeAmount the thirdpartyFeeAmount to set
     */
    public void setThirdpartyFeeAmount(BigDecimal thirdpartyFeeAmount) {
        this.thirdpartyFeeAmount = thirdpartyFeeAmount;
    }

    /**
     * @return the thirdpartyFeeAmount2
     */
    public BigDecimal getThirdpartyFeeAmount2() {
        return thirdpartyFeeAmount2;
    }

    /**
     * @param thirdpartyFeeAmount2 the thirdpartyFeeAmount2 to set
     */
    public void setThirdpartyFeeAmount2(BigDecimal thirdpartyFeeAmount2) {
        this.thirdpartyFeeAmount2 = thirdpartyFeeAmount2;
    }

    /**
     * @return the beneficaryBank
     */
    public String getBeneficaryBank() {
        beneficaryBank = beneficiary == null ? "" : beneficiary.getAccount().getBankName();
        return beneficaryBank;
    }
    
      /**
     * @return the beneficaryBank
     */
    public String getPaymentBank() {
        Account account1 = beneficiary.getAccount();
        Bank mfbBankk = null;
        if(account1 instanceof BankAccount) {
           mfbBankk= ((BankAccount)beneficiary.getAccount()).getBank();
        }
        if(mfbBankk==null) {
            return "";
        }
        paymentBank = mfbBankk.getAffliateBank()==null?"":mfbBankk.getAffliateBank().getBankName();
        return paymentBank;
    }

    /**
     * @param beneficaryBank the beneficaryBank to set
     */
    public void setBeneficaryBank(String beneficaryBank) {
        this.beneficaryBank = beneficaryBank;
    }

    /**
     * @return the beneficaryName
     */
    public String getBeneficaryName() {
        beneficaryName = beneficiary == null ? "" : beneficiary.getAccount().getAccountName();
        return beneficaryName;
    }

    /**
     * @param beneficaryName the beneficaryName to set
     */
    public void setBeneficaryName(String beneficaryName) {
        this.beneficaryName = beneficaryName;
    }

    /**
     * @return the beneficaryAccount
     */
    public String getBeneficaryAccount() {
        beneficaryAccount = beneficiary == null ? "" : beneficiary.getAccount().getAccountNumber();
        return beneficaryAccount;
    }

    /**
     * @param beneficaryAccount the beneficaryAccount to set
     */
    public void setBeneficaryAccount(String beneficaryAccount) {
        this.beneficaryAccount = beneficaryAccount;
    }

    /**
     * @return the initiatingBank
     */
    public String getInitiatingBank() {
        initiatingBank = corporate.getBank().toString();
        return initiatingBank;
    }

    /**
     * @return the paymentReference
     */
    public String getPaymentReference() {
        paymentReference = payment == null ? "" : payment.getId().toString();
        return paymentReference;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    /**
     * @return the authDate
     */
    public String getAuthDate() {
        authDate = payment == null ? "" : payment.getAuthorizedDateStr();
        return authDate;
    }

    /**
     * @param authDate the authDate to set
     */
    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    /**
     * @return the authBy
     */
    public String getAuthBy() {
        authBy = payment == null ? "" : payment.getAuthorizedBy();
        return authBy;
    }

    /**
     * @param authBy the authBy to set
     */
    public void setAuthBy(String authBy) {
        this.authBy = authBy;
    }

    /**
     * @return the sbatchId
     */
    public String getSbatchId() {
        return sbatchId;
    }

    /**
     * @param sbatchId the sbatchId to set
     */
    public void setSbatchId(String sbatchId) {
        this.sbatchId = sbatchId;
    }

    /**
     * @return the paymentDateStr
     */
    public String getPaymentDateStr() {
        paymentDateStr = AppUtil.formatDate(paymentDate == null ? getDateCreated() : paymentDate, "EEE, d MMM yyyy HH:mm:ss");
        return paymentDateStr;
    }

    /**
     * @return the parentTransId
     */
    public Transaction getParentTransId() {
        return parentTransId;
    }

    /**
     * @param parentTransId the parentTransId to set
     */
    public void setParentTransId(Transaction parentTransId) {
        this.parentTransId = parentTransId;
    }

    /**
     * @return the mfbBank
     */
    public Bank getMfbBank() {
        return mfbBank;
    }

    /**
     * @param mfbBank the mfbBank to set
     */
    public void setMfbBank(Bank mfbBank) {
        this.mfbBank = mfbBank;
    }

    /**
     * @return the revDate
     */
    public Date getRevDate() {
        return revDate;
    }

    /**
     * @param revDate the revDate to set
     */
    public void setRevDate(Date revDate) {
        this.revDate = revDate;
    }

    /**
     * @return the revResponse
     */
    public String getRevResponse() {
        return revResponse;
    }

    /**
     * @param revResponse the revResponse to set
     */
    public void setRevResponse(String revResponse) {
        this.revResponse = revResponse;
    }

    /**
     * @return the dateReversedStr
     */
    public String getDateReversedStr() {
        dateReversedStr = AppUtil.formatDate(revDate == null ? new Date() : revDate, "EEE, d MMM yyyy HH:mm:ss");
        return dateReversedStr;
    }

    /**
     * @param dateReversedStr the dateReversedStr to set
     */
    public void setDateReversedStr(String dateReversedStr) {
        this.dateReversedStr = dateReversedStr;
    }

    /**
     * @return the revReference
     */
    public String getRevReference() {
        return revReference;
    }

    /**
     * @param revReference the revReference to set
     */
    public void setRevReference(String revReference) {
        this.revReference = revReference;
    }

    /**
     * @return the statusUnknown
     */
    public boolean isStatusUnknown() {
        statusUnknown = transactionStatus.equals(AppConstants.TRANSACTION_STATUS_UNKNOWN);
        return statusUnknown;
    }

    /**
     * @return the settlementUniqueId
     */
    public String getSettlementUniqueId() {
        return settlementUniqueId;
    }

    /**
     * @param settlementUniqueId the settlementUniqueId to set
     */
    public void setSettlementUniqueId(String settlementUniqueId) {
        this.settlementUniqueId = settlementUniqueId;
    }

 
    
}
