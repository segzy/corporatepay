/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.persistence.PersistenceException;

/**
 *
 * @author Oluwasegun.Idowu
 */
public abstract class AbstractWorkflowFacade extends AbstractFacade<ApprovalTask> implements AbstractWorkflowFacadeLocal<ApprovalTask> {

    protected Serializable target;

    public AbstractWorkflowFacade() {
        super(ApprovalTask.class);
    }

    @Override
    public ApprovalTask startWorkflow(Class clazz, Object objectId, String requestType, String routeAlias, byte[] attachment) throws Exception {
        log.log(Level.INFO, "starting workflow for .... {0}", clazz.getSimpleName());
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();

        } catch (PersistenceException pex) {
            log.log(Level.SEVERE, "Error", pex.fillInStackTrace());
            throw new PersistenceException("error starting approval route");
        }

        if (approvalType != null) {
            target = (Serializable) getEntityManager().find(clazz, objectId);
//            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            List<ApprovalRoute> approvalRoutes;
            ApprovalRoute level1ApprovalRoute;
            String ext = "";
            if (approvalType.getAllowMultipleRoute() && routeAlias != null) {
                ext += " and a.routeAlias='" + routeAlias + "'";
            }
            try {
                if (isBankUser()) {
//                    BankUser buser = (BankUser) loggedInUser;
                    approvalRoutes = getEntityManager().createQuery("select a from BankApprovalRoute a where a.approvalType=:approvalType"
                            + " and a.active=true and a.bank=:bank" + ext, ApprovalRoute.class)
                            .setParameter("approvalType", approvalType).setParameter("bank", getLoggedBank()).getResultList();
                } else if (isCorporateUser()) {
//                    CorporateUser corporateUser = (CorporateUser) loggedInUser;
                    approvalRoutes = getEntityManager().createQuery("select a from CorporateApprovalRoute a where a.approvalType=:approvalType"
                            + " and  a.active=true and a.corporate=:corporate" + ext, ApprovalRoute.class)
                            .setParameter("approvalType", approvalType).setParameter("corporate", getLoggedCorporate()).getResultList();
                } else {
                    approvalRoutes = getEntityManager().createQuery("select a from ApprovalRoute a where a.approvalType=:approvalType" + ext
                            + " and a.active=true and a.central=true", ApprovalRoute.class)
                            .setParameter("approvalType", approvalType).getResultList();
                }
                if (approvalRoutes.isEmpty()) {
                    throw new PersistenceException("An Approval Route for must exist for " + clazz.getSimpleName());
                }
                if (approvalRoutes.size() < 2) {
                    throw new PersistenceException("At Least 2 levels of Approval Route Allowed ");
                }
            } catch (PersistenceException ex) {
                throw ex;
            }

            Comparator<ApprovalRoute> approvalRouteComparable = new Comparator<ApprovalRoute>() {

                @Override
                public int compare(ApprovalRoute o1, ApprovalRoute o2) {
                    return o1.getLevel() - o2.getLevel();
                }
            };
            Collections.sort(approvalRoutes, approvalRouteComparable);
            int i = 1;
            for (ApprovalRoute ar : approvalRoutes) {
                if (ar.getLevel() != i) {
                    throw new PersistenceException("Level " + i + " is missing in the Approval Route");
                }
                i++;
            }
            level1ApprovalRoute = approvalRoutes.get(0);
//            if(!level1ApprovalRoute.getApprover().getId().equals(getLoggedUser().getId())) {
//                throw new PersistenceException("Unauthorized Operation");
//            }
            ApprovalRoute finalLevel = approvalRoutes.get(approvalRoutes.size() - 1);
            if (approvalType.getFinancialLike()) {
                if ((finalLevel.getApprover() instanceof ApprovalGroup)) {
                    ApprovalGroup approvalGroup = (ApprovalGroup) finalLevel.getApprover();
                    Set<User> users = approvalGroup.getUsers();
                    boolean allFinalAuth = true;
                    for (User u : users) {
                        if (u.getRole().getFinalAuthorizerRole()) {
                            allFinalAuth = false;
                            break;
                        }
                    }
                    if (!allFinalAuth) {
                        throw new PersistenceException("All Users placed in the Last Approver Group must be Final Authorizer");
                    }
//                    throw new PersistenceException("Final Authorizer must be a User and not a Group");
                } else if ((finalLevel.getApprover() instanceof User)) {
                    User user = (User) finalLevel.getApprover();
                    if (!user.getRole().getFinalAuthorizerRole()) {
                        throw new PersistenceException("Last Approver on the Approval Route  must be a Final Authorizer");
                    }
                }
            }
            //          log.log(Level.INFO, "approvalRoute ....{0}", level1ApprovalRoute);
            if (level1ApprovalRoute != null && level1ApprovalRoute.getActive() && level1ApprovalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(level1ApprovalRoute);
                approvalTask.setTargetObjectId(String.valueOf(objectId));
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject(requestType));
                approvalTask.setTaskAccessor(level1ApprovalRoute.getApprover());
                approvalTask.setDocument(attachment);
                approvalTask.setRequestApprovalType(requestType);
                ApprovalTask create = super.create(approvalTask);
                create.setCurrentRouteId(create.getId());
                for (TaskObjectNameValuePair pair : create.getNameValuePairs()) {
                    pair.setApprovalTask(create);
                }
                return create;
            }
        }
        return null;
    }
    
    @Override
    public ApprovalTask startWorkflow(PaymentFacadeLocal paymentFacadeLocal,Class clazz, Object objectId, String requestType, String routeAlias, byte[] attachment) throws Exception {
        log.log(Level.INFO, "starting workflow for .... {0}", clazz.getSimpleName());
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();

        } catch (PersistenceException pex) {
            log.log(Level.SEVERE, "Error", pex.fillInStackTrace());
            throw new PersistenceException("error starting approval route");
        }

        if (approvalType != null) {
            target = (Serializable) getEntityManager().find(clazz, objectId);
//            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            List<ApprovalRoute> approvalRoutes;
            ApprovalRoute level1ApprovalRoute;
            String ext = "";
            if (approvalType.getAllowMultipleRoute() && routeAlias != null) {
                ext += " and a.routeAlias='" + routeAlias + "'";
            }
            try {
                if (isBankUser()) {
//                    BankUser buser = (BankUser) loggedInUser;
                    approvalRoutes = getEntityManager().createQuery("select a from BankApprovalRoute a where a.approvalType=:approvalType"
                            + " and a.active=true and a.bank=:bank" + ext, ApprovalRoute.class)
                            .setParameter("approvalType", approvalType).setParameter("bank", getLoggedBank()).getResultList();
                } else if (isCorporateUser()) {
//                    CorporateUser corporateUser = (CorporateUser) loggedInUser;
                    approvalRoutes = getEntityManager().createQuery("select a from CorporateApprovalRoute a where a.approvalType=:approvalType"
                            + " and  a.active=true and a.corporate=:corporate" + ext, ApprovalRoute.class)
                            .setParameter("approvalType", approvalType).setParameter("corporate", getLoggedCorporate()).getResultList();
                } else {
                    approvalRoutes = getEntityManager().createQuery("select a from ApprovalRoute a where a.approvalType=:approvalType" + ext
                            + " and a.active=true and a.central=true", ApprovalRoute.class)
                            .setParameter("approvalType", approvalType).getResultList();
                }
                if (approvalRoutes.isEmpty()) {
                    throw new PersistenceException("An Approval Route for must exist for " + clazz.getSimpleName());
                }
                if (approvalRoutes.size() < 2) {
                    throw new PersistenceException("At Least 2 levels of Approval Route Allowed ");
                }
            } catch (PersistenceException ex) {
                throw ex;
            }

            Comparator<ApprovalRoute> approvalRouteComparable = new Comparator<ApprovalRoute>() {

                @Override
                public int compare(ApprovalRoute o1, ApprovalRoute o2) {
                    return o1.getLevel() - o2.getLevel();
                }
            };
            Collections.sort(approvalRoutes, approvalRouteComparable);
            int i = 1;
            for (ApprovalRoute ar : approvalRoutes) {
                if (ar.getLevel() != i) {
                    throw new PersistenceException("Level " + i + " is missing in the Approval Route");
                }
                i++;
            }
            level1ApprovalRoute = approvalRoutes.get(0);
//            if(!level1ApprovalRoute.getApprover().getId().equals(getLoggedUser().getId())) {
//                throw new PersistenceException("Unauthorized Operation");
//            }
            ApprovalRoute finalLevel = approvalRoutes.get(approvalRoutes.size() - 1);
            if (approvalType.getFinancialLike()) {
                if ((finalLevel.getApprover() instanceof ApprovalGroup)) {
                    ApprovalGroup approvalGroup = (ApprovalGroup) finalLevel.getApprover();
                    Set<User> users = approvalGroup.getUsers();
                    boolean allFinalAuth = true;
                    for (User u : users) {
                        if (u.getRole().getFinalAuthorizerRole()) {
                            allFinalAuth = false;
                            break;
                        }
                    }
                    if (!allFinalAuth) {
                        throw new PersistenceException("All Users placed in the Last Approver Group must be Final Authorizer");
                    }
//                    throw new PersistenceException("Final Authorizer must be a User and not a Group");
                } else if ((finalLevel.getApprover() instanceof User)) {
                    User user = (User) finalLevel.getApprover();
                    if (!user.getRole().getFinalAuthorizerRole()) {
                        throw new PersistenceException("Last Approver on the Approval Route  must be a Final Authorizer");
                    }
                }
            }
            //          log.log(Level.INFO, "approvalRoute ....{0}", level1ApprovalRoute);
            if (level1ApprovalRoute != null && level1ApprovalRoute.getActive() && level1ApprovalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(level1ApprovalRoute);
                approvalTask.setTargetObjectId(String.valueOf(objectId));
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject(requestType));
                approvalTask.setTaskAccessor(level1ApprovalRoute.getApprover());
                approvalTask.setDocument(attachment);
                approvalTask.setRequestApprovalType(requestType);
                ApprovalTask create = super.create(approvalTask);
                create.setCurrentRouteId(create.getId());
                for (TaskObjectNameValuePair pair : create.getNameValuePairs()) {
                    pair.setApprovalTask(create);
                }
                if (level1ApprovalRoute.getApprovalType().getDescription().equals(Payment.class.getSimpleName())) {
                    Payment payment = paymentFacadeLocal.find(Long.valueOf(create.getTargetObjectId()));
                    MailUtil.getInstance().sendTaskAlert(new Approver[0], approvalTask.getTaskAccessor(), payment);
                }
                return create;
            }
        }
        return null;
    }

    @Override
    public ApprovalTask startWorkflowEtz(Class clazz, Object objectId, String requestType, String routeAlias, byte[] attachment) throws Exception {
        log.info("starting workflow....");
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();

        } catch (PersistenceException pex) {
            log.log(Level.SEVERE, "Error", pex.fillInStackTrace());
            throw new PersistenceException("error starting approval route");
        }

        if (approvalType != null) {
            target = (Serializable) getEntityManager().find(clazz, objectId);
            List<ApprovalRoute> approvalRoutes;
            ApprovalRoute level1ApprovalRoute;
            String ext = "";
            if (approvalType.getAllowMultipleRoute() && routeAlias != null) {
                ext += " and a.routeAlias='" + routeAlias + "'";
            }
            try {
                approvalRoutes = getEntityManager().createQuery("select a from ApprovalRoute a where a.approvalType=:approvalType" + ext
                        + " and a.active=true and a.central=true", ApprovalRoute.class)
                        .setParameter("approvalType", approvalType).getResultList();
                if (approvalRoutes.isEmpty()) {
                    throw new PersistenceException("An Approval Route for must exist for " + clazz.getSimpleName());
                }
                if (approvalRoutes.size() < 2) {
                    throw new PersistenceException("At Least 2 levels of Approval Route Allowed ");
                }
            } catch (PersistenceException ex) {
                throw ex;
            }

            Comparator<ApprovalRoute> approvalRouteComparable = new Comparator<ApprovalRoute>() {

                @Override
                public int compare(ApprovalRoute o1, ApprovalRoute o2) {
                    return o1.getLevel() - o2.getLevel();
                }
            };
            Collections.sort(approvalRoutes, approvalRouteComparable);
            int i = 1;
            for (ApprovalRoute ar : approvalRoutes) {
                if (ar.getLevel() != i) {
                    throw new PersistenceException("Level " + i + " is missing in the Approval Route");
                }
                i++;
            }
            level1ApprovalRoute = approvalRoutes.get(0);
//             if(level1ApprovalRoute.getApprover().getId()!=getLoggedUser().getId().longValue()) {
//                throw new PersistenceException("Unauthorized Operation");
//            }
            log.log(Level.INFO, "approvalRoute ....{0}", level1ApprovalRoute);
            if (level1ApprovalRoute != null && level1ApprovalRoute.getActive() && level1ApprovalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(level1ApprovalRoute);
                approvalTask.setTargetObjectId(String.valueOf(objectId));
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject(requestType));
                approvalTask.setTaskAccessor(level1ApprovalRoute.getApprover());
                approvalTask.setDocument(attachment);
                approvalTask.setRequestApprovalType(requestType);
                ApprovalTask create = super.create(approvalTask);
                create.setCurrentRouteId(create.getId());
                log.log(Level.INFO, "size... {0}", create.getNameValuePairs().size());
                for (TaskObjectNameValuePair pair : create.getNameValuePairs()) {
                    log.log(Level.INFO, "pair... {0}", pair);
                    pair.setApprovalTask(create);
                }
                log.log(Level.INFO, "returning ....{0}");
                return create;
            }
        }
        return null;
    }

    @Override
    public ApprovalTask startWorkflowCorporate(Class clazz, Object objectId, String requestType, String routeAlias, Corporate corCoporate, byte[] attachment) throws Exception {
        log.info("starting workflow....");
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();

        } catch (PersistenceException pex) {
            log.log(Level.SEVERE, "Error", pex.fillInStackTrace());
            throw new PersistenceException("error starting approval route");
        }

        if (approvalType != null) {
            target = (Serializable) getEntityManager().find(clazz, objectId);
            List<ApprovalRoute> approvalRoutes;
            ApprovalRoute level1ApprovalRoute;
            String ext = "";
            if (approvalType.getAllowMultipleRoute() && routeAlias != null) {
                ext += " and a.routeAlias='" + routeAlias + "'";
            }
            try {
                approvalRoutes = getEntityManager().createQuery("select a from CorporateApprovalRoute a where a.approvalType=:approvalType"
                        + " and  a.active=true and a.corporate=:corporate" + ext, ApprovalRoute.class)
                        .setParameter("approvalType", approvalType).setParameter("corporate", corCoporate).getResultList();
                if (approvalRoutes.isEmpty()) {
                    throw new PersistenceException("An Approval Route for must exist for " + clazz.getSimpleName());
                }
                if (approvalRoutes.size() < 2) {
                    throw new PersistenceException("At Least 2 levels of Approval Route Allowed ");
                }
            } catch (PersistenceException ex) {
                throw ex;
            }

            Comparator<ApprovalRoute> approvalRouteComparable = new Comparator<ApprovalRoute>() {

                @Override
                public int compare(ApprovalRoute o1, ApprovalRoute o2) {
                    return o1.getLevel() - o2.getLevel();
                }
            };
            Collections.sort(approvalRoutes, approvalRouteComparable);
            int i = 1;
            for (ApprovalRoute ar : approvalRoutes) {
                if (ar.getLevel() != i) {
                    throw new PersistenceException("Level " + i + " is missing in the Approval Route");
                }
                i++;
            }
            level1ApprovalRoute = approvalRoutes.get(0);
//               if(level1ApprovalRoute.getApprover().getId()!=getLoggedUser().getId().longValue()) {
//                throw new PersistenceException("Unauthorized Operation");
//            }
            ApprovalRoute finalLevel = approvalRoutes.get(approvalRoutes.size() - 1);
            if (approvalType.getFinancialLike()) {
                if ((finalLevel.getApprover() instanceof ApprovalGroup)) {
                    throw new PersistenceException("Final Authorizer must be a User and not a Group");
                } else if ((finalLevel.getApprover() instanceof User)) {
                    User user = (User) finalLevel.getApprover();
                    if (!user.getRole().getFinalAuthorizerRole()) {
                        throw new PersistenceException("Last Approver on the Approval Route  must be a Final Authorizer");
                    }
                }
            }
            log.log(Level.INFO, "approvalRoute ....{0}", level1ApprovalRoute);
            if (level1ApprovalRoute != null && level1ApprovalRoute.getActive() && level1ApprovalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(level1ApprovalRoute);
                approvalTask.setTargetObjectId(String.valueOf(objectId));
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject(requestType));
                approvalTask.setTaskAccessor(level1ApprovalRoute.getApprover());
                approvalTask.setDocument(attachment);
                approvalTask.setRequestApprovalType(requestType);
                ApprovalTask create = super.create(approvalTask);
                create.setCurrentRouteId(create.getId());
                log.log(Level.INFO, "size... {0}", create.getNameValuePairs().size());
                for (TaskObjectNameValuePair pair : create.getNameValuePairs()) {
                    log.log(Level.INFO, "pair... {0}", pair);
                    pair.setApprovalTask(create);
                }
                log.log(Level.INFO, "returning ....{0}");
                return create;
            }
        }
        return null;
    }

    @Override
    public ApprovalTask startWorkflowBank(Class clazz, Object objectId, String requestType, String routeAlias, Bank bank, byte[] attachment) throws Exception {
        log.info("starting workflow....");
        ApprovalType approvalType = null;
        try {
            approvalType = getEntityManager().createQuery("select a from ApprovalType a where a.description=:description", ApprovalType.class)
                    .setParameter("description", clazz.getSimpleName()).getSingleResult();

        } catch (PersistenceException pex) {
            log.log(Level.SEVERE, "Error", pex.fillInStackTrace());
            throw new PersistenceException("error starting approval route");
        }

        if (approvalType != null) {
            target = (Serializable) getEntityManager().find(clazz, objectId);
            List<ApprovalRoute> approvalRoutes;
            ApprovalRoute level1ApprovalRoute;
            String ext = "";
            if (approvalType.getAllowMultipleRoute() && routeAlias != null) {
                ext += " and a.routeAlias='" + routeAlias + "'";
            }
            try {
                approvalRoutes = getEntityManager().createQuery("select a from BankApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.active=true and a.bank=:bank" + ext, ApprovalRoute.class)
                        .setParameter("approvalType", approvalType).setParameter("bank", bank).getResultList();
                if (approvalRoutes.isEmpty()) {
                    throw new PersistenceException("An Approval Route for must exist for " + clazz.getSimpleName());
                }
                if (approvalRoutes.size() < 2) {
                    throw new PersistenceException("At Least 2 levels of Approval Route Allowed ");
                }
            } catch (PersistenceException ex) {
                throw ex;
            }

            Comparator<ApprovalRoute> approvalRouteComparable = new Comparator<ApprovalRoute>() {

                @Override
                public int compare(ApprovalRoute o1, ApprovalRoute o2) {
                    return o1.getLevel() - o2.getLevel();
                }
            };
            Collections.sort(approvalRoutes, approvalRouteComparable);
            int i = 1;
            for (ApprovalRoute ar : approvalRoutes) {
                if (ar.getLevel() != i) {
                    throw new Exception("Level " + i + " is missing in the Approval Route");
                }
                i++;
            }
            level1ApprovalRoute = approvalRoutes.get(0);
//               if(level1ApprovalRoute.getApprover().getId()!=getLoggedUser().getId().longValue()) {
//                throw new PersistenceException("Unauthorized Operation");
//            }
            log.log(Level.INFO, "approvalRoute ....{0}", level1ApprovalRoute);
            if (level1ApprovalRoute != null && level1ApprovalRoute.getActive() && level1ApprovalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED)) {
                ApprovalTask approvalTask = new ApprovalTask();
                approvalTask.setApprovalRoute(level1ApprovalRoute);
                approvalTask.setTargetObjectId(String.valueOf(objectId));
                approvalTask.setDateCreated(new Date());
                approvalTask.setNameValuePairs(getTaskObject(requestType));
                approvalTask.setTaskAccessor(level1ApprovalRoute.getApprover());
                approvalTask.setDocument(attachment);
                approvalTask.setRequestApprovalType(requestType);
                ApprovalTask create = super.create(approvalTask);
                create.setCurrentRouteId(create.getId());
                log.log(Level.INFO, "size... {0}", create.getNameValuePairs().size());
                for (TaskObjectNameValuePair pair : create.getNameValuePairs()) {
                    log.log(Level.INFO, "pair... {0}", pair);
                    pair.setApprovalTask(create);
                }
                log.log(Level.INFO, "returning ....{0}");
                return create;
            }
        }
        return null;
    }

    @Override
    protected ApprovalTask edit(ApprovalTask entity) throws Exception {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
