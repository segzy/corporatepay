/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var wsocket;

var serviceLocation = getRootUri() + "/CorporatePay/chat/";
var $username;
var $message;
var $chatWindow;
var $onlineUsers;
var room = '';
var $currentFromUser = '';

function getRootUri() {
    return "ws://" + (document.location.hostname == "" ? "localhost" : document.location.hostname) +
            (document.location.port == "" ? "" : ":" + document.location.port);
}

$(document).ready(function () {
    $username = $('#chatuser').text();
    //alert(serviceLocation);
    $message = $('#usermsg');
    $chatWindow = $('#roleuser').text();
    $onlineUsers = $('#chatbox2');
    //    alert($chatWindow );
    if ($chatWindow == '0002') {
        $chatWindow = $('#chatbox1');
    } else {
        $chatWindow = $('#chatbox');
    }
    $message.focus();
    $('#connectmsg').click(function (evt) {
        connectToChatserver();

        $message.focus();
    });
    $('#disconnectmsg').click(function (evt) {
        disConnectToChatserver();

        $message.focus();
    });
    $('#submitmsg').click(function (evt) {
        sendMessage();
        $message.focus();
    });
    
	     $('#closechatmsg').click(function (evt) {
        endChat();
        $message.focus();
    });


});

function sendOnlineUsersRequest() {
    var msg = '{"message":"", "sender":"", "received":"","target":""}';
    wsocket.send(msg);
}

function sendMessage() {
    console.log('msg sent ');
    var msg = '{"message":"' + $message.val() + '", "sender":"'
            + $username + '", "received":"","target":"' + $currentFromUser + '"}';
    		 if ($('#roleuser').text() == '0002' && $currentFromUser=='') {
				    $chatWindow.append('<br><span style="color=\'red\'"><img src="images/error-icon.gif" width="16px" height="16px" alt="online"/> User is not getting message, Ensure you have selected the user</span>');
			 }
			 	 if ($('#roleuser').text() == '0002' && $currentFromUser!='') {
		 $('#closechatmsg').show();
				 }
				 
    wsocket.send(msg);
    $message.val('').focus();
}

function focusUser(user) {
			 if ($('#roleuser').text() == '0002' && $username!=user) {
		 $('#closechatmsg').show();
		 $currentFromUser = user;
	 } else {
		 
	 }
			$('#chatwith').html('you are currently on chat session with ' + $currentFromUser);
}

function endChat() {
	$currentFromUser='';
	window.name = '';
        $chatWindow.html('');
		 $('#closechatmsg').hide();
		 $('#chatwith').html('you are not on any chat session');
}

function onMessageReceived(evt) {
    var msg = JSON.parse(evt.data); // native API
    var $messageLine = "";
    var $sender = "";
    console.log(msg.message)
    if (msg.sender == '') {
        var array = msg.message.split(",");
        var msg = ""
		 var msgg = ""
        for (var i = 0; i < array.length; i++) {

            msg += '<tr><td><img src="images/online.gif" width="16px" height="16px" alt="online"/></td><td style="cursor: pointer" class="onlineuser" onclick="focusUser(\'' + array[i] + '\')">' + array[i] + '</td></tr>'

        }
        $messageLine += msg;
        console.log($messageLine)
        $onlineUsers.html($messageLine);

    } else {
        if (msg.sender == $username) {
            $messageLine = $('<tr><td class="message">' + msg.message
                    + '</td></tr><tr><td class="senderme">' + 'me @ ' + msg.received
                    + '</td></tr>');
					msgg='<tr><td class="message">' + msg.message
                    + '</td></tr><tr><td class="senderme">' + 'me @ ' + msg.received
                    + '</td></tr>';
        } else {
            $messageLine = $('<tr style="text-align:center"><td class="message">' + msg.message
                    + '</td></tr><tr style="text-align:center; padding-left:10px"><td class="sender">' + msg.sender + ' @ ' + msg.received
                    + '</td></tr>');
					msgg = '<tr style="text-align:center"><td class="message">' + msg.message
                    + '</td></tr><tr style="text-align:center; padding-left:10px"><td class="sender">' + msg.sender + ' @ ' + msg.received
                    + '</td></tr>';
            $sender = msg.sender;

        }
		if ($('#roleuser').text() != '0002') {
			   $currentFromUser = $sender;
		}
        if($currentFromUser!='') {
		window.name += msgg + "%%%" + $currentFromUser + "%%%";
		$('#chatwith').html('you are currently on chat session with ' + $currentFromUser);
	} else {
		window.name += msgg;
	}
        $chatWindow.append($messageLine);
    }
}

function onConnectionOpen() {
    sendOnlineUsersRequest();
//	alert('window name: ' + window.name)
	//alert('$currentFromUser: ' + $currentFromUser)
	if(window.name !='') {
var split =window.name.split('%%%');		
console.log('lenght: ' + split.lenght)
var message;
for (var i = 0; i < split.length; i++) {
	if(i%2 ==0) {
		 message += split[i];
	}
   	if(i%2 !=0) {
		if(split[i]!='') {
			$currentFromUser = split[i];
		}
       
	}
	
}
if($currentFromUser!='') {
		$('#chatwith').html('you are currently on chat session with ' + $currentFromUser);
	} else {
		 $('#chatwith').html('you are not on any chat session');
	}
//alert('still working iwth :' + $currentFromUser + ":")
	
	   $chatWindow.html(message);	
	}
}

function connectToChatserver() {
    wsocket = new WebSocket(serviceLocation + $username + '/' + $('#roleuser').text());
    wsocket.onmessage = onMessageReceived;
    $('#disconnectmsg').show();
    $('#submitmsg').show();
    $('#closechatmsg').hide();
    $('#connectmsg').hide();
    wsocket.onopen = onConnectionOpen;
}

function disConnectToChatserver() {
    wsocket.close();
    $chatWindow.empty();
    $('#disconnectmsg').hide();
    $('#submitmsg').hide();
    $('#closechatmsg').hide();
    $('#connectmsg').show();
    doChatClick();
    $message.focus();
    $onlineUsers.empty();
		$currentFromUser='';
	window.name='';
		 $('#chatwith').html('you are not on any chat session');
}


function doChatClick() {
    var cstate = $('#chartcomp').css('display');
    if (cstate == 'none')
    {
        document.getElementById('chartcomp').style.display = 'block'
    }
    else if (cstate == 'block') {
        document.getElementById('chartcomp').style.display = 'none'
    }
    ;
}

