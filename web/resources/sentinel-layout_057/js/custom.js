function userChart() {
                this.cfg.shadow = false;
                this.cfg.seriesColors = ['#E60000'];
                this.cfg.grid = {
                    background: '#ffffff',
                    borderColor: '#e5ebf0',
                    gridLineColor: '#e5ebf0',
                    shadow: false
                };
                    this.cfg.axes.yaxis.tickOptions = {
        textColor: '#E60000'
    };
    this.cfg.axes.xaxis.tickOptions = {
        textColor: '#E60000',
        angle: 45

    };
                this.cfg.seriesDefaults = {
                    shadow: false,
                    markerOptions: {
                        shadow: false
                    }
                }
            }
            
                   function skinPie() {
                this.cfg.seriesColors = ['#dae8ef','#D80000','#33ccff','#9fadb5'];
                this.cfg.grid = {
                    background: '#ffffff',
                    borderColor: '#eaeaea',
                    gridLineColor: '#e5ebf0',
                    shadow: false,
                    borderWidth: 0
                };
                this.cfg.seriesDefaults.shadow = false;
            }
            
                    function showProgress() {
                document.getElementById('pmodal').className = 'processmodal';
                 document.getElementById('pmodal').innerHTML = 'processing...'; 
            }
            
               function hideProgress() {
                document.getElementById('pmodal').className = 'processmodalhidden';;
            }