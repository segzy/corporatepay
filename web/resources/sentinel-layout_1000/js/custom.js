function userChart() {
    this.cfg.shadow = false;
    this.cfg.seriesColors = ['#0000ff'];
    this.cfg.grid = {
        background: '#ffffff',
        borderColor: '#e5ebf0',
        gridLineColor: '#e5ebf0',
        shadow: false
    };
    this.cfg.axes.yaxis.tickOptions = {
        textColor: '#0000ff'
    };
    this.cfg.axes.xaxis.tickOptions = {
        textColor: '#0000ff',
        angle: 45

    };
    this.cfg.seriesDefaults = {
        shadow: false,
        markerOptions: {
            shadow: false
        }
    }
}

function skinPie() {
    this.cfg.seriesColors = ['#dae8ef', '#298FFF', '#33ccff', '#9fadb5'];
    this.cfg.grid = {
        background: '#ffffff',
        borderColor: '#eaeaea',
        gridLineColor: '#e5ebf0',
        shadow: false,
        borderWidth: 0
    };
    this.cfg.seriesDefaults.shadow = false;
}

function showProgress() {
    document.getElementById('pmodal').className = 'processmodal';
//                 document.getElementById('pmodal').innerHTML = 'processing...'; 
    document.getElementById('pmodal').innerHTML = '<img src="images/progress.gif" alt="processing, please wait....." style="width:100px;height:100px;"/>';
}

function hideProgress() {
    document.getElementById('pmodal').className = 'processmodalhidden';
    ;
}