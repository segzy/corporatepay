/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;

/**
 *
 * @author Oluwasegun.Idowu
 * @param <ApprovalTask>
 */
@Stateless
public interface AbstractWorkflowFacadeLocal<ApprovalTask> {

    ApprovalTask startWorkflow(Class clazz, Object objectId, String requestType,String routeAlias, byte[] attachment) throws Exception;

        ApprovalTask startWorkflowEtz(Class clazz, Object objectId, String requestType,String routeAlias,byte[] attachment) throws Exception;
        
        ApprovalTask startWorkflowCorporate(Class clazz, Object objectId, String requestType,String routeAlias,Corporate corCoporate, byte[] attachment) throws Exception;
           
        ApprovalTask startWorkflowBank(Class clazz, Object objectId, String requestType,String routeAlias, Bank bank,byte[] attachment) throws Exception;
    
    List<TaskObjectNameValuePair> getTaskObject(String reqType);
    
       public ApprovalTask startWorkflow(PaymentFacadeLocal paymentFacadeLocal,Class clazz, Object objectId, String requestType, String routeAlias, byte[] attachment) throws Exception;


}
