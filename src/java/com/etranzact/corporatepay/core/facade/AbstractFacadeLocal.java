/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Oluwasegun.Idowu
 * @param <T>
 */
public interface AbstractFacadeLocal<T extends Serializable> {

    T create(T entity) throws Exception;

    T find(Long id) throws Exception;

    T find(String id) throws Exception;
    
       T find(BigInteger id) throws Exception;

    List<T> findAll() throws Exception;

    List<T> findAll(int min, int max) throws Exception;

    List<T> findAll(Map<String, Object> filters, String... orderFields) throws Exception;

    List<T> findAll(int min, int max, Map<String, Object> filters, String... orderFields) throws Exception;

    List<T> findAll(Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception;

    List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception;

    List<T> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception;

    List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception;

        List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses,Date fromDate,Date toDate,String DateField, String... orderFields) throws Exception;
        
      List<T> findAll( Map<String, Object> filters, boolean activeOnly, String[] flagStatuses,Date fromDate,Date toDate,String DateField, String... orderFields) throws Exception;
    
         long findAllCount( Map<String, Object> filters, boolean activeOnly, String[] flagStatuses,Date fromDate,Date toDate,String DateField, String... orderFields) throws Exception;
            
      List<T> findAll(Map<String, Object> filters, boolean activeOnly) throws Exception;

    List<T> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly) throws Exception;

    void setEntityNamePrefix(String prefix);
    



}
