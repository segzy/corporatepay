/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.InMail;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface InMailFacadeLocal extends AbstractFacadeLocal<InMail>{
   
    
}
