/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Preferences;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface PreferenceFacadeLocal extends AbstractFacadeLocal<Preferences>{
    
   Preferences getCorporatePreferences() throws Exception;
   
     Preferences getCorporatePreferences(Corporate corporate) throws Exception;
     
          Preferences getBankPreferences(Bank bank) throws Exception;
    
}
