/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.User;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface WorkflowTaskFacadeLocal extends AbstractFacadeLocal<ApprovalTask> {

    ApprovalTask doTaskAction(ApprovalTask approvalTask) throws Exception;

    List<ApprovalTask> findUserApprovalTasks() throws Exception;
    
      List<ApprovalTask> findPreviousApprovalTasks(ApprovalTask approvalTask) throws Exception;

    List<ApprovalTask> findUserApprovalTasks(int min, int max) throws Exception;
    
    
    List<ApprovalTask> findUserApprovalTasks( String taskStatefilter, String focusFilter, Date... date) throws Exception;

    List<ApprovalTask> findUserApprovalTasks(int min, int max, String taskTypefilter, String focusFilter, Date... date) throws Exception;

    EntityManager getEntityManager();
}
