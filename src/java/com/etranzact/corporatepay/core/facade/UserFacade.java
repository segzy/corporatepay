package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import com.etz.security.util.Cryptographer;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;

@Stateless
public class UserFacade extends AbstractFacade<User>
        implements UserFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    private Cryptographer crpt = new Cryptographer();

    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;
    @EJB
    private ParametersFacadeLocal parametersFacadeLocal;

    public UserFacade() {
        super(User.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return this.em;
    }

    @Override
    public User login(String username, String password) throws Exception {
        User user = this.userSetupFacadeLocal.findGlobalUser(username);
        if (user == null) {
            throw new Exception("login failed: check username or password");
        }
        String supplied = this.crpt.doMd5Hash(StringUtils.join(new String[]{user.getUsername().trim(), password.trim()}));

        if (!user.getPasswordHash().equals(supplied)) {
            user.setPasswordMissed(user.getPasswordMissed()+ 1);
            throw new Exception("login failed: check username or password");
            
        }
        String validateUserMsg = validateUser(user);
        if (validateUserMsg != null) {
            throw new Exception(validateUserMsg);
        }
        boolean doESAReg = false;
        if (user instanceof CorporateUser) {
            Boolean enableESA = ((CorporateUser) user).getCorporate().getEnableESA();
            if (enableESA!=null && enableESA) {
                if (((CorporateUser) user).getEnableESA() == null || !((CorporateUser) user).getEnableESA()) {
                    doESAReg = true;
                }
            }
        } else 
        if (user instanceof BankUser) {
            Boolean enableESA = ((BankUser) user).getBank().getEnableESA();
            if (enableESA!=null && enableESA) {
                if (((BankUser) user).getEnableESA() == null || !((BankUser) user).getEnableESA()) {
                    doESAReg = true;
                }
            }
        } else {
            doESAReg= ConfigurationUtil.instance().getPlatformUserOnToken();
        }
        
        
        try {
            if (doESAReg) {
                String esaURL = parametersFacadeLocal.find("ESA_URL").getParamValue();
                String esaAPI = parametersFacadeLocal.find("ESA_APP_ID").getParamValue();
                String esaRespnse = registerUser(esaURL, user, "1", esaAPI);
                if (!StringUtils.equals(esaRespnse, "0")) {
                    throw new Exception();
                } else {
                    user.setEnableESA(doESAReg);
                }
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "ESA Registration issues", e);
        }
        user.setLoginTime(new Date());
        user.setPasswordMissed(0);
        return user;
    }

    @Override
    public void validateToken(User user, String token) throws Exception {
        String tokenURL = parametersFacadeLocal.find("TOKEN_URL").getParamValue();
        Map<String, Object> map = new HashMap<>();
        map.put("issuerCode", user.getBankCode());
        map.put("channelCode", "00");
        map.put("requestType", "AUT");
        map.put("tokenSerial", user.getTokenUserGroup());
        map.put("requester", user.getTokenUserName());
        map.put("tokenValue", token);
        Map<String, Object> responseMap = null;
        try {
            //responseMap = AppUtil.doHttpPost(tokenURL, map);
        } catch (Exception e) {
            throw new Exception("Invalid Token");
        }
        if (responseMap != null) {
            Object responseCode = responseMap.get("responseCode");
            if (StringUtils.equals(String.valueOf(responseCode), "0")) {
                throw new Exception("Invalid Token");
            }
        }
    }

    @Asynchronous
    @Override
    public void finalizeLogout(User entity)
            throws Exception {
        super.create(entity);
    }

    @Override
    protected User edit(User entity)
            throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private String validateUser(User user) {
        String customeExMsg = null;
        Calendar calendar = Calendar.getInstance();
        if ((user.getFlagStatus().equals("RJ")) || (user.getFlagStatus().equals("CR"))) {
            customeExMsg = "user has not being approved";
        }
        if (!user.getActive()) {
            customeExMsg = "user not active";
        }
        if (user.getUserDisabled()) {
            customeExMsg = "user is disabled";
        }
        if (user.getUserLocked()) {
            customeExMsg = "user's account has been locked";
        }
        if ((user.getFromTimeAccess() != null) || (user.getToTimeAccess() != null)) {
            boolean accessTimeFailed = false;
            if (user.getToTimeAccess() == null) {
                if (user.getFromTimeAccess().getTime() > calendar.getTimeInMillis()) {
                    accessTimeFailed = true;
                }
            } else if (user.getFromTimeAccess() == null) {
                if (user.getToTimeAccess().getTime() < calendar.getTimeInMillis()) {
                    accessTimeFailed = true;
                }
            } else {
                this.log.log(Level.INFO, "from {0}", Long.valueOf(user.getFromTimeAccess().getTime()));
                this.log.log(Level.INFO, "to {0}", Long.valueOf(user.getToTimeAccess().getTime()));
                this.log.log(Level.INFO, " {0}", Long.valueOf(calendar.getTimeInMillis()));
                if ((user.getFromTimeAccess().getTime() > calendar.getTimeInMillis()) || (user.getToTimeAccess().getTime() < calendar.getTimeInMillis())) {
                    accessTimeFailed = true;
                }
            }
            customeExMsg = accessTimeFailed ? "user has no access to Corporate Pay at this time" : customeExMsg;
        }

        int dayOfWeek = calendar.get(7);
        boolean dayFailed = dayOfWeek == 7 ? user.getSunday().equals("0") : dayOfWeek == 6 ? user.getFriday().equals("0") : dayOfWeek == 5 ? user.getThursday().equals("0") : dayOfWeek == 4 ? user.getWednesday().equals("0") : dayOfWeek == 3 ? user.getTuesday().equals("0") : dayOfWeek == 2 ? user.getMonday().equals("0") : dayOfWeek == 1 ? user.getSunday().equals("0") : false;

        customeExMsg = dayFailed ? "user has no access to Corporate Pay on " + calendar.getDisplayName(7, 2, Locale.ENGLISH) : customeExMsg;

        return customeExMsg;
    }

}
