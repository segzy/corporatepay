/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankApprovalRoute;
import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.HoldingAccount;
import com.etranzact.corporatepay.model.PFA;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.StatutoryDeductionAuthority;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.Tax;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.model.Workflow;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryAdditionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryAddition;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.setup.facade.AccountDetailsFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApprovalGroupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApproverFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankCommissionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CommissionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.HoldingAccountFacadeLocal;
import com.etranzact.corporatepay.setup.facade.PFASetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.TaxOfficeSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.WorkflowFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.MailUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkflowTaskFacadeFacade extends AbstractFacade<ApprovalTask> implements WorkflowTaskFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateSetupFacadeLocal;
    @EJB
    private ApprovalGroupFacadeLocal approvalGroupFacadeLocal;
    @EJB
    private CommissionFacadeLocal commissionFacadeLocal;
    @EJB
    private BankCommissionFacadeLocal bankCommissionFacadeLocal;
    @EJB
    private AccountDetailsFacadeLocal accountDetailsFacadeLocal;
    @EJB
    private PFASetupFacadeLocal pFASetupFacadeLocal;
    @EJB
    private TaxOfficeSetupFacadeLocal taxSetupFacadeLocal;
    @EJB
    private HoldingAccountFacadeLocal holdingAccountFacadeLocal;
    @EJB
    private WorkflowFacadeLocal workflowFacadeLocal;
    @EJB
    private PaymentFacadeLocal paymentFacadeLocal;
    @EJB
    private WorkersFacadeLocal workerFacadeLocal;
    @EJB
    private WorkersSalaryDeductionFacadeLocal workersSalaryDeductionFacadeLocal;
    @EJB
    private WorkersSalaryAdditionFacadeLocal workersSalaryAdditionFacadeLocal;

    public WorkflowTaskFacadeFacade() {
        super(ApprovalTask.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ApprovalTask edit(ApprovalTask entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void finalizeApprove(ApprovalTask t) throws Exception {
        log.log(Level.INFO, "finalizing   taskkk ..... {0}", t.getApprovalRoute().getApprovalType().getDescription());
        if (User.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            User user = em.find(User.class, Long.valueOf(t.getTargetObjectId()));
            userSetupFacadeLocal.finalizeApprove(user, t);
        } else if (Bank.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            Bank bank = em.find(Bank.class, String.valueOf(t.getTargetObjectId()));
            bankSetupFacadeLocal.finalizeApprove(bank, t);
        } else if (Corporate.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            Corporate corporate = em.find(Corporate.class, Long.valueOf(t.getTargetObjectId()));
            corporateSetupFacadeLocal.finalizeApprove(corporate, t);
        } else if (ApprovalGroup.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            ApprovalGroup approvalGroup = em.find(ApprovalGroup.class, Long.valueOf(t.getTargetObjectId()));
            approvalGroupFacadeLocal.finalizeApprove(approvalGroup, t);
        } else if (Commission.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            Commission commission = em.find(Commission.class, Long.valueOf(t.getTargetObjectId()));
            commissionFacadeLocal.finalizeApprove(commission, t);
        } else if (BankCommission.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            BankCommission commission = em.find(BankCommission.class, Long.valueOf(t.getTargetObjectId()));
            bankCommissionFacadeLocal.finalizeApprove(commission, t);
        } else if (Card.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            CorporateCard card = (CorporateCard) em.find(Card.class, Long.valueOf(t.getTargetObjectId()));
            accountDetailsFacadeLocal.finalizeApprove(card, t);
        } else if (PFA.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            PFA pfa = (PFA) em.find(PFA.class, t.getTargetObjectId());
            pFASetupFacadeLocal.finalizeApprove(pfa, t);
        } else if (Tax.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            Tax tax = (Tax) em.find(Tax.class, t.getTargetObjectId());
            taxSetupFacadeLocal.finalizeApprove(tax, t);
        } else if (HoldingAccount.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            HoldingAccount holdingAccount = (HoldingAccount) em.find(HoldingAccount.class, Long.valueOf(t.getTargetObjectId()));
            holdingAccountFacadeLocal.finalizeApprove(holdingAccount, t);
        } else if (Workflow.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            Workflow workflow = (Workflow) em.find(Workflow.class, Long.valueOf(t.getTargetObjectId()));
            workflowFacadeLocal.finalizeApprove(workflow, t);
        } else if (Payment.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            Payment payment = (Payment) em.find(Payment.class, Long.valueOf(t.getTargetObjectId()));
            payment.setCard(t.getCorporateCard());
            paymentFacadeLocal.finalizeApprove(payment, t);
        } else if (Worker.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            Worker worker = (Worker) em.find(Worker.class, Long.valueOf(t.getTargetObjectId()));
            workerFacadeLocal.finalizeApprove(worker, t);
        } else if (WorkersSalaryDeduction.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            WorkersSalaryDeduction workersSalaryDeduction = (WorkersSalaryDeduction) em.find(WorkersSalaryDeduction.class, Long.valueOf(t.getTargetObjectId()));
            workersSalaryDeductionFacadeLocal.finalizeApprove(workersSalaryDeduction, t);
        } else if (WorkersSalaryAddition.class.getSimpleName().equals(t.getApprovalRoute().getApprovalType().getDescription())) {
            WorkersSalaryAddition workersSalaryAddition = (WorkersSalaryAddition) em.find(WorkersSalaryAddition.class, Long.valueOf(t.getTargetObjectId()));
            workersSalaryAdditionFacadeLocal.finalizeApprove(workersSalaryAddition, t);
        }
    }

    @Override
    public ApprovalTask doTaskAction(ApprovalTask approvalTask) throws Exception {
        String cardDetail = approvalTask.getCardDetail();
        CorporateCard corporateCard = approvalTask.getCorporateCard();
        String expMonth = approvalTask.getExpMonth();
        String expYear = approvalTask.getExpYear();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        if (!approvalTask.getApprovalAction().equals(AppConstants.KEPT_IN_VIEW)) {
            approvalTask.setActionDate(new Date());
            approvalTask.setActor((Approver) externalContext.getSessionMap().get(AppConstants.LOGIN_USER));
            approvalTask = super.create(approvalTask);
            approvalTask.setCardDetail(cardDetail);
            approvalTask.setCorporateCard(corporateCard);
            approvalTask.setExpMonth(expMonth);
            approvalTask.setExpYear(expYear);
            if (approvalTask.getApprovalAction().equals(AppConstants.REJECTED)) {
                finalizeApprove(approvalTask);
                return approvalTask;
            }
        } else {
            approvalTask = super.create(approvalTask);
            return approvalTask;
        }
        ApprovalRoute approvalRoute = approvalTask.getApprovalRoute();
        ApprovalTask nextApprovalTask = null;
        ApprovalRoute nextApprovalRoute = null;
        List<? extends ApprovalRoute> prevApprovalRoutes = null;
        String ext = "";
        if (approvalRoute.getApprovalType().getAllowMultipleRoute() && approvalRoute.getRouteAlias() != null) {
            ext += " and a.routeAlias='" + approvalRoute.getRouteAlias() + "'";
        }
        try {
            if (approvalRoute instanceof BankApprovalRoute) {
                nextApprovalRoute = getEntityManager().createQuery("select a from BankApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level=" + (approvalRoute.getLevel() + 1) + " and a.active=true and a.flagStatus=:flagStatus and a.bank=:bank" + ext, BankApprovalRoute.class)
                        .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).setParameter("bank", ((BankApprovalRoute) approvalRoute).getBank()).getSingleResult();
                prevApprovalRoutes = getEntityManager().createQuery("select a from BankApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level<" + (approvalRoute.getLevel()) + " and a.active=true and a.flagStatus=:flagStatus and a.bank=:bank" + ext, BankApprovalRoute.class)
                        .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).setParameter("bank", ((BankApprovalRoute) approvalRoute).getBank()).getResultList();
            } else if (approvalRoute instanceof CorporateApprovalRoute) {
                nextApprovalRoute = getEntityManager().createQuery("select a from CorporateApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level=" + (approvalRoute.getLevel() + 1) + " and a.active=true and a.flagStatus=:flagStatus and a.corporate=:corporate" + ext, CorporateApprovalRoute.class)
                        .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).setParameter("corporate", ((CorporateApprovalRoute) approvalRoute).getCorporate()).getSingleResult();
                prevApprovalRoutes = getEntityManager().createQuery("select a from CorporateApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level<" + (approvalRoute.getLevel() + 1) + " and a.active=true and a.flagStatus=:flagStatus and a.corporate=:corporate" + ext, CorporateApprovalRoute.class)
                        .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).setParameter("corporate", ((CorporateApprovalRoute) approvalRoute).getCorporate()).getResultList();
            } else {
                nextApprovalRoute = getEntityManager().createQuery("select a from ApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level=" + (approvalRoute.getLevel() + 1) + " and a.active=true and a.central=true and a.flagStatus=:flagStatus" + ext, ApprovalRoute.class)
                        .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).getSingleResult();
                prevApprovalRoutes = getEntityManager().createQuery("select a from ApprovalRoute a where a.approvalType=:approvalType"
                        + " and a.level<" + (approvalRoute.getLevel() + 1) + " and a.active=true and a.central=true and a.flagStatus=:flagStatus" + ext, ApprovalRoute.class)
                        .setParameter("approvalType", approvalRoute.getApprovalType()).setParameter("flagStatus", AppConstants.APPROVED).getResultList();
            }
        } catch (PersistenceException pex) {
//            log.log(Level.SEVERE, "Error getting next Appoval Route", pex.fillInStackTrace());
        }
        if (nextApprovalRoute != null && nextApprovalRoute.getActive() && nextApprovalRoute.getFlagStatus().equalsIgnoreCase(AppConstants.APPROVED) && approvalTask.getApprovalAction().equalsIgnoreCase(AppConstants.APPROVED)) {
            nextApprovalTask = new ApprovalTask();
            nextApprovalTask.setApprovalRoute(nextApprovalRoute);
            nextApprovalTask.setTargetObjectId(approvalTask.getTargetObjectId());
            nextApprovalTask.setDateCreated(new Date());
            nextApprovalTask.setNameValuePairs(approvalTask.getNameValuePairs());
            nextApprovalTask.setTaskAccessor(nextApprovalRoute.getApprover());
            nextApprovalTask.setDocument(approvalTask.getDocument());
            nextApprovalTask.setRequestApprovalType(approvalTask.getRequestApprovalType());
            nextApprovalTask.setCurrentRouteId(approvalTask.getId());
            nextApprovalTask = super.create(nextApprovalTask);
            approvalTask = em.find(ApprovalTask.class, approvalTask.getId());
            for (TaskObjectNameValuePair taskObjectNameValuePair : approvalTask.getNameValuePairs()) {
                taskObjectNameValuePair.setApprovalTask(nextApprovalTask);
            }
            if (nextApprovalRoute.getApprovalType().getDescription().equals(Payment.class.getSimpleName())) {
                Approver[] prevreceipients = null;
                if (prevApprovalRoutes != null) {
                    prevreceipients = new Approver[prevApprovalRoutes.size()];
                    int i = 0;
                    for (ApprovalRoute prevApprovalRoute : prevApprovalRoutes) {
                        prevreceipients[i] = prevApprovalRoute.getApprover();
                        i++;
                    }
                }
                Payment payment = paymentFacadeLocal.find(Long.valueOf(nextApprovalTask.getTargetObjectId()));
                MailUtil.getInstance().sendTaskAlert(prevreceipients, nextApprovalTask.getTaskAccessor(), payment);
            }
        } else {
            finalizeApprove(approvalTask);
        }

        return nextApprovalTask;
    }

    @Override
    public List<ApprovalTask> findUserApprovalTasks() throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User user = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (user == null) {
            return new ArrayList<>();
        }
        user = em.find(User.class, user.getId());
        List<ApprovalTask> userTaskList = getEntityManager().createQuery("select a from ApprovalTask a where a.taskAccessor=:taskAccessor and (a.approvalAction is null or a.actionDate is null)", ApprovalTask.class).setParameter("taskAccessor", user).getResultList();
        for (ApprovalTask approvalTask : userTaskList) {

            approvalTask.setTaskType(AppConstants.USER_TASK_TYPE);
        }
        for (ApprovalGroup approvalGroup : user.getApprovalGroups()) {
            List<ApprovalTask> groupTaskList = getEntityManager().createQuery("select a from ApprovalTask a where a.taskAccessor=:taskAccessor and (a.approvalAction is null or a.actionDate is null)", ApprovalTask.class).setParameter("taskAccessor", approvalGroup).getResultList();
            for (ApprovalTask approvalTask : groupTaskList) {
                approvalTask.setTaskType(AppConstants.GROUP_TASK_TYPE);
            }
            userTaskList.addAll(groupTaskList);
        }
        for (ApprovalTask approvalTask : userTaskList) {
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()) {
                approvalTask.setTokenRequired(true);
            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())
                    && approvalTask.getRequestApprovalType().equals(AppConstants.REQUEST_CREATION_APPROVAL)) {
                approvalTask.setCardRequired(true);
                approvalTask.setCardDetailsRequired(true);
            }
//            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
//                    && user.getRole().getFinalAuthorizerRole()
//                    && (approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Card.class.getSimpleName()))) {
//                approvalTask.setCardDetailsRequired(true);
//            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())) {
                approvalTask.setCardRequired(true);
                approvalTask.setCardDetailsRequired(true);
            }
             approvalTask = resolveTargetName(approvalTask);

        }
        return userTaskList;
    }

    @Override
    public List<ApprovalTask> findUserApprovalTasks(int min, int max) throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User user = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        user = em.find(User.class, user.getId());
        List<ApprovalTask> userTaskList = getEntityManager().createQuery("select a from ApprovalTask a where a.taskAccessor=:taskAccessor and (a.approvalAction is null or a.actionDate is null)", ApprovalTask.class).setParameter("taskAccessor", user).getResultList();
        for (ApprovalTask approvalTask : userTaskList) {
            approvalTask.setTaskType(AppConstants.USER_TASK_TYPE);
        }
        for (ApprovalGroup approvalGroup : user.getApprovalGroups()) {
            List<ApprovalTask> groupTaskList = getEntityManager().createQuery("select a from ApprovalTask a where a.taskAccessor=:taskAccessor and (a.approvalAction is null or a.actionDate is null)", ApprovalTask.class).setParameter("taskAccessor", approvalGroup).getResultList();
            for (ApprovalTask approvalTask : groupTaskList) {
                approvalTask.setTaskType(AppConstants.GROUP_TASK_TYPE);
            }
            userTaskList.addAll(groupTaskList);
        }
        //     log.log(Level.INFO, "min {0} max {1}", new Object[]{min, max});
        userTaskList = userTaskList.subList(min, userTaskList.size() < max ? userTaskList.size() : max);
        for (ApprovalTask approvalTask : userTaskList) {
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()) {
                approvalTask.setTokenRequired(true);
            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())
                    && approvalTask.getRequestApprovalType().equals(AppConstants.REQUEST_CREATION_APPROVAL)) {
                approvalTask.setCardRequired(true);
                approvalTask.setCardDetailsRequired(true);
            }
//            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
//                    && user.getRole().getFinalAuthorizerRole()
//                    && (approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Card.class.getSimpleName()))) {
//                approvalTask.setCardDetailsRequired(true);
//            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())) {
                approvalTask.setCardDetailsRequired(true);
                approvalTask.setCardRequired(true);
            }
             approvalTask = resolveTargetName(approvalTask);

        }
        return userTaskList;
    }

    @Override
    public List<ApprovalTask> findPreviousApprovalTasks(ApprovalTask approvalTask) throws Exception {
        String q = "select a from ApprovalTask a where a.targetObjectId='" + approvalTask.getTargetObjectId() + "'"
                + " and a.currentRouteId=" + approvalTask.getCurrentRouteId() + " and a.id !=" + approvalTask.getId();
//        log.log(Level.INFO, "Query::: {0}", q);
//           String q = "select a from ApprovalTask a where a.targetObjectId='" + approvalTask.getTargetObjectId() +  "' and a.approvalRoute=" + approvalTask.getApprovalRoute().getId() +
//                " and a.currentRouteId=" + approvalTask.getCurrentRouteId();
//         log.log(Level.INFO, "Queryyy::: {0}", em.createQuery(q).getResultList().size());
//        log.log(Level.INFO, "Query::: {0}", q);
        return em.createQuery(q).getResultList();
    }

    @Override
    public List<ApprovalTask> findUserApprovalTasks(String taskStatefilter, String focusFilter, Date... date) throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User user = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        user = em.find(User.class, user.getId());
        String ext = "";
        if (taskStatefilter != null) {
            if (taskStatefilter.equals(AppConstants.OPEN)) {
                ext += " and a.approvalAction is null";
            } else {
                ext += " and a.approvalAction='" + taskStatefilter + "'";
            }
        }
        if (date.length > 0) {
            Calendar from = Calendar.getInstance();
            from.setTime(date[0]);
            String fromDateStr = from.get(Calendar.YEAR) + "-" + (from.get(Calendar.MONTH) + 1) + "-" + from.get(Calendar.DAY_OF_MONTH);
            Calendar to = Calendar.getInstance();

            to.setTime(date[1]);

            String toDateStr = to.get(Calendar.YEAR) + "-" + (to.get(Calendar.MONTH) + 1) + "-" + to.get(Calendar.DAY_OF_MONTH);
            ext += " and a.dateCreated >= '" + java.sql.Date.valueOf(fromDateStr) + "' and a.dateCreated <= '" + java.sql.Date.valueOf(toDateStr) + "'";
        }
//        String q = "select a from ApprovalTask a where a.taskAccessor=:taskAccessor " + ext + " and (a.approvalAction is null or a.actionDate is null)";
        String q = "select a from ApprovalTask a where a.taskAccessor=:taskAccessor " + ext;
        //       log.log(Level.INFO, "queryyyy:: {0}", q);
        List<ApprovalTask> userTaskList = getEntityManager().createQuery(q, ApprovalTask.class).setParameter("taskAccessor", user).getResultList();
        for (ApprovalTask approvalTask : userTaskList) {
            approvalTask.setTaskType(AppConstants.USER_TASK_TYPE);
        }
        for (ApprovalGroup approvalGroup : user.getApprovalGroups()) {
            List<ApprovalTask> groupTaskList = getEntityManager().createQuery(q, ApprovalTask.class).setParameter("taskAccessor", approvalGroup).getResultList();
            for (ApprovalTask approvalTask : groupTaskList) {
                approvalTask.setTaskType(AppConstants.GROUP_TASK_TYPE);
            }
            userTaskList.addAll(groupTaskList);
        }
        for (ApprovalTask approvalTask : userTaskList) {
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()) {
                approvalTask.setTokenRequired(true);
            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())
                    && approvalTask.getRequestApprovalType().equals(AppConstants.REQUEST_CREATION_APPROVAL)) {
                approvalTask.setCardRequired(true);
                approvalTask.setCardDetailsRequired(true);
            }
//            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
//                    && user.getRole().getFinalAuthorizerRole()
//                    && (approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Card.class.getSimpleName()))) {
//                approvalTask.setCardDetailsRequired(true);
//            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())) {
                approvalTask.setCardRequired(true);
                approvalTask.setCardDetailsRequired(true);
            }
            approvalTask = resolveTargetName(approvalTask);

        }
        return userTaskList;
    }

    @Override
    public List<ApprovalTask> findUserApprovalTasks(int min, int max, String taskStatefilter, String focusFilter, Date... date) throws Exception {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User user = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        user = em.find(User.class, user.getId());
        String ext = "";
        if (taskStatefilter != null) {
            if (taskStatefilter.equals(AppConstants.OPEN)) {
                ext += " and a.approvalAction is null";
            } else {
                ext += " and a.approvalAction='" + taskStatefilter + "'";
            }
        }
        if (date.length > 0) {
            Calendar from = Calendar.getInstance();
            from.setTime(date[0]);
            String fromDateStr = from.get(Calendar.YEAR) + "-" + (from.get(Calendar.MONTH) + 1) + "-" + from.get(Calendar.DAY_OF_MONTH);
            Calendar to = Calendar.getInstance();
            to.setTime(date[1]);
            String toDateStr = to.get(Calendar.YEAR) + "-" + (to.get(Calendar.MONTH) + 1) + "-" + to.get(Calendar.DAY_OF_MONTH);
            ext += " and a.dateCreated >= '" + java.sql.Date.valueOf(fromDateStr) + "' and a.dateCreated <= '" + java.sql.Date.valueOf(toDateStr) + "'";
        }
        //String q = "select a from ApprovalTask a where a.taskAccessor=:taskAccessor " + ext + " and (a.approvalAction is null or a.actionDate is null)";
        String q = "select a from ApprovalTask a where a.taskAccessor=:taskAccessor " + ext;
//log.log(Level.INFO, "query:: {0}", q);
        List<ApprovalTask> userTaskList = getEntityManager().createQuery(q, ApprovalTask.class).setParameter("taskAccessor", user).getResultList();
        for (ApprovalTask approvalTask : userTaskList) {
            approvalTask.setTaskType(AppConstants.USER_TASK_TYPE);
        }
        for (ApprovalGroup approvalGroup : user.getApprovalGroups()) {
            List<ApprovalTask> groupTaskList = getEntityManager().createQuery(q, ApprovalTask.class).setParameter("taskAccessor", approvalGroup).getResultList();
            for (ApprovalTask approvalTask : groupTaskList) {
                approvalTask.setTaskType(AppConstants.GROUP_TASK_TYPE);
            }
            userTaskList.addAll(groupTaskList);
        }
        //log.log(Level.INFO, "min {0} max {1}", new Object[]{min, max});
        userTaskList = userTaskList.subList(min, userTaskList.size() < max ? userTaskList.size() : max);
        for (ApprovalTask approvalTask : userTaskList) {
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()) {
                approvalTask.setTokenRequired(true);
            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())
                    && approvalTask.getRequestApprovalType().equals(AppConstants.REQUEST_CREATION_APPROVAL)) {
                approvalTask.setCardRequired(true);
                approvalTask.setCardDetailsRequired(true);
            }
//            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
//                    && user.getRole().getFinalAuthorizerRole()
//                    && (approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Card.class.getSimpleName()))) {
//                approvalTask.setCardDetailsRequired(true);
//            }
            if (approvalTask.getApprovalRoute().getApprovalType().getFinancialLike()
                    && user.getRole().getFinalAuthorizerRole()
                    && approvalTask.getApprovalRoute().getApprovalType().getDescription().equals(Payment.class.getSimpleName())) {
                approvalTask.setCardRequired(true);
                approvalTask.setCardDetailsRequired(true);
            }
              approvalTask = resolveTargetName(approvalTask);
        }
        return userTaskList;
    }

}
