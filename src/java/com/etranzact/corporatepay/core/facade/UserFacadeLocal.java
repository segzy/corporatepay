/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.setup.facade.*;
import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.User;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface UserFacadeLocal extends AbstractFacadeLocal<User>{
    

     User login(String username, String password) throws Exception;
     
     void validateToken(User user, String token) throws Exception;
         
     void finalizeLogout(User entity) throws Exception;


    
}
