/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.facade;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Preferences;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class PreferenceFacade extends AbstractFacade<Preferences> implements PreferenceFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public PreferenceFacade() {
        super(Preferences.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Preferences edit(Preferences entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Preferences getCorporatePreferences() throws Exception {
        Map<String, Object> m = new HashMap<>();
        if (getLoggedCorporate() != null) {
            m.put("p.corporate.id=" + getLoggedCorporate().getId(), null);
        }
        if (getLoggedBank() != null) {
            m.put("p.bank.id='" + getLoggedBank().getId() + "'", null);
        }
        List<Preferences> findAll = findAll(m, true);
        if (findAll.isEmpty()) {
            return null;
        } else if (findAll.size() > 1) {
            throw new Exception("Unexpected result getting preference");
        } else {
            return findAll.get(0);
        }
    }

    @Override
    public Preferences getCorporatePreferences(Corporate corporate) throws Exception {
        Map<String, Object> m = new HashMap<>();
        m.put("p.corporate.id=" + corporate.getId(), null);
        List<Preferences> findAll = findAll(m, true);
        if (findAll.isEmpty()) {
            return null;
        } else if (findAll.size() > 1) {
            throw new Exception("Unexpected result getting preference");
        } else {
            return findAll.get(0);
        }
    }

    @Override
    public Preferences getBankPreferences(Bank bank) throws Exception {
           Map<String, Object> m = new HashMap<>();
             m.put("p.bank.id='" + bank.getId() + "'", null);
        List<Preferences> findAll = findAll(m, true);
        if (findAll.isEmpty()) {
            return null;
        } else if (findAll.size() > 1) {
            throw new Exception("Unexpected result getting preference");
        } else {
            return findAll.get(0);
        }
    }

}
