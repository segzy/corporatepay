/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.ParametersFacadeLocal;
import com.etranzact.corporatepay.core.facade.WorkflowTaskFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.Event;
import com.etranzact.corporatepay.model.Parameters;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.setup.facade.AccountDetailsFacadeLocal;
import com.etranzact.corporatepay.setup.facade.EventFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "taskBean")
@SessionScoped
public class TaskBean extends AbstractBean<ApprovalTask> {

    @EJB
    private WorkflowTaskFacadeLocal workflowTaskFacadeLocal;
    @EJB
    private AccountDetailsFacadeLocal accountDetailsFacadeLocal;
    @EJB
    private PaymentFacadeLocal paymentFacadeLocal;
    @EJB
    private EventFacadeLocal eventFacadeLocal;
    private List<ApprovalTask> previousApprovalTasks;
    private boolean commentRequired;
    private boolean alreadyActedOn;
    private List<CorporateCard> corporateCards;
    private CorporateCard cc;

    @Override
    protected WorkflowTaskFacadeLocal getFacade() {
        return workflowTaskFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public TaskBean() {
        super(ApprovalTask.class);
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        try {
            if (selectedItem.getApprovalAction().equals(AppConstants.APPROVED)
                    || selectedItem.getApprovalAction().equals(AppConstants.REJECTED)) {
                alreadyActedOn = true;
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('alreadyActedMessage').show();");
            }
            previousApprovalTasks = workflowTaskFacadeLocal.findPreviousApprovalTasks(selectedItem);
//        if (selectedItem.getApprovalRoute().getApprovalType()
//                .getDescription().equals(Card.class.getSimpleName())
//                || selectedItem.getApprovalRoute().getApprovalType()
//                .getDescription().equals(Payment.class.getSimpleName())) {
//            try {
//                Parameters parameter = parametersFacadeLocal.find("TOKEN_URL");
//                if (parameter != null) {
//                    Map<String, Object> map = new HashMap<>();
//                    //Map<String, Object> doHttpPost = AppUtil.doHttpPost(parameter.getParamValue(), map);
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
        } catch (Exception ex) {
            Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
            previousApprovalTasks = new ArrayList<>();
        }
    }

    public void checkBalance() {
        FacesMessage m = new FacesMessage();

        try {
            String reference = "08" + System.currentTimeMillis();
            log.info("reference " + reference);
            log.info("cardNumber " + selectedItem.getCorporateCard().getCardNumber());
            log.info("expYear " + expYear);
            log.info("expMonth " + expMonth);
            Double balance = balanceCheck("08" + System.currentTimeMillis(), selectedItem.getCorporateCard().getCardNumber(), pin, expYear, expMonth);
            String balanceStr = "";
            if (balance != null) {
                balanceStr = AppUtil.formatAsAmount(balance.toString());
            }
//            String balanceStr = "NGN 0";
            if (balance == null) {
                balanceStr = "Unavailable Balance";
            } else {
                balanceStr = "NGN " + balanceStr;
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Current Account Balance (" + balanceStr + ")");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void action() {
        FacesMessage m = new FacesMessage();
        String evtMsg = null;
        String description = null; 
        try {
            selectedItem.setCardDetail(pin);
            selectedItem.setToken(token);
            selectedItem.setExpMonth(expMonth);
            selectedItem.setExpYear(expYear);
            if (selectedItem.getId() == null) {
                throw new Exception("Select a Task first");
            }
            if (Payment.class.getSimpleName().equals(selectedItem.getApprovalRoute().getApprovalType().getDescription())) {
                description = selectedItem.getApprovalRoute().getApprovalType().getDescription();
                paymentFacadeLocal.increaseTrial(Long.valueOf(selectedItem.getTargetObjectId()), selectedItem.getCorporateCard());
            }
            workflowTaskFacadeLocal.doTaskAction(selectedItem);
            UserBean userBean = (UserBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userBean");
            userBean.setApprovalTasks(workflowTaskFacadeLocal.findUserApprovalTasks());
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(selectedItem.getApprovalAction().equals(AppConstants.APPROVED) ? "Approved"
                    : selectedItem.getApprovalAction().equals(AppConstants.REJECTED) ? "Rejected"
                            : selectedItem.getApprovalAction().equals(AppConstants.KEPT_IN_VIEW) ? "Kept In View" : "");
            FacesContext.getCurrentInstance().addMessage(null, m);
            evtMsg = "Authorized";
        } catch (Exception ex) {
            Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            evtMsg = ex.getMessage();
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
       
            if (description!=null || Payment.class.getSimpleName().equals(description) && getLoggedUser().getRole().getFinalAuthorizerRole()) {
                log.info("event......");
                Event evt = new Event();
                evt.setEventObject("08" + selectedItem.getTargetObjectId());
                evt.setEventType(Payment.class.getSimpleName());
                evt.setMessage(evtMsg);
                evt.setLoggedUser(getLoggedUser().getUsername());
                evt.setCorporate(getLoggedCorporate());
                        log.info("creating event......");
                try {
                    evt = eventFacadeLocal.create(evt);
                               log.info("created event......" + evt);
                } catch (Exception e) {
                    Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, e);
                }
            }
                 selectedItem = new ApprovalTask();
            commentRequired = false;
            expMonth = null;
            expYear = null;
        }

    }

    public void handleActionChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        Object approvalType = ((UISelectOne) e.getComponent()).getValue();
        log.log(Level.INFO, "approval type {0}", approvalType);
        if (String.valueOf(approvalType).equals(AppConstants.REJECTED)) {
            commentRequired = true;
        } else {
            commentRequired = false;
        }
    }

    /**
     * @return the commentRequired
     */
    public boolean isCommentRequired() {
        return commentRequired;
    }

    /**
     * @param commentRequired the commentRequired to set
     */
    public void setCommentRequired(boolean commentRequired) {
        this.commentRequired = commentRequired;
    }

    /**
     * @return the corporateCards
     */
    public List<CorporateCard> getCorporateCards() {
        if (selectedItem.isCardRequired()) {
            if (corporateCards == null) {
                try {
                    Map<String, Object> map = new HashMap<>();
                    map.put("corporate", getLoggedCorporate());
                    corporateCards = accountDetailsFacadeLocal.findAll(map, true);
                } catch (Exception ex) {
                    Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
                    corporateCards = new ArrayList<>();
                }
            }
        }
        return corporateCards;
    }

    /**
     * @param corporateCards the corporateCards to set
     */
    public void setCorporateCards(List<CorporateCard> corporateCards) {
        this.corporateCards = corporateCards;
    }

    /**
     * @return the cc
     */
    public CorporateCard getCc() {
        return cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(CorporateCard cc) {
        this.cc = cc;
    }

    /**
     * @return the previousApprovalTasks
     */
    public List<ApprovalTask> getPreviousApprovalTasks() {
        return previousApprovalTasks;
    }

    /**
     * @return the alreadyActedOn
     */
    public boolean isAlreadyActedOn() {
        return alreadyActedOn;
    }

}
