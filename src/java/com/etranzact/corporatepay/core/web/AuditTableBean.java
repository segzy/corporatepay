/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.core.facade.UserFacadeLocal;
import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.AuditLog;
import com.etranzact.corporatepay.model.AuditTable;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.HoldingAccount;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payment.facade.AuditFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "auditTableBean")
@SessionScoped
public class AuditTableBean extends AbstractTableBean<AuditLog> {

    @EJB
    protected AuditLogFacadeLocal audittFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateFacadeLocal;

    private Bank bankFilter;

    private List<Bank> banks;

    private Corporate corporateFilter;

    private List<Corporate> corporates;

    private String loggedInUserFilter = "";
    private String targetObjectFilter = "";
    private String operationTypeFilter = "";

    public AuditTableBean() {
        super(AuditLog.class);
    }

    @Override
    protected AuditLogFacadeLocal getFacade() {
        return audittFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        super.init();
    }

    @Override
    public AuditLog getTableRowData(String rowKey) {
        List<AuditLog> wrappedData = (List<AuditLog>) getLazyDataModel().getWrappedData();
        for (AuditLog t : wrappedData) {
            if (rowKey.equals(t.getId().toString())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(AuditLog t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (loggedInUserFilter!=null && !loggedInUserFilter.isEmpty()) {
            properties.getAdditionalFilter().put("username", loggedInUserFilter);
        }
        if (targetObjectFilter!=null && !targetObjectFilter.isEmpty()) {
            properties.getAdditionalFilter().put("target", targetObjectFilter);
        }
        if (operationTypeFilter!=null && !operationTypeFilter.isEmpty()) {
            properties.getAdditionalFilter().put("auditType", operationTypeFilter);
        }
        if (bankFilter != null) {
            properties.getAdditionalFilter().put("bankCode", bankFilter.getId());
        }
        if (corporateFilter != null) {
            properties.getAdditionalFilter().put("corporateId", corporateFilter.getId());
        }
        properties.setFlagStatuses(new String[0]);
        properties.setOrderFields(new String[]{"id"});
        return properties;
    }

//    public static void main(String[] args) {
//        System.out.println("sjndjdjdjd,jdjdjdjd,".substring(0, "sjndjdjdjd,jdjdjdjd,".length() -1));
//    }
    /**
     * @return the bankFilter
     */
    public Bank getBankFilter() {
        return bankFilter;
    }

    /**
     * @param bankFilter the bankFilter to set
     */
    public void setBankFilter(Bank bankFilter) {
        this.bankFilter = bankFilter;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null) {

            try {
                banks = bankFacadeLocal.findAll(new HashMap(), true, "bankName");
                corporateFilter = null;
            } catch (Exception ex) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
        }
        return banks;
    }

    /**
     * @return the corporateFilter
     */
    public Corporate getCorporateFilter() {
        return corporateFilter;
    }

    /**
     * @param corporateFilter the corporateFilter to set
     */
    public void setCorporateFilter(Corporate corporateFilter) {
        this.corporateFilter = corporateFilter;
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        try {
            if (bankFilter == null) {
                bankFilter = getLoggedBank();
            }
            if (bankFilter != null) {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("bank", bankFilter);
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
            } else {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            corporates = new ArrayList<>();
        }

        return corporates;
    }

    /**
     * @return the loggedInUserFilter
     */
    public String getLoggedInUserFilter() {
        return loggedInUserFilter;
    }

    /**
     * @param loggedInUserFilter the loggedInUserFilter to set
     */
    public void setLoggedInUserFilter(String loggedInUserFilter) {
        this.loggedInUserFilter = loggedInUserFilter;
    }

    /**
     * @return the targetObjectFilter
     */
    public String getTargetObjectFilter() {
        return targetObjectFilter;
    }

    /**
     * @return the operationTypeFilter
     */
    public String getOperationTypeFilter() {
        return operationTypeFilter;
    }

    /**
     * @param targetObjectFilter the targetObjectFilter to set
     */
    public void setTargetObjectFilter(String targetObjectFilter) {
        this.targetObjectFilter = targetObjectFilter;
    }

    /**
     * @param operationTypeFilter the operationTypeFilter to set
     */
    public void setOperationTypeFilter(String operationTypeFilter) {
        this.operationTypeFilter = operationTypeFilter;
    }

}
