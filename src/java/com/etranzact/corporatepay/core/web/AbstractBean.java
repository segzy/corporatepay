/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.AuditLog;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import com.etz.http.etc.Card;
import com.etz.http.etc.HttpHost;
import com.etz.http.etc.TransCode;
import com.etz.http.etc.XProcessor;
import com.etz.http.etc.XRequest;
import com.etz.http.etc.XResponse;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 * @param <T>
 */
public abstract class AbstractBean<T extends Serializable> extends Bean {
    
    private Class<T> clazz;
    protected Logger log;
    protected T selectedItem;
    protected String token;
    protected String pin;
    protected String expMonth;
    protected String expYear;
    private boolean tokenEnabled;
    
    public AbstractBean(Class<T> classT) {
        this.clazz = classT;
        log = Logger.getLogger(clazz.getName());
    }
    
    protected abstract AbstractFacadeLocal<T> getFacade();
    
    public void onRowSelect(SelectEvent selectEvent) {
        setSelectedItem((T) selectEvent.getObject());
    }

    /**
     * @return the selectedItem
     */
    public T getSelectedItem() {
        
        try {
            if (selectedItem == null) {
                this.selectedItem = clazz.newInstance();
            }
            return selectedItem;
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(AbstractBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List<String> getYears() {
        List<String> years = new ArrayList<>();
        for (int i = 2015; i < 2090; i++) {
            years.add(String.valueOf(i));
        }
        return years;
    }

    /**
     * @param selectedItem the selectedItem to set
     */
    public void setSelectedItem(T selectedItem) {
        this.selectedItem = selectedItem;
    }

    /**
     *
     */
    public void reset() {
        try {
            this.selectedItem = clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(AbstractBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin the pin to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * @return the expMonth
     */
    public String getExpMonth() {
        return expMonth;
    }

    /**
     * @param expMonth the expMonth to set
     */
    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    /**
     * @return the expYear
     */
    public String getExpYear() {
        return expYear;
    }

    /**
     * @param expYear the expYear to set
     */
    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }
    
    public Double balanceCheck(String reference,
            String cardNum, String cardPin, String expYY, String expMM) {
        Double balance = null;
        
        try {
            String ip = ConfigurationUtil.instance().getAutoSwitchIP();
            int port = ConfigurationUtil.instance().getAutoSwitchPort();
            
            XProcessor processor = new XProcessor();
            
            HttpHost host = new HttpHost();
            host.setServerAddress(ip);
            host.setPort(port);
            String key = "123456";
            host.setSecureKey(key);
            
            XRequest request = new XRequest();
            Card card = new Card();
            card.setCardNumber(cardNum);
            card.setCardExpiration(expMM + "" + expYY);
            card.setCardPin(cardPin);
            request.setCard(card);
            
            request.setChannelId("08");
            request.setTransCode(TransCode.BALANCE);
            request.setReference(reference);
            request.setOtherReference("");
            
            log.info(" CPAY service locaton (balanceCheck) >>>>>> " + host.getServerAddress());
            log.info(" CPAY service port (balanceCheck) >>>>>> " + host.getPort());
            XResponse response = processor.process(host, request);
            
            String resp = "" + response.getResponse();
            log.info("resp: " + resp);
            balance = response.getBalance();
            log.info("desc: " + response.getMessage());
            log.info("bal: " + balance);
            log.info("ref:: " + response.getReference());
            log.info("cust xml:: " + response.getCustomXml());
            
            log.info(" PaymentClient (authorizePayment) >>>>>> " + resp);
            if (AppConstants.SWITH_TRANSACTION_SUCCESSFUL1.equals(resp) || AppConstants.SWITH_TRANSACTION_SUCCESSFUL2.equals(resp)
                    || AppConstants.SWITH_TRANSACTION_SUCCESSFUL3.equals(resp)) {
                
            }
        } catch (Exception ex) {
            //ex.printStackTrace(LogWriter.appClient);
            log.info(" CPAY service locaton (authorizePayment) errrrrrrooooorrrrrrr>>>>>>");
            ex.printStackTrace();
        }
        
        return balance;
    }
    
    AuditLog buildAuditLog(String auditType, String target, List<TaskObjectNameValuePair> modifications) {
        AuditLog auditLog = new AuditLog();
        auditLog.setAuditType(auditType);
        String organization;
        Bank loggedBank = getLoggedBank();
        if (loggedBank != null) {
            auditLog.setBankCode(loggedBank.getId());
            organization = loggedBank.getBankName();
        } else {
            Corporate loggedCorporate = getLoggedCorporate();
            if (loggedCorporate != null) {
                auditLog.setCorporateId(loggedCorporate.getId());
                organization = loggedCorporate.getCorporateName() + "(" + loggedCorporate.getBank().getBankName() + ")";
            } else {
                organization = "Platform";
            }
        }
        String remoteIp = "";
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        remoteIp = request.getHeader("X-Forwarded-For");
        if (remoteIp == null || (request != null && remoteIp.trim().equals(""))) {
            remoteIp = request.getRemoteAddr();
        }
        auditLog.setClientIP(remoteIp);
        auditLog.setOrganization(organization);
        auditLog.setSubject(clazz.getSimpleName());
        auditLog.setUserRole(getLoggedUser() == null ? "" : getLoggedUser().getRole().getRoleName());
        auditLog.setUsername(getLoggedUser() == null ? target : getLoggedUser().getUsername());
        auditLog.setNameValuePairs(modifications);
        auditLog.setTarget(target);
        return auditLog;
    }

    /**
     * @return the tokenEnabled
     */
    public boolean isTokenEnabled() {
        User user = getLoggedUser();
        if (user instanceof CorporateUser) {
            Boolean enableESA = ((CorporateUser) user).getCorporate().getEnableESA();
            if (enableESA != null && enableESA) {
                tokenEnabled = true;
            }
        } else if (user instanceof BankUser) {
            Boolean enableESA = ((BankUser) user).getBank().getEnableESA();
            log.info("enableESA::: " + enableESA);
            if (enableESA != null && enableESA) {
                tokenEnabled = true;
            }
        } else {
            tokenEnabled = ConfigurationUtil.instance().getPlatformUserOnToken();
        }
        return tokenEnabled;
    }
    
}
