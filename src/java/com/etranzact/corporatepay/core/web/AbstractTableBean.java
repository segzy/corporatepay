/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author oluwasegun.idowu
 * @param <T>
 */
public abstract class AbstractTableBean<T extends Serializable> extends Bean {

    protected LazyDataModel<T> lazyDataModel;
    protected String statusFilter = "";
    protected Date fromDateFilter;
    protected Date toDateFilter;
    protected String dateField;
    protected int dataSize;
    protected boolean prefetch = true;
    protected boolean reportPage;
    protected boolean dateRequired = false;
    protected List<T> allList;

    protected abstract AbstractFacadeLocal<T> getFacade();

    protected abstract TableProperties getTableProperties();
    protected int rowcount = 50;

    protected Logger log;

    private Class<T> clazz;

    public AbstractTableBean(Class<T> classT) {
        this.clazz = classT;
        log = Logger.getLogger(clazz.getName());
    }

    public List<SelectItem> getStatusOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.CREATED, AppConstants.CREATED_TEXT));
        options.add(new SelectItem(AppConstants.MODIFIED, AppConstants.MODIFIED_TEXT));
        options.add(new SelectItem(AppConstants.APPROVED, AppConstants.APPROVED_TEXT));
        options.add(new SelectItem(AppConstants.MODIFIED_REJECTED, AppConstants.MODIFIED_REJECTED_TEXT));
        return options;
    }

    protected int first;

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public void refresh() {
        init();
    }

    @PostConstruct
    public void init() {
        lazyDataModel = new LazyDataModel<T>() {

            @Override
            public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    filters.putAll(getTableProperties().getAdditionalFilter());
                    try {
                        clazz.getDeclaredField("central");
//                        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//                        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
                        if (!(isBankUser() || isCorporateUser())) {
                            if (!clazz.getSimpleName().equals(User.class.getSimpleName())) {
                                filters.put("central", Boolean.TRUE);
                            }
                        }
                    } catch (NoSuchFieldException nex) {

                    }
                    pageSize = dataSize == 0 ? pageSize : dataSize;
//                    if (clazz.getSimpleName().equals(PayrollPeriod.class.getSimpleName())) {
//                        log.log(Level.INFO, "fromDateFilter:: {0}", fromDateFilter);
//                        log.log(Level.INFO, "toDateFilter:: {0}", toDateFilter);
//                        log.log(Level.INFO, "dateField:: {0}", dateField);
//                        log.log(Level.INFO, "first:: {0}", first);
//                        log.log(Level.INFO, "pageSize:: {0}", pageSize);
//
//                    }
                    List<T> pageList;
                    long allCount = 0;
//                                 log.info("dateRequired: " + dateRequired);
//                        log.info("fromDateFilter: " + fromDateFilter);
//                                           log.info("toDateFilter: " + toDateFilter);
                    if (dateRequired) {
                                
                        if (fromDateFilter == null || toDateFilter == null) {
                            pageList = new ArrayList<>();
                            allList = new ArrayList<>();
                        } else {
                            if (prefetch) {
                              
                                pageList = getFacade().findAll(first, pageSize, filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), fromDateFilter, toDateFilter, dateField, getTableProperties().getOrderFields());
                                if (reportPage) {
                                  //    log.info("fffffromDateFilter: " + fromDateFilter);
                                  //    log.info("ttttttoDateFilter: " + toDateFilter);
                                    allList = getFacade().findAll(filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), fromDateFilter, toDateFilter, dateField, getTableProperties().getOrderFields());
                                    allCount = allList.size();
                                } else {
                                    allCount = getFacade().findAllCount(filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), fromDateFilter, toDateFilter, dateField, getTableProperties().getOrderFields());
                                }
                            } else {
                                pageList = new ArrayList<>();
                                allList = new ArrayList<>();
                                allCount = 0;
                            }
                        }
                    } else {
//                        log.info("prefetch: " + prefetch);
                        if (prefetch) {
                            pageList = getFacade().findAll(first, pageSize, filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), fromDateFilter, toDateFilter, dateField, getTableProperties().getOrderFields());
//                              log.info("reportPage: " + reportPage);
                            if (reportPage) {
                                allList = getFacade().findAll(filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), fromDateFilter, toDateFilter, dateField, getTableProperties().getOrderFields());
                                allCount = allList.size();
                            } else {
                                allCount = getFacade().findAllCount(filters, getTableProperties().isActiveRecordsOnly(), getTableProperties().getFlagStatuses(), fromDateFilter, toDateFilter, dateField, getTableProperties().getOrderFields());
                            }
//                                 log.info("allList: " + allList);
//                                       log.info("allCount: " + allCount);
                        } else {
                            pageList = new ArrayList<>();
                            allList = new ArrayList<>();
                            allCount = 0;
                        }
                    }
    
                    lazyDataModel.setPageSize(rowcount);
                    lazyDataModel.setRowCount((int) allCount);
                    if (sortField != null) {
                 //       log.log(Level.INFO, "sortfiield:: {0}", sortField);
                 //       log.log(Level.INFO, "sortorder:: {0}", sortOrder);
                        Collections.sort(pageList, new LazySorter(sortField, sortOrder));
                    }
                    return pageList;
                } catch (Exception ex) {
                    Logger.getLogger(AbstractTableBean.class.getName()).log(Level.SEVERE, null, ex);
                    return new ArrayList<>();
                }
            }

            @Override
            public T getRowData(String rowKey) {
                return getTableRowData(rowKey);
            }

            @Override
            public Object getRowKey(T t) {
                return getTableKey(t);
            }
        };
    }

    public abstract T getTableRowData(String rowKey);

    public abstract Object getTableKey(T t);

    /**
     * @return the lazyDataModel
     */
    public LazyDataModel<T> getLazyDataModel() {

        return lazyDataModel;
    }

    /**
     * @param lazyDataModel the lazyDataModel to set
     */
    public void setLazyDataModel(LazyDataModel<T> lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }

    /**
     * @return the rowcount
     */
    public int getRowcount() {
        return rowcount;
    }

    /**
     * @param rowcount the rowcount to set
     */
    public void setRowcount(int rowcount) {
        this.rowcount = rowcount;
    }

    /**
     * @return the statusFilter
     */
    public String getStatusFilter() {
        return statusFilter;
    }

    /**
     * @param statusFilter the statusFilter to set
     */
    public void setStatusFilter(String statusFilter) {
        this.statusFilter = statusFilter;
    }

    /**
     * @return the fromDateFilter
     */
    public Date getFromDateFilter() {
        return fromDateFilter;
    }

    /**
     * @param fromDateFilter the fromDateFilter to set
     */
    public void setFromDateFilter(Date fromDateFilter) {
        this.fromDateFilter = fromDateFilter;
    }

    /**
     * @return the toDateFilter
     */
    public Date getToDateFilter() {
        return toDateFilter;
    }

    /**
     * @param toDateFilter the toDateFilter to set
     */
    public void setToDateFilter(Date toDateFilter) {
        this.toDateFilter = toDateFilter;
    }

    /**
     * @return the allList
     */
    public List<T> getAllList() {
        return allList;
    }

    /**
     * @param prefetch the prefetch to set
     */
    public void setPrefetch(boolean prefetch) {
        this.prefetch = prefetch;
    }

    protected static class TableProperties {

        private boolean activeRecordsOnly;
        private String[] flagStatuses = new String[]{AppConstants.CREATED, AppConstants.MODIFIED,
            AppConstants.APPROVED, AppConstants.MODIFIED_REJECTED, AppConstants.CREATED_IN_APPROVAL,
            AppConstants.PROCESSING, AppConstants.PROCESSED_OK,
            AppConstants.PROCESSED_WITH_ERRORS, AppConstants.PAYROLL_APPROVED,
            AppConstants.PAYROLL_GENERATED, AppConstants.PAYROLL_IN_APPROVAL,
            AppConstants.PAYROLL_IN_PROGRESS, AppConstants.PAYROLL_ROLLEDBACK,
            AppConstants.CREATED_NOT_SETUP, AppConstants.SET_UP_IN_PROGRESS,
            AppConstants.SETUP_COMPLETED,
            AppConstants.PAYMENT_HELD, AppConstants.PAYMENT_CLOSE};
        private Map<String, Object> additionalFilter = new HashMap<>();
               private String[] orderFields = new String[0];

        public TableProperties() {
        }

        /**
         * @return the activeRecordsOnly
         */
        public boolean isActiveRecordsOnly() {
            return activeRecordsOnly;
        }

        /**
         * @param activeRecordsOnly the activeRecordsOnly to set
         */
        public void setActiveRecordsOnly(boolean activeRecordsOnly) {
            this.activeRecordsOnly = activeRecordsOnly;
        }

        /**
         * @return the flagStatuses
         */
        public String[] getFlagStatuses() {
            return flagStatuses;
        }

        /**
         * @param flagStatuses the flagStatuses to set
         */
        public void setFlagStatuses(String[] flagStatuses) {
            this.flagStatuses = flagStatuses;
        }

        /**
         * @return the additionalFilter
         */
        public Map<String, Object> getAdditionalFilter() {
            return additionalFilter;
        }

        /**
         * @param additionalFilter the additionalFilter to set
         */
        public void setAdditionalFilter(Map<String, Object> additionalFilter) {
            this.additionalFilter = additionalFilter;
        }

        /**
         * @return the orderFields
         */
        public String[] getOrderFields() {
            return orderFields;
        }

        /**
         * @param orderFields the orderFields to set
         */
        public void setOrderFields(String[] orderFields) {
            this.orderFields = orderFields;
        }
        
        

    }

    
}
