/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.WorkflowTaskFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "taskTableBean")
@SessionScoped
public class TaskTableBean {

    @EJB
    private WorkflowTaskFacadeLocal workflowTaskFacadeLocal;
    private LazyDataModel lazyDataModel;
    private String taskStatefilter;
    private String taskTypefilter;
    private String focusfilter;
    private Date fromDate;
    private Date toDate;

    private int rowcount = 50;

    private Logger log = Logger.getLogger(TaskTableBean.class.getName());

    public TaskTableBean() {
    }

    public List<SelectItem> getStateOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.OPEN, AppConstants.OPEN_TEXT));
        options.add(new SelectItem(AppConstants.APPROVED, "Approved"));
        options.add(new SelectItem(AppConstants.REJECTED, "Rejected"));
        options.add(new SelectItem(AppConstants.KEPT_IN_VIEW, AppConstants.KEPT_IN_VIEW_TEXT));
        return options;
    }

    public List<SelectItem> getTaskTypeOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.GROUP_TASK_TYPE, AppConstants.GROUP_TASK_TYPE));
        options.add(new SelectItem(AppConstants.USER_TASK_TYPE, AppConstants.USER_TASK_TYPE));
        return options;
    }

    @PostConstruct
    public void init() {
        setLazyDataModel(new LazyDataModel<ApprovalTask>() {
            @Override
            public List<ApprovalTask> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                         List<ApprovalTask> pageList;
                                    List<ApprovalTask> allList;
                    Calendar now = Calendar.getInstance();
                    now.set(Calendar.HOUR_OF_DAY, 0);
                    now.set(Calendar.MINUTE, 0);
                    now.set(Calendar.SECOND, 0);
                     now.set(Calendar.MILLISECOND, 0);
                     taskStatefilter = taskStatefilter==null?AppConstants.OPEN:taskStatefilter;
                    if(fromDate==null && toDate==null) {
                        log.info("no date filter...");
                    pageList = getFacade().findUserApprovalTasks(first, pageSize,taskStatefilter,focusfilter);
                    allList = getFacade().findUserApprovalTasks(taskStatefilter,focusfilter);
                  
                    } else if(fromDate==null && toDate!=null) {
                    pageList = getFacade().findUserApprovalTasks(first, pageSize,taskStatefilter,focusfilter,now.getTime(),toDate);
                    allList = getFacade().findUserApprovalTasks(taskStatefilter,focusfilter,now.getTime(),toDate); 
                    } else if(fromDate!=null && toDate==null) {
                    pageList = getFacade().findUserApprovalTasks(first, pageSize,taskStatefilter,focusfilter,fromDate,now.getTime());
                    allList = getFacade().findUserApprovalTasks(taskStatefilter,focusfilter,fromDate,now.getTime());  
                    } else {
                    pageList = getFacade().findUserApprovalTasks(first, pageSize,taskStatefilter,focusfilter,fromDate,toDate);
                    allList = getFacade().findUserApprovalTasks(taskStatefilter,focusfilter,fromDate,toDate);  
                    }
                      log.log(Level.INFO, "task pageList.. {0}", pageList.size());
                        log.log(Level.INFO, "task allList.. {0}", allList.size());
                    getLazyDataModel().setRowCount(allList.size());
                    getLazyDataModel().setPageSize(getRowCount());
                    if (sortField != null) {
                        Collections.sort(pageList, new LazySorter(sortField, sortOrder));
                    }
                    return pageList;
                } catch (Exception ex) {
                    Logger.getLogger(AbstractTableBean.class.getName()).log(Level.SEVERE, null, ex);
                    return new ArrayList<>();
                }
            }

            @Override
            public ApprovalTask getRowData(String rowKey) {
                List<ApprovalTask> wrappedData = (List<ApprovalTask>) getLazyDataModel().getWrappedData();
                for (ApprovalTask t : wrappedData) {
                    if (Long.parseLong(rowKey) == t.getId()) {
                        return t;
                    }
                }
                return null;
            }

            @Override
            public Object getRowKey(ApprovalTask t) {
                return t.getId();
            }
        });
    }

    protected WorkflowTaskFacadeLocal getFacade() {
        return workflowTaskFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the lazyDataModel
     */
    public LazyDataModel getLazyDataModel() {
        return lazyDataModel;
    }

    /**
     * @param lazyDataModel the lazyDataModel to set
     */
    public void setLazyDataModel(LazyDataModel lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }

    /**
     * @return the rowcount
     */
    public int getRowcount() {
        return rowcount;
    }

    /**
     * @param rowcount the rowcount to set
     */
    public void setRowcount(int rowcount) {
        this.rowcount = rowcount;
    }

    /**
     * @return the taskStatefilter
     */
    public String getTaskStatefilter() {
        return taskStatefilter;
    }

    /**
     * @param taskStatefilter the taskStatefilter to set
     */
    public void setTaskStatefilter(String taskStatefilter) {
        this.taskStatefilter = taskStatefilter;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the taskTypefilter
     */
    public String getTaskTypefilter() {
        return taskTypefilter;
    }

    /**
     * @param taskTypefilter the taskTypefilter to set
     */
    public void setTaskTypefilter(String taskTypefilter) {
        this.taskTypefilter = taskTypefilter;
    }

    /**
     * @return the focusfilter
     */
    public String getFocusfilter() {
        return focusfilter;
    }

    /**
     * @param focusfilter the focusfilter to set
     */
    public void setFocusfilter(String focusfilter) {
        this.focusfilter = focusfilter;
    }

    
    
}
