/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.InMailFacadeLocal;
import com.etranzact.corporatepay.core.facade.UserFacadeLocal;
import com.etranzact.corporatepay.core.facade.WorkflowTaskFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.InMail;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "mailTableBean")
@SessionScoped
public class MailTableBean extends Bean {

    @EJB
    private InMailFacadeLocal InMailFacadeFacadeLocal;
  
    @EJB
    private UserFacadeLocal userFacadeLocal;
    private LazyDataModel lazyDataModel;
    private String taskStatefilter;
    private Date fromDate;
    private Date toDate;
    private InMail selectedItem;
    private boolean showMessage;

    private int rowcount = 50;

    private Logger log = Logger.getLogger(MailTableBean.class.getName());

    public MailTableBean() {
    }

    public List<SelectItem> getStateOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.OPEN, AppConstants.OPEN_TEXT));
        options.add(new SelectItem(AppConstants.CLOSED, AppConstants.CLOSED_TEXT));
        return options;
    }

    @PostConstruct
    public void init() {
        setLazyDataModel(new LazyDataModel<InMail>() {
            @Override
            public List<InMail> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    List<InMail> pageList;
                    List<InMail> allList;
                    Calendar now = Calendar.getInstance();
                    now.set(Calendar.HOUR_OF_DAY, 0);
                    now.set(Calendar.MINUTE, 0);
                    now.set(Calendar.SECOND, 0);
                    now.set(Calendar.MILLISECOND, 0);
                    Map<String, Object> map = new HashMap<>();
                    map.put("i.targetObjectId='" + String.valueOf(getLoggedUser().getId()) + "'", null);
                    if(taskStatefilter!=null && !taskStatefilter.isEmpty()) {
                        map.put("status", taskStatefilter);
                    }
                     if (fromDate == null && toDate != null) {
                        fromDate = new Date();
                    } else if (fromDate != null && toDate == null) {
                        toDate = new Date();
                    }
                    if (fromDate !=null && toDate!=null) {
                        Calendar from = Calendar.getInstance();
                        from.setTime(fromDate);
                        String fromDateStr = from.get(Calendar.YEAR) + "-" + (from.get(Calendar.MONTH) + 1) + "-" + from.get(Calendar.DAY_OF_MONTH);
                        Calendar to = Calendar.getInstance();
                        to.setTime(toDate);
                        String toDateStr = to.get(Calendar.YEAR) + "-" + (to.get(Calendar.MONTH) + 1) + "-" + to.get(Calendar.DAY_OF_MONTH);
                        map.put("i.dateCreated >= '" + java.sql.Date.valueOf(fromDateStr) + "' and i.dateCreated <= '" + java.sql.Date.valueOf(toDateStr) + "'", null);
                    }
                    pageList = getFacade().findAll(first, pageSize, map, false, new String[]{"id"});
                    allList = getFacade().findAll(map, false, new String[]{"id"});
                    getLazyDataModel().setRowCount(allList.size());
                    getLazyDataModel().setPageSize(getRowCount());
                    if (sortField != null) {
                        Collections.sort(pageList, new LazySorter(sortField, sortOrder));
                    }
                    return pageList;
                } catch (Exception ex) {
                    Logger.getLogger(AbstractTableBean.class.getName()).log(Level.SEVERE, null, ex);
                    return new ArrayList<>();
                }
            }

            @Override
            public InMail getRowData(String rowKey) {
                List<InMail> wrappedData = (List<InMail>) getLazyDataModel().getWrappedData();
                for (InMail t : wrappedData) {
                    if (Long.parseLong(rowKey) == t.getId()) {
                        return t;
                    }
                }
                return null;
            }

            @Override
            public Object getRowKey(InMail t) {
                return t.getId();
            }
        });
    }

    protected InMailFacadeLocal getFacade() {
        return InMailFacadeFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the lazyDataModel
     */
    public LazyDataModel getLazyDataModel() {
        return lazyDataModel;
    }

    /**
     * @param lazyDataModel the lazyDataModel to set
     */
    public void setLazyDataModel(LazyDataModel lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }

    /**
     * @return the rowcount
     */
    public int getRowcount() {
        return rowcount;
    }

    /**
     * @param rowcount the rowcount to set
     */
    public void setRowcount(int rowcount) {
        this.rowcount = rowcount;
    }

    /**
     * @return the taskStatefilter
     */
    public String getTaskStatefilter() {
        return taskStatefilter;
    }

    /**
     * @param taskStatefilter the taskStatefilter to set
     */
    public void setTaskStatefilter(String taskStatefilter) {
        this.taskStatefilter = taskStatefilter;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the selectedItem
     */
    public InMail getSelectedItem() {
        return selectedItem;
    }

    /**
     * @param selectedItem the selectedItem to set
     */
    public void setSelectedItem(InMail selectedItem) {
        this.selectedItem = selectedItem;
    }

    public void onRowSelect(SelectEvent selectEvent) {
        try {
            setSelectedItem((InMail) selectEvent.getObject());
            selectedItem.setStatus(AppConstants.CLOSED);
              if (User.class.getSimpleName().equals(selectedItem.getClazz())) {
                    User us = userFacadeLocal.find(Long.valueOf(selectedItem.getTargetObjectId()));
                    Object[] params = {us.getUsername(), us.getRole().getRoleName()};
                    String property = ConfigurationUtil.instance().getMailProperties().getProperty(AppConstants.NEW_USER_MESSAGE_CODE);
                    property = property == null ? "" : property;
                    String format = MessageFormat.format(property, params);
                    selectedItem.setMessage(format);
                }
                  UserBean userBean = (UserBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userBean");
             Map<String, Object> map = new HashMap<>();
                    map.put("i.targetObjectId='" + String.valueOf(getLoggedUser().getId()) + "'", null);
                      map.put("status", AppConstants.OPEN);
                  userBean.setInMails(getFacade().findAll(map, false));
            getFacade().create(selectedItem);
            setShowMessage(true);
        } catch (Exception ex) {
            Logger.getLogger(MailTableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the showMessage
     */
    public boolean isShowMessage() {
        return showMessage;
    }

    /**
     * @param showMessage the showMessage to set
     */
    public void setShowMessage(boolean showMessage) {
        this.showMessage = showMessage;
    }

}
