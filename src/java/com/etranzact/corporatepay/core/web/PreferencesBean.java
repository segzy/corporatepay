/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.model.Preferences;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "preferenceBean")
@SessionScoped
public class PreferencesBean extends AbstractBean<Preferences> {

    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;

    @Override
    protected PreferenceFacadeLocal getFacade() {
        return preferenceFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void init() {
        try {
            selectedItem = preferenceFacadeLocal.getCorporatePreferences();
        } catch (Exception ex) {
            FacesMessage m = new FacesMessage();
            Logger.getLogger(PreferencesBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public PreferencesBean() {

        super(Preferences.class);
    }


    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            if(isCorporateUser()) {
            if (selectedItem.getId() == null) {
                selectedItem.setCorporate(getLoggedCorporate());
                selectedItem.setBank(null);
            }
            }else if(isBankUser()) {
                selectedItem.setCorporate(null);
                selectedItem.setBank(getLoggedBank());
            }
            preferenceFacadeLocal.create(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Preference Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(PreferencesBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

}
