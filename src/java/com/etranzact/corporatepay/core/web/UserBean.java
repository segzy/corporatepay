/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.core.facade.InMailFacade;
import com.etranzact.corporatepay.core.facade.InMailFacadeLocal;
import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.core.facade.UserFacadeLocal;
import com.etranzact.corporatepay.core.facade.WorkflowTaskFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.AuditLog;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporatePayService;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.InMail;
import com.etranzact.corporatepay.model.Menu;
import com.etranzact.corporatepay.model.MenuItem;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payment.web.TransactionTableBean;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import com.etranzact.corporatepay.util.DESEncrypter;
import com.etranzact.corporatepay.util.MailUtil;
import com.etz.mail.util.MailSender;
import com.etz.security.util.Cryptographer;
import com.google.gson.Gson;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialResponseWriter;
import javax.faces.context.PartialViewContext;
import javax.faces.context.ResponseWriter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.weld.bean.builtin.FacadeInjectionPoint;
import org.primefaces.context.RequestContext;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean extends AbstractBean<User> {

    private User loggedInUser;
    private List<ApprovalTask> approvalTasks;
    private List<InMail> inMails;
    private String currPassword;
    private String newPassword;
    private String repeatPassword;
    private int pollCount;
    public static String POLL_COUNT_PARAM = "pollCount";
    private String helpMsg = "";
    private static final Cryptographer crpt = new Cryptographer();
    private List<Bank> banks;
    private List<Corporate> corporates;

    public UserBean() {
        super(User.class);
    }

    @EJB
    private UserFacadeLocal userFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;
    @EJB
    private WorkflowTaskFacadeLocal workflowTaskFacadeLocal;
    @EJB
    private InMailFacadeLocal inMailFacadeLocal;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankFacadeLocal;
    @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;
    private User user;
    private List<Menu> userMenus;
    private String email;
    private String username;

    @Override
    protected UserFacadeLocal getFacade() {
        return userFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void changePassword() {
        FacesMessage m = new FacesMessage();
        try {
            getFacade().validateToken(loggedInUser, token);
            String doMd5Hash = crpt.doMd5Hash(StringUtils.join(new String[]{StringUtils.trim(loggedInUser.getUsername()), StringUtils.trim(currPassword)}));
            if (!loggedInUser.getPasswordHash().equals(doMd5Hash)) {
                throw new Exception("Current Password is incorrect");
            }
            if (!newPassword.equals(repeatPassword)) {
                throw new Exception("New Password does not match ");
            }
            User u = userFacadeLocal.find(loggedInUser.getId());
            AppUtil.checkPasswordPolicy(newPassword);
            u.setPassword(newPassword);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + ConfigurationUtil.instance().getPassowrdExpirationPeriod());
            u.setPasswordDateExpired(cal.getTime());
            userFacadeLocal.create(u);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            if (isBankUser()) {
                u.setBankCode(getLoggedBank().getId());
            } else if (isCorporateUser()) {
                u.setBankCode(getLoggedCorporate().getBank().getId());
            }
            externalContext.getSessionMap().put(AppConstants.LOGIN_USER, u);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Password Changed Succesfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            externalContext.redirect(externalContext.getRequestContextPath() + AppConstants.DASHBOARD_PAGE);
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
            token = null;
        }

    }

    public void initiateForgotPassword() {
        FacesMessage m = new FacesMessage();
        try {
            username = user.getUsername();
            if (username == null || username.isEmpty()) {
                m.setSeverity(FacesMessage.SEVERITY_ERROR);
                m.setSummary("Provide your username");
                FacesContext.getCurrentInstance().addMessage("loginGrowl", m);
            } else {
                user = userSetupFacadeLocal.findGlobalUser(username);
                if (user == null) {
                    m.setSeverity(FacesMessage.SEVERITY_ERROR);
                    m.setSummary("User not found");
                    FacesContext.getCurrentInstance().addMessage("loginGrowl", m);
                    return;
                }
                email = user.getEmail();
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('forgetpass').show();");
            }
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary("Unexpected Error");
            FacesContext.getCurrentInstance().addMessage("loginGrowl", m);
        }
    }

    public void cancelPasswordReset() {
        username = null;
        email = null;
        user = new User();
    }

    public void requestPasswordReset() {
        FacesMessage m = new FacesMessage();
        try {
            user.setResetRequestTime(new Date());
            user.setApprovalGroups(null);
            user.setRole(null);
            String link = new Gson().toJson(user);
            link = StringEscapeUtils.escapeJson(link);
            link = DESEncrypter.instance().encrypt(link);
            MailUtil.getInstance().sendPasswordResetRequest(new String[]{email}, username, link);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Password Request Initiated Successfully, Check your email");
            FacesContext.getCurrentInstance().addMessage("loginGrowl", m);
        } catch (Exception e) {
            cancelPasswordReset();
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, e);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage("loginGrowl", m);
        }
    }

    public void unlockUser(User user) {
        FacesMessage m = new FacesMessage();
        try {
            user.setPasswordMissed(0);
            userFacadeLocal.create(user);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("User unlocked successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception e) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, e);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void login() {
        FacesMessage m = new FacesMessage();
        try {

            //           log.info("..... login started....");
            setLoggedInUser(getFacade().login(user.getUsername(), user.getPassword()));

//            log.log(Level.INFO, "..... login user ....{0}", getLoggedInUser());
//            log.log(Level.INFO, "..... bankuser? ....{0}", getLoggedInUser() instanceof BankUser);
//            log.log(Level.INFO, "..... corporateuser? ....{0}", getLoggedInUser() instanceof CorporateUser);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            HttpSession session = (HttpSession) externalContext.getSession(false);
            if (getLoggedInUser() instanceof BankUser) {
                BankUser bankUser = ((BankUser) getLoggedInUser());
                getLoggedInUser().setBankCode(bankUser.getBank().getId());
                if (!bankUser.getBank().getActive()) {
                    throw new Exception("Bank is inactive");
                }
                if (!bankUser.getBank().getFlagStatus().equals(AppConstants.APPROVED)) {
                    throw new Exception("Bank has not being approved");
                }
                session.setMaxInactiveInterval(bankUser.getBank().getTimeOutPreference() * 60);
            } else if (getLoggedInUser() instanceof CorporateUser) {
                CorporateUser corporateUser = ((CorporateUser) getLoggedInUser());
                getLoggedInUser().setBankCode(corporateUser.getCorporate().getBank().getId());
                if (!corporateUser.getCorporate().getActive()) {
                    throw new Exception("Corporate is inactive");
                }
                if (!corporateUser.getCorporate().getFlagStatus().equals(AppConstants.APPROVED)) {
                    throw new Exception("Corporate has not being approved");
                }
                int timeoutinmin = corporateUser.getCorporate().getBank().getTimeOutPreference();
                Preferences corporatePreferences = preferenceFacadeLocal.getCorporatePreferences(corporateUser.getCorporate());
                if (corporatePreferences != null && corporatePreferences.getSessionTime() != null) {
                    timeoutinmin = corporatePreferences.getSessionTime();
                }
                session.setMaxInactiveInterval(timeoutinmin * 60);
            }
            getLoggedInUser().setPendingToken(loggedInUser.getEnableESA());
            externalContext.getSessionMap().put(AppConstants.LOGIN_USER, getLoggedInUser());
            List<User> allLoggedInUsers = (List<User>) externalContext.getApplicationMap().get(AppConstants.ALL_LOGIN_USER);
            if (allLoggedInUsers == null) {
                allLoggedInUsers = new ArrayList<>();
            }
            if (allLoggedInUsers.contains(loggedInUser)) {
                throw new Exception("You are already logged on another instance, please logout or continue from that instance");
            } else {
                allLoggedInUsers.add(loggedInUser);
            }
            externalContext.getApplicationMap().put(AppConstants.ALL_LOGIN_USER, allLoggedInUsers);
//            log.log(Level.INFO, "responseee :::: {0}", externalContext.getResponse());
//            log.log(Level.INFO, "contaxt path :::: {0}", externalContext.getRequestContextPath());
            String preferredPages = getPreferredPages();
            auditLogFacadeLocal.create(buildAuditLog(AppConstants.AUDIT_TYPE_LOGIN, loggedInUser.getUsername(), null));
            if (loggedInUser.getEnableESA() != null && loggedInUser.getEnableESA()) {
                log.info("transiting to token page..................");
                log.info("transiting to token page.....................");
                externalContext.redirect(externalContext.getRequestContextPath() + AppConstants.TOKEN_LOGIN_PAGE);
            } else {
                externalContext.redirect(externalContext.getRequestContextPath()
                        + (getLoggedInUser().getChangePassword() ? AppConstants.PASSWORD_CHANGE_PAGE : preferredPages));
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage("loginGrowl", m);
            user.setPassword(null);
            try {
                auditLogFacadeLocal.create(buildAuditLog(AppConstants.AUDIT_TYPE_LOGIN_ATTEMPT, user.getUsername(), null));
            } catch (Exception ex1) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public void validateToken() {
        FacesMessage m = new FacesMessage();
        try {

            getFacade().validateToken(user, token);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            getLoggedInUser().setPendingToken(Boolean.FALSE);
            externalContext.getSessionMap().put(AppConstants.LOGIN_USER, getLoggedInUser());
            String preferredPages = getPreferredPages();
            externalContext.redirect(externalContext.getRequestContextPath() + (getLoggedInUser().getChangePassword() ? AppConstants.PASSWORD_CHANGE_PAGE : preferredPages));
        } catch (Exception ex) {
            log.log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage("loginGrowl", m);
            user.setPassword(null);
        } finally {
            token = null;
        }
    }

    public String logout() {
        try {
            log.log(Level.INFO, "..... logging out... {0}", getLoggedInUser());
            loggedInUser = getLoggedInUser();
            if (loggedInUser != null) {
                User uu = userFacadeLocal.find(loggedInUser.getId());
                uu.setLastAccessTime(loggedInUser.getLoginTime());
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                List<User> allLoggedInUsers = (List<User>) externalContext.getApplicationMap().get(AppConstants.ALL_LOGIN_USER);
                allLoggedInUsers.remove(uu);
                externalContext.getApplicationMap().put(AppConstants.ALL_LOGIN_USER, allLoggedInUsers);
                userFacadeLocal.finalizeLogout(uu);
                auditLogFacadeLocal.create(buildAuditLog(AppConstants.AUDIT_TYPE_LOGOUT, loggedInUser.getUsername(), null));
                ((HttpSession) externalContext.getSession(false)).invalidate();
            }
            return "/login?faces-redirect=true";
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public String logout(User loggedUser) {
        try {
            log.log(Level.INFO, "..... logging out... {0}", loggedUser);
            if (loggedUser != null) {
                User uu = userFacadeLocal.find(loggedUser.getId());
                uu.setLastAccessTime(loggedUser.getLoginTime());
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                List<User> allLoggedInUsers = (List<User>) externalContext.getApplicationMap().get(AppConstants.ALL_LOGIN_USER);
                allLoggedInUsers.remove(uu);
                externalContext.getApplicationMap().put(AppConstants.ALL_LOGIN_USER, allLoggedInUsers);
                userFacadeLocal.finalizeLogout(uu);
                auditLogFacadeLocal.create(buildAuditLog(AppConstants.AUDIT_TYPE_LOGOUT, loggedUser.getUsername(), null));
                ((HttpSession) externalContext.getSession(false)).invalidate();
            }
            return "/login?faces-redirect=true";
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void handleHelpChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        String val = (String) ((UISelectOne) e.getComponent()).getValue();
        Set<Map.Entry<Object, Object>> entrySet = ConfigurationUtil.instance().getMainProperties().entrySet();
        String configHelpPrefix = "HELP";
        for (Map.Entry<Object, Object> configEntry : entrySet) {
            if ((configHelpPrefix + "_" + val).equals(configEntry.getKey())) {
                helpMsg = (String) configEntry.getValue();
            }
        }
    }

    public void navigate() {

    }

    /**
     * @return the user
     */
    public User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the loggedInUser
     */
    public User getLoggedInUser() {
        return loggedInUser;
    }

    /**
     * @param loggedInUser the loggedInUser to set
     */
    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    /**
     * @return the userMenus
     */
    public List<Menu> getUserMenus() {
        if (loggedInUser == null /**
                 * || (loggedInUser.getRole().getId().equals("0002") &&
                 * (supportBank==null && supportCorporate==null))*
                 */
                ) {
            return new ArrayList<>();
        }
        Preferences preference = null;
        try {
            if (getLoggedCorporate() != null) {
                preference = preferenceFacadeLocal.getCorporatePreferences(getLoggedCorporate());
            }
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        Set<MenuItem> resources = loggedInUser.getRole().getResources();
        userMenus = new ArrayList<>();
        for (MenuItem menuItem : resources) {
            if (preference != null) {
                if (preference.getPayrollMode().equals(AppConstants.PAYROLL_LITE) && menuItem.getId().equals("024")) {
                    continue;
                }
            }
            Menu menu = menuItem.getMenu();
            if (!menu.getItems().contains(menuItem)) {
                menu.getItems().add(menuItem);
            }
            if (!userMenus.contains(menu)) {
                List<CorporatePayService> corporatePayServices = new ArrayList<>();
                if (getLoggedCorporate() != null) {
                    corporatePayServices = getLoggedCorporate().getCorporatePayServices();
                }
                if (menu.getCorporatePayService().getDefaultService() || corporatePayServices.contains(menu.getCorporatePayService())) {
                    userMenus.add(menu);
                }

            }
        }
        Comparator<Menu> menuComparable = new Comparator<Menu>() {

            @Override
            public int compare(Menu o1, Menu o2) {
                return o1.getPosition() - o2.getPosition();
            }
        };
        Comparator<MenuItem> menuItemComparable = new Comparator<MenuItem>() {

            @Override
            public int compare(MenuItem o1, MenuItem o2) {
                return o1.getPosition() - o2.getPosition();
            }
        };
        Collections.sort(userMenus, menuComparable);
        for (Menu menu : userMenus) {
            Collections.sort(menu.getItems(), menuItemComparable);
        }
        return userMenus;
    }

    /**
     * @param userMenus the userMenus to set
     */
    public void setUserMenus(List<Menu> userMenus) {
        this.userMenus = userMenus;
    }

    /**
     * @return the approvalTasks
     */
    public List<ApprovalTask> getApprovalTasks() {
        if (approvalTasks == null || approvalTasks.isEmpty()) {

            try {
                approvalTasks = workflowTaskFacadeLocal.findUserApprovalTasks();
            } catch (Exception ex) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
                return new ArrayList<>();
            }
        }
        return approvalTasks;
    }

    public void pollApprovalTasks() {
        try {
            //log.info("polling task.....");
            approvalTasks = workflowTaskFacadeLocal.findUserApprovalTasks();
            //log.log(Level.INFO, "count .....{0}", approvalTasks.size());
            setPollCount(getPollCount() + 1);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(POLL_COUNT_PARAM, getPollCount());
        } catch (Exception ex) {
            //Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void pollInMail() {
        try {
            Map<String, Object> m = new HashMap<>();
            m.put("i.status is null", null);
            m.put("i.targetObjectId='" + String.valueOf(getLoggedUser().getId()) + "'", null);
            inMails = inMailFacadeLocal.findAll(m, false);
            for (InMail mail : inMails) {
                if (User.class.getSimpleName().equals(mail.getClazz())) {
                    User us = userFacadeLocal.find(Long.valueOf(mail.getTargetObjectId()));
                    Object[] params = {us.getUsername(), us.getRole().getRoleName()};
                    String property = ConfigurationUtil.instance().getMailProperties().getProperty(AppConstants.NEW_USER_MESSAGE_CODE);
                    property = property == null ? "" : property;
                    String format = MessageFormat.format(property, params);
                    mail.setMessage(format);
                }
            }
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put(POLL_COUNT_PARAM, getPollCount());
        } catch (Exception ex) {
            //Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @param approvalTasks the approvalTasks to set
     */
    public void setApprovalTasks(List<ApprovalTask> approvalTasks) {
        this.approvalTasks = approvalTasks;
    }

    /**
     * @return the currPassword
     */
    public String getCurrPassword() {
        return currPassword;
    }

    /**
     * @param currPassword the currPassword to set
     */
    public void setCurrPassword(String currPassword) {
        this.currPassword = currPassword;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the repeatPassword
     */
    public String getRepeatPassword() {
        return repeatPassword;
    }

    /**
     * @param repeatPassword the repeatPassword to set
     */
    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    /**
     * @return the inMails
     */
    public List<InMail> getInMails() {
        try {
            Map<String, Object> m = new HashMap<>();
            m.put("i.status is null", null);
            m.put("i.targetObjectId='" + String.valueOf(getLoggedUser().getId()) + "'", null);
            inMails = (inMails == null || inMails.isEmpty()) ? inMailFacadeLocal.findAll(m, false) : inMails;
            for (InMail mail : inMails) {
                if (User.class.getSimpleName().equals(mail.getClazz())) {
                    User us = userFacadeLocal.find(Long.valueOf(mail.getTargetObjectId()));
                    Object[] params = {us.getUsername(), us.getRole().getRoleName()};
                    String property = ConfigurationUtil.instance().getMailProperties().getProperty(AppConstants.NEW_USER_MESSAGE_CODE);
                    property = property == null ? "" : property;
                    String format = MessageFormat.format(property, params);
                    mail.setMessage(format);
                }
            }
            return inMails;
        } catch (Exception ex) {
            //Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    /**
     * @param inMails the inMails to set
     */
    public void setInMails(List<InMail> inMails) {
        this.inMails = inMails;
    }

    /**
     * @return the pollCount
     */
    public int getPollCount() {
        return pollCount;
    }

    /**
     * @param pollCount the pollCount to set
     */
    public void setPollCount(int pollCount) {
        this.pollCount = pollCount;
    }

    /**
     * @return the helpMsg
     */
    public String getHelpMsg() {
        return helpMsg;
    }

    /**
     * @param helpMsg the helpMsg to set
     */
    public void setHelpMsg(String helpMsg) {
        this.helpMsg = helpMsg;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null) {

            try {
                banks = bankFacadeLocal.findAll(new HashMap(), true, "bankName");
            } catch (Exception ex) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {

        try {
            Bank loggedBank = getLoggedBank();
            if (supportBank != null && loggedBank != null) {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                if (supportBank != null) {
                    map.put("bank", supportBank);
                } else if (getLoggedBank() != null) {
                    map.put("bank", loggedBank);
                }
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
            } else {
                corporates = new ArrayList<>();
            }
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            corporates = new ArrayList<>();
        }

        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }

    public void handleBankChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        try {
            supportBank = (Bank) ((UISelectOne) e.getComponent()).getValue();
            if (supportBank == null) {
                FacesMessage m = new FacesMessage();
                m.setSeverity(FacesMessage.SEVERITY_INFO);
                m.setSummary("You are now in the General Support Space");
                FacesContext.getCurrentInstance().addMessage(null, m);
                supportCorporate = null;
                corporates = new ArrayList<>();

            } else {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("c.bank.id='" + supportBank.getId() + "'", null);
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
                supportCorporate = null;
                FacesMessage m = new FacesMessage();
                m.setSeverity(FacesMessage.SEVERITY_INFO);
                m.setSummary("You are now in the Support Space of " + supportBank.getBankName() + ", Select All Banks to leave this Space");
                FacesContext.getCurrentInstance().addMessage(null, m);
            }
            TransactionTableBean transactionTableBean = (TransactionTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionTableBean");
            transactionTableBean.setSrcBankFilter(supportBank);
            transactionTableBean.setCorporateFilter(null);
            transactionTableBean.init();
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void handleCorporateChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        try {
            supportCorporate = (Corporate) ((UISelectOne) e.getComponent()).getValue();
            if (supportCorporate == null) {
                FacesMessage m = new FacesMessage();
                m.setSeverity(FacesMessage.SEVERITY_INFO);
                m.setSummary("You are now in the General Support Space of " + supportBank.getBankName());
                FacesContext.getCurrentInstance().addMessage(null, m);
            } else {
                FacesMessage m = new FacesMessage();
                m.setSeverity(FacesMessage.SEVERITY_INFO);
                m.setSummary("You are now in the Support Space of " + supportCorporate.getCorporateName() + " for " + supportCorporate.getBank().getBankName() + ", Select All Corporates to leave this Space");
                FacesContext.getCurrentInstance().addMessage(null, m);
            }
            TransactionTableBean transactionTableBean = (TransactionTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionTableBean");
            transactionTableBean.setSrcBankFilter(supportBank);
            transactionTableBean.setCorporateFilter(supportCorporate);
            transactionTableBean.init();

        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private String getPreferredPages() {
        String preferredPage = AppConstants.DASHBOARD_PAGE;
        String role = getLoggedInUser().getRole().getId();
        if (null != role) {
            switch (role) {
                case "0007":
                case "0011":
                    return "/payment/paymanager";
                case "0008":
                case "0009":
                case "0012":
                case "0013":
                    return "/tasks";
            }
        }
        return preferredPage;
    }

}
