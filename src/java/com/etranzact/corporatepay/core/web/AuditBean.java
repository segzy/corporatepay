package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.AuditLog;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;

@ManagedBean(name = "auditBean")
@SessionScoped
public class AuditBean extends AbstractBean<AuditLog> {

    @EJB
    private AuditLogFacadeLocal auditFacadeLocal;
  


    @Override
    protected AuditLogFacadeLocal getFacade() {
        return auditFacadeLocal;
    }

    public AuditBean() {
        super(AuditLog.class);
    }

     public void onDialogClose(CloseEvent event) {
       selectedItem = null;
    }
     
         /**
     * @param selectedItem the selectedItem to set
     */
    @Override
    public void setSelectedItem(AuditLog selectedItem) {
        try {
            super.setSelectedItem(selectedItem);
            
            log.info("selected " + this.selectedItem);
            if(this.selectedItem!=null) {
            this.selectedItem = auditFacadeLocal.find(selectedItem.getId());
            log.info("selected " + this.selectedItem.getAuditType());
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('revisionDatails').show();");
            }
        } catch (Exception ex) {
            Logger.getLogger(AuditBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
