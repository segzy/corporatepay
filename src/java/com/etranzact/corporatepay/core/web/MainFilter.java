/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author oluwasegun.idowu
 */
public class MainFilter implements Filter{
    
       private FilterConfig filterConfig;
    private final String LOGIN_PAGE = AppConstants.LOGIN_PAGE;
        private final String TOKEN_PAGE = AppConstants.TOKEN_LOGIN_PAGE;
        private final String DASHBOARD =AppConstants.DASHBOARD_PAGE;
            private static final Logger logger = Logger.getLogger(MainFilter.class.getName());

    @Override
    public void init(FilterConfig fc) throws ServletException {
       this.filterConfig = fc; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);
        //logger.log(Level.INFO, "session... {0}", session);
        httpRequest.setCharacterEncoding("UTF-8");
         boolean loginPage = httpRequest.getRequestURI().equals(httpRequest.getContextPath() + LOGIN_PAGE);
//                  boolean dashboardPage = httpRequest.getRequestURI().equals(httpRequest.getContextPath() + DASHBOARD);
                                boolean tokenPage = httpRequest.getRequestURI().equals(httpRequest.getContextPath() + TOKEN_PAGE);
         User user = null;
         if(session !=null) {
             user = (User) session.getAttribute(AppConstants.LOGIN_USER);   
         }
        if (loginPage || httpRequest.getServletPath().contains("/javax.faces.resource") || httpRequest.getServletPath().equals("/reset")) {
            fc.doFilter(request, response);
        }  else if(user==null) {
                  if(httpRequest.getServletPath().contains("images")) {
                       fc.doFilter(request, response);
                  }
                  if(!httpResponse.isCommitted()) {
                          logger.log(Level.INFO, "redirecting to login.... ");
                          if(session!=null) {
                           UserBean userBean =(UserBean) session.getAttribute("userBean"); 
                                 User loggedUser =(User) session.getAttribute(AppConstants.LOGIN_USER);
                           userBean.logout(loggedUser);
                          }
                         httpResponse.sendRedirect(httpRequest.getContextPath() + LOGIN_PAGE);
                  }
               
        }  else {
             if(user.getPendingToken()!=null && user.getPendingToken()) {
                   logger.log(Level.INFO, "committed? ::: {0}", httpResponse.isCommitted());
                   if(!tokenPage) {
                       httpResponse.sendRedirect(httpRequest.getContextPath() + TOKEN_PAGE);
                  }
             fc.doFilter(request, response);
             } else {
                fc.doFilter(request, response);  
             }
        }
    }

    @Override
    public void destroy() {
       this.filterConfig = null; //To change body of generated methods, choose Tools | Templates.
    }
    
}
