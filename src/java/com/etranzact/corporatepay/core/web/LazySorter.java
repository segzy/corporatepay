/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtils;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Oluwasegun.Idowu
 */
public class LazySorter<T extends Serializable> implements Comparator {

    private String sortField;

    private SortOrder sortOrder;

    protected Logger log = Logger.getLogger(LazySorter.class.getName());

    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(Object t1, Object t2) {

        try {
            Object value1 = BeanUtils.getProperty(t1, sortField);  
            Object value2 = BeanUtils.getProperty(t2, sortField);
          
              int value;
              try {
                Long.parseLong(String.valueOf(value1));  
                      long result = Long.parseLong(String.valueOf(value1)) - Long.parseLong(String.valueOf(value2)); 
             value = result==0?1:result>1?1:result<1?-1:1;
           //   log.log(Level.INFO, " for long value {0}", new Object[]{value});
              } catch(NumberFormatException ex) {
                //    log.log(Level.INFO, "compare {0} to {1}", new Object[]{value1, value2});
                 try {
                 value = ((Comparable) value1).compareTo(value2); 
                 }catch(NullPointerException nex) {
                     value = 1;
                 }
              }
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            Logger.getLogger(LazySorter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
