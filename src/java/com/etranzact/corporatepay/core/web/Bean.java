/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Oluwasegun.Idowu
 * @param <T>
 */
public abstract class Bean<T extends Serializable> implements Serializable {

    protected Bank supportBank;
    protected Corporate supportCorporate;

    /**
     * @return the bankUser
     */
    public boolean isBankUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return bankUser = loggedInUser instanceof BankUser || (supportBank != null && supportCorporate==null);
    }

    /**
     * @return the bankUser
     */
    public Bank getLoggedBank() {
        if (supportBank != null  && supportCorporate==null) {
            return supportBank;
        }
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (!(loggedInUser instanceof BankUser)) {
            return null;
        } else {
            return ((BankUser) loggedInUser).getBank();
        }
    }

    /**
     * @return the bankUser
     */
    public Corporate getLoggedCorporate() {
        if (supportCorporate != null && supportBank!=null) {
            return supportCorporate;
        }
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (!(loggedInUser instanceof CorporateUser)) {
            return null;
        } else {
            return ((CorporateUser) loggedInUser).getCorporate();
        }
    }

    /**
     * @return the bankUser
     */
    public User getLoggedUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return loggedInUser;
    }

    /**
     * @return the corporateUser
     */
    public boolean isCorporateUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return corporateUser = loggedInUser instanceof CorporateUser || (supportCorporate != null && supportBank!=null);
    }

    /**
     * @return the etzUser
     */
    public boolean isEtzUser() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        return etzUser = !(loggedInUser instanceof BankUser || loggedInUser instanceof CorporateUser);
    }

    private boolean bankUser;
    private boolean corporateUser;
    private boolean etzUser;

    /**
     * @return the supportBank
     */
    public Bank getSupportBank() {
        if(isCorporateUser()) {
           return getLoggedCorporate().getBank();
        } else if(isBankUser()) {
            return getLoggedBank();
        }
        return supportBank;
    }

    /**
     * @param supportBank the supportBank to set
     */
    public void setSupportBank(Bank supportBank) {
        this.supportBank = supportBank;
    }

    /**
     * @return the supportCorporate
     */
    public Corporate getSupportCorporate() {
           if(isCorporateUser()) {
           return getLoggedCorporate();
        }
        return supportCorporate;
    }

    /**
     * @param supportCorporate the supportCorporate to set
     */
    public void setSupportCorporate(Corporate supportCorporate) {
        this.supportCorporate = supportCorporate;
    }

}
