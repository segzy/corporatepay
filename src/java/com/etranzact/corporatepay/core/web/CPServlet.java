/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.UserFacadeLocal;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.DESEncrypter;
import com.etranzact.corporatepay.util.MailUtil;
import com.google.gson.Gson;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
public class CPServlet extends HttpServlet {

    protected Logger log = Logger.getLogger(CPServlet.class.getName());
    @EJB
    private UserFacadeLocal userFacadeLocal;

    @Override
    public void init() {
        log.info("called servelet:::::::::::: ");
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }

    private void doRequest(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getServletPath().equals("/reset")) {
            try {
                String user = req.getParameter("u");
                user = user.replace(" ", "+");
                user = DESEncrypter.instance().decrypt(user);
                user = StringEscapeUtils.unescapeJson(user);
                User foundUser = new Gson().fromJson(user, User.class);
                Date resetRequestTime = foundUser.getResetRequestTime();
                if (System.currentTimeMillis() - resetRequestTime.getTime() > 21600000) {
                    req.getRequestDispatcher("/reqexpired.html").forward(req, resp);
                    return;
                }
                foundUser = userFacadeLocal.find(foundUser.getId());
                String resetPassword = AppUtil.randomPassword(8, null);
                foundUser.setPassword(resetPassword);
                foundUser.setPasswordExpired(Boolean.TRUE);
                foundUser.setPasswordDateExpired(new Date());
                userFacadeLocal.create(foundUser);
                MailUtil.getInstance().sendPasswordReset(new String[]{foundUser.getEmail()}, foundUser.getUsername(), resetPassword);
                req.getRequestDispatcher("/passwordreset.html").forward(req, resp);
            } catch (Exception ex) {
                Logger.getLogger(CPServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
