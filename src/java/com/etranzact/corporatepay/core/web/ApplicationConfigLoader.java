/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.app.LocationManager;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 *
 * @author oluwasegun.idowu
 */
public class ApplicationConfigLoader implements ServletContextListener{
    
        private static final Logger logger = Logger.getLogger(ApplicationConfigLoader.class.getName());
    
   

    @Override
    public void contextInitialized(ServletContextEvent sce) {
         try {
             ConfigurationUtil.instance().init(sce.getServletContext()); //To change body of generated methods, choose Tools | Templates.
         } catch (Exception ex) {
                logger.log(Level.SEVERE,"Error creating Configuration",ex.fillInStackTrace());
                System.exit(1);
         }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ConfigurationUtil.instance().releasePropertiesResource(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
