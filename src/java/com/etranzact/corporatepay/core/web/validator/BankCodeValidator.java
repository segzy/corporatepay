/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web.validator;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesValidator("bankCodeValidator")
public class BankCodeValidator implements Validator {

    private static final Logger log = Logger.getLogger(BankCodeValidator.class.getName());

    @Inject
    private BankSetupFacadeLocal bankSetupFacadeLocal;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if(String.valueOf(value).length() <3 && String.valueOf(value).length()>10) {
             FacesMessage msg = new FacesMessage("Bank Code must be between 3 and 10 digit");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
        } 
        if(!String.valueOf(ConfigurationUtil.instance().getMainProperties().get("cp.valid.banks")).contains(String.valueOf(value))) {
           FacesMessage msg = new FacesMessage("Bank Code not recongnized");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg); 
        }
        Bank bank =getBank(String.valueOf(value));
                   log.log(Level.INFO, "bank {0}", bank);
            if (bank != null)
            {
                FacesMessage msg = new FacesMessage("Bank Code already exist");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
            
        
    }
    
    private Bank getBank(String bankCode) {
        try {
                Map<String,Object> map = new HashMap<>();
        map.put("id", bankCode);
            List<Bank> banks = bankSetupFacadeLocal.findAll(map, false);
            if(banks.isEmpty()) {
                return null;
            } else if(banks.size() > 1) {
                throw new Exception("More than one bank has bank code: " + bankCode);
            } else {
                return banks.get(0);
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, "Error", ex.fillInStackTrace());
            return new Bank();
        }
    }

}
