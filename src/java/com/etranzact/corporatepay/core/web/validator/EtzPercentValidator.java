/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web.validator;

import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesValidator("etzPercentValidator")
public class EtzPercentValidator implements Validator {

    private static final Logger log = Logger.getLogger(EtzPercentValidator.class.getName());


    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String percent = String.valueOf(value);
        if (percent != null) {
            log.info("validator:: minimum percent.....");
            BigDecimal etzMinPercentCommission = ConfigurationUtil.instance().getEtzMinPercentCommission();
            if (etzMinPercentCommission.compareTo(new BigDecimal(percent)) > 0) {
                FacesMessage msg = new FacesMessage("In valid percent for Etranzact: cannot be less than " + percent);
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }

        }

    }



}
