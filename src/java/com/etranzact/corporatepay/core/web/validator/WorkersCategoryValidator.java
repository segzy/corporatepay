/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web.validator;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.facade.PayrollPeriodFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryFacadeLocal;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesValidator("workersCategoryValidator")
public class WorkersCategoryValidator implements Validator {

    private static final Logger log = Logger.getLogger(WorkersCategoryValidator.class.getName());

    @Inject
    private WorkersCategoryFacadeLocal workersCategoryFacadeLocal;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        try {
            Map<String, Object> m = new HashMap<>();
            User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(AppConstants.LOGIN_USER);
            if (user instanceof CorporateUser) {
                m.put("corporate", ((CorporateUser) user).getCorporate());
            }
            m.put("id", value);
            List<WorkersCategory> workersCategorys = workersCategoryFacadeLocal.findAll(m, true);
            if (!workersCategorys.isEmpty()) {
                FacesMessage msg = new FacesMessage("Workers Category already exist");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryValidator.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage("Error validating workers category");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }



}
