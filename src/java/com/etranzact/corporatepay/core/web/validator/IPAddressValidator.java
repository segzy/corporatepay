/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web.validator;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesValidator("ipAddressValidator")
public class IPAddressValidator implements Validator {

    private static final Logger log = Logger.getLogger(IPAddressValidator.class.getName());

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
           boolean valid =IPValid(String.valueOf(value));
                   log.log(Level.INFO, "valid {0}", valid);
            if (!valid)
            {
                FacesMessage msg = new FacesMessage("Invalid Address ");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        
    }
    
    private static boolean IPValid(String ipaddress) {
        try {
            return ipaddress.split("\\.").length == 4;
        } catch (Exception ex) {
            log.log(Level.SEVERE, "Error", ex.fillInStackTrace());
            return false;
        }
    }
    
    public static void main(String[] args) {
        boolean IPValid = IPValid("12.34.56.33");
        System.out.println("" + IPValid);
    }

}
