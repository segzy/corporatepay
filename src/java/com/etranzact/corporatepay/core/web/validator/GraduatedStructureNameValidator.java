/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web.validator;

import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.CommissionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesValidator("structureNameValidator")
public class GraduatedStructureNameValidator implements Validator {

    private static final Logger log = Logger.getLogger(GraduatedStructureNameValidator.class.getName());

    @Inject
    private CommissionFacadeLocal commissionFacadeLocal;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String structureName = String.valueOf(value);
        if (structureName != null) {
            log.info("validator:: structure name.....");
                User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(AppConstants.LOGIN_USER);
              Map<String, Object> m = new HashMap<>();
                if (user instanceof BankUser) {
                m.put("bank", ((BankUser) user).getBank());
            }
            m.put("structureName", structureName);
            List<Commission> findAll = null;
            try {
                findAll = commissionFacadeLocal.findAll(m, false);
            } catch (Exception ex) {
                Logger.getLogger(GraduatedStructureNameValidator.class.getName()).log(Level.SEVERE, null, ex);

            }
            if (findAll != null && !findAll.isEmpty()) {
                FacesMessage msg = new FacesMessage("Structure name already exist ");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }

        }

    }

}
