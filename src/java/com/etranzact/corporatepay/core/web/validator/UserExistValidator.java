/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web.validator;

import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesValidator("usernameexistValidator")
public class UserExistValidator implements Validator {

    private static final Logger log = Logger.getLogger(UserExistValidator.class.getName());

    @Inject
    private UserSetupFacadeLocal userSetupFacadeLocal;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
           User user =getUser(String.valueOf(value));
                   log.log(Level.INFO, "user {0}", user);
            if (user != null)
            {
                  log.log(Level.INFO, "user {0}", user.getId());
              log.log(Level.INFO, "user {0}", user.getUsername());
                log.info("validator:: username exist.....");
                FacesMessage msg = new FacesMessage("Username already in use");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        
    }
    
    private User getUser(String username) {
        try {
            User user = userSetupFacadeLocal.findGlobalUser(username);
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

}
