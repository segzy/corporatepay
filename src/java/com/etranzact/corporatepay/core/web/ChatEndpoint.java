/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Oluwasegun.Idowu
 */
@ServerEndpoint(value = "/chat/{user}/{role}", decoders = ChatMessageDecoder.class, encoders = ChatMessageEncoder.class)
public class ChatEndpoint {

    private static final String USER_PARAM = "user";
    private static final String USER_ROLE_PARAM = "role";
    private static final String SUPPORT_ROLE_PARAM = "0002";

    private static final Logger log = Logger.getLogger(ChatEndpoint.class.getName());

    @OnOpen
    public void open(final Session session, @PathParam("user") final String user, @PathParam("role") final String role) {
          log.log(Level.INFO, "session openend and bound to user: {0} {1}", new Object[]{user, role});
        session.getUserProperties().put(USER_PARAM, user);
        session.getUserProperties().put(USER_ROLE_PARAM, role);
     
    }

    @OnMessage
    public void OnMessage(final Session session, final ChatMessage chatMessage) {
        String target = chatMessage.getTarget();
        String sender = chatMessage.getSender();
        //  log.log(Level.INFO, "chat message sent to {0}", target);
        //      log.log(Level.INFO, "sender {0}", sender);
        StringBuilder onlineUsers = new StringBuilder("");
        try {
            if (sender.isEmpty()) {
                for (Session s : session.getOpenSessions()) {
                    onlineUsers.append(s.getUserProperties().get(USER_PARAM)).append(",");
                }
                chatMessage.setMessage(onlineUsers.toString().substring(0, onlineUsers.toString().length() - 1));
                for (Session s : session.getOpenSessions()) {
                    if (s.getUserProperties().get(USER_ROLE_PARAM).equals(SUPPORT_ROLE_PARAM)) {
                        s.getBasicRemote().sendObject(chatMessage);
                    }
                }
            } else {
                if (target.isEmpty()) {
                    for (Session s : session.getOpenSessions()) {
                        if (s.isOpen() && s.getUserProperties().get(USER_ROLE_PARAM).equals(SUPPORT_ROLE_PARAM)) {
                            s.getBasicRemote().sendObject(chatMessage);
                            //    log.info("sent chat to support guy");
                        }
                        if (s.isOpen() && s.getUserProperties().get(USER_PARAM).equals(chatMessage.getSender())) {
                            if (!s.getUserProperties().get(USER_ROLE_PARAM).equals(SUPPORT_ROLE_PARAM)) {
                                s.getBasicRemote().sendObject(chatMessage);
                            }
                            //     log.info("getting back my message");
                        }
                    }
                } else {
                    for (Session s : session.getOpenSessions()) {

                        if (s.isOpen() && (target.equals(s.getUserProperties().get(USER_PARAM))
                                || sender.equals(s.getUserProperties().get(USER_PARAM)))) {
                            s.getBasicRemote().sendObject(chatMessage);
                            //      log.info("sent chat particular support  guy and copy me");
                        }
                    }
                }
            }
        } catch (IOException | EncodeException e) {
            log.log(Level.SEVERE, "On Message Failed", e);
        }

    }

    class ChatMessage {

        private Date received = new Date();
        private String sender;
        private String message;
        private String target;

        /**
         * @return the received
         */
        public Date getReceived() {
            return received;
        }

        /**
         * @param received the received to set
         */
        public void setReceived(Date received) {
            this.received = received;
        }

        /**
         * @return the sender
         */
        public String getSender() {
            return sender;
        }

        /**
         * @param sender the sender to set
         */
        public void setSender(String sender) {
            this.sender = sender;
        }

        /**
         * @return the message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message the message to set
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return the target
         */
        public String getTarget() {
            return target;
        }

        /**
         * @param target the target to set
         */
        public void setTarget(String target) {
            this.target = target;
        }

    }
}

class ChatMessageDecoder implements Decoder.Text<ChatEndpoint.ChatMessage> {

    public ChatMessageDecoder() {
    }

    @Override
    public ChatEndpoint.ChatMessage decode(String s) throws DecodeException {
        Gson gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(Date.class, new JsonDeserializer<Date>() {
                    DateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a");

                    @Override
                    public Date deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
                        if (je.getAsString() == null || je.getAsString().isEmpty()) {
                            return new Date();
                        } else {
                            try {
                                return df.parse(je.getAsString());
                            } catch (ParseException ex) {
                                Logger.getLogger(ChatMessageDecoder.class.getName()).log(Level.SEVERE, null, ex);
                                return null;
                            }
                        }
                    }

                }).create();

        return gson.fromJson(s, ChatEndpoint.ChatMessage.class);
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }

    @Override
    public void init(EndpointConfig config) {

    }

    @Override
    public void destroy() {

    }
}

class ChatMessageEncoder implements Encoder.Text<ChatEndpoint.ChatMessage> {

    public ChatMessageEncoder() {
    }

    @Override
    public String encode(ChatEndpoint.ChatMessage object) throws EncodeException {
        return new Gson().toJson(object);
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
