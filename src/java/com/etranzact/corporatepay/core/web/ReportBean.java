/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Settlement;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.model.dto.SettlementDto;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.payment.web.IncomeTableBean;
import com.etranzact.corporatepay.payment.web.PaymentTableBean;
import com.etranzact.corporatepay.payment.web.SettlementBean;
import com.etranzact.corporatepay.payment.web.SettlementTableBean;
import com.etranzact.corporatepay.payment.web.TransactionReversalTableBean;
import com.etranzact.corporatepay.payment.web.TransactionSummaryTableBean;
import com.etranzact.corporatepay.payment.web.TransactionTableBean;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRImageRenderer;
import net.sf.jasperreports.engine.JRRenderable;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.ExporterInputItem;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleExporterInputItem;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.apache.commons.collections4.map.HashedMap;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author Oluwasegun.Idowu
 */
@ManagedBean(name = "reportBean")
@SessionScoped
public class ReportBean extends AbstractBean<Serializable> {

    private List<String> userCharts;
    private Boolean chartRendered = Boolean.FALSE;
    private LineChartModel userChart;
    private LineChartModel transactionVolumeChart;
    private Bank bank;
    private List<Bank> banks;
    private List<Transaction> transactions;
    private List<Transaction> repTransactions;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateSetupFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankFacadeLocal;
    @EJB
    private TransactionFacadeLocal transactionFacadeLocal;
    @EJB
    private BeneficiaryFacadeLocal beneficiaryFacadeLocal;
    private String statusFilter;
    private Date fromDateFilter;
    private Date toDateFilter;
    private long treshTransCount = 100;
    private List<Transaction> sftransactions;

    public ReportBean() {
        super(Serializable.class);
    }

    public ReportBean(Class<Serializable> classT) {
        super(classT);
    }

    /**
     * @return the userCharts
     */
    public List<String> getUserCharts() {
        List<String> array = null;
        if (userCharts == null) {
            if (isBankUser()) {
                log.info("bank....");
                array = new ArrayList<>();
                //array.add(AppConstants.BANK_SUBSCRIBER_USERS);
            } else {
                array = new ArrayList<>();
                array.add(AppConstants.OVERALL_SUBSCRIBER_USERS);
                array.add(AppConstants.BANK_SUBSCRIBER_USERS);
            }
        }
        userCharts = array;
        return userCharts;
    }

    /**
     * @param userCharts the userCharts to set
     */
    public void setUserCharts(List<String> userCharts) {
        this.userCharts = userCharts;
    }

    @Override
    protected AbstractFacadeLocal<Serializable> getFacade() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the chartRendered
     */
    public Boolean getChartRendered() {
        return chartRendered;
    }

    /**
     * @param chartRendered the chartRendered to set
     */
    public void setChartRendered(Boolean chartRendered) {
        this.chartRendered = chartRendered;
    }

    public void doChart(String chartType) {

    }

    public void pullSuccesfulTransactions(Payment payment) {
        sftransactions = payment.getPaidTrans();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('successfultransactions').show();");
    }

    public void pullFailedTransactions(Payment payment) {
        sftransactions = payment.getPendingTrans();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('failedtransactions').show();");
    }

    public void onDialogClose(CloseEvent event) {
        sftransactions = new ArrayList<>();
    }

    public void doTransChart() {
        try {
            transactionVolumeChart = new LineChartModel();

            LineChartSeries corporatesSeries = new LineChartSeries();
            corporatesSeries.setFill(true);
            corporatesSeries.setLabel("Transactions");
            if (isBankUser()) {
                bank = getLoggedBank();
            }
            if (bank == null) {
                corporatesSeries.set("", 0);
            } else {
                //log.log(Level.INFO, "bank {0}", bank);
                Map<String, Object> filter = new HashMap<>();
                filter.put("bank", bank);
                List<Corporate> corporates = corporateSetupFacadeLocal.findAll(filter, true);
                log.log(Level.INFO, "corporates count {0}", corporates.size());
                if (corporates.isEmpty()) {
                    corporatesSeries.set("", 0);
                } else {
                    for (Corporate corporate : corporates) {
                        Map m = new HashMap<>();
                        if (getStatusFilter() != null) {
                            m.put("t.transactionStatus='" + getStatusFilter() + "'", null);
                        }
                        m.put("corporate", corporate);
                        transactions = transactionFacadeLocal.findAll(m, false, new String[0], getFromDateFilter(), getToDateFilter(), "dateCreated");
                        if (!transactions.isEmpty()) {
                            repTransactions = transactions;
                        }
                        //log.log(Level.INFO, "transactions:: {0}", transactions.size());
                        if (transactions.isEmpty()) {
                            corporatesSeries.set(corporate.getCorporateName(), 0);
                        } else {

                            corporatesSeries.set(corporate.getCorporateName(), transactions.size());
                            log.log(Level.INFO, "add series");
                        }
                    }
                }
            }
            transactionVolumeChart.addSeries(corporatesSeries);
            transactionVolumeChart.setTitle("");
            transactionVolumeChart.setLegendPosition("ne");
            transactionVolumeChart.setShowPointLabels(true);
            transactionVolumeChart.setExtender("userChart");

            transactionVolumeChart.setStacked(true);
            transactionVolumeChart.setShowPointLabels(true);

            Axis xAxis = new CategoryAxis("Corporates");

            transactionVolumeChart.getAxes().put(AxisType.X, xAxis);
            Axis yAxis = transactionVolumeChart.getAxis(AxisType.Y);
            yAxis.setLabel("Transaction(s) Count");
            yAxis.setMin(0);
            yAxis.setMax(getTreshTransCount());
        } catch (Exception ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void doCorporateTransChart() {
        try {
            transactionVolumeChart = new LineChartModel();
            LineChartSeries corporatesSeries = new LineChartSeries();
            corporatesSeries.setFill(true);
            corporatesSeries.setLabel("Transactions");
            Corporate corporate = null;
            if (isCorporateUser()) {
                corporate = getLoggedCorporate();
            }
            log.log(Level.INFO, "fromDateFilter {0}", fromDateFilter);
            log.log(Level.INFO, "toDateFilter {0}", toDateFilter);
            log.log(Level.INFO, "corporate {0}", corporate);
            boolean xaxisset = false;
            if ((fromDateFilter == null && toDateFilter == null) || corporate == null || (getStatusFilter() == null || getStatusFilter().isEmpty())) {

                FacesMessage m = new FacesMessage();
                m.setSeverity(FacesMessage.SEVERITY_INFO);
                m.setSummary("You are only allowed to get charts for selected date period filter and a selected transaction status filter");
                FacesContext.getCurrentInstance().addMessage(null, m);
            } else {
                Map<String, Object> m = new HashMap<>();
                m.put("corporate", corporate);
                List<Beneficiary> beneficiaries = beneficiaryFacadeLocal.findAll(m, true);

                for (Beneficiary beneficiary : beneficiaries) {
                    m = new HashMap<>();
                    if (getStatusFilter() != null) {
                        m.put("t.transactionStatus='" + getStatusFilter() + "'", null);
                    }
                    m.put("corporate", corporate);
                    m.put("t.beneficiary.id=" + beneficiary.getId(), null);
                    if (getFromDateFilter() == null && getToDateFilter() == null) {
                        transactions = transactionFacadeLocal.findAll(m, false, new String[0], "dateCreated");
                    } else {
                        transactions = transactionFacadeLocal.findAll(m, false, new String[0], getFromDateFilter(), getToDateFilter(), "dateCreated");
                    }
                    // log.log(Level.INFO, "transactions:: {0}", transactions.size());
                    if (!transactions.isEmpty()) {
                        repTransactions = transactions;
                        corporatesSeries.set(beneficiary.getAccount().getAccountName(), transactions.size());
                        xaxisset = true;
                    }

                    //  log.log(Level.INFO, "add series");
                }

            }
            if (!xaxisset) {
                corporatesSeries.set("", 0);
            }
            transactionVolumeChart.addSeries(corporatesSeries);
            transactionVolumeChart.setTitle("");
            transactionVolumeChart.setLegendPosition("ne");
            transactionVolumeChart.setShowPointLabels(true);
            transactionVolumeChart.setExtender("userChart");

            transactionVolumeChart.setStacked(true);
            transactionVolumeChart.setShowPointLabels(true);

            Axis xAxis = new CategoryAxis("Beneficiaries");
            // xAxis.setTickAngle(-30);
            transactionVolumeChart.getAxes().put(AxisType.X, xAxis);
            Axis yAxis = transactionVolumeChart.getAxis(AxisType.Y);
            yAxis.setLabel("Transaction(s) Count");
            yAxis.setMin(0);
            yAxis.setMax(getTreshTransCount());
        } catch (Exception ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the userChart
     */
    public LineChartModel getUserChart() {
        if (userChart == null) {
            designUserChart();
        }
        return userChart;
    }

    public void generateTransReceipt(Transaction transaction) {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            FileInputStream jasperFile = new FileInputStream(ConfigurationUtil.instance().getTransactionReceiptFile());
            Map<String, Object> m = new HashedMap<>();
            m.put("transRef", transaction.getTransactionId().toString());
            m.put("corporate", transaction.getCorporate().toString());
            m.put("bank", transaction.getBeneficaryBank());
            m.put("bName", transaction.getBeneficaryName());
            m.put("bAccount", transaction.getBeneficaryAccount());
            m.put("narration", transaction.getNarration());
            m.put("paymentDate", transaction.getPaymentDateStr());
            m.put("amount", transaction.getAmountf());
            byte[] runReportToPdf = JasperRunManager.runReportToPdf(jasperFile, m, new JREmptyDataSource());
            ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionRceipt_" + System.currentTimeMillis());
            ec.getResponseOutputStream().write(runReportToPdf);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generateTransSummaryReport(String type) {
        try {
            Map m = new HashMap();
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ServletContext sc = ((ServletContext) ec.getContext());
            String sentPath = "";
            String repOwner = null;
            InputStream is;
//            User loggedUser = getLoggedUser();
            TransactionSummaryTableBean paymentTableBean = (TransactionSummaryTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionSummaryTableBean");
            if (paymentTableBean.getCorporateFilter() != null || getSupportCorporate() != null) {
                byte[] logo = null;
                try {
                    logo = paymentTableBean.getCorporateFilter().getLogo();
                } catch (NullPointerException nex) {
                    logo = getSupportCorporate().getLogo();
                }
                if (logo != null) {
                    is = new ByteArrayInputStream(logo);
                    try {
                        repOwner = paymentTableBean.getCorporateFilter().getCorporateName();
                    } catch (NullPointerException npe) {
                        repOwner = getSupportCorporate().getCorporateName();
                    }
                } else {
                    try {
                        sentPath = sc.getRealPath("/resources/sentinel-layout_" + paymentTableBean.getCorporateFilter().getBank().getId());
                        repOwner = paymentTableBean.getCorporateFilter().getCorporateName();
                    } catch (NullPointerException npe) {
                        sentPath = sc.getRealPath("/resources/sentinel-layout_" + getSupportCorporate().getBank().getId());
                        repOwner = getSupportCorporate().getCorporateName();
                    }
                    File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                    is = new FileInputStream(logoFile);
                }

            } else if (getSupportBank() != null) {
                try {
                    sentPath = sc.getRealPath("/resources/sentinel-layout_" + paymentTableBean.getCorporateFilter().getId());
                    repOwner = paymentTableBean.getCorporateFilter().getCorporateName();
                } catch (NullPointerException npe) {
                    sentPath = sc.getRealPath("/resources/sentinel-layout_" + getSupportBank().getId());
                    repOwner = getSupportBank().getBankName();
                }
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);

            } else {
                repOwner = "Corporate Pay";
                sentPath = sc.getRealPath("/resources/sentinel-layout_1000");
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);
            }

            byte[] b = new byte[is.available()];
            is.read(b);
            is.close();
            JRImageRenderer rend = JRImageRenderer.getInstance(b);
            m.put("cop_logo", rend);
            m.put("fromDate", paymentTableBean.getFromDateFilter());
            m.put("toDate", paymentTableBean.getToDateFilter());
            m.put("ownerCaption", repOwner);
            List<Payment> paymentss = paymentTableBean.getAllList();
            log.log(Level.INFO, "transactions summary to report on.. {0}", paymentss.size());
            FileInputStream jasperFile = isBankUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionSummaryRepFileName("bank")) : isCorporateUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionSummaryRepFileName("corporate"))
                    : true ? new FileInputStream(ConfigurationUtil.instance().getTransactionSummaryRepFileName(null)) : null;
            if (type.equals(AppConstants.REPORT_TRANS_INFO_PDF)) {

                byte[] runReportToPdf = JasperRunManager.runReportToPdf(jasperFile, m, new JRBeanCollectionDataSource(paymentss));
                ec.setResponseContentType("application/pdf");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionSummaryReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(runReportToPdf);
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL)) {
                JRXlsExporter exporter = new JRXlsExporter();
                JasperPrint print = JasperFillManager.fillReport(jasperFile, m, new JRBeanCollectionDataSource(paymentss));
                ExporterInput ei = new SimpleExporterInput(print);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                OutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(os);
                exporter.setExporterInput(ei);
                exporter.setExporterOutput(output);
                exporter.exportReport();
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionSummaryReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(os.toByteArray());
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL2)) {
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionSummaryReport_" + System.currentTimeMillis());
                byte[] excel = AppUtil.convertExcel(paymentss, new String[]{"corporate", "id", "paymentDescription", "totalCount", "totalAmountf", "paid", "amountPaidf", "pendingPayment", "amountPendingPaymentf"});
                ec.getResponseOutputStream().write(excel);
                FacesContext.getCurrentInstance().responseComplete();
            }
            paymentTableBean.setPrefetch(false);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void generateSettlementReport(String type) {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        SettlementBean settlementBean = (SettlementBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("settlementBean");
        List<SettlementDto> settlementDtos = settlementBean.getSettlementDtos();
        log.info("settlementDtos TO REPORT " + settlementDtos.size());
        SettlementTableBean settlementTableBean = (SettlementTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("settlementTableBean");
        String reportName = null;
//        Bank supportBank1 = getSupportBank();
//        if(supportBank1==null) {
        Bank bankFilter = settlementTableBean.getSrcBankFilter();
        String settlementTypeFilter = settlementTableBean.getSettlementTypeFilter();
        String[] fields = new String[]{"paymentId", "companyName", "bank", "hostErrorCode", "reference", "issuerCode", "accountNumber", "transAmount", "beneficiaryName", "narration", "created"};
//        }
        if (settlementTypeFilter != null && bankFilter != null) {
            if (AppConstants.REPORT_SETTLEMENT_INCOMING.equals(settlementTypeFilter)) {
                reportName = bankFilter.getId() + "_INCOMING_CPAY_DETAILS";
            } else if (AppConstants.REPORT_SETTLEMENT_OUTGOING.equals(settlementTypeFilter)) {
                reportName = bankFilter.getId() + "_OUTGOING_CPAY_DETAILS";
            } else if (AppConstants.REPORT_SETTLEMENT_SAME.equals(settlementTypeFilter)) {
                reportName = bankFilter.getId() + "_SAMEBANK_CPAY_DETAILS";
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(settlementTableBean.getToDateFilter());
            cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + 1);
            reportName = reportName + AppUtil.formatDate(cal.getTime(), "ddMMyyyy");
            byte[] excel = getSettlementFileReport(settlementDtos, fields);
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=" + reportName);
            try {
                ec.getResponseOutputStream().write(excel);
            } catch (IOException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (settlementTypeFilter == null && bankFilter != null) {

        } else if (settlementTypeFilter != null && bankFilter == null) {
            log.info("bank not selected..............");
            Map<String, List<SettlementDto>> map = new HashMap<>();
            for (SettlementDto settlementDto : settlementDtos) {
                String bank1 = null;
                if (AppConstants.REPORT_SETTLEMENT_SAME.equals(settlementTypeFilter) || AppConstants.REPORT_SETTLEMENT_OUTGOING.equals(settlementTypeFilter)) {
                    bank1 = settlementDto.getBank();
                } else if (AppConstants.REPORT_SETTLEMENT_INCOMING.equals(settlementTypeFilter)) {
                    bank1 = settlementDto.getIssuerCode();
                }
                List<SettlementDto> bankSetlements = map.get(bank1);
                if (bankSetlements == null) {
                    bankSetlements = new ArrayList<>();
                }
                bankSetlements.add(settlementDto);
                map.put(bank1, bankSetlements);

            }
            log.info("found report for ............. " + map.size() + " banks");
            ByteArrayOutputStream zipByteArrayOutputStream = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(zipByteArrayOutputStream);
            for (Entry<String, List<SettlementDto>> entry : map.entrySet()) {

                List<SettlementDto> settlementDtos1 = entry.getValue();
                String bankCode = entry.getKey();
                log.info("for " + bankCode);
                log.info("found " + settlementDtos1.size());
                if (AppConstants.REPORT_SETTLEMENT_INCOMING.equals(settlementTypeFilter)) {
                    reportName = bankCode + "_INCOMING_CPAY_DETAILS";
                } else if (AppConstants.REPORT_SETTLEMENT_OUTGOING.equals(settlementTypeFilter)) {
                    reportName = bankCode + "_OUTGOING_CPAY_DETAILS";
                } else if (AppConstants.REPORT_SETTLEMENT_SAME.equals(settlementTypeFilter)) {
                    reportName = bankCode + "_SAMEBANK_CPAY_DETAILS";
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(settlementTableBean.getToDateFilter());
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + 1);
                reportName = reportName + AppUtil.formatDate(cal.getTime(), "ddMMyyyy");
                byte[] excel = getSettlementFileReport(settlementDtos1, fields);
                try {
                    zipOutputStream.putNextEntry(new ZipEntry(reportName + ".xls"));
                    zipOutputStream.write(excel);
                } catch (IOException ex) {
                    Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            try {
                zipOutputStream.flush();
                zipByteArrayOutputStream.flush();
                zipOutputStream.close();
                zipByteArrayOutputStream.close();
                ec.setResponseContentType("application/zip");
                String app = AppConstants.REPORT_SETTLEMENT_INCOMING.equals(settlementTypeFilter) ? "incoming" : AppConstants.REPORT_SETTLEMENT_OUTGOING.equals(settlementTypeFilter) ? "outgoing" : "same";
                ec.setResponseHeader("Content-Disposition", "attachment; filename=settlement_" + app + "_" + AppUtil.formatDate(Calendar.getInstance().getTime(), "ddMMyyyy") + ".zip");
                ec.getResponseOutputStream().write(zipByteArrayOutputStream.toByteArray());
            } catch (IOException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        settlementTableBean.setPrefetch(false);
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void generateTransIncomeReport(String type) {
        try {
            Map m = new HashMap();
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ServletContext sc = ((ServletContext) ec.getContext());
            String sentPath = "";
            String repOwner = null;
            InputStream is;
//            User loggedUser = getLoggedUser();
            IncomeTableBean incomeTableBean = (IncomeTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("incomeTableBean");
            if (getSupportBank() != null) {
                sentPath = sc.getRealPath("/resources/sentinel-layout_" + getSupportBank().getId());
                repOwner = getSupportBank().getBankName();
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);

            } else {
                repOwner = "Corporate Pay";
                sentPath = sc.getRealPath("/resources/sentinel-layout_1000");
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);
            }

            byte[] b = new byte[is.available()];
            is.read(b);
            is.close();
            JRImageRenderer rend = JRImageRenderer.getInstance(b);
            m.put("cop_logo", rend);
            m.put("fromDate", incomeTableBean.getFromDateFilter());
            m.put("toDate", incomeTableBean.getToDateFilter());
            m.put("ownerCaption", repOwner);
            List<Settlement> settlements = incomeTableBean.getAllList();
            for (Settlement s : settlements) {
                try {
                    s.setDestBank(bankFacadeLocal.find(s.getdIssuerCode()).getBankName());
                } catch (Exception ex) {
                    Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            log.log(Level.INFO, "settlements to report on.. {0}", settlements.size());
            FileInputStream jasperFile = isBankUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionIncomeRepFileName("bank")) : isCorporateUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionIncomeRepFileName("corporate"))
                    : true ? new FileInputStream(ConfigurationUtil.instance().getTransactionIncomeRepFileName(null)) : null;
            if (type.equals(AppConstants.REPORT_TRANS_INFO_PDF)) {

                byte[] runReportToPdf = JasperRunManager.runReportToPdf(jasperFile, m, new JRBeanCollectionDataSource(settlements));
                ec.setResponseContentType("application/pdf");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=incomeReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(runReportToPdf);
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL)) {
                JasperPrint print = JasperFillManager.fillReport(jasperFile, m, new JRBeanCollectionDataSource(settlements));

                JRXlsExporter exporter = new JRXlsExporter();
                ExporterInput ei = new SimpleExporterInput(print);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                OutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(os);
                exporter.setExporterInput(ei);
                exporter.setExporterOutput(output);
                exporter.exportReport();
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=incomeReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(os.toByteArray());
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL2)) {
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReversalReport_" + System.currentTimeMillis());
                String[] fields = null;
                if (getSupportBank() != null) {
                    fields = new String[]{"corporate", "bank", "owner", "sbatchId", "batchId", "narration", "amount"};
                } else {
                    fields = new String[]{"corporate", "bank", "sbatchId", "batchId", "narration", "amount"};
                }
                byte[] excel = AppUtil.convertExcel(settlements, fields);
                ec.getResponseOutputStream().write(excel);
                FacesContext.getCurrentInstance().responseComplete();
            }
            incomeTableBean.setPrefetch(false);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generateSettlementReversalReport(List<Settlement> settlements, String type) {
        try {
            List<Transaction> revtransactions = null;
            for (Settlement settlement : settlements) {
                Map<String, Object> map = new HashMap<>();
                  map.put("sbatchId='" + settlement.getSbatchId() + "'", null);;
                   map.put("transactionStatus in ('" + AppConstants.TRANSACTION_REVERSED + "')" , null);
                try {
                    List<Transaction> findAll = transactionFacadeLocal.findAll(map, false);
                    for(Transaction transaction: findAll) {
                        transaction.setSettlementUniqueId(settlement.getUniqueTransId());
                    }
                    if (revtransactions == null) {
                        revtransactions = new ArrayList<>();
                    }
                    revtransactions.addAll(findAll);
                    //log.info("transactions:: " + transactions.size());
                } catch (Exception ex) {
                    Logger.getLogger(SettlementBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//            ServletContext sc = ((ServletContext) ec.getContext());
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReversalReport_" + System.currentTimeMillis());
            String[] fields = null;
            fields = new String[]{"transactionId", "beneficaryBank", "beneficaryName", "beneficaryAccount", "narration", "amount", "authDate","revDate", "authBy", "settlementUniqueId"};
            byte[] excel = AppUtil.convertExcel(revtransactions, fields);
            ec.getResponseOutputStream().write(excel);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generateTransRevReport(String type) {
        try {
            Map m = new HashMap();
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ServletContext sc = ((ServletContext) ec.getContext());
            String sentPath = "";
            String repOwner = null;
            InputStream is;
//            User loggedUser = getLoggedUser();
            TransactionReversalTableBean transactionRevTableBean = (TransactionReversalTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionReversalTableBean");
            if (transactionRevTableBean.getCorporateFilter() != null || getSupportCorporate() != null) {
                byte[] logo = null;
                try {
                    logo = transactionRevTableBean.getCorporateFilter().getLogo();
                } catch (NullPointerException nex) {
                    logo = getSupportCorporate().getLogo();
                }
                if (logo != null) {
                    is = new ByteArrayInputStream(logo);
                    try {
                        repOwner = transactionRevTableBean.getCorporateFilter().getCorporateName();
                    } catch (NullPointerException npe) {
                        repOwner = getSupportCorporate().getCorporateName();
                    }
                } else {
                    try {
                        sentPath = sc.getRealPath("/resources/sentinel-layout_" + transactionRevTableBean.getCorporateFilter().getBank().getId());
                        repOwner = transactionRevTableBean.getCorporateFilter().getCorporateName();
                    } catch (NullPointerException npe) {
                        sentPath = sc.getRealPath("/resources/sentinel-layout_" + getSupportCorporate().getBank().getId());
                        repOwner = getSupportCorporate().getCorporateName();
                    }
                    File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                    is = new FileInputStream(logoFile);
                }

            } else if (transactionRevTableBean.getSrcBankFilter() != null || getSupportBank() != null) {
                try {
                    sentPath = sc.getRealPath("/resources/sentinel-layout_" + transactionRevTableBean.getSrcBankFilter().getId());
                    repOwner = transactionRevTableBean.getSrcBankFilter().getBankName();
                } catch (NullPointerException npe) {
                    sentPath = sc.getRealPath("/resources/sentinel-layout_" + getSupportBank().getId());
                    repOwner = getSupportBank().getBankName();
                }
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);

            } else {
                repOwner = "Corporate Pay";
                sentPath = sc.getRealPath("/resources/sentinel-layout_1000");
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);
            }

            byte[] b = new byte[is.available()];
            is.read(b);
            is.close();
            JRImageRenderer rend = JRImageRenderer.getInstance(b);
            m.put("cop_logo", rend);
            m.put("fromDate", transactionRevTableBean.getFromDateFilter());
            m.put("toDate", transactionRevTableBean.getToDateFilter());
            m.put("ownerCaption", repOwner);
            List<Transaction> transactionss = transactionRevTableBean.getAllList();
            log.log(Level.INFO, "transactions to report on.. {0}", transactionss.size());
            FileInputStream jasperFile = isBankUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionReversalRepFileName("bank")) : isCorporateUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionReversalRepFileName("corporate"))
                    : true ? new FileInputStream(ConfigurationUtil.instance().getTransactionReversalRepFileName(null)) : null;
            if (type.equals(AppConstants.REPORT_TRANS_INFO_PDF)) {

                byte[] runReportToPdf = JasperRunManager.runReportToPdf(jasperFile, m, new JRBeanCollectionDataSource(transactionss));
                ec.setResponseContentType("application/pdf");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReversalReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(runReportToPdf);
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL)) {
                JasperPrint print = JasperFillManager.fillReport(jasperFile, m, new JRBeanCollectionDataSource(transactionss));

                JRXlsExporter exporter = new JRXlsExporter();
                ExporterInput ei = new SimpleExporterInput(print);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                OutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(os);
                exporter.setExporterInput(ei);
                exporter.setExporterOutput(output);
                exporter.exportReport();
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReversalReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(os.toByteArray());
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL2)) {
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReversalReport_" + System.currentTimeMillis());
                String[] fields = null;
                if (transactionRevTableBean.getCorporateFilter() != null || getSupportCorporate() != null) {
                    fields = new String[]{"transactionId", "beneficaryBank", "beneficaryName", "beneficaryAccount", "narration", "amount", "authDate", "authBy", "responseCode"};
                } else if (transactionRevTableBean.getSrcBankFilter() != null || getSupportBank() != null) {
                    fields = new String[]{"transactionId", "beneficaryBank", "beneficaryName", "beneficaryAccount", "narration", "amount", "authDate", "authBy", "responseCode"};
                } else {
                    fields = new String[]{"transactionId", "beneficaryBank", "beneficaryName", "beneficaryAccount", "narration", "amount", "authDate", "authBy", "responseCode"};
                }
                byte[] excel = AppUtil.convertExcel(transactionss, fields);
                ec.getResponseOutputStream().write(excel);
                FacesContext.getCurrentInstance().responseComplete();
            }
            transactionRevTableBean.setPrefetch(false);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generateTransReport(String type) {
        try {
            Map m = new HashMap();
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ServletContext sc = ((ServletContext) ec.getContext());
            String sentPath = "";
            String repOwner = null;
            InputStream is;
//            User loggedUser = getLoggedUser();
            TransactionTableBean transactionTableBean = (TransactionTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionReportTableBean");
            if (transactionTableBean.getCorporateFilter() != null || getSupportCorporate() != null) {
                byte[] logo = null;
                try {
                    logo = transactionTableBean.getCorporateFilter().getLogo();
                } catch (NullPointerException nex) {
                    logo = getSupportCorporate().getLogo();
                }
                if (logo != null) {
                    is = new ByteArrayInputStream(logo);
                    try {
                        repOwner = transactionTableBean.getCorporateFilter().getCorporateName();
                    } catch (NullPointerException npe) {
                        repOwner = getSupportCorporate().getCorporateName();
                    }
                } else {
                    try {
                        sentPath = sc.getRealPath("/resources/sentinel-layout_" + transactionTableBean.getCorporateFilter().getBank().getId());
                        repOwner = transactionTableBean.getCorporateFilter().getCorporateName();
                    } catch (NullPointerException npe) {
                        sentPath = sc.getRealPath("/resources/sentinel-layout_" + getSupportCorporate().getBank().getId());
                        repOwner = getSupportCorporate().getCorporateName();
                    }
                    File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                    is = new FileInputStream(logoFile);
                }

            } else if (transactionTableBean.getSrcBankFilter() != null || getSupportBank() != null) {
                try {
                    sentPath = sc.getRealPath("/resources/sentinel-layout_" + transactionTableBean.getSrcBankFilter().getId());
                    repOwner = transactionTableBean.getSrcBankFilter().getBankName();
                } catch (NullPointerException npe) {
                    sentPath = sc.getRealPath("/resources/sentinel-layout_" + getSupportBank().getId());
                    repOwner = getSupportBank().getBankName();
                }
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);

            } else {
                repOwner = "Corporate Pay";
                sentPath = sc.getRealPath("/resources/sentinel-layout_1000");
                File logoFile = new File(sentPath + File.separator + "images" + File.separator + "bank-logo.png");
                is = new FileInputStream(logoFile);
            }

            byte[] b = new byte[is.available()];
            is.read(b);
            is.close();
            JRImageRenderer rend = JRImageRenderer.getInstance(b);
            m.put("cop_logo", rend);
            m.put("fromDate", transactionTableBean.getFromDateFilter());
            m.put("toDate", transactionTableBean.getToDateFilter());
            m.put("ownerCaption", repOwner);
            List<Transaction> transactionss = transactionTableBean.getAllList();
            log.log(Level.INFO, "transactions to report on.. {0}", transactionss.size());
            FileInputStream jasperFile = isBankUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionRepFileName("bank")) : isCorporateUser() ? new FileInputStream(ConfigurationUtil.instance().getTransactionRepFileName("corporate"))
                    : true ? new FileInputStream(ConfigurationUtil.instance().getTransactionRepFileName(null)) : null;
            if (type.equals(AppConstants.REPORT_TRANS_INFO_PDF)) {

                byte[] runReportToPdf = JasperRunManager.runReportToPdf(jasperFile, m, new JRBeanCollectionDataSource(transactionss));
                ec.setResponseContentType("application/pdf");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(runReportToPdf);
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL)) {
                JasperPrint print = JasperFillManager.fillReport(jasperFile, m, new JRBeanCollectionDataSource(transactionss));

                JRXlsExporter exporter = new JRXlsExporter();
                ExporterInput ei = new SimpleExporterInput(print);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                OutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(os);
                exporter.setExporterInput(ei);
                exporter.setExporterOutput(output);
                exporter.exportReport();
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReport_" + System.currentTimeMillis());
                ec.getResponseOutputStream().write(os.toByteArray());
                FacesContext.getCurrentInstance().responseComplete();
            } else if (type.equals(AppConstants.REPORT_TRANS_INFO_EXCEL2)) {
                ec.setResponseContentType("application/vnd.ms-excel");
                ec.setResponseHeader("Content-Disposition", "attachment; filename=transactionReport_" + System.currentTimeMillis());
                String[] fields = null;
                if (transactionTableBean.getCorporateFilter() != null || getSupportCorporate() != null) {
                    fields = new String[]{"corporate", "initiatingBank", "transactionId", "beneficaryBank", "beneficaryName", "beneficaryAccount","inititatingCardNumber", "narration", "amount", "authDate", "authBy", "status", "responseCode"};
                } else if (transactionTableBean.getSrcBankFilter() != null || getSupportBank() != null) {
                    fields = new String[]{"corporate", "initiatingBank", "transactionId", "beneficaryBank", "beneficaryName", "beneficaryAccount","holdingCardNumber","inititatingCardNumber",  "narration", "amount", "bankFeeAmount", "thirdpartyFeeAmount", "thirdpartyFeeAmount2", "authDate", "authBy", "status", "responseCode"};
                } else {
                    fields = new String[]{"corporate", "initiatingBank", "transactionId", "beneficaryBank", "beneficaryName", "beneficaryAccount","holdingCardNumber","inititatingCardNumber",  "narration", "amount", "etzFeeAmount","bankFeeAmount", "thirdpartyFeeAmount", "thirdpartyFeeAmount2","sbatchId", "authDate", "authBy", "status", "responseCode"};
                }
                byte[] excel = AppUtil.convertExcel(transactionss, fields);
                ec.getResponseOutputStream().write(excel);
                FacesContext.getCurrentInstance().responseComplete();
            }
            transactionTableBean.setPrefetch(false);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void designUserChart() {
        userChart = new LineChartModel();
        try {
            if (isBankUser()) {
                bank = getLoggedBank();
                chartRendered = Boolean.TRUE;
            }
            List<Corporate> corporates;
            Map<String, Object> filter;
            // log.log(Level.INFO, "bank {0}", bank);
            if (bank == null) {
                corporates = new ArrayList<>();
            } else {
                filter = new HashMap<>();
                filter.put("bank", bank);
                corporates = corporateSetupFacadeLocal.findAll(filter, true);
            }
            // log.log(Level.INFO, "corporates count {0}", corporates.size());
            LineChartSeries corporatesSeries = new LineChartSeries();
            corporatesSeries.setFill(true);
            corporatesSeries.setLabel("Users");
            if (corporates.isEmpty()) {
                corporatesSeries.set("", 0);
            } else {
                for (Corporate corporate : corporates) {
                    List<User> users;
                    filter = new HashMap<>();
                    filter.put("corporate", corporate);
                    userSetupFacadeLocal.setEntityNamePrefix("Corporate");
                    users = userSetupFacadeLocal.findAll(filter, true);
                    //   log.log(Level.INFO, "users:: {0}", users.size());
                    corporatesSeries.set(corporate.getCorporateName(), users.size());
                    //   log.log(Level.INFO, "add series");
                }
            }

            userChart.addSeries(corporatesSeries);
            userChart.setTitle("");
            userChart.setLegendPosition("ne");
            userChart.setShowPointLabels(true);
            userChart.setExtender("userChart");

            userChart.setStacked(true);
            userChart.setShowPointLabels(true);

            Axis xAxis = new CategoryAxis("Corporates");
            userChart.getAxes().put(AxisType.X, xAxis);
            Axis yAxis = userChart.getAxis(AxisType.Y);
            yAxis.setLabel("Active Users Count");
            yAxis.setMin(0);
            yAxis.setMax(100);
        } catch (Exception ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleBankChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        setBank((Bank) ((UISelectOne) e.getComponent()).getValue());
        chartRendered = Boolean.TRUE;
        designUserChart();
    }

    /**
     * @param userChart the userChart to set
     */
    public void setUserChart(LineChartModel userChart) {
        this.userChart = userChart;
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null) {
            try {
                banks = bankFacadeLocal.findAll(new HashMap(), true);
            } catch (Exception ex) {
                banks = new ArrayList<>();
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the transactionVolumeChart
     */
    public LineChartModel getTransactionVolumeChart() {
        if (transactionVolumeChart != null) {
            return transactionVolumeChart;
        }
        transactionVolumeChart = new LineChartModel();

        LineChartSeries corporatesSeries = new LineChartSeries();
        corporatesSeries.setFill(true);
        corporatesSeries.setLabel("Transactions");
        corporatesSeries.set("", 0);
        transactionVolumeChart.addSeries(corporatesSeries);
        transactionVolumeChart.setTitle("");
        transactionVolumeChart.setLegendPosition("ne");
        transactionVolumeChart.setShowPointLabels(true);
        transactionVolumeChart.setExtender("userChart");

        transactionVolumeChart.setStacked(true);
        transactionVolumeChart.setShowPointLabels(true);

        Axis xAxis = new CategoryAxis("Corporates");
        transactionVolumeChart.getAxes().put(AxisType.X, xAxis);
        Axis yAxis = transactionVolumeChart.getAxis(AxisType.Y);
        yAxis.setLabel("Transaction(s) Count");
        yAxis.setMin(0);
        yAxis.setMax(getTreshTransCount());
        return transactionVolumeChart;
    }

    /**
     * @param transactionVolumeChart the transactionVolumeChart to set
     */
    public void setTransactionVolumeChart(LineChartModel transactionVolumeChart) {
        this.transactionVolumeChart = transactionVolumeChart;
    }

    /**
     * @return the statusFilter
     */
    public String getStatusFilter() {
        return statusFilter;
    }

    /**
     * @param statusFilter the statusFilter to set
     */
    public void setStatusFilter(String statusFilter) {
        this.statusFilter = statusFilter;
    }

    /**
     * @return the fromDateFilter
     */
    public Date getFromDateFilter() {
        return fromDateFilter;
    }

    /**
     * @param fromDateFilter the fromDateFilter to set
     */
    public void setFromDateFilter(Date fromDateFilter) {
        this.fromDateFilter = fromDateFilter;
    }

    /**
     * @return the toDateFilter
     */
    public Date getToDateFilter() {
        return toDateFilter;
    }

    /**
     * @param toDateFilter the toDateFilter to set
     */
    public void setToDateFilter(Date toDateFilter) {
        this.toDateFilter = toDateFilter;
    }

    /**
     * @return the treshTransCount
     */
    public long getTreshTransCount() {
        return treshTransCount;
    }

    /**
     * @param treshTransCount the treshTransCount to set
     */
    public void setTreshTransCount(long treshTransCount) {
        this.treshTransCount = treshTransCount;
    }

    /**
     * @return the copTransactions
     */
    public List<Transaction> getCopTransactions() {
        if (repTransactions == null) {
            repTransactions = new ArrayList<>();
        }
        return repTransactions;
    }

    public static void main(String[] args) {
        String[] split = "C:\\Users\\oluwasegun.idowu\\Downloads\\wildfly-8.2.0.Final\\standalone\\corporatepay\\config\\report\\transaction_plain.jasper".split("\\");
        String ekek = split[split.length - 1];
        System.out.println("split: " + ekek);
        String fileName = split[0] + " bank" + "." + "jasper";
        System.out.println("fileName: " + fileName);
    }

    /**
     * @return the sftransactions
     */
    public List<Transaction> getSftransactions() {
        return sftransactions;
    }

    private byte[] getSettlementFileReport(List<SettlementDto> settlementDtos, String[] fields) {

        return AppUtil.convertExcel(settlementDtos, fields);
    }

}
