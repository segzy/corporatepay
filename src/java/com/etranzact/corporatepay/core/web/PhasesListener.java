/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.core.web;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Segun Idowu
 */
public class PhasesListener implements PhaseListener {
    
    private static final Logger log = Logger.getLogger(PhasesListener.class.getName());
    
    @Override
    public void afterPhase(PhaseEvent pe) {
        
    }
    
    @Override
    public void beforePhase(PhaseEvent pe) {
        try {
            HttpSession session = (HttpSession) pe.getFacesContext().getExternalContext().getSession(false);
            UserBean userBean = (UserBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userBean");
            Object count = pe.getFacesContext().getExternalContext().getRequestMap().get(UserBean.POLL_COUNT_PARAM);
            if (pe.getPhaseId().equals(PhaseId.RENDER_RESPONSE)) {
                if (count == null) {
                    if (userBean != null) {
                        userBean.setPollCount(0);
                    }
                } else {
                    int pollCount = userBean.getPollCount();
                    if (pollCount >= (session.getMaxInactiveInterval()/60)) {
                        //log.info("see timeout...");
                        RequestContext.getCurrentInstance().execute("PF('timeout').show();");
//                        session.invalidate();
                    }
                }
//                }
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Error", e);
        }
    }
    
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }
    
}
