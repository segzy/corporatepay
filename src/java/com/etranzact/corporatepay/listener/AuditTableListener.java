/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.listener;

import com.etranzact.corporatepay.app.LocationManager;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.AuditTable;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.envers.EntityTrackingRevisionListener;
import org.hibernate.envers.RevisionListener;
import org.hibernate.envers.RevisionType;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
//public class AuditTableListener implements RevisionListener {
public class AuditTableListener implements EntityTrackingRevisionListener {

    @Inject
    private LocationManager locationManager;

    private Logger log = Logger.getLogger(AuditTableListener.class.getName());

    @Override
    public void newRevision(Object o) {

        AuditTable auditTable = (AuditTable) o;
        log.info("new revision...");
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

            Approver loginUser = (Approver) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            auditTable.setUsername(loginUser.getId());

            auditTable.setUser((User) loginUser);
            if (loginUser instanceof BankUser) {
                auditTable.setUserType("Bank");
                auditTable.setUserOrg(((BankUser) loginUser).getBank().getBankName());
            } else if (loginUser instanceof CorporateUser) {
                auditTable.setUserType("Corporate");
                auditTable.setUserOrg(((CorporateUser) loginUser).getCorporate().getCorporateName());
            } else {
                auditTable.setUserType("Etranzact");
            }

        } catch (Exception e) {
            //e.printStackTrace();
        }

        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

            HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
            auditTable.setLocation(locationManager.retrieveDetails(request));
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void entityChanged(Class type, String string, Serializable srlzbl, RevisionType rt, Object o) {
        AuditTable auditTable = (AuditTable) o;
        auditTable.setRevisionType(rt.toString());
        auditTable.setEntityId(String.valueOf(srlzbl));
        String[] split = string.split("\\.");
        string = split[split.length - 1];
        auditTable.setEntityName(string);
        if (srlzbl instanceof String) {
            auditTable.setIdentityDataType(AppConstants.DATA_TYPE_STRING);
        } else if (srlzbl instanceof Long) {
            auditTable.setIdentityDataType(AppConstants.DATA_TYPE_LONG);
        } else if (srlzbl instanceof BigInteger) {
            auditTable.setIdentityDataType(AppConstants.DATA_TYPE_BIGINTEGER);
        }
    }

}
