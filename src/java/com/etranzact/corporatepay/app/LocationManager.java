/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.app;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Location;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Singleton
@Startup
public class LocationManager implements Serializable {
    private static final Logger logger = Logger.getLogger(LocationManager.class.getName());
    @PersistenceContext
    private EntityManager em;
    
    private DatabaseReader reader;
    private Map<String, String> bankMap = new HashMap<>();
    
    @PostConstruct
    public void init() {
        try {
            // A File object pointing to your GeoIP2 ISP database
            java.io.File database = new java.io.File("GeoLite2-City.mmdb");
            logger.log(Level.INFO, " database path ::::::::: {0} :::::: {1}", new Object[]{database.getPath(), database.getAbsolutePath()});
            reader = new DatabaseReader.Builder(database).build();
            logger.log(Level.INFO, "after loading information .......... {0}", reader);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            List<Bank> issuerList = getEm().createQuery("select i from Bank i").getResultList();

            for (Bank param : issuerList) {
                //log.info(" adding issuer :::::: " + param.getDbIp() + " name " + param.getIssuerName() );
                bankMap.put(param.getBankIp(), param.getBankName());
            }

            logger.log(Level.INFO, "bank size information .......... {0}", bankMap.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Location retrieveDetails(HttpServletRequest request) {
        Location location = new Location();

        try {
            String remoteIp = "";
            
            try {
                remoteIp = request.getHeader("X-Forwarded-For");
                if (remoteIp == null || (request != null && remoteIp.trim().equals(""))) {
                    remoteIp = request.getRemoteAddr();
                }

                location.setIp(remoteIp);

                if (StringUtils.isNotBlank(remoteIp) && StringUtils.contains(remoteIp, ",")) {
                    String ipCheck = remoteIp;
                    remoteIp = StringUtils.substringBefore(ipCheck, ",");

                    try {
                        String ip = StringUtils.substringAfterLast(ipCheck, ",");
                        String bankName = bankMap.get(StringUtils.trim(ip));
                        location.setCity(bankName);
                        location.setCountry("Nigeria");
                    } catch (Exception e) {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                remoteIp = "";
            }

            CityResponse response = reader.city(InetAddress.getByName(StringUtils.trim(remoteIp)));

            try {
                if (StringUtils.isNotBlank(location.getCountry())) {
                    location.setCountry(StringUtils.join(new String[]{location.getCountry(), ", ", response.getCountry().getName()}));
                } else {
                    location.setCountry(response.getCountry().getName());
                }
            } catch (Exception e) {
            }
            
            try {
                if (StringUtils.isNotBlank(location.getCity())) {
                    location.setCity(StringUtils.join(new String[]{location.getCity(), ", ", response.getMostSpecificSubdivision().getName()}));
                } else {
                    location.setCity(response.getMostSpecificSubdivision().getName());
                }
            } catch (Exception e) {
            }
            try {
                location.setLatitude(response.getLocation().getLatitude());
            } catch (Exception e) {
            }
            try {
                location.setLongitude(response.getLocation().getLongitude());
            } catch (Exception e) {
            }
            
        } catch (com.maxmind.geoip2.exception.AddressNotFoundException ae) {
            logger.info("Cannot get location " + ae.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * @return the em
     */
    public EntityManager getEm() {
        return em;
    }
    
    
    
}
