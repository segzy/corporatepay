package com.etranzact.corporatepay.util.encryption;

import java.lang.annotation.*;

/**
 * Created by Emmanuel.Kpoudosu on 9/4/2014.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Encrypted {
    Class encryptor() default BasicCrypto.class;
}
