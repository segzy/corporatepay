package com.etranzact.corporatepay.util.encryption;

import javax.ejb.EJB;
import javax.ejb.Startup;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.util.text.BasicTextEncryptor;



/**
 * Created by kpoudosu on 8/5/14.
 */
//@Name("basicCrypto")
//@AutoCreate
//@EJB
//@Startup
public class BasicCrypto implements Encryptor {
    private static BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
    private final static String ENCRYPTION_PREFIX = "{ETZ}";

    public BasicCrypto () {
        textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("etr1235680@#$");
    }
    /*static {
        textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("etr1235680@#$");
    }*/

    @Override
    public String encrypt(String clearData) {
        if ( StringUtils.isEmpty(clearData) || clearData.startsWith(ENCRYPTION_PREFIX)) {
            return clearData;
        }

        clearData = textEncryptor.encrypt(clearData);

        return StringUtils.join(new String[] {ENCRYPTION_PREFIX, clearData});
    }

    @Override
    public String decrypt(String obfuscatedData) {

        if ( StringUtils.isEmpty(obfuscatedData) || !obfuscatedData.startsWith(ENCRYPTION_PREFIX)) {
            return obfuscatedData;
        }

        obfuscatedData = StringUtils.substringAfter(obfuscatedData, ENCRYPTION_PREFIX);

        return textEncryptor.decrypt(obfuscatedData);
    }

   /* public static String textEncrypt(String text) {
        String result = "";
        try {
            result = textEncryptor.encrypt(text);
        }  catch (Exception e) {
            result = text;
            //e.printStackTrace();
        }

        return result;
    }

    public static String textDecrypt(String text) {
        String result = "";
        try {
            result = textEncryptor.decrypt(text);
        }  catch (Exception e) {
            result = text;
            //e.printStackTrace();
        }
        return result;
    }*/

   public static void main(String[] args) {
       BasicCrypto crypto = new BasicCrypto();
        String email = "0";
        String encEmail = crypto.encrypt(email);
        System.out.println(" encEmail " + encEmail);
        email = crypto.decrypt(encEmail);
        System.out.println(" email " + email);

    }


}
