package com.etranzact.corporatepay.util.encryption;

/**
 * Created by Emmanuel.Kpoudosu on 9/4/2014.
 */
public interface Encryptor {
    String encrypt(String s);
    String decrypt(String s);
}
