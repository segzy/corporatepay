/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
public interface AppConstants {

    public static final String LOGIN_USER = "loginUser";
     public static final String ALL_LOGIN_USER = "allLoginUser";
    public static final String LOGIN_PAGE = "/login.cp";
    public static final String DASHBOARD_PAGE = "/dashboard";
    public static final String PASSWORD_CHANGE_PAGE = "/changepassword";
    public static final String TOKEN_LOGIN_PAGE = "/vtoken";

    //flag statuses
    public static final String CREATED = "CR";
    public static final String MODIFIED = "MD";
    public static final String APPROVED = "AP";
    public static final String OPEN = "OP";
    public static final String REJECTED = "RJ";
    public static final String CREATED_REJECTED = "CRJ";
    public static final String MODIFIED_REJECTED = "MRJ";
    public static final String CREATED_IN_APPROVAL = "CRAPP";
    public static final String PAYROLL_IN_PROGRESS = "PIP";
    public static final String PAYROLL_GENERATED = "PG";
    public static final String PAYROLL_ROLLEDBACK = "PRB";
    public static final String PAYROLL_IN_APPROVAL = "PIP";
    public static final String PAYROLL_APPROVED = "PA";
    public static final String CREATED_NOT_SETUP = "CNS";
    public static final String SET_UP_IN_PROGRESS = "CSIP";
    public static final String SETUP_COMPLETED = "SC";

    public static final String CREATED_TEXT = "CREATED";
    public static final String MODIFIED_TEXT = "MODIFIED";
    public static final String APPROVED_TEXT = "APPROVED";
    public static final String REJECTED_TEXT = "REJECTED";
    public static final String CREATED_REJECTED_TEXT = "CREATED BUT REJECTED";
    public static final String MODIFIED_REJECTED_TEXT = "MODIFIED BUT REJECTED";
    public static final String CREATED_IN_APPROVAL_TEXT = "IN APPROVAL PROCESS";
    public static final String PAYROLL_IN_PROGRESS_TEXT = "PAYROLL IN PROGRESS";
    public static final String PAYROLL_GENERATED_TEXT = "PAYROLL GENERATED";
    public static final String PAYROLL_ROLLEDBACK_TEXT = "PAYROLL ROLLED BACK";
    public static final String PAYROLL_IN_APPROVAL_TEXT = "IN APPROVAL PROCESS";
    public static final String PAYROLL_APPROVED_TEXT = "PAYROLL APPROVED";
    public static final String CREATED_NOT_SETUP_TEXT = "CREATED NOT SETUP";
    public static final String SET_UP_IN_PROGRESS_TEXT = "SETUP IN PROGRESS";
    public static final String SETUP_COMPLETED_TEXT = "SETUP COMPLETED";
    public static final String KEPT_IN_VIEW = "KIV";
    public static final String KEPT_IN_VIEW_TEXT = "Kept In View";
    public static final String OPEN_TEXT = "Open";
    public static final String CLOSED_TEXT = "Closed";
    public static final String CLOSED = "CL";

    //accesstypes
    public static final String ADMINISTRATOR_ACCESS = "ADA";
    public static final String BANK_ADMINISTRATOR_ACCESS = "BADA";
    public static final String CORPORATE_ADMINISTRATOR_ACCESS = "CADA";
    public static final String ADMINISTRATOR_BANKADMINISTRATOR_ACCESS = "ADA_BADA";
    public static final String ADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS = "ADA_CADA";
    public static final String BANKADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS = "BADA_CADA";
    public static final String ALL_ROLE_TYPE_ACCESS = "ARTA";
    public static final String NO_ACCESS = "NA";

    //transactiontypes
    public static final String SALARY_PAYMENT_TRANSACTION_TYPE = "SPTT";
    public static final String COMPLETE_SALARY_PAYMENT_TRANSACTION_TYPE = "CSPTT";
    public static final String VENDOR_PAYMENT_TRANSACTION_TYPE = "VPTT";
    public static final String PENSION_PAYMENT_TRANSACTION_TYPE = "PPTT";
    public static final String TAX_PAYMENT_TRANSACTION_TYPE = "TPTT";
    public static final String PAYROLL_PAYMENT_TRANSACTION_TYPE = "PRPTT";
    public static final String SMS_CHARGE_TRANSACTION_TYPE = "SCTT";
    public static final String INIDIVIDUAL_TRANSACTION_TYPE = "INTT";

    //task access level
    public static final String USER_TASK_TYPE = "User";
    public static final String GROUP_TASK_TYPE = "Group";

    //task access level
    public static final String AMOUNT_TYPE_FIXED = "FX";
    public static final String AMOUNT_TYPE_PERCENT = "PC";

    //workflow requests
    public static final String REQUEST_CREATION_APPROVAL = "RCA";
    public static final String REQUEST_MODIFIED_APPROVAL = "RMA";
    public static final String REQUEST_APPROVAL = "RA";

    //turn on and off
    public static final String TURNED_ON = "1";
    public static final String TURNED_OFF = "0";

    //inmail codes
    public static final String NEW_USER_MESSAGE_CODE = "001";

    //inmail subject
    public static final String NEW_USER_MESSAGE_SUBJECT = "Welcome";

    public static final String ETZ_COMMISSION_TYPE = "Etranzact";
    public static final String BANK_COMMISSION_TYPE = "Bank";
    public static final String THIRDPARTY_COMMISSION_TYPE = "ThirdParty";
    public static final String THIRDPARTY_COMMISSION_TYPE2 = "ThirdParty2";

    //PAYMENT STATUSES 
    public static final String PROCESSING = "PR";
    public static final String PROCESSED_WITH_ERRORS = "PRE";
    public static final String PROCESSED_OK = "PROK";

    public static final String PROCESSING_TEXT = "PROCESSING";
    public static final String PROCESSING_WITH_ERRORS_TEXT = "PROCESSED WITH ERRORS";
    public static final String PROCESSING_OK_TEXT = "PROCESSED OK";

    //TRANSACTION STATUSES 
    public static final String TRANSACTION_CREATED_WITH_ERROR = "0";
    public static final String TRANSACTION_CREATED_OK = "1";
    public static final String TRANSACTION_PENDING_APPROVAL = "2";
    public static final String TRANSACTION_PENDING_PAYMENT = "3";
    public static final String TRANSACTION_PAID = "4";

    public static final String SWITH_TRANSACTION_SUCCESSFUL1 = "00";
    public static final String SWITH_TRANSACTION_SUCCESSFUL2 = "000";
    public static final String SWITH_TRANSACTION_SUCCESSFUL3 = "0";

    public static final String TRANSACTION_FAILED = "5";
    public static final String TRANSACTION_REVERSED = "6";
    public static final String TRANSACTION_WAITING = "7";
    public static final String TRANSACTION_PROCESSING = "8";
    public static final String TRANSACTION_STATUS_UNKNOWN = "9";

    public static final String PAYMENT_HELD = "PH";
    public static final String PAYMENT_CLOSE = "PC";
    public static final String PAYMENT_HELD_TEXT = "PROCESSED OK"/**
             * "PAYMENT IN HOLDING"*
             */
            ;
    public static final String PAYMENT_CLOSE_TEXT = "PAYMENT CLOSED";

    public static final String TRANSACTION_CREATED_WITH_ERROR_TEXT = "Transaction Created with Errors";
    public static final String TRANSACTION_CREATED_OK_TEXT = "Transactions Created";
    public static final String TRANSACTION_PENDING_APPROVAL_TEXT = "Transactions Sent for Approval";
    public static final String TRANSACTION_PENDING_PAYMENT_TEXT = "Payment in Progress";
    public static final String TRANSACTION_PAID_TEXT = "Transaction Successful";
    public static final String TRANSACTION_FAILED_TEXT = "Transaction Failed/Pending";
    public static final String TRANSACTION_REVERSED_TEXT = "Transaction Reversed";
    public static final String TRANSACTION_STATUSUNKNOWN_TEXT = "Transaction Status Unknown";

    public static final String NUMBER_CELL = "NUMBER";
    public static final String TEXT_CELL = "TEXT";
    public static final String EMAIL_CELL = "EMAIL";
    public static final String DECIMAL_CELL = "DECIMAL";
    public static final String CHARACTER_CELL = "CHARACTER";

    //chart and reports
    public static final String OVERALL_SUBSCRIBER_USERS = "Users by Bank";
    public static final String BANK_SUBSCRIBER_USERS = "Users by Corporate";

    public static final String BANK_ACCOUNT_TYPE = "B";
    public static final String POCKET_MONEY_ACCOUNT_TYPE = "P";
    public static final String ETZ_CARD_ACCOUNT_TYPE = "E";

    public static final String COMPLETE_PAYROLL = "PC";
    public static final String PAYROLL_LITE = "PL";

    public static final String STATUTORY_DEDUCTION = "SD";
    public static final String STATUTORY_ADDITION = "SA";
    public static final String ONE_TIME_DEDUCTION = "OD";
    public static final String RECURRING_DEDUCTION = "RD";
    public static final String LOAN = "LN";
    public static final String ONE_TIME_ADDITION = "OA";
    public static final String RECURRING_ADDITION = "RA";
    public static final String TERMINATE_WORKER = "TW";
    public static final String SUSPEND_WORKER = "SW";
    public static final String REINSTATE_WORKER = "RW";

    public static final int ADDITIVE_PAYCOMPONENT = 1;
    public static final int DEDUCTIVE_PAYCOMPONENT = 2;

    //PAYROLL SALARY PROCESSING EXCEPTION CODE
    public static final String NO_SALARY_POISTION = "001";
    public static final String SALARY_POISTION_NOT_SETUP = "002";
    public static final String GROSS_SALARY_NOT_FOUND = "003";
    public static final String NEGATIVE_OR_ZERO_NET_SALARY = "004";

    public static final String TSS_SETTLEMENT = "TS";
    public static final String BANK_COMMISION_SETTLEMENT = "BS";
    public static final String ETZ_COMMISION_SETTLEMENT = "ES";
    public static final String THIRDPARTY_COMMISION_SETTLEMENT = "TPS";
    public static final String THIRDPARTY_COMMISION_SETTLEMENT2 = "TPS2";

    public static final String CHARGE_BEARER_BENEFICIARY = "1";
    public static final String CHARGE_BEARER_PAYER = "2";

    public static final String REPORT_TRANS_INFO_PDF = "pdfTransRep";
    public static final String REPORT_TRANS_INFO_EXCEL = "excelTransRep";
    public static final String REPORT_TRANS_INFO_EXCEL2 = "excelTransRep2";

    public static final String REPORT_SETTLEMENT_INCOMING = "IS";
    public static final String REPORT_SETTLEMENT_OUTGOING = "OS";
    public static final String REPORT_SETTLEMENT_SAME = "SBS";

    public static final String DATA_TYPE_LONG = "Long";
    public static final String DATA_TYPE_BIGINTEGER = "BigInteger";
    public static final String DATA_TYPE_STRING = "String";

    public static final String AUDIT_TYPE_LOGIN = "Login";
    public static final String AUDIT_TYPE_LOGOUT = "Logout";
    public static final String AUDIT_TYPE_MD = "Modification";
    public static final String AUDIT_TYPE_CR = "Creation";
    public static final String AUDIT_TYPE_LOGIN_ATTEMPT = "Login Attempt";
    public static final String AUDIT_TYPE_PAYMENT_UPLOAD = "Payment Initiation";
    public static final String AUDIT_TYPE_PAYMENT_AUTHORIZATION = "Payment Authorization";
}
