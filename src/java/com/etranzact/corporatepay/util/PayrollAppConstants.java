/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
public interface PayrollAppConstants extends AppConstants{
   
    
    // deduction types
          public static final String ONEOFF_DEDUCTION_TYPE = "ODT";
      public static final String RECURING_DEDUCTION_TYPE = "RDT";
       public static final String LOAN_DEDUCTION_TYPE = "LDT";     
              
              
    
}
