/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;

import java.util.logging.Level;
import java.util.logging.Logger;
    import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Oluwasegun.Idowu
 */
public class DESEncrypter {

  private final Cipher ecipher;

  private final Cipher dcipher;
  
  private static DESEncrypter dESEncrypter;

 private DESEncrypter() throws Exception {
          SecretKey key = KeyGenerator.getInstance("DES").generateKey();
    ecipher = Cipher.getInstance("DES");
    dcipher = Cipher.getInstance("DES");
    ecipher.init(Cipher.ENCRYPT_MODE, key);
    dcipher.init(Cipher.DECRYPT_MODE, key);
  }
 
 public static DESEncrypter instance() {
     if(dESEncrypter==null) {
         try {
             dESEncrypter = new DESEncrypter();
         } catch (Exception ex) {
             Logger.getLogger(DESEncrypter.class.getName()).log(Level.SEVERE, null, ex);
         }  
     }
     return dESEncrypter;
 }

  public String encrypt(String str) throws Exception {
    // Encode the string into bytes using utf-8
    byte[] utf8 = str.getBytes("UTF8");

    // Encrypt
    byte[] enc = ecipher.doFinal(utf8);

    // Encode bytes to base64 to get a string
    return new  Base64().encodeAsString(enc);
  }

  public String decrypt(String str) throws Exception {
    // Decode base64 to get bytes
    byte[] dec = new  Base64().decode(str);

    byte[] utf8 = dcipher.doFinal(dec);

    // Decode using utf-8
    return new String(utf8, "UTF8");
  }

    public static void main(String[] args) {
      try {
          String dd= "{\"username\":\"desmondtutu@etranzact.com\",\"passwordHash\":\"CB8703D326F31E7500BA096A4FA21BA8\",\"securityKey\":\"\",\"firstName\":\"desmond\",\"lastName\":\"tutu\",\"email\":\"id.segzy@gmail.com\",\"mobilePhone\":\"4858686868\",\"changePassword\":false,\"passwordExpired\":false,\"passwordLockDate\":\"Apr 15, 2015 9:11:14 AM\",\"passwordDateExpired\":\"Oct 15, 2015 9:23:33 AM\",\"lastAccessTime\":\"Sep 16, 2015 11:52:45 AM\",\"userDisabled\":false,\"userLocked\":false,\"passwordMissed\":0,\"monday\":\"1\",\"tmonday\":false,\"tuesday\":\"1\",\"ttuesday\":false,\"wednesday\":\"1\",\"twednesday\":false,\"thursday\":\"1\",\"tthursday\":false,\"friday\":\"1\",\"tfriday\":false,\"saturday\":\"1\",\"tsaturday\":false,\"sunday\":\"1\",\"tsunday\":false,\"fromTimeAccess\":\"Apr 15, 2015 9:11:14 AM\",\"toTimeAccess\":\"Apr 15, 2016 9:11:14 AM\",\"active\":true,\"flagStatus\":\"AP\",\"createdDate\":\"Apr 15, 2015 9:11:14 AM\",\"central\":true,\"resetRequestTime\":\"Sep 17, 2015 10:31:42 AM\",\"id\":1}";
         String encrypt = DESEncrypter.instance().encrypt(dd);
         System.out.println("enc: " + encrypt);
         // String ee = "Xo8dFAlHUD4lre1RfeG+/ykj2BLh/8X905GnP1Z6WSZYbfhPmoK6wzCrVe0KsqHeciXueXOPw8jG8ltc+cRuJ2YPt8+ony4VVGl5xlVUakL8+WY7XTrBVI4V6m7Eaz2fbAXOMpqYiKXBup3oIW+mH8d2KieqHk6u/QfmcJ7vU/RmA9YAwi/yFaPCAd3KiZhDKYqCkLQan3dln8TbmbB3Pj66SLWYplxnTgmKK23+zWuGMS2Ups+pDeXCa+/WxjnZIVVpsNPIzNCa74YzQUbDsacOY9pFcuBE15mT1JJwUzScy7As1hqqN9jRrOZl4WQ/mrxPlrEDYICcy7As1hqqN6c3iTr5PeqBUH5IttCbS/hAwLTyo6VD3NyEqL3AuzsHQfbtzoY9IY2B/bw2btEzrI2ry4hht8kubeXCYn4oYJ63piy4NsK/w75YvOV19o2eT7XGB3/2dJd1vOBfbHRFPNmc2kPwNg1F0kvtTgJFYmOFFRDo5S4EIeXKtyNGiIQFjfyG7sCUZdOjWjBD44L7jTxZLWXpZfOTf4g/FeJpzIdMpXyeXKxyNm0wLZ5YPEi4ncypVGa5sP5wpTs6IqAPkDCEiQKyCfQUmL+9/bL1lJekomY/WNrhswpnwqfUrqG8hX8T44WWnXQOB0i0ddVv+8+3NiLGzcsWWa2S8jBSOCI30X6Btg1Z7I3t0u6Mgt91ZhHUK52aqAWu2ExCmacsjJx7o358SYjx0swJvWSWa9ace6N+fEmI8V3XVSVnFTlpMyzVckgNMxb1VHu6+qdQe6SiZj9Y2uGznDuXBh7X7MBb4QgTTr51Q6e2wxf47Q4J7lejf40J1bxkcak/WpmxgIV/E+OFlp10lkl4PZyvo5RNmf2CmofY4AY0dCQVC0zgmDkn5CqA0jUyjd4x2OhTNrvwyiPHlNjsJI858/+3qnPUyacbLoNynQZY5wrn+5OWePbRuRIxaqr+9/vUp7gjN/3LvND7dq11rfZ1ZuQ+WfCBuP42neyS4TwdHNFgTcurfBG9UC+/Dwok1dHkfwrwDJOSF2ECx8OrBSpOXipT4wpzPku7twjJff3LvND7dq119dl7DsQK4uzaBkFmQYqOeQbNZCGcDTQGQMx+hbQczZW2R0qN9rMGShR+hMkgg/Vmtx7LHx/dCo1eyXVpdCGgFexCeB+YX8biWSD4C0T1uWA=";
         // ee = "Xo8dFAlHUD4lre1RfeG /ykj2BLh/8X905GnP1Z6WSZYbfhPmoK6wzCrVe0KsqHeciXueXOPw8jG8ltc cRuJ2YPt8 ony4VVGl5xlVUakL8 WY7XTrBVI4V6m7Eaz2fbAXOMpqYiKXBup3oIW mH8d2KieqHk6u/QfmcJ7vU/RmA9YAwi/yFaPCAd3KiZhDKYqCkLQan3dln8TbmbB3Pj66SLWYplxnTgmKK23 zWuGMS2Ups pDeXCa /WxjnZIVVpsNPIzNCa74YzQUbDsacOY9pFcuBE15mT1JJwUzScy7As1hqqN9jRrOZl4WQ/mrxPlrEDYICcy7As1hqqN6c3iTr5PeqBUH5IttCbS/hAwLTyo6VD3NyEqL3AuzsHQfbtzoY9IY2B/bw2btEzrI2ry4hht8kubeXCYn4oYJ63piy4NsK/w75YvOV19o2eT7XGB3/2dJd1vOBfbHRFPNmc2kPwNg1F0kvtTgJFYmOFFRDo5S4EIeXKtyNGiIQFjfyG7sCUZdOjWjBD44L7jTxZLWXpZfOTf4g/FeJpzIdMpXyeXKxyNm0wLZ5YPEi4ncypVGa5sP5wpTs6IqAPkDCEiQKyCfQUmL 9/bL1lJekomY/WNrhswpnwqfUrqG8hX8T44WWnXQOB0i0ddVv 8 3NiLGzcsWWa2S8jBSOCI30X6Btg1Z7I3t0u6Mgt91ZhHUK52aqAWu2ExCmacsjJx7o358SYjx0swJvWSWa9ace6N fEmI8V3XVSVnFTlpMyzVckgNMxb1VHu6 qdQe6SiZj9Y2uGznDuXBh7X7MBb4QgTTr51Q6e2wxf47Q4J7lejf40J1bxkcak/WpmxgIV/E OFlp10lkl4PZyvo5RNmf2CmofY4AY0dCQVC0zgmDkn5CqA0jUyjd4x2OhTNrvwyiPHlNjsJI858/ 3qnPUyacbLoNynQZY5wrn 5OWePbRuRIxaqr 9/vUp7gjN/3LvND7dq11rfZ1ZuQ WfCBuP42neyS4TwdHNFgTcurfBG9UC /Dwok1dHkfwrwDJOSF2ECx8OrBSpOXipT4wpzPku7twjJff3LvND7dq119dl7DsQK4uzaBkFmQYqOeQbNZCGcDTQGQMx hbQczZW2R0qN9rMGShR hMkgg/Vmtx7LHx/dCo1eyXVpdCGgFexCeB YX8biWSD4C0T1uWA=";
          //ee="Xo8dFAlHUD4lre1RfeG+/ykj2BLh/8X905GnP1Z6WSZYbfhPmoK6wzCrVe0KsqHeciXueXOPw8jG8ltc+cRuJ2YPt8+ony4VVGl5xlVUakL8+WY7XTrBVI4V6m7Eaz2fbAXOMpqYiKXBup3oIW+mH8d2KieqHk6u/QfmcJ7vU/RmA9YAwi/yFaPCAd3KiZhDKYqCkLQan3dln8TbmbB3Pj66SLWYplxnTgmKK23+zWuGMS2Ups+pDeXCa+/WxjnZIVVpsNPIzNCa74YzQUbDsacOY9pFcuBE15mT1JJwUzScy7As1hqqN9jRrOZl4WQ/mrxPlrEDYICcy7As1hqqN6c3iTr5PeqBUH5IttCbS/hAwLTyo6VD3NyEqL3AuzsHQfbtzoY9IY2B/bw2btEzrI2ry4hht8kubeXCYn4oYJ63piy4NsK/w75YvOV19o2eT7XGB3/2dJd1vOBfbHRFPNmc2kPwNg1F0kvtTgJFYmOFFRDo5S4EIeXKtyNGiIQFjfyG7sCUZdOjWjBD44L7jTxZLWXpZfOTf4g/FeJpzIdMpXyeXKxyNm0wLZ5YPEi4ncypVGa5sP5wpTs6IqAPkDCEiQKyCfQUmL+9/bL1lJekomY/WNrhswpnwqfUrqG8hX8T44WWnXQOB0i0ddVv+8+3NiLGzcsWWa2S8jBSOCI30X6Btg1Z7I3t0u6Mgt91ZhHUK52aqAWu2ExCmacsjJx7o358SYjx0swJvWSWa9ace6N+fEmI8V3XVSVnFTlpMyzVckgNMxb1VHu6+qdQe6SiZj9Y2uGznDuXBh7X7MBb4QgTTr51Q6e2wxf47Q4J7lejf40J1bxkcak/WpmxgIV/E+OFlp10lkl4PZyvo5RNmf2CmofY4AY0dCQVC0zgmDkn5CqA0jUyjd4x2OhTNrvwyiPHlNjsJI858/+3qnPUyacbLoNynQZY5wrn+5OWePbRuRIxaqr+9/vUp7gjN/3LvND7dq11rfZ1ZuQ+WfCBuP42neyS4TwdHNFgTcurfBG9UC+/Dwok1dHkfwrwDJOSF2ECx8OrBSpOXipT4wpzPku7twjJff3LvND7dq119dl7DsQK4uzaBkFmQYqOeQbNZCGcDTQGQMx+hbQczZW2R0qN9rMGShR+hMkgg/Vmtx7LHx/dCo1eyXVpdCGgFexCeB+YX8biWSD4C0T1uWA=";
          String decrypt = DESEncrypter.instance().decrypt(encrypt);
          
          System.out.println("dec: " + decrypt);
      } catch (Exception ex) {
          Logger.getLogger(DESEncrypter.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

}
