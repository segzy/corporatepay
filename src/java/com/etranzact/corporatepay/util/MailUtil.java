/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;

import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etz.mail.util.MailSender;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.util.ByteArrayDataSource;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
public class MailUtil {

    private static MailUtil mailUtil;
    Logger log = Logger.getLogger(MailUtil.class.getName());

    public static MailUtil getInstance() {
        if (mailUtil == null) {
            mailUtil = new MailUtil();
        }
        return mailUtil;
    }

    public void sendWelcomeMail(User benficiary) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String[] recepients = new String[1];
        recepients[0] = benficiary.getEmail();
        String msg = mailProperties.getProperty("mail.body.welcome");
        String subj = mailProperties.getProperty("mail.subj.welcome");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(),
            benficiary.getUsername(), benficiary.getPassword(), benficiary.getRole().getRoleName(),
            ConfigurationUtil.instance().getPassowrdExpirationPeriod()});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.addAttachment(ConfigurationUtil.instance().getGettingStarted());
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendEditMail(Serializable ser, String[] recepients, String identity, String... blindCopy) throws Exception {
        log.info("sending edit mail....");
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.edit");
        String subj = mailProperties.getProperty("mail.subj.edit");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName(), getPropertyDiff(ser), identity});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setBccRecipients(blindCopy);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendThresholdAlert(String[] recepients, String paymentId, String authorizer, String corporate, String transAmount, String transCount, String debitCard) throws Exception {
        log.info("sending threshold mail....");
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.payment.above.treshold");
        String subj = mailProperties.getProperty("mail.subj.payment.above.treshold");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), paymentId});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), paymentId, authorizer, corporate, transAmount, transCount, debitCard});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendPaymentApproved(String[] recepients, String[] brecepients, String paymentId, String authorizer, String corporate, String transAmount, String transCount, String contact, String cardNum, String payDesc, String authtime) throws Exception {
        log.info("sending payment approved mail....");
        cardNum = getMaskCard(cardNum);
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.payment.approved");
        String subj = mailProperties.getProperty("mail.subj.payment.approved");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), "08" + paymentId});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), "08" + paymentId, authorizer, corporate, transAmount, transCount, contact, cardNum, payDesc, authtime});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setCcRecipients(brecepients);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendPaymentFailed(String[] recepients, String[] brecepients, String paymentId, String authorizer, String corporate, String transAmount, String transCount, String contact, String response, String cardNum, String payDesc, String authtime) throws Exception {
        log.info("sending payment failed mail....");
         cardNum = getMaskCard(cardNum);
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.payment.failed");
        String subj = mailProperties.getProperty("mail.subj.payment.failed");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), "08" + paymentId});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), "08" + paymentId, authorizer, corporate, transAmount, transCount, contact, response, cardNum, payDesc, authtime});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setCcRecipients(brecepients);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendEditMail2(ApprovalTask approvalTask, Serializable ser, String[] recepients, String identity, String... blindCopy) throws Exception {
        log.info("sending edit mail....");
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.edit");
        String subj = mailProperties.getProperty("mail.subj.edit");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName(), getProperty(approvalTask), identity});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setBccRecipients(blindCopy);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendCreationMail(Serializable ser, String[] recepients, String identity, String... blindCopy) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.creation");
        String subj = mailProperties.getProperty("mail.subj.creation");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName(), getProperty(ser), identity});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setBccRecipients(blindCopy);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }
    
        public void sendEtzCommissionExceptionMail(String[] recepients, String identity,String commissionAmount,String bsetupcommissionAmount,String bankName, String... copy) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.etzCommissionException");
        String subj = mailProperties.getProperty("mail.subj.etzCommissionException");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(),identity});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), identity, commissionAmount,bsetupcommissionAmount,bankName});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setCcRecipients(copy);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    log.info("sending mail.....");
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }
        
                public void sendMFBTransactionMail(String[] recepients, String identity,String affliateBankName,String affliateBankAccount,Integer noofTransaction,String totalAmount,byte[] attachement, String... copy) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.mfbTransactionMail");
        String subj = mailProperties.getProperty("mail.subj.mfbTransactionMail");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(),affliateBankName + " (" + affliateBankAccount + ")"});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), identity, affliateBankName,affliateBankAccount,noofTransaction,totalAmount});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setBccRecipients(copy);
        mailSender.addAttachment(new ByteArrayDataSource(attachement, "application/vnd.ms-excel"));
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendTaskAlert(Approver[] prevApprovers, Approver approver, Payment payment) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.task.alert");
        String subj = mailProperties.getProperty("mail.subj.task.alert");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        String[] recepients = null;
        String taskTo = null;
        if (approver instanceof User) {
            recepients = new String[1];
            recepients[0] = ((User) approver).getEmail();
            taskTo = ((User) approver).getFirstName() + " " + ((User) approver).getLastName();
        } else if (approver instanceof ApprovalGroup) {
            ApprovalGroup approvalGroup = ((ApprovalGroup) approver);
            taskTo = approvalGroup.getGroupAlias();
            Set<User> users = approvalGroup.getUsers();
            recepients = new String[users.size()];
            int i = 0;
            for (User user : users) {
                recepients[i] = user.getEmail();
                i++;
            }
        }
        String prevApproversStr = "";
        int i = 0;
        if (prevApprovers != null) {
            for (Approver a : prevApprovers) {
                prevApproversStr += a.getIdentity();
                i++;
                if (i != prevApprovers.length) {
                    prevApproversStr += ";";
                }
            }
        }

        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), "08" + payment.getId().toString()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), recepients != null && recepients.length == 1 ? "you" : "your group(" + taskTo + "", "08" + payment.getId().toString(), prevApproversStr, payment.getPaymentDescription() == null ? "" : payment.getPaymentDescription(), payment.getTotalAmountf(), payment.getTotalCount()});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendPasswordResetRequest(String[] recepients, String identity, String hashedUser) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        final String restUrl = ConfigurationUtil.instance().getApplicationPath() + "/reset?u=" + hashedUser;
        String msg = mailProperties.getProperty("mail.body.password.reset.request");
        String subj = mailProperties.getProperty("mail.subj.password.reset.request");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), identity, restUrl});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendPasswordReset(String[] recepients, String identity, String password) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.password.reset");
        String subj = mailProperties.getProperty("mail.subj.password.reset");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), identity, password});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        for (String rec : recepients) {
            System.out.println("recepientttt: " + rec);
        }
        mailSender.setToRecipients(recepients);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    public void sendCreationMail2(ApprovalTask approvalTask, Serializable ser, String[] recepients, String identity, String... blindCopy) throws Exception {
        final MailSender mailSender = new MailSender();
        final Properties mailProperties = ConfigurationUtil.instance().getMailProperties();
        String msg = mailProperties.getProperty("mail.body.creation");
        String subj = mailProperties.getProperty("mail.subj.creation");
        if (msg == null || subj == null) {
            throw new Exception("Mail not configured properly");
        }
        subj = MessageFormat.format(subj, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName()});
        msg = MessageFormat.format(msg, new Object[]{ConfigurationUtil.instance().getAppName(), ser.getClass().getSimpleName(), getProperty(approvalTask), identity});
        mailSender.setSubject(subj);
        mailSender.setHtmlMessage(msg);
        mailSender.setFrom(mailSender.getProtocolPropertyPrefix() + "user");
        mailSender.setToRecipients(recepients);
        mailSender.setBccRecipients(blindCopy);
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    mailSender.sendMail(mailProperties);
                } catch (Exception ex) {
                    Logger.getLogger(MailUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        thread.start();

    }

    private String getPropertyDiff(Serializable serializable) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Map<String, Object> describe = BeanUtils.describe(serializable);
        String htmlFrag = "";
        for (Entry<String, Object> entry : describe.entrySet()) {
            if (entry.getKey().contains("Tmp")) {
                continue;
            }
            String replaceFirst = String.valueOf(entry.getKey().charAt(0)).toUpperCase();
            String newV = null;
            try {
                newV = BeanUtils.getProperty(serializable, "tmp" + replaceFirst + entry.getKey().substring(1));
            } catch (NoSuchMethodException ex) {

            }
            String original = BeanUtils.getProperty(serializable, entry.getKey());
            if (newV == null || original == null) {
                continue;
            }

            if (!newV.equals(original)) {
                if (original.equals("1")) {
                    original = "Yes";
                }
                if (original.equals("0")) {
                    original = "No";
                }
                if (newV.equals("1")) {
                    newV = "Yes";
                }
                if (newV.equals("0")) {
                    newV = "No";
                }
                htmlFrag += "<span style=\"border: 1px solid #CCCCCC; font-family:Century Gothic,verdana;font-size: 14px;\"><b><span style=\" float:left\">" + entry.getKey() + "</span></b> : <span style=\" float:right\">" + original + " to " + newV + "</span></span><br>";
            }
        }
        return htmlFrag;
    }

    private String getProperty(Serializable serializable) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Map<String, Object> describe = BeanUtils.describe(serializable);
        String htmlFrag = "";
        for (Entry<String, Object> entry : describe.entrySet()) {
            if (entry.getKey().contains("Tmp") || entry.getKey().contains("stat")) {
                continue;
            }
            Class propertyType = PropertyUtils.getPropertyType(serializable, entry.getKey());
            if (!(propertyType.getSimpleName().equals(String.class.getSimpleName())
                    || propertyType.getSimpleName().equals(Integer.class.getSimpleName()))) {
                continue;
            }
            String original = BeanUtils.getProperty(serializable, entry.getKey());
            if (original != null && !original.isEmpty()) {
                if (original.equals("1")) {
                    original = "Yes";
                }
                if (original.equals("0")) {
                    original = "No";
                }
                htmlFrag += "<span style=\"border: 1px solid #CCCCCC; font-family:Century Gothic,verdana;font-size: 14px;\"><b><span style=\" float:left\">" + entry.getKey() + "</span></b> : <span style=\" float:right\">" + original + "</span></span><br>";
            }
        }
        return htmlFrag;
    }

    private String getProperty(ApprovalTask approvalTask) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String htmlFrag = "";
        for (TaskObjectNameValuePair nameValuePair : approvalTask.getNameValuePairs()) {
            htmlFrag += "<span style=\"border: 1px solid #CCCCCC; font-family:Century Gothic,verdana;font-size: 14px;\"><b><span style=\" float:left\">" + nameValuePair.getName() + "</span></b> : <span style=\" float:right\">" + nameValuePair.getTargetValue() + "</span></span><br>";
        }
        return htmlFrag;
    }

    private String getMaskCard(String cardNumber) {
//          String x = "1234567890123456";
         String output = "";
        try {
        char[] toCharArray = cardNumber.toCharArray();
       
        for (int i = 0; i < toCharArray.length; i++) {
            if (i < 5 || i > 10) {
                output += toCharArray[i];
            } else {
                output += "*";
            }
          
        }
        }catch(Exception e) {
            return output;
        }
          return output;
    }
}
