/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;

import com.etranzact.corporatepay.app.LocationManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author oluwasegun.idowu
 */
public class ConfigurationUtil {

    private static ConfigurationUtil util;
    private String applicationDir;
    private String uploadDir;
    private Properties mainProperties;
    private Properties mailProperties;
    private Properties etzErrorProperties;
    public final String CONFIG_DIR_NAME = "corporatepay\\config";
    private static final Logger logger = Logger.getLogger(ConfigurationUtil.class.getName());
    private String appName;
    private int loginRetrialAllowed = 3;
    private int passwordExpirationPeriod; //in months
    private int sessiontimeoutPollCountNo = 5;
    private File gettingStarted;
    private File sampleUploadFile;
    private File transactionPlainRep;
    private File corporatetransactionSummaryPlainRep;
    private File banktransactionSummaryPlainRep;
    private File bankTransactionPlainRep;
    private File corporateTransactionPlainRep;
    private File corporateTransactionReversalPlainRep;
    private File transactionIncomePlainRep;
    private File bankTransactionIncomePlainRep;
    private File transactionReceiptFile;
    private String autoSwitchIP;
    private int autoSwitchPort;
    private BigDecimal etzMinCommission;
      private BigDecimal etzMinPercentCommission;
    private String paymentServiceURL;
    private String applicationPath;
     private Boolean platformUserOnToken;

    public static ConfigurationUtil instance() {
        if (util == null) {
            util = new ConfigurationUtil();
        }
        return util;
    }

    public void init(ServletContext context) throws Exception {
        findHomeDirectory(context);
        loadProperties();
    }

    private void findHomeDirectory(ServletContext context) throws Exception {
        String configPath = context.getRealPath("/");
        int tmpIndex = configPath.lastIndexOf(File.separator + "tmp" + File.separator);
        if (tmpIndex > 0) {
            configPath = configPath.substring(0, tmpIndex) + File.separator + CONFIG_DIR_NAME;
        }
        gettingStarted = new File(configPath + File.separator + "getting_started.pdf");
        setSampleUploadFile(new File(configPath + File.separator + "upload_template.csv"));
        transactionPlainRep = new File(configPath + File.separator + "report" + File.separator + "transaction_plain.jasper");
        corporatetransactionSummaryPlainRep = new File(configPath + File.separator + "report" + File.separator + "corporate_transaction_summary_plain.jasper");
        banktransactionSummaryPlainRep = new File(configPath + File.separator + "report" + File.separator + "bank_transaction_summary_plain.jasper");
        bankTransactionPlainRep = new File(configPath + File.separator + "report" + File.separator + "bank_transaction_plain.jasper");
        corporateTransactionPlainRep = new File(configPath + File.separator + "report" + File.separator + "corporate_transaction_plain.jasper");
        corporateTransactionReversalPlainRep = new File(configPath + File.separator + "report" + File.separator + "transaction_reversal_plain.jasper");
        transactionIncomePlainRep = new File(configPath + File.separator + "report" + File.separator + "transaction_income_plain.jasper");
        bankTransactionIncomePlainRep = new File(configPath + File.separator + "report" + File.separator + "bank_transaction_income_plain.jasper");
        transactionReceiptFile = new File(configPath + File.separator + "report" + File.separator + "transaction_receipt.jasper");
        if (new File(configPath).isDirectory()) {
            applicationDir = configPath;
            logger.log(Level.INFO, "Application Directory {0}", applicationDir);
            setUploadDir(applicationDir + File.separator + "upload");
        } else {
            logger.log(Level.SEVERE, "Application Directory Not Found. Contact the Administrator");
            throw new Exception("Application Directory Not Found. Contact the Administrator");

        }
    }

    private void loadProperties() throws IOException {
        mainProperties = readConfig("main.properties");
        mailProperties = readConfig("mail.properties");
        etzErrorProperties = readConfig("etz-error.properties");
    }

    /**
     * @return the applicationDir
     */
    public String getApplicationDir() {
        return applicationDir;
    }

    /**
     * @return the mainProperties
     */
    public Properties getMainProperties() {
        return mainProperties;
    }

    /**
     * @return the mailProperties
     */
    public Properties getMailProperties() {
        return mailProperties;
    }

    private Properties readConfig(String configFile) throws IOException {
        File file = new File(applicationDir + File.separator + configFile);
        String[] split = configFile.split("\\.");
        String ext = split[split.length - 1];
        Properties properties = null;
        switch (ext) {
            case "properties":

                if (file.exists()) {
                    properties = new Properties();
                    properties.load(new FileInputStream(file));
                    return properties;
                }
                break;
        }

        return null;
    }

    public void releasePropertiesResource() {
        mainProperties = null;
        mailProperties = null;
    }

    /**
     * @return the loginRetrialAllowed
     */
    public int getLoginRetrialAllowed() {
        String property = mainProperties.getProperty("no-of-login-retrial-allowed", "3");
        try {
            loginRetrialAllowed = Integer.parseInt(property);
            return loginRetrialAllowed;
        } catch (NumberFormatException ex) {
            logger.log(Level.SEVERE, "no-of-login-retrial-allowed not set", ex);
            return loginRetrialAllowed;
        }
    }

    public int getPassowrdExpirationPeriod() {
        String property = mainProperties.getProperty("password-expiration-period", "6");
        try {
            passwordExpirationPeriod = Integer.parseInt(property);
            return passwordExpirationPeriod;
        } catch (NumberFormatException ex) {
            return passwordExpirationPeriod;
        }
    }

    /**
     * @return the appName
     */
    public String getAppName() {
        appName = mainProperties.getProperty("cp.app.name", "Corporate Pay");
        return appName;
    }

    /**
     * @param appName the appName to set
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }

    /**
     * @return the gettingStarted
     */
    public File getGettingStarted() {
        return gettingStarted;
    }

    /**
     * @return the uploadDir
     */
    public String getUploadDir() {
        return uploadDir;
    }

    /**
     * @param uploadDir the uploadDir to set
     */
    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }

    /**
     * @return the sessiontimeoutPollCountNo
     */
    public int getSessiontimeoutPollCountNo() {
        try {
            String value = mainProperties.getProperty("cp.session.timeout.poll.count", "5");
            this.sessiontimeoutPollCountNo = Integer.parseInt(value);
        } catch (Exception se) {
            return sessiontimeoutPollCountNo;

        }
        return sessiontimeoutPollCountNo;
    }

    /**
     * @return the transactionPlainRep
     */
    public File getTransactionPlainRep() {
        return transactionPlainRep;
    }

    public String getTransactionRepFileName(String type) {
        if (type == null) {
            return transactionPlainRep.getPath();
        } else if ("bank".equals(type)) {
            return bankTransactionPlainRep.getPath();
        } else {
            return corporateTransactionPlainRep.getPath();
        }
    }

    public String getTransactionReversalRepFileName(String type) {
        return corporateTransactionReversalPlainRep.getPath();

    }

    public String getTransactionSummaryRepFileName(String type) {
        if (type == null) {
            return corporatetransactionSummaryPlainRep.getPath();
        } else if ("bank".equals(type)) {
            return banktransactionSummaryPlainRep.getPath();
        } else {
            return corporatetransactionSummaryPlainRep.getPath();
        }

    }

    public String getTransactionIncomeRepFileName(String type) {
        if (type == null) {
            return transactionIncomePlainRep.getPath();
        } else {
            return bankTransactionIncomePlainRep.getPath();
        }

    }

    public static String formatAsAmount(String amount) {
        String newAmount;
        try {
            if (amount == null || amount.equals("null")) {
                return "-";
            }
            DecimalFormatSymbols format = DecimalFormatSymbols.getInstance();
            DecimalFormat formatter = new DecimalFormat("###,###.###", format);
            newAmount = formatter.format(Double.parseDouble(amount));
        } catch (NumberFormatException e) {
            return amount;
        }
        return newAmount;
    }

    /**
     * @return the autoSwitchIP
     */
    public String getAutoSwitchIP() {
        try {
            autoSwitchIP = mainProperties.getProperty("cp.auto.switch.ip", "127.0.0.1");
        } catch (Exception se) {
            return autoSwitchIP;

        }
        return autoSwitchIP;
    }

    /**
     * @param autoSwitchIP the autoSwitchIP to set
     */
    public void setAutoSwitchIP(String autoSwitchIP) {
        this.autoSwitchIP = autoSwitchIP;
    }

    /**
     * @return the autoSwitchPort
     */
    public int getAutoSwitchPort() {
        try {
            String value = mainProperties.getProperty("cp.auto.switch.port", "80");
            this.autoSwitchPort = Integer.parseInt(value);
        } catch (Exception se) {
            return autoSwitchPort;

        }
        return autoSwitchPort;
    }

    /**
     * @param autoSwitchPort the autoSwitchPort to set
     */
    public void setAutoSwitchPort(int autoSwitchPort) {
        this.autoSwitchPort = autoSwitchPort;
    }

    /**
     * @return the paymentServiceURL
     */
    public String getPaymentServiceURL() {
        try {
            paymentServiceURL = mainProperties.getProperty("cp.payment.service.url", "http://172.16.10.5:81/WebAccess/corporatePayService.js");
        } catch (Exception se) {
            return paymentServiceURL;

        }
        return paymentServiceURL;
    }

    /**
     * @param paymentServiceURL the paymentServiceURL to set
     */
    public void setPaymentServiceURL(String paymentServiceURL) {
        this.paymentServiceURL = paymentServiceURL;
    }

    /**
     * @return the applicationPath
     */
    public String getApplicationPath() {
        try {
            applicationPath = mainProperties.getProperty("cp.application.root.url", "http://127.0.0.1/CorporatePay");
        } catch (Exception se) {
            return applicationPath;

        }
        return applicationPath;
    }

    /**
     * @param applicationPath the applicationPath to set
     */
    public void setApplicationPath(String applicationPath) {
        this.applicationPath = applicationPath;
    }

    /**
     * @return the etzMinCommission
     */
    public BigDecimal getEtzMinCommission() {
        try {
            String com = mainProperties.getProperty("cp-etz-min-commission", "40");
            etzMinCommission = new BigDecimal(com);
        } catch (Exception se) {
            return etzMinCommission;

        }
        return etzMinCommission;
    }

    /**
     * @param etzMinCommission the etzMinCommission to set
     */
    public void setEtzMinCommission(BigDecimal etzMinCommission) {
        this.etzMinCommission = etzMinCommission;
    }

    /**
     * @return the transactionReceiptFile
     */
    public File getTransactionReceiptFile() {
        return transactionReceiptFile;
    }

    /**
     * @return the etzErrorProperties
     */
    public Properties getEtzErrorProperties() {
        return etzErrorProperties;
    }

    /**
     * @return the sampleUploadFile
     */
    public File getSampleUploadFile() {
        return sampleUploadFile;
    }

    /**
     * @param sampleUploadFile the sampleUploadFile to set
     */
    public void setSampleUploadFile(File sampleUploadFile) {
        this.sampleUploadFile = sampleUploadFile;
    }

    /**
     * @return the platformUserOnToken
     */
    public Boolean getPlatformUserOnToken() {
        Object val = mainProperties.get("platform-provider-use-token");
        if(val==null) {
            return Boolean.FALSE;
        } else {
            try {
                platformUserOnToken=  Boolean.parseBoolean((String) val);
            }catch(Exception e) {
                logger.log(Level.SEVERE, "Error", e);
                platformUserOnToken= Boolean.FALSE;
            }
        }
        return platformUserOnToken;
    }

    /**
     * @return the etzMinPercentCommission
     */
    public BigDecimal getEtzMinPercentCommission() {
             try {
            String com = mainProperties.getProperty("cp-etz-min-percent-commission", "40");
            etzMinPercentCommission = new BigDecimal(com);
        } catch (Exception se) {
            return etzMinPercentCommission;

        }
        return etzMinPercentCommission;
    }

    
    
}
