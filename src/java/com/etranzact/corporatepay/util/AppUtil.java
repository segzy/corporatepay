/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.util;

import com.etranzact.corporatepay.model.Corporate;
import com.etz.security.util.Cryptographer;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Oluwasegun.Idowu
 */
public class AppUtil {

    private static final Logger logger = Logger.getLogger(AppUtil.class.getSimpleName());

    public static String formatDate(Date date, String pattern) {

        SimpleDateFormat fm = new SimpleDateFormat(pattern);
        String sDate = fm.format(date);
        return sDate;
    }

    public static synchronized long generateRandomPin() {

        int START = 1000000;
        long END = 9999999L;

        return createRandomInteger(START, END);
    }

    public static synchronized long generateRandomPin2() {

        int START = 10;
        long END = 99L;

        return createRandomInteger(START, END);
    }

    private static long createRandomInteger(int aStart, long aEnd) {
        Random aRandom = new Random();
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = aEnd - (long) aStart + 1;
//        logger.log(Level.INFO, "range>>>>>>>>>>>{0}", range);
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long) (range * aRandom.nextDouble());
//        logger.log(Level.INFO, "fraction>>>>>>>>>>>>>>>>>>>>{0}", fraction);
        long randomNumber = fraction + (long) aStart;
//        logger.log(Level.INFO, "Generated : {0}", randomNumber);
        return randomNumber;
    }

    public static String formatAsAmount(String amount) {
        String newAmount;
        try {
            if (amount == null || amount.equals("null")) {
                return "-";
            }
            DecimalFormatSymbols format = DecimalFormatSymbols.getInstance();
            DecimalFormat formatter = new DecimalFormat("###,###.###", format);
            newAmount = formatter.format(Double.parseDouble(amount));
        } catch (NumberFormatException e) {
            return amount;
        }
        return newAmount;
    }

    public static String formatAsAmount(String amount, String pattern) {
        String newAmount;
        try {
            if (amount == null || amount.equals("null")) {
                return "-";
            }
            DecimalFormatSymbols format = DecimalFormatSymbols.getInstance();
            DecimalFormat formatter = new DecimalFormat(pattern, format);
            newAmount = formatter.format(Double.parseDouble(amount));
        } catch (NumberFormatException e) {
            return amount;
        }
        return newAmount;
    }

    public static void checkPasswordPolicy(String password) throws Exception {
        if (password == null) {
            throw new Exception("password not supplied");
        }
        boolean hasUppercase = !password.equals(password.toLowerCase());
        if (!hasUppercase) {
            throw new Exception("Password does not have an upper case character");
        }
        if (password.length() <= 8) {
            throw new Exception("Password cannot be less than 8 characters");
        }
        boolean hasSpecial = !password.matches("[A-Za-z0-9 ]*");
        if (hasSpecial) {
            throw new Exception("Special characters is not allowed");
        }
    }
    
     public static String makeURLPOSTCalls(String urlStr,String parameters) throws Exception {
        String response = "";

        try {
            //log.info(" (makeURLPOSTCalls) url calls >>>>>>>>>>> " + esaServiceUrl);
            //log.info(" (makeURLPOSTCalls) data >>>>>>>>>>> " + parameters);

            URL url = new URL(urlStr);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(parameters);
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                // Process line...
                response += line;
            }
            wr.close();
            rd.close();

            //log.info(" makeURLPOSTCalls (response) " + response);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //log.info(" response returned >>>>>>> " + response);

        return response;
    }

    public static String randomPassword(Integer length, String passarray) {
        if (length == null || length == 0) {
            length = 6;
        }//Default Length of Password 4
        if (passarray == null || passarray.trim().equals("")) {
            passarray = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        }
        long range = passarray.length();
        String password = "";
        java.util.Random generator = new java.util.Random();
        for (int i = 0; i < length;) {
            int rnd = (int) (range * generator.nextDouble());
            String ch = passarray.substring(rnd, rnd + 1);
            if (password.indexOf(ch) == -1) {
                password += ch;
                i++;
            }
        }
        return password;
    }

    public static Map<String, Object> doHttpPost(String url, Map<String, Object> params) throws IOException, ParseException {
        JSONObject jsonObj = new JSONObject();
        Map<String, Object> result = new HashMap();
        try {
            for (Map.Entry<String, Object> entry : params.entrySet()) {

                jsonObj.put(entry.getKey(), entry.getValue());
            }
                StringWriter writer = new StringWriter();
                jsonObj.writeJSONString(writer);

                String jsonString = writer.toString();

                HttpClient client = new DefaultHttpClient();
                HttpPost post = new HttpPost(url);
                StringEntity input = new StringEntity(jsonString);
                input.setContentType("application/json");
                post.setEntity(input);
                HttpResponse response = client.execute(post);

                if (response != null) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                    String textRetrieved = "";
                    String output;
                    while ((output = br.readLine()) != null) {
                        textRetrieved = textRetrieved + output;
                    }
                    JSONParser parser = new JSONParser();
                    logger.info("response ......." + textRetrieved);
                    Object obj = parser.parse(new StringReader(textRetrieved));
                    JSONObject jsonRecieved = (JSONObject) obj;
      logger.info("responseCode ......." + jsonRecieved.get("responseCode"));
                    result.put("responseCode", jsonRecieved.get("responseCode") != null ? null : jsonRecieved.get("responseCode").toString());
                    result.put("responseDescription", jsonRecieved.get("responseDescription") != null ? null : jsonRecieved.get("responseDescription").toString());
//                System.out.println("  jsonRecieved.get('responseCode') [" + jsonRecieved.get("responseCode").toString() + "]");
//                System.out.println("  jsonRecieved.get('responseDescription') [" + jsonRecieved.get("responseDescription").toString() + "]");
//                System.out.println("  jsonRecieved.get('tokenReference') [" + jsonRecieved.get("tokenReference").toString() + "]");
//                System.out.println("  jsonRecieved.get('additionalInformation') [" + jsonRecieved.get("additionalInformation").toString() + "]");
                } else {
                    logger.info("No response form middleware .......");
                    result.put("responseCode", "-1");
                    result.put("responseDescription", "No response from middleware");
                }

            
        } catch (IOException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
            result.put("responseCode", "-1");
            result.put("responseDescription", "No response from middleware");
              throw ex;
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        catch (Exception ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return result;
    }

    public static String doHttpPost2(String urlString, Map<String, Object> params) throws UnsupportedEncodingException, MalformedURLException, IOException {

        String parameters = "";
        String response = "";
        try {
            int i = 0;
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                parameters += (i == 0 ? "" : "&") + URLEncoder.encode(entry.getKey(), "UTF-8") + "=" + URLEncoder.encode(entry.getValue().toString(), "UTF-8");
                i++;
            }
            logger.info("connection url:: " + urlString);
            logger.info("parameter:: " + parameters);
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            BufferedReader rd;
            try (OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream())) {
                wr.write(parameters);
                wr.flush();
                // Get the response
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    // Process line...
                    logger.info("line: " + line);
                    response += line;
                }
                logger.info("response:: " + response);
            }
            rd.close();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } catch (MalformedURLException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } catch (IOException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        catch (Exception ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return response;
    }

    public static byte[] convertExcel(List<? extends Serializable> data, String[] fields) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Sheet1");
        HSSFRow headerRow = sheet.createRow(0);
        CellStyle cellStyle = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setFontName("Arial");
        font.setColor(IndexedColors.DARK_BLUE.getIndex());
        short bw = 5;
        font.setBoldweight(bw);
        font.setItalic(false);
        cellStyle.setFont(font);
        int i = 0;
        for (String field : fields) {
            HSSFCell cell = headerRow.createCell(i++);

            cell.setCellValue(splitCamelCase(field));
            cell.setCellStyle(cellStyle);
        }

        int rownum = 1;
        CellStyle cellStyle2 = workbook.createCellStyle();
        HSSFFont font2 = workbook.createFont();
        font2.setFontHeightInPoints((short) 10);
        font2.setFontName("Arial");
        font2.setColor(IndexedColors.BLACK.getIndex());
        bw = 1;
        font2.setBoldweight(bw);
        font2.setItalic(false);
        cellStyle2.setFont(font2);
        for (Serializable s : data) {
            i = 0;
            HSSFRow row = sheet.createRow(rownum++);
            for (String field : fields) {
                HSSFCell rowCell = row.createCell(i++);
                try {
                    String value = BeanUtils.getProperty(s, field);
                    if ("id".equals(field) || "transactionId".equals(field) || "paymentId".equals(field)) {
                        value = "08" + value;
                    }
                    rowCell.setCellValue(value);
                    rowCell.setCellStyle(cellStyle2);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
                    rowCell.setCellValue("");
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
                    rowCell.setCellValue("");
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
                    rowCell.setCellValue("");
                }
            }
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            workbook.write(outputStream);
        } catch (IOException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte[] toByteArray = outputStream.toByteArray();
        try {
            outputStream.close();
        } catch (IOException ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return toByteArray;
    }

    public static void main(String[] args) {
        try {
//            checkPasswordPolicy("seGun2345!");
//            System.out.println(splitCamelCase("SegunIdowu"));
//            List<Corporate> corporates = new ArrayList<>();
//            Corporate corporate = new Corporate();
//            corporate.setCorporateName("Adewale Carson");
//            corporate.setContactEmail("wfjfj@vkgvkkvkv");
//            corporate.setContactState("137479896699");
//            corporates.add(corporate);
//            byte[] convertExcel = convertExcel(corporates, new String[]{"corporateName", "contactEmail", "dateCreated", "contactState"});
//            FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\oluwasegun.idowu\\Documents\\repository\\deekkf.xls");
//            fileOutputStream.write(convertExcel);
//            fileOutputStream.close();
            
              Cryptographer crptt = new Cryptographer();
                 String doMd5Hash = crptt.doMd5Hash(StringUtils.
                join(new String[]{StringUtils.trim("stanbic7@stanbicbank.com"), StringUtils.trim("Stanbic123")}));
        
        System.out.println(" " + doMd5Hash);
        } catch (Exception ex) {
            Logger.getLogger(AppUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static String splitCamelCase(String s) {
        return s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        );
    }

}
