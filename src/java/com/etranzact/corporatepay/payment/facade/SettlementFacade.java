/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.setup.facade.*;
import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Settlement;
import com.etranzact.corporatepay.model.SingleBeneficiaryPayment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.model.UploadPayment;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.MailUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class SettlementFacade extends AbstractFacade<Settlement> implements SettlementFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public SettlementFacade() {
        super(Settlement.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Settlement edit(Settlement entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
