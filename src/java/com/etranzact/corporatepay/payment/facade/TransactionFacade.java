/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.setup.facade.*;
import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class TransactionFacade extends AbstractFacade<Transaction> implements TransactionFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @EJB
    private HoldingAccountFacadeLocal holdingAccountFacadeLocal;
    @EJB
    private TransactionWorkflowFacadeLocal transactionWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    public TransactionFacade() {
        super(Transaction.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Transaction edit(Transaction entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openOfflineTransactions(List<Transaction> transactions) throws Exception {
        for (Transaction transaction : transactions) {
            transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
            create(transaction);
        }
    }

    @Override
    public List<Transaction> findAll(Map<String, Object> filters, String... orderFields) throws Exception {
        List<Transaction> findAll = super.findAll(filters, orderFields);
        for (Transaction transaction : findAll) {
            if (transaction.getPayment().getHoldingAccountId() != null) {
                transaction.getPayment().setHoldingAccount(holdingAccountFacadeLocal.find(transaction.getPayment().getHoldingAccountId()));
            }
        }
        return findAll;
    }

    @Override
    public List<Transaction> findAll(int min, int max, Map<String, Object> filters, String... orderFields) throws Exception {
        List<Transaction> findAll = super.findAll(min, max, filters, orderFields);
        for (Transaction transaction : findAll) {
            if (transaction.getPayment().getHoldingAccountId() != null) {
                transaction.getPayment().setHoldingAccount(holdingAccountFacadeLocal.find(transaction.getPayment().getHoldingAccountId()));
            }
        }
        return findAll;
    }

    @Override
    public List<Transaction> findAll(Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception {
        List<Transaction> findAll = super.findAll(filters, activeOnly, orderFields);
        for (Transaction transaction : findAll) {
            if (transaction.getPayment().getHoldingAccountId() != null) {
                transaction.getPayment().setHoldingAccount(holdingAccountFacadeLocal.find(transaction.getPayment().getHoldingAccountId()));
            }
        }
        return findAll;
    }

    @Override
    public List<Transaction> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String... orderFields) throws Exception {
        List<Transaction> findAll = super.findAll(min, max, filters, activeOnly, orderFields);
        for (Transaction transaction : findAll) {
            if (transaction.getPayment().getHoldingAccountId() != null) {
                transaction.getPayment().setHoldingAccount(holdingAccountFacadeLocal.find(transaction.getPayment().getHoldingAccountId()));
            }
        }
        return findAll;
    }

    @Override
    public List<Transaction> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, String... orderFields) throws Exception {
        List<Transaction> findAll = super.findAll(filters, activeOnly, flagStatuses, orderFields);
        for (Transaction transaction : findAll) {
            if (transaction.getPayment().getHoldingAccountId() != null) {
                transaction.getPayment().setHoldingAccount(holdingAccountFacadeLocal.find(transaction.getPayment().getHoldingAccountId()));
            }
        }
        return findAll;
    }

    @Override
    public List<Transaction> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        List<Transaction> findAll = super.findAll(min, max, filters, activeOnly, flagStatuses, fromDate, toDate, dateField, orderFields);
        for (Transaction transaction : findAll) {
            if (transaction.getPayment().getHoldingAccountId() != null) {
                transaction.getPayment().setHoldingAccount(holdingAccountFacadeLocal.find(transaction.getPayment().getHoldingAccountId()));
            }
        }
        return findAll;
    }

    @Override
    public List<Transaction> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        List<Transaction> findAll = super.findAll(filters, activeOnly, flagStatuses, fromDate, toDate, dateField, orderFields);
        for (Transaction transaction : findAll) {
            if (transaction.getPayment().getHoldingAccountId() != null) {
                transaction.getPayment().setHoldingAccount(holdingAccountFacadeLocal.find(transaction.getPayment().getHoldingAccountId()));
            }
        }
        return findAll;
    }

    @Override
    public void save(BigInteger transactionId, String accountNumber) throws Exception {
        Transaction transaction = find(transactionId);
        transaction.getAccount().setTmpAccountNumber(accountNumber);
        transaction = super.create(transaction);
        transactionWorkflowFacadeLocal.startWorkflow(Transaction.class, transaction.getTransactionId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
    }

    @Override
    public void finalizeApprove(Transaction t, ApprovalTask task) throws Exception {
        log.info("finalizin transaction....");

        Map map = new HashMap<>();
        map.put("u.role.id in ('0003')", null);
        List<User> blincopies = userSetupFacadeLocal.findAll(map, true);
        String[] bcopies = new String[blincopies.size()];
        for (int i = 0; i < blincopies.size(); i++) {
            bcopies[i] = blincopies.get(i).getEmail();
        }
        map = new HashMap<>();
        map.put("u.role.id in ('0002')", null);
        List<User> findAll = userSetupFacadeLocal.findAll(map, true);
        String[] recepients = new String[findAll.size()];

        int i = 0;
        for (; i < findAll.size(); i++) {
            recepients[i] = findAll.get(i).getEmail();
        }
       
        t.getAccount().setAccountNumber(t.getAccount().getTmpAccountNumber());
        t.getAccount().setTmpAccountNumber(null);

        MailUtil.getInstance().sendEditMail(t, recepients, "08" + t.getTransactionId().toString(), bcopies);
                t.setCheckSum(generateCheckSum(t.getAccount().getAccountNumber(), t.getAmount().toString(), t.getAccount() instanceof BankAccount ? ((BankAccount) t.getAccount()).getBank().getId() : "700"));
    }
}
