/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.SchedulePayment;
import com.etranzact.corporatepay.model.SingleBeneficiaryPayment;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.UploadPayment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class PaymentWorkflowFacade extends AbstractWorkflowFacade implements PaymentWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        Payment payment = (Payment) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(0, new TaskObjectNameValuePair("Payment Reference: ", payment.getPaymentReference(),
                null));
        int i = 1;
        if (payment instanceof SingleBeneficiaryPayment) {
            SingleBeneficiaryPayment sPayment = (SingleBeneficiaryPayment) payment;
            BigDecimal tamount = BigDecimal.ZERO;
            BigDecimal tfamount = BigDecimal.ZERO;
            for (Transaction transaction : sPayment.getTransactions()) {
                String account = null;
                if (transaction.getAccount() instanceof PocketMoneyAccount) {
                    PocketMoneyAccount pma = (PocketMoneyAccount) transaction.getAccount();
                    account = "POCKET MONEY ACCOUNT/" + pma.getAccountNumber();
                } else if (transaction.getAccount() instanceof BankAccount) {
                    BankAccount ba = (BankAccount) transaction.getAccount();
                    account = ba.getBank().getBankName() + "/" + ba.getAccountName() + "(" + ba.getAccountNumber() + ")";
                }
                tamount = tamount.add(transaction.getAmount());
                tfamount = tfamount.add(transaction.getTotalFeeAmount());
                nameValuePairs.add(i, new TaskObjectNameValuePair("Transaction ", "Account Number: " + account,
                        AppUtil.formatAsAmount("NGN " + transaction.getAmount().toString(), "###,###.###")));
                i++;
                nameValuePairs.add(i, new TaskObjectNameValuePair("Narration", transaction.getNarration(),
                        null));
                i++;
            }
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Amount to Beneficiary(s) ", AppUtil.formatAsAmount("NGN " + tamount.toString(), "###,###.###"),
                    null));
            i++;
            tamount = AppConstants.CHARGE_BEARER_BENEFICIARY.equals(payment.getChargeBearer()) ? tamount.add(tfamount) : tamount;
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Amount to Debit ", AppUtil.formatAsAmount("NGN " + tamount.toString(), "###,###.###"),
                    null));
            i++;
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Commission ", AppUtil.formatAsAmount("NGN " + tfamount.toString(), "###,###.###"),
                    null));
            i++;
            if (payment.getCorporate().getApplyCommission()) {
                nameValuePairs.add(i, new TaskObjectNameValuePair("Who handles Commission ", (AppConstants.CHARGE_BEARER_BENEFICIARY.equals(payment.getChargeBearer()) ? "Beneficiary" : "Payer"),
                        null));
                i++;
            }
            nameValuePairs.add(i, new TaskObjectNameValuePair("View", sPayment.getId().toString(),
                    null));

        } else if (payment instanceof UploadPayment) {
            UploadPayment uploadPayment = (UploadPayment) payment;

            String amount = "NGN ";
            String tfamount = "NGN ";
            BigDecimal amt = BigDecimal.ZERO;
            BigDecimal famt = BigDecimal.ZERO;
            try {
                String query = "select sum(t.amount) from Transaction t where "
                        + "t.corporate.id=" + getLoggedCorporate().getId() + " "
                        + "and t.payment.id=" + uploadPayment.getId() + " and t.schedule=null";
                //log.log(Level.INFO, "query::: {0}", query);
                amt = (BigDecimal) em.createQuery(query)
                        .getSingleResult();
                //log.log(Level.INFO, "amount::: {0}", amt);
//                amount += amt.toString();
                query = "select sum(t.totalFeeAmount) from Transaction t where "
                        + "t.corporate.id=" + getLoggedCorporate().getId() + " "
                        + "and t.payment.id=" + uploadPayment.getId() + " and t.schedule=null";
                famt = (BigDecimal) em.createQuery(query)
                        .getSingleResult();
                tfamount += famt != null ? famt.toString() : "0.00";
            } catch (PersistenceException pex) {
                log.log(Level.SEVERE, "Persistence Exception", pex);
            }
            nameValuePairs.add(i, new TaskObjectNameValuePair("Uploaded File ", uploadPayment.getUploadPath(),
                    null));
            i++;
             nameValuePairs.add(i, new TaskObjectNameValuePair("Total Amount to Beneficiary(s) ", AppUtil.formatAsAmount(amount, "###,###.###"),
                    null));
            i++;
            amount = (AppConstants.CHARGE_BEARER_BENEFICIARY.equals(payment.getChargeBearer()) ? amt.add(famt) : amt).toString();
            
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Amount  to Debit ", AppUtil.formatAsAmount(amount, "###,###.###"),
                    null));
            i++;
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Commission ", AppUtil.formatAsAmount(tfamount, "###,###.###"),
                    null));
            i++;
            if (payment.getCorporate().getApplyCommission()) {
                nameValuePairs.add(i, new TaskObjectNameValuePair("Who handles Commission ", (AppConstants.CHARGE_BEARER_BENEFICIARY.equals(payment.getChargeBearer()) ? "Beneficiary" : "Payer"),
                        null));
                i++;
            }
            nameValuePairs.add(i, new TaskObjectNameValuePair("View", uploadPayment.getId().toString(),
                    null));
        } else if (payment instanceof SchedulePayment) {
            SchedulePayment schedulePayment = (SchedulePayment) payment;

            String amount = "NGN ";
            String tfamount = "NGN ";
            BigDecimal amt = BigDecimal.ZERO;
            BigDecimal famt = BigDecimal.ZERO;
            try {
                String query = "select sum(t.amount) from Transaction t where "
                        + "t.corporate.id=" + getLoggedCorporate().getId() + " "
                        + "and t.payment.id=" + schedulePayment.getId() + "  and t.schedule=null";
                // log.log(Level.INFO, "query::: {0}", query);
                amt = (BigDecimal) em.createQuery(query)
                        .getSingleResult();
                // log.log(Level.INFO, "amount::: {0}", amt);
                amount += amt.toString();
                query = "select sum(t.totalFeeAmount) from Transaction t where "
                        + "t.corporate.id=" + getLoggedCorporate().getId() + " "
                        + "and t.payment.id=" + schedulePayment.getId() + " and t.schedule=null";
                famt = (BigDecimal) em.createQuery(query)
                        .getSingleResult();

                tfamount += famt != null ? amt.toString() : "0.00";
            } catch (PersistenceException pex) {
                log.log(Level.SEVERE, "Persistence Exception", pex);
            }
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Amount to Beneficiary(s) ", AppUtil.formatAsAmount(amount, "###,###.###"),
                    null));
            i++;
            amount = (AppConstants.CHARGE_BEARER_BENEFICIARY.equals(payment.getChargeBearer()) ? amt.add(famt) : amt).toString();
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Amount to Debit ", AppUtil.formatAsAmount(amount, "###,###.###"),
                    null));
            i++;
            nameValuePairs.add(i, new TaskObjectNameValuePair("Total Commission ", AppUtil.formatAsAmount(tfamount, "###,###.###"),
                    null));
            i++;
            if (payment.getCorporate().getApplyCommission()) {
                nameValuePairs.add(i, new TaskObjectNameValuePair("Who handles Commission ", (AppConstants.CHARGE_BEARER_BENEFICIARY.equals(payment.getChargeBearer()) ? "Beneficiary" : "Payer"),
                        null));
                i++;
            }
            nameValuePairs.add(i, new TaskObjectNameValuePair("View Transactions ", schedulePayment.getId().toString(),
                    null));
        }
        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
