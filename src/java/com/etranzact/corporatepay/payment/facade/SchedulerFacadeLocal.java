/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.PaymentScheduler;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface SchedulerFacadeLocal extends AbstractFacadeLocal<PaymentScheduler> {

    public void refresh(PaymentScheduler paymentScheduler) throws Exception;

}
