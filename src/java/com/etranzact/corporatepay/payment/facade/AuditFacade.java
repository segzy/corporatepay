/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.AuditTable;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.core.web.AuditTableBean;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class AuditFacade extends AbstractFacade<AuditTable> implements AuditFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public AuditFacade() {
        super(AuditTable.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected AuditTable edit(AuditTable entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AuditTable> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        List<AuditTable> findAll = super.findAll(min, max, filters, activeOnly, flagStatuses, fromDate, toDate, dateField, orderFields);
        for (AuditTable auditTable : findAll) {
            if (auditTable.getEntityId() != null && auditTable.getEntityName() != null) {
                setEntityFocus(auditTable);
            }
        }
        return findAll;
    }

    private void setEntityFocus(AuditTable t) {
        String focus = null;
        try {
            Object object = null;
            if (AppConstants.DATA_TYPE_STRING.equals(t.getIdentityDataType())) {
                object = em.find(Class.forName("com.etranzact.corporatepay.model." + t.getEntityName()), String.valueOf(t.getEntityId()));
            } else if (AppConstants.DATA_TYPE_BIGINTEGER.equals(t.getIdentityDataType())) {
                object = em.find(Class.forName("com.etranzact.corporatepay.model." + t.getEntityName()), new BigInteger(t.getEntityId()));
            } else if (AppConstants.DATA_TYPE_LONG.equals(t.getIdentityDataType())) {
                object = em.find(Class.forName("com.etranzact.corporatepay.model." + t.getEntityName()), Long.valueOf(t.getEntityId()));
            }

            if (object instanceof User) {
                focus = ((User) object).getFirstName() + " " + ((User) object).getLastName() + " (" + ((User) object).getUsername() + ")";
            } else if (object instanceof Bank) {
                focus = ((Bank) object).getBankName();
            } else if (object instanceof Account) {
                focus = ((Account) object).getAccountIdentity();
            } else if (object instanceof ApprovalGroup) {
                focus = ((ApprovalGroup) object).getIdentity();
            } else if (object instanceof Beneficiary) {
                focus = ((Beneficiary) object).getBeneficiaryName();
            } else if (object instanceof com.etranzact.corporatepay.model.Card) {
                focus = ((com.etranzact.corporatepay.model.Card) object).getDescription() + " (" + ((com.etranzact.corporatepay.model.Card) object).getCardNumber() + ")";
            } else if (object instanceof Corporate) {
                focus = ((Corporate) object).getCorporateName() + " (" + ((Corporate) object).getCorporateName() + ")";
            } else if (object instanceof Payment) {
                focus = "08" + ((Payment) object).getPaymentDescription();
            } else if (object instanceof Transaction) {
                focus = "08" + ((Transaction) object).getTransactionId().toString() + " (NGN " + ((Transaction) object).getAmountf() + ")";
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AuditTableBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        log.info("setting " + focus);
        t.setEntityFocus(focus);
    }
    
    public void test() {
         AuditReader reader = AuditReaderFactory.get(em);
        User find = reader.find(User.class, 12, 11);
      
    }
}
