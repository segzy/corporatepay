/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.setup.facade.*;
import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.core.facade.ParametersFacadeLocal;
import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.BankHoldingAccount;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.CommissionFee;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.HoldingAccount;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.PaymentScheduler;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.model.SchedulePayment;
import com.etranzact.corporatepay.model.SingleBeneficiaryPayment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.model.UploadPayment;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.web.CorporateSetupBean;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import com.etranzact.corporatepay.util.MailUtil;
import com.etz.http.etc.Card;
import com.etz.http.etc.HttpHost;
import com.etz.http.etc.TransCode;
import com.etz.http.etc.XProcessor;
import com.etz.http.etc.XRequest;
import com.etz.http.etc.XResponse;
import com.etz.security.util.Cryptographer;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class PaymentFacade extends AbstractFacade<Payment> implements PaymentFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;
    @EJB
    private PaymentWorkflowFacadeLocal paymentWorkflowFacadeLocal;
    @EJB
    private BankCommissionFacadeLocal bankCommissionFacadeLocal;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;
    @EJB
    private HoldingAccountFacadeLocal holdingAccountFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    public PaymentFacade() {
        super(Payment.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finalizeApprove(Payment t, ApprovalTask task) throws Exception {
        log.info("finalizin payment....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
//        Cryptographer crpt = new Cryptographer();
//        String doMd5Hash = crpt.doMd5Hash(StringUtils.
//                join(new String[]{t.getCard().getCardNumber(), task.getCardDetail()}));
//        if(!doMd5Hash.equals(t.getCard().getCardPIN())) {
//            throw new PersistenceException("Incorrect card pin");
//        }
//               try {
//                Parameters parameter = parametersFacadeLocal.find("TOKEN_URL");
//                log.log(Level.INFO, "parameter: {0}", parameter);
//                if (parameter != null) {
//                    Map<String, Object> map = new HashMap<>();
//                Bank loggedBank = getLoggedBank();
//                String issuerCode = null;
//                if (loggedBank == null) {
//                    loggedBank = getLoggedCorporate().getBank();
//                }
//                if (loggedBank == null) {
//                    issuerCode = "700";
//                }
//                if (issuerCode == null) {
//                    issuerCode = loggedBank.getId();
//                }
//                log.log(Level.INFO, "issuerCode: {0}", issuerCode);
//                map.put("issuerCode", issuerCode);
//                map.put("channelCode", "00");
//                map.put("requestType", "AUT");
//                map.put("responseCode", getLoggedUser().getTokenUserGroup());
//                map.put("requester", getLoggedUser().getTokenUserName());
//                map.put("tokenValue", task.getToken());
//                Map<String, Object> result = AppUtil.doHttpPost(parameter.getParamValue(), map);
//                String responseCode = result.get("responseCode").toString();
//                log.log(Level.INFO, "responseCpde:: {0}", responseCode);
//                if(responseCode.equals("-1")) {
//                   throw new PersistenceException("Token Authentication Error"); 
//                }
//                
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
//               throw new PersistenceException("Token Authentication Error"); 
//            }
        String paymentReference = "08" + t.getId().toString();
        String cardNumber = t.getCard().getCardNumber();
        Corporate corporate = t.getCorporate();
        HoldingAccount holdingAccount = null;
        Map<String, Object> filter;
        List<HoldingAccount> holdingAccounts;
        Bank otherHoldingbank = null;
        Bank bank = t.getCorporate().getBank();
        String sourceissuer = cardNumber.substring(0, 3);
        Bank sourcebank = bankSetupFacadeLocal.find(sourceissuer);
        if (corporate.getApplyCommission() && !corporate.getApproveCommission()) {
            log.info("commission to apply and commission is less than default etranzact commission: ");
            throw new PersistenceException("Error: Commission setup for your Corporate is less than default platfom provider's commission, kindly contact your bank");
        }
        if (!bank.getId().equals(sourceissuer) && !cardNumber.startsWith("7")) {
            otherHoldingbank = bankSetupFacadeLocal.find(cardNumber.substring(0, 3));
            holdingAccountFacadeLocal.setEntityNamePrefix("Bank");
            filter = new HashMap<>();
            filter.put("flagStatus", AppConstants.APPROVED);
            filter.put("bank", otherHoldingbank);
            holdingAccounts = holdingAccountFacadeLocal.findAll(filter, true);
            if (holdingAccounts.isEmpty()) {
                throw new PersistenceException("Error: Issues with making payment because your bank's account setups is incomplete");
            }
            holdingAccount = holdingAccounts.get(0);
            log.info("other bank holding account: " + holdingAccount);
        }
        Preferences preference;
        if (otherHoldingbank != null) {
            preference = preferenceFacadeLocal.getBankPreferences(otherHoldingbank);
        } else {
            preference = preferenceFacadeLocal.getBankPreferences(bank);
        }
        if (holdingAccount == null) {
            filter = new HashMap<>();
            filter.put("corporate", corporate);
            filter.put("flagStatus", AppConstants.APPROVED);
            holdingAccounts = holdingAccountFacadeLocal.findAll(filter, true);
            if (!holdingAccounts.isEmpty()) {
                holdingAccount = holdingAccounts.get(0);
            } else {
                holdingAccountFacadeLocal.setEntityNamePrefix("Bank");
                filter = new HashMap<>();
                filter.put("flagStatus", AppConstants.APPROVED);
                filter.put("bank", bank);
                holdingAccounts = holdingAccountFacadeLocal.findAll(filter, true);
                if (holdingAccounts.isEmpty()) {
                    throw new PersistenceException("Error: Issues with making payment because your bank's account setups is incomplete");
                }
                holdingAccount = holdingAccounts.get(0);
            }
            log.info("host bank holding account: " + holdingAccount);
        }
        if (holdingAccount == null) {
            throw new PersistenceException("Error: Issues with making payment because your bank's account setups is incomplete");
        }
        String holdingAccountStr = holdingAccount.getCardNumber();
        String pin = task.getCardDetail();
        String expMonth = task.getExpMonth();
        String expYear = task.getExpYear();
        BigDecimal principalAmount = t.getTotalAmount();
        paymentReference = (t.getTrial() == 0) ? paymentReference : paymentReference + t.getTrial();
        // String narration = paymentReference + "; " + (t.getPaymentDescriptionn() == null ? "" : t.getPaymentDescriptionn());
        String narration = (t.getPaymentDescriptionn() == null ? "" : t.getPaymentDescriptionn());
        log.info("paymentReference:: " + paymentReference);
        log.info("narration:: " + narration);
        log.info("card number:: " + cardNumber);
        log.info("holding account:: " + holdingAccountStr);
//        log.info("Pin:: " + pin);
//        log.info("exp month:: " + expMonth);
//        log.info("expYear:: " + expYear);
        log.info("principalAmount:: " + principalAmount);
        Set<Transaction> transactions = t.getTransactions();
        BigDecimal totalCharge = BigDecimal.ZERO;
//
        if (corporate.getApplyCommission()) {
            String query = "select sum(t.totalFeeAmount) from Transaction t where "
                    + "t.corporate.id=" + t.getCorporate().getId() + " "
                    + "and t.payment.id=" + t.getId() + " and t.schedule=null";
            totalCharge = (BigDecimal) em.createQuery(query)
                    .getSingleResult();
        }
        log.info("totalCharge:: " + totalCharge);
        BigDecimal totalAmount = principalAmount.add(totalCharge);
        log.info("totalAmount:: " + totalAmount);
        String response = null;
//        boolean tmcBank = sourcebank.isPM() || sourcebank.isTmc();
        boolean tmcBank = true;
        response = authorizePayment(tmcBank, paymentReference, holdingAccountStr, cardNumber, pin, expYear, expMonth, totalAmount, /**
                 * to do calculate commission *
                 */
                BigDecimal.ZERO, narration, "N");
        response = response == null ? "100" : response;
        log.info("response:: " + response);
        t.setResponse(response);
        boolean success = response.equals(AppConstants.SWITH_TRANSACTION_SUCCESSFUL1) || response.equals(AppConstants.SWITH_TRANSACTION_SUCCESSFUL2)
                || response.equals(AppConstants.SWITH_TRANSACTION_SUCCESSFUL3);
        log.info("Payment Successful::" + success);
        if (success) {
            t.setFlagStatus(AppConstants.PAYMENT_HELD);
            t.setHoldingAccountId(holdingAccount.getId());
            t.setHoldingAccountNumber(holdingAccount.getCardNumber());
            t.setSourceCardNumber(cardNumber);
            t.setActive(Boolean.TRUE);
            Map<Bank, List<Transaction>> mfbMap = new HashMap<>();
            for (Transaction transaction : transactions) {
                Account account = transaction.getAccount();
                if (account instanceof BankAccount) {
                    Bank bank1 = ((BankAccount) account).getBank();
                    if (bank1.getMfbBank()) {
                        List<Transaction> mfBankTrans = mfbMap.get(bank1);
                        if (mfBankTrans == null) {
                            mfBankTrans = new ArrayList<>();

                        }
                        mfBankTrans.add(transaction);
                        mfbMap.put(bank1, mfBankTrans);
                    }
                    if (bank1.isSpecialized()) {
                        transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_WAITING);
                    } else {
                        transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
                    }
                } else if (account instanceof PocketMoneyAccount) {
                    transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
                }

            }
            Map<String, Object> map = new HashMap<>();
            map.put("u.role.id='0002'", null);
            List<User> support = userSetupFacadeLocal.findAll(map, true);
            String[] supportEmails = new String[support.size()];
            String[] bsupportEmails = new String[0];
            int i = 0;
            for (User supportPerson : support) {
                supportEmails[i] = supportPerson.getEmail();
            }
            if (!mfbMap.isEmpty()) {
                for (Entry<Bank, List<Transaction>> entry : mfbMap.entrySet()) {
                    String mfbankName = entry.getKey().getBankName();
                    String mfbankEmail = entry.getKey().getBankEmail();
                    String affliateBankAccount = entry.getKey().getAffliateBankAccount();
                    Bank affliateBank = entry.getKey().getAffliateBank();
                    if (affliateBank == null) {
                        continue;
                    }
                    List<Transaction> value = entry.getValue();
                    BigDecimal totalMFBAmount = BigDecimal.ZERO;
                    for (Transaction t1 : value) {
//                        if (t.getChargeBearer().equals(AppConstants.CHARGE_BEARER_PAYER)) {
                        totalMFBAmount = totalMFBAmount.add(t1.getAmount());
//                        } else if (t.getChargeBearer().equals(AppConstants.CHARGE_BEARER_BENEFICIARY)) {
//                            totalMFBAmount = totalMFBAmount.add(calculateCharge(t1.getCorporate(), t1.getAmount()));
//                        }
                    }
                    log.info("sending mdfB mail:: " + mfbankName);
                    log.info("mdfB Email:: " + mfbankEmail);
                    log.info("affliateBank:: " + affliateBank.getBankName());
                    log.info("affliateBankAccount:: " + affliateBankAccount);
                    log.info("no of Transactions:: " + value.size());
                    log.info("total amt of Transactions:: " + totalMFBAmount);
                    String[] fields = new String[]{"corporate", "initiatingBank", "transactionId", "beneficaryBank", "paymentBank", "beneficaryName", "beneficaryAccount", "narration", "amount", "authDate", "authBy"};
                    byte[] excel = AppUtil.convertExcel(value, fields);
                    MailUtil.getInstance().sendMFBTransactionMail(new String[]{mfbankEmail}, mfbankName, affliateBank.getBankName(), affliateBankAccount, value.size(), AppUtil.formatAsAmount(totalMFBAmount.toString()), excel, supportEmails);
                }
            }
            User loggedUser = getLoggedUser();
            String authorizer = loggedUser.getFirstName() + "(" + loggedUser.getUsername() + ")";
            String authorizerEmail = loggedUser.getEmail();
            String authorizerPhone = loggedUser.getMobilePhone();
            t.setAuthorizedBy(authorizer);
            t.setDateAuthorized(new Date());
            if (preference != null && preference.getMinimumPaymentAmount() != null && !preference.getMinimumPaymentAmount().isEmpty()
                    && preference.getMinimumPaymentAmountEmails() != null && !preference.getMinimumPaymentAmountEmails().isEmpty()) {
                if (new BigDecimal(preference.getMinimumPaymentAmount()).compareTo(totalAmount) <= 0) {
                    String[] bankThreshAlertBeneficiaries = preference.getMinimumPaymentAmountEmails().split(",");
                    String cardNumber1 = t.getCard().getCardNumber();
                    int length = cardNumber1.substring(9, cardNumber1.length()).length();
                    cardNumber1 = cardNumber1.substring(0, 9);
                    for (i = 0; i < length; i++) {
                        cardNumber1 = cardNumber1 + "*";
                    }
                    MailUtil.getInstance().sendThresholdAlert(bankThreshAlertBeneficiaries, "08" + t.getId().toString(), authorizer, t.getCorporate().getCorporateName(), totalAmount.toString(), String.valueOf(t.getTransactions().size()), cardNumber1);
                }
            }

            String contact = authorizerPhone != null ? (authorizerPhone + ";") : "" + authorizerEmail != null ? (authorizerEmail + "; ") : "" + t.getCorporate().getContactEmail() == null ? "" : "; " + t.getCorporate().getContactEmail() + t.getCorporate().getContactPhone() == null ? "" : "; " + t.getCorporate().getContactPhone();
            bsupportEmails = preference == null ? bsupportEmails : preference.getAuthorizationSupportEmails() == null ? bsupportEmails : preference.getAuthorizationSupportEmails().split(",");
            MailUtil.getInstance().sendPaymentApproved(supportEmails, bsupportEmails, "08" + t.getId().toString(), authorizer, t.getCorporate().getCorporateName(), totalAmount.toString(), String.valueOf(t.getTransactions().size()), contact, cardNumber, t.getPaymentDescription(), t.getAuthorizedDateStr());
            auditLogFacadeLocal.create(buildAuditLog(AppConstants.AUDIT_TYPE_PAYMENT_AUTHORIZATION, t.getPaymentDescription() + "/NGN" + t.getTotalAmount(), null));
        } else {
            User loggedUser = getLoggedUser();
            String authorizer = loggedUser.getFirstName() + "(" + loggedUser.getUsername() + ")";
            String authorizerEmail = loggedUser.getEmail();
            String authorizerPhone = loggedUser.getMobilePhone();
            Map<String, Object> map = new HashMap<>();
            map.put("u.role.id='0002'", null);
            List<User> support = userSetupFacadeLocal.findAll(map, true);
            String[] supportEmails = new String[support.size()];
            String[] bsupportEmails = new String[0];
            int i = 0;
            for (User supportPerson : support) {
                supportEmails[i] = supportPerson.getEmail();
            }
            String contact = authorizerPhone != null ? (authorizerPhone + ";") : "" + authorizerEmail != null ? (authorizerEmail + "; ") : "" + t.getCorporate().getContactEmail() == null ? "" : "; " + t.getCorporate().getContactEmail() + t.getCorporate().getContactPhone() == null ? "" : "; " + t.getCorporate().getContactPhone();
            String property = ConfigurationUtil.instance().getEtzErrorProperties().getProperty(response);
            bsupportEmails = preference == null ? bsupportEmails : preference.getAuthorizationSupportEmails() == null ? bsupportEmails : preference.getAuthorizationSupportEmails().split(",");
            MailUtil.getInstance().sendPaymentFailed(supportEmails, bsupportEmails, "08" + t.getId().toString(), authorizer, t.getCorporate().getCorporateName(), totalAmount.toString(), String.valueOf(t.getTransactions().size()), contact, response + ": " + property, cardNumber, t.getPaymentDescription(), AppUtil.formatDate(new Date(), "EEE, d MMM yyyy HH:mm:ss"));
//            saveP(t);
            throw new PersistenceException("Payment not Successful. " + (property == null ? " Generic Error" : property));
        }

    }

    @Asynchronous
    public void saveP(Payment entity)
            throws Exception {
        em.merge(entity);
    }

    @Override
    public Payment finalizeApprove(Payment t) throws Exception {
        log.info("finalizin payment.... " + t);
//        Cryptographer crpt = new Cryptographer();
//        String doMd5Hash = crpt.doMd5Hash(StringUtils.
//                join(new String[]{t.getCard().getCardNumber(), task.getCardDetail()}));
//        if(!doMd5Hash.equals(t.getCard().getCardPIN())) {
//            throw new PersistenceException("Incorrect card pin");
//        }
//               try {
//                Parameters parameter = parametersFacadeLocal.find("TOKEN_URL");
//                log.log(Level.INFO, "parameter: {0}", parameter);
//                if (parameter != null) {
//                    Map<String, Object> map = new HashMap<>();
//                Bank loggedBank = getLoggedBank();
//                String issuerCode = null;
//                if (loggedBank == null) {
//                    loggedBank = getLoggedCorporate().getBank();
//                }
//                if (loggedBank == null) {
//                    issuerCode = "700";
//                }
//                if (issuerCode == null) {
//                    issuerCode = loggedBank.getId();
//                }
//                log.log(Level.INFO, "issuerCode: {0}", issuerCode);
//                map.put("issuerCode", issuerCode);
//                map.put("channelCode", "00");
//                map.put("requestType", "AUT");
//                map.put("responseCode", getLoggedUser().getTokenUserGroup());
//                map.put("requester", getLoggedUser().getTokenUserName());
//                map.put("tokenValue", task.getToken());
//                Map<String, Object> result = AppUtil.doHttpPost(parameter.getParamValue(), map);
//                String responseCode = result.get("responseCode").toString();
//                log.log(Level.INFO, "responseCpde:: {0}", responseCode);
//                if(responseCode.equals("-1")) {
//                   throw new PersistenceException("Token Authentication Error"); 
//                }
//                
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
//               throw new PersistenceException("Token Authentication Error"); 
//            }
        if (t.getCard() == null || t.getCard().getCardNumber() == null) {
            throw new PersistenceException("Error: Payment Card has not been provided by the Corporate's Authorizer");
        }
        String cardNumber = t.getCard().getCardNumber();
        Corporate corporate = t.getCorporate();
        HoldingAccount holdingAccount = null;
        Map<String, Object> filter;
        List<HoldingAccount> holdingAccounts;
        Bank otherHoldingbank = null;
        Bank bank = t.getCorporate().getBank();
        String sourceissuer = cardNumber.substring(0, 3);
        if (corporate.getApplyCommission() && !corporate.getApproveCommission()) {
            log.info("commission applies and commission is less than default etranzact commission: ");
            throw new PersistenceException("Error: Commission setup for your Corporate is less than default platfom provider's commission");
        }
        if (!bank.getId().equals(sourceissuer) && !cardNumber.startsWith("7")) {
            otherHoldingbank = bankSetupFacadeLocal.find(cardNumber.substring(0, 3));
            holdingAccountFacadeLocal.setEntityNamePrefix("Bank");
            filter = new HashMap<>();
            filter.put("flagStatus", AppConstants.APPROVED);
            filter.put("bank", otherHoldingbank);
            holdingAccounts = holdingAccountFacadeLocal.findAll(filter, true);
            if (holdingAccounts.isEmpty()) {
                throw new PersistenceException("Error: Issues with making payment because your bank's account setups is incomplete");
            }
            holdingAccount = holdingAccounts.get(0);
            log.info("other bank holding account: " + holdingAccount);
        }
        Preferences preference;
        if (otherHoldingbank != null) {
            preference = preferenceFacadeLocal.getBankPreferences(otherHoldingbank);
        } else {
            preference = preferenceFacadeLocal.getBankPreferences(bank);
        }
        if (holdingAccount == null) {
            filter = new HashMap<>();
            filter.put("corporate", corporate);
            filter.put("flagStatus", AppConstants.APPROVED);
            holdingAccounts = holdingAccountFacadeLocal.findAll(filter, true);
            if (!holdingAccounts.isEmpty()) {
                holdingAccount = holdingAccounts.get(0);
            } else {
                holdingAccountFacadeLocal.setEntityNamePrefix("Bank");
                filter = new HashMap<>();
                filter.put("flagStatus", AppConstants.APPROVED);
                filter.put("bank", bank);
                holdingAccounts = holdingAccountFacadeLocal.findAll(filter, true);
                if (holdingAccounts.isEmpty()) {
                    throw new PersistenceException("Error: Issues with making payment because your bank's account setups is incomplete");
                }
                holdingAccount = holdingAccounts.get(0);
            }
            log.info("host bank holding account: " + holdingAccount);
        }
        if (holdingAccount == null) {
            throw new PersistenceException("Error: Issues with making payment because your bank's account setups is incomplete");
        }
        BigDecimal principalAmount = t.getTotalAmount();
        Set<Transaction> transactions = t.getTransactions();
        BigDecimal totalCharge = BigDecimal.ZERO;

        if (corporate.getApplyCommission()) {
            if (t.getChargeBearer().equals(AppConstants.CHARGE_BEARER_PAYER)) {
                String query = "select sum(t.totalFeeAmount) from Transaction t where "
                        + "t.corporate.id=" + t.getCorporate().getId() + " "
                        + "and t.payment.id=" + t.getId() + " and t.schedule=null";
                totalCharge = (BigDecimal) em.createQuery(query)
                        .getSingleResult();
                log.info(" calc:: " + totalCharge);
            }
        }
        log.info("totalCharge:: " + totalCharge);
        BigDecimal totalAmount = principalAmount.add(totalCharge);
        log.info("totalAmount:: " + totalAmount);
        String response = AppConstants.SWITH_TRANSACTION_SUCCESSFUL1;
        boolean success = response.equals(AppConstants.SWITH_TRANSACTION_SUCCESSFUL1) || response.equals(AppConstants.SWITH_TRANSACTION_SUCCESSFUL2)
                || response.equals(AppConstants.SWITH_TRANSACTION_SUCCESSFUL3);
        log.info("success:: " + success);
        if (success) {
            t.setFlagStatus(AppConstants.PAYMENT_HELD);
            t.setHoldingAccountId(holdingAccount.getId());
            t.setHoldingAccountNumber(holdingAccount.getCardNumber());
            t.setSourceCardNumber(cardNumber);
            t.setActive(Boolean.TRUE);
            Map<Bank, List<Transaction>> mfbMap = new HashMap<>();
            for (Transaction transaction : transactions) {
                Account account = transaction.getAccount();
                if (account instanceof BankAccount) {
                    Bank bank1 = ((BankAccount) account).getBank();
                    if (bank1.getMfbBank()) {
                        List<Transaction> mfBankTrans = mfbMap.get(bank1);
                        if (mfBankTrans == null) {
                            mfBankTrans = new ArrayList<>();

                        }
                        mfBankTrans.add(transaction);
                        mfbMap.put(bank1, mfBankTrans);
                    }
                    if (bank1.isSpecialized()) {
                        transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_WAITING);
                    } else {
                        transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
                    }
                } else if (account instanceof PocketMoneyAccount) {
                    transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
                }

            }
            Map<String, Object> map = new HashMap<>();
            map.put("u.role.id='0002'", null);
            List<User> support = userSetupFacadeLocal.findAll(map, true);
            String[] supportEmails = new String[support.size()];
            String[] bsupportEmails = new String[0];
            int i = 0;
            for (User supportPerson : support) {
                supportEmails[i] = supportPerson.getEmail();
            }
            if (!mfbMap.isEmpty()) {
                for (Entry<Bank, List<Transaction>> entry : mfbMap.entrySet()) {
                    String mfbankName = entry.getKey().getBankName();
                    String mfbankEmail = entry.getKey().getBankEmail();
                    String affliateBankAccount = entry.getKey().getAffliateBankAccount();
                    Bank affliateBank = entry.getKey().getAffliateBank();
                    if (affliateBank == null) {
                        continue;
                    }
                    List<Transaction> value = entry.getValue();
                    BigDecimal totalMFBAmount = BigDecimal.ZERO;
                    for (Transaction t1 : value) {
                        totalMFBAmount = totalMFBAmount.add(t1.getAmount());
                    }
                    log.info("sending mdfB mail:: " + mfbankName);
                    log.info("mdfB Email:: " + mfbankEmail);
                    log.info("affliateBank:: " + affliateBank.getBankName());
                    log.info("affliateBankAccount:: " + affliateBankAccount);
                    log.info("no of Transactions:: " + value.size());
                    log.info("total amt of Transactions:: " + totalMFBAmount);
                    String[] fields = new String[]{"corporate", "initiatingBank", "transactionId", "beneficaryBank", "paymentBank", "beneficaryName", "beneficaryAccount", "narration", "amount", "authDate", "authBy"};
                    byte[] excel = AppUtil.convertExcel(value, fields);
                    MailUtil.getInstance().sendMFBTransactionMail(new String[]{mfbankEmail}, mfbankName, affliateBank.getBankName(), affliateBankAccount, value.size(), AppUtil.formatAsAmount(totalMFBAmount.toString()), excel, supportEmails);
                }
            }
            User loggedUser = getLoggedUser();
            String authorizer = loggedUser.getFirstName() + "(" + loggedUser.getUsername() + ")";
            String authorizerEmail = loggedUser.getEmail();
            String authorizerPhone = loggedUser.getMobilePhone();
            t.setAuthorizedBy(authorizer);
            t.setDateAuthorized(new Date());
            if (preference != null && preference.getMinimumPaymentAmount() != null && !preference.getMinimumPaymentAmount().isEmpty()
                    && preference.getMinimumPaymentAmountEmails() != null && !preference.getMinimumPaymentAmountEmails().isEmpty()) {
                if (new BigDecimal(preference.getMinimumPaymentAmount()).compareTo(totalAmount) <= 0) {
                    String[] bankThreshAlertBeneficiaries = preference.getMinimumPaymentAmountEmails().split(",");
                    String cardNumber1 = t.getCard().getCardNumber();
                    int length = cardNumber1.substring(9, cardNumber1.length()).length();
                    cardNumber1 = cardNumber1.substring(0, 9);
                    for (i = 0; i < length; i++) {
                        cardNumber1 = cardNumber1 + "*";
                    }
                    MailUtil.getInstance().sendThresholdAlert(bankThreshAlertBeneficiaries, "08" + t.getId().toString(), authorizer, t.getCorporate().getCorporateName(), totalAmount.toString(), String.valueOf(t.getTransactions().size()), cardNumber1);
                }
            }

            String contact = authorizerPhone != null ? (authorizerPhone + ";") : "" + authorizerEmail != null ? (authorizerEmail + "; ") : "" + t.getCorporate().getContactEmail() == null ? "" : "; " + t.getCorporate().getContactEmail() + t.getCorporate().getContactPhone() == null ? "" : "; " + t.getCorporate().getContactPhone();
            bsupportEmails = preference == null ? bsupportEmails : preference.getAuthorizationSupportEmails() == null ? bsupportEmails : preference.getAuthorizationSupportEmails().split(",");
            MailUtil.getInstance().sendPaymentApproved(supportEmails, bsupportEmails, "08" + t.getId().toString(), authorizer, t.getCorporate().getCorporateName(), totalAmount.toString(), String.valueOf(t.getTransactions().size()), contact, cardNumber, t.getPaymentDescription(), t.getAuthorizedDateStr());
            auditLogFacadeLocal.create(buildAuditLog(AppConstants.AUDIT_TYPE_PAYMENT_AUTHORIZATION, t.getPaymentDescription() + "/NGN" + t.getTotalAmount(), null));

        }
        t = em.merge(t);
        return t;
    }

    @Override
    public void increaseTrial(Long paymentId) throws Exception {
        Payment t = this.find(paymentId);
        t.setTrial(t.getTrial() + 1);
    }

    @Override
    public void increaseTrial(Long paymentId, com.etranzact.corporatepay.model.Card card) throws Exception {
        Payment t = this.find(paymentId);
        t.setTrial(t.getTrial() + 1);
        t.setCard(card);
    }

    @Override
    public Payment create(Payment entity) throws Exception {
        TransactionType transactionType = null;
        long randomPin = AppUtil.generateRandomPin();
        if (entity instanceof SingleBeneficiaryPayment) {
            log.info("paying single beneficiary");
            transactionType = em.find(TransactionType.class, "#SBEN");
            SingleBeneficiaryPayment beneficiaryPayment = (SingleBeneficiaryPayment) entity;
            Set<Beneficiary> beneficiaries = beneficiaryPayment.getBeneficiaries();
            for (Beneficiary beneficiary : beneficiaries) {
                Transaction transaction = new Transaction();
                transaction.setAccount(beneficiary.getAccount());
                if (beneficiary.getAmount() == null) {
                    throw new Exception("Amount must be entered");
                }

                transaction.setNarration(beneficiary.getNarration());
                transaction.setBeneficiary(beneficiary);
                transaction.setCorporate(getLoggedCorporate());
                transaction.setTransactionType(transactionType.getId());
                transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_CREATED_OK);
                transaction.setTransactionReference(String.valueOf(AppUtil.generateRandomPin2()));
                BigDecimal[] charges ;
                if("0".equals(transaction.getCorporate().getCommissionType())) {
                    charges = calculateCharge(transaction.getCorporate(), transaction.getAmount()); 
                } else {
                    transaction.setAmount(beneficiary.getAmount());
                    charges = calculateGradCharges(transaction);
                    
                }
               
                transaction.setEtzFeeAmount(charges[0]);
                transaction.setBankFeeAmount(charges[1]);
                transaction.setThirdpartyFeeAmount(charges[2]);
                transaction.setThirdpartyFeeAmount2(charges[3]);
                transaction.setTotalFeeAmount(charges[0].add(charges[1]).add(charges[2]).add(charges[3]));
                transaction.setAmount(AppConstants.CHARGE_BEARER_BENEFICIARY.equals(beneficiaryPayment.getChargeBearer()) ? beneficiary.getAmount().subtract(transaction.getTotalFeeAmount()) : beneficiary.getAmount());
                transaction.setCheckSum(generateCheckSum(transaction.getAccount().getAccountNumber(), transaction.getAmount().toString(), transaction.getAccount() instanceof BankAccount ? ((BankAccount) transaction.getAccount()).getBank().getId() : "700"));
                Transaction merge = em.merge(transaction);
                entity.getTransactions().add(merge);
            }
        } else if (entity instanceof UploadPayment) {
            UploadPayment uploadPayment = (UploadPayment) entity;
            String uploadPath = uploadPayment.getUploadPath();
            log.info("uploadPath:::: " + uploadPath);
            String filename = uploadPath.split("\\.")[0];
            String ext = uploadPath.split("\\.")[1];
            uploadPayment.setUploadPath(filename + "_" + randomPin + "." + ext);
            transactionType = uploadPayment.getUploadType();
            uploadFile(uploadPayment.getUploadPath(), uploadPayment.getUpload(), transactionType);
        } else if (entity instanceof SchedulePayment) {
            PaymentScheduler paymentScheduler = em.find(PaymentScheduler.class, ((SchedulePayment) entity).getSchedulerId());
            if (paymentScheduler.getPayment() instanceof SingleBeneficiaryPayment) {
                transactionType = em.find(TransactionType.class, "#SBEN");
            } else if (paymentScheduler.getPayment() instanceof UploadPayment) {
                transactionType = ((UploadPayment) paymentScheduler.getPayment()).getUploadType();
            }
            Set<Transaction> schedulePaymentTrans = new HashSet<>();
            for (Transaction transaction : entity.getTransactions()) {
                Transaction t = new Transaction();
                transaction.setPaymentDate(new Date());
                BeanUtils.copyProperties(t, transaction);
                transaction.setPaymentDate(null);
                t.setTransactionId(null);
                t.setPaymentDate(null);
                t.setSchedule(null);
                t = em.merge(t);
                schedulePaymentTrans.add(t);
            }
            entity.setTransactions(schedulePaymentTrans);
            ((SchedulePayment) entity).setTransactionType(transactionType);
        }
        entity.setPaymentReference(transactionType.getId().substring(1) + "/" + randomPin);
//        Map<String, Object> map = new HashMap<>();
//        map.put("transactionType", transactionType);
//        map.put("bank", getLoggedCorporate().getBank());
//        map.put("central", false);
//        List<BankCommission> bankCommissions = bankCommissionFacadeLocal.findAll(map, true);
//        if (bankCommissions.isEmpty() && getLoggedCorporate().getApplyCommission()) {
//            throw new PersistenceException(transactionType.getDescription() + " Payment Not Available, Contact Your Bank");
//        }
//        if (bankCommissions.size() > 1) {
//            throw new PersistenceException("Unexpected Outcome, Contact your Bank");
//        }
//        BankCommission bankCommission = bankCommissions.isEmpty() ? null : bankCommissions.get(0);
//        entity.setCommissionFees(bankCommission);
        entity.setCorporate(getLoggedCorporate());
        entity.setFlagStatus(entity.getFlagStatus() == null ? AppConstants.CREATED : entity.getFlagStatus());
        entity = super.create(entity);
        for (Transaction transaction : entity.getTransactions()) {
            transaction.setPayment(entity);
            transaction.setTransactionReference(entity.getPaymentReference() + "/" + ((entity instanceof SchedulePayment) ? transaction.getTransactionReference() : transaction.getTransactionReference()));
            transaction.setNarration(transaction.getTransactionReference() + ";" + transaction.getNarration());
        }
        return entity;
    }

    @Override
    public Payment createSupplementaryFile(Payment entity) throws Exception {
        UploadPayment uploadPayment = (UploadPayment) entity;
        String uploadPath = uploadPayment.getSuppUploadPath();
        String filename = uploadPath.split("\\.")[0];
        String ext = uploadPath.split("\\.")[1];
        uploadPayment.setSuppUploadPath(filename + "_" + AppUtil.generateRandomPin() + "." + ext);
        uploadFile(uploadPayment.getSuppUploadPath(), uploadPayment.getUpload(), uploadPayment.getUploadType());
        return super.create(entity);
    }

    @Override
    public void initiatePayment(Payment t, String approvalPath) throws Exception {
        for (Transaction transaction : t.getTransactions()) {
            transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_APPROVAL);
            em.merge(transaction);
        }
        t.setFlagStatus(AppConstants.CREATED_IN_APPROVAL);
        t = super.create(t);
        paymentWorkflowFacadeLocal.startWorkflow(this, Payment.class, Long.valueOf(t.getId().toString()), AppConstants.REQUEST_CREATION_APPROVAL, approvalPath, null);
    }

    @Override
    public Payment edit(Payment entity) throws Exception {
        return super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }

    private void uploadFile(String fileName, byte[] upload, TransactionType transactionType) throws Exception {
        FileOutputStream stream = new FileOutputStream(ConfigurationUtil.instance().getUploadDir()
                + File.separator + transactionType.getId() + File.separator + fileName);
        stream.write(upload);
        stream.flush();
        stream.close();
    }

    public String authorizePayment(boolean tmcBank, String reference, String merchantCode,
            String cardNum, String cardPin, String expYY, String expMM, BigDecimal transAmount, BigDecimal fee,
            String transDescr, String transCode) {
        String resp = "-1";

        if (cardPin == null || (cardPin != null && cardPin.trim().equalsIgnoreCase(""))) {
            log.info(" authorizePayment :::::: no pin");
        }

        try {
            //try { cardNum = Utility.decryptCard(cardNum); } catch (Exception e) {}
            //try { merchantCode = Utility.decryptCard(merchantCode); } catch (Exception e) {}

            //if (cardNum != null && (cardNum.startsWith("014") || cardNum.startsWith("700") || cardNum.startsWith("701") || cardNum.startsWith("702"))) {
//            if (cardNum != null && (cardNum.startsWith("014") || cardNum.startsWith("7"))) {
            if (tmcBank) {
                log.info("MG Processnig.........");
                resp = authorizeMGPayment(reference, merchantCode, cardNum, cardPin, expYY, expMM, transAmount, fee, transDescr);
                log.info("resp........." + resp);
            } /*else if (cardNum != null && cardNum.length() == 16) {
             resp = authorizePayment(reference, merchantCode, cardNum, cardPin, expYY, expMM, transAmount, fee,transDescr);
             }*/ else {
                Map<String, Object> map = new HashMap<>();
                map.put("op", "TRANSFER");
                map.put("cardNum", cardNum);
                map.put("merchantCode", merchantCode);
                map.put("cardPin", cardPin);
                map.put("expYY", expYY);
                map.put("expMM", expMM);
                map.put("transAmount", transAmount);
                map.put("fee", fee);
                map.put("transDescr", transDescr);
                map.put("transCode", transCode);
                map.put("reference", reference);
                log.info("HTTP service processing.........");
                resp = AppUtil.doHttpPost2(ConfigurationUtil.instance().getPaymentServiceURL(), map);
                log.info("resp........." + resp);
            }

            //log.info(" PaymentClient (authorizePayment) >>>>>> " + resp);
        } catch (Exception ex) {
            //ex.printStackTrace(LogWriter.appClient);
            ex.printStackTrace();
            resp = "-1";
        } finally {
            try {
                if (StringUtils.equals(resp, "-1")) {
                    log.info("sending reversal .........");
                    //int response = 1;
                    if (StringUtils.startsWith(cardNum, "014") || StringUtils.startsWith(cardNum, "7")) {
                        reverseMGTransaction(reference, merchantCode, cardNum, cardPin, expYY, expMM,
                                transAmount, fee, transDescr);
                    } else {
                        reverseTransaction(StringUtils.substring(merchantCode, 0, 3), reference);
                    }

                    //log.info("sending reversal ......... response ::::::: " + response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return resp;
    }

    private String authorizeMGPayment(String reference, String merchantCode,
            String cardNum, String cardPin, String expYY, String expMM, BigDecimal transAmount, BigDecimal fee,
            String transDescr) {
        String resp = "-1";

        try {
            String ip = ConfigurationUtil.instance().getAutoSwitchIP();
            int port = ConfigurationUtil.instance().getAutoSwitchPort();

            XProcessor processor = new XProcessor();

            HttpHost host = new HttpHost();
            host.setServerAddress(ip);
            host.setPort(port);
            String key = "123456";
            host.setSecureKey(key);

            XRequest request = new XRequest();
            Card card = new Card();
            card.setCardNumber(cardNum);
            card.setCardExpiration(expMM + "" + expYY);
            card.setCardPin(cardPin);
            request.setCard(card);

            request.setChannelId("08");
            request.setDescription(transDescr);
            try {
                request.setFee(fee.doubleValue());
            } catch (Exception e) {
                request.setFee(0.00d);
            }

            //if (cardNum != null && (cardNum.startsWith("014") || cardNum.startsWith("700") || cardNum.startsWith("701") || cardNum.startsWith("702"))) {
            if (cardNum != null && (cardNum.startsWith("014") || cardNum.startsWith("7"))) {
                request.setTransCode(TransCode.EFT);
            } else {
                request.setTransCode(TransCode.EFT_NORMAL);
            }

            request.setMerchantCode(merchantCode);

            request.setReference(reference);
            request.setOtherReference("");
            try {
                request.setTransAmount(transAmount.doubleValue());
            } catch (Exception e) {
                request.setTransAmount(0.00d);
            }

            log.info(" CPAY service locaton (authorizePayment) >>>>>> " + host.getServerAddress());
            log.info(" CPAY service port (authorizePayment) >>>>>> " + host.getPort());
            XResponse response = processor.process(host, request);

            resp = "" + response.getResponse();
            log.info("desc: " + response.getMessage());
            log.info("bal: " + response.getBalance());
            log.info("ref:: " + response.getReference());
            log.info("cust xml:: " + response.getCustomXml());

            log.info(" PaymentClient (authorizePayment) >>>>>> " + resp);
        } catch (Exception ex) {
            //ex.printStackTrace(LogWriter.appClient);
            log.info(" CPAY service locaton (authorizePayment) errrrrrrooooorrrrrrr>>>>>>");
            ex.printStackTrace();
            resp = "-1";
        } finally {
            if (StringUtils.equals(resp, "-1")) {
                log.info(" Sending reversal ..........................");
                reverseMGTransaction(reference, merchantCode, cardNum, cardPin, expYY, expMM, transAmount, fee, transDescr);
            }
        }

        return resp;
    }

    public void reverseTransaction(String issuerCode, String uniqueReference) {
        //int bal = -1;
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("op", "REVERSE");
            map.put("ISSUER_CODE", issuerCode);
            map.put("UNIQUE_REF", uniqueReference);

            String resp = AppUtil.doHttpPost2(ConfigurationUtil.instance().getPaymentServiceURL(), map);
//            System.out.println(" FundGate PaymentClient (reverseTransaction) resp >>>>>> " + resp);
//            bal = Integer.valueOf(resp);

            log.info(" (reverseTransaction) reponse (" + uniqueReference + ") >>>>>> " + resp);
        } catch (Exception ex) {
            //ex.printStackTrace(LogWriter.appClient);
            ex.printStackTrace();
            //bal = -1;
        }

        //return bal;
    }

    public void reverseMGTransaction(String reference, String merchantCode,
            String cardNum, String cardPin, String expYY, String expMM, BigDecimal transAmount, BigDecimal fee,
            String transDescr) {
        // int resp = -1;

        try {

            String ip = ConfigurationUtil.instance().getAutoSwitchIP();
            int port = ConfigurationUtil.instance().getAutoSwitchPort();

            XProcessor processor = new XProcessor();
            HttpHost host = new HttpHost();
            host.setServerAddress(ip);
            host.setPort(port);
            String key = "123456";
            host.setSecureKey(key);

            XRequest request = new XRequest();

            Card card = new Card();
            card.setCardNumber(cardNum);
            card.setCardExpiration(expMM + "" + expYY);
            card.setCardPin(cardPin);
            request.setCard(card);

            //System.out.println(" (authorizePayment) card.getCardPin() >>>>>> " + card.getCardPin());
            request.setChannelId("08");
            request.setDescription(transDescr == null ? "RVSR" : transDescr.concat("RVSR"));
            request.setFee(fee.doubleValue());

            //if (StringUtils.equalsIgnoreCase(msg, "P") || StringUtils.equalsIgnoreCase(msg, "V")) {
            request.setTransCode(TransCode.REVERSAL);
            request.setMerchantCode(merchantCode);
            /*} else {
             if (StringUtils.equalsIgnoreCase(accountType, "A")) {
             request.setTransCode(TransCode.REVERSAL);
             request.setMerchantCode(destinationIssuer.trim().concat(merchantCode));
             } else {
             request.setTransCode(TransCode.REVERSAL);
             request.setMerchantCode(merchantCode);
             }
             }*/

            //TransCode.PAYMENT;
            request.setReference(reference);
            request.setOtherReference("");
            request.setTransAmount(transAmount.doubleValue());

            log.info(" (reverseTransaction) >>>>>> " + host.getServerAddress());
            XResponse response = processor.process(host, request);

            int resp = response.getResponse();

            log.info(" (reverseTransaction) PocketMoni (" + reference + ") >>>>>> " + resp);
        } catch (Exception ex) {
            ex.printStackTrace();
            //resp = -1;
        }

        //return resp;
    }

    public static void main(String[] args) {
        String cardNumber1 = "12345678901234";
        int length = cardNumber1.substring(9, cardNumber1.length()).length();
        cardNumber1 = cardNumber1.substring(0, 9);
        for (int i = 0; i < length; i++) {
            cardNumber1 = cardNumber1 + "*";

        }
        System.out.println("cardnumb:: " + cardNumber1);
    }

    private BigDecimal[] calculateCharge(Corporate corporate, BigDecimal principalAmount) throws PersistenceException {
        String commissionType = corporate.getCommissionType();
        BigDecimal[] charges = new BigDecimal[]{BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO};
        try {
                charges[0] = corporate.getEtzCommission();

                if (corporate.getBankCommissionType() != null && corporate.getBankCommission() != null) {
                    BigDecimal bankFee = charges[1];
                    if (corporate.getBankCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                        bankFee = corporate.getBankCommission();
                    } else if (corporate.getBankCommissionType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                        bankFee = (corporate.getBankCommission().divide(new BigDecimal("100"))).multiply(principalAmount);
                    }
                    charges[1] = bankFee;
                }
            
            if (corporate.getThirdPartyCommissionType() != null && corporate.getThirdPartyCommission() != null) {

                BigDecimal thirdPartyFee = charges[2];
                if (corporate.getThirdPartyCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    thirdPartyFee = corporate.getThirdPartyCommission();
                } else if (corporate.getThirdPartyCommissionType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    thirdPartyFee = (corporate.getThirdPartyCommission().divide(new BigDecimal("100"))).multiply(principalAmount);
                }
                charges[2] = thirdPartyFee;

            }
            if (corporate.getThirdPartyCommissionType2() != null && corporate.getThirdPartyCommission2() != null) {

                BigDecimal thirdParty2Fee = charges[3];
                if (corporate.getThirdPartyCommissionType2().equals(AppConstants.AMOUNT_TYPE_FIXED)) {
                    thirdParty2Fee = corporate.getThirdPartyCommission2();
                } else if (corporate.getThirdPartyCommissionType2().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    thirdParty2Fee = (corporate.getThirdPartyCommission2().divide(new BigDecimal("100"))).multiply(principalAmount);
                }
                charges[3] = thirdParty2Fee;
            }
        } catch (Exception e) {
            Logger.getLogger(PaymentFacade.class.getName()).log(Level.SEVERE, null, e);
            throw new PersistenceException("Internal Error, please contact the administrator");
        }
        return charges;
    }

    
     private static BigDecimal calculateGradComm(boolean cummulative, List<CommissionFee> commissionFees, BigDecimal transAmount) {
        BigDecimal commission = BigDecimal.ZERO;
        if (cummulative) {
            for (CommissionFee commissionFee : commissionFees) {
                if (transAmount.compareTo(new BigDecimal(commissionFee.getMinRange())) >= 0) {
                    if (commissionFee.getAmountType().equals("FX")) {
                        commission = commission.add(commissionFee.getAmount());
                    } else {
                        commission = commission.add((commissionFee.getAmount().divide(new BigDecimal("100")).multiply(transAmount)));
                    }
                }
            }
        } else {
            for (CommissionFee commissionFee : commissionFees) {
                if (transAmount.compareTo(new BigDecimal(commissionFee.getMinRange())) >= 0 && transAmount.compareTo(new BigDecimal(commissionFee.getMaxRange())) <= 0) {
                    if (commissionFee.getAmountType().equals("FX")) {
                        commission = commissionFee.getAmount();
                    } else {
                        commission = commissionFee.getAmount().divide(new BigDecimal("100")).multiply(transAmount);
                    }
                } else if (transAmount.compareTo(new BigDecimal(commissionFee.getMaxRange())) >= 0) {
                    if (commissionFee.getAmountType().equals("FX")) {
                        commission = commissionFee.getAmount();
                    } else {
                        commission = commissionFee.getAmount().divide(new BigDecimal("100")).multiply(transAmount);
                    }
//                    break;
                }
            }
        }
        return commission;
    }

     
     private BigDecimal[] calculateGradCharges(Transaction transaction) {
         BigDecimal[] charges = new BigDecimal[4];
         Corporate corporate = transaction.getCorporate();
         Commission gradBankCommission = corporate.getGradBankCommission();
            Set<CommissionFee> commissionFees = gradBankCommission.getCommissionFees();
            int calMode = corporate.getGradBankCommission().getCalculationMode();
            boolean cumm = calMode == 0;
            Comparator<CommissionFee> comparable = new Comparator<CommissionFee>() {

                @Override
                public int compare(CommissionFee o1, CommissionFee o2) {

                    int val = new BigDecimal(o1.getMinRange()).compareTo(new BigDecimal(o2.getMinRange()));
                    return val;
                }
            };
            List<CommissionFee> comFees = new ArrayList<>();
            comFees.addAll(commissionFees);
               Collections.sort(comFees, comparable);
//            int i = 1;
//            for (CommissionFee commissionFee : comFees) {
////                logger.log(Level.INFO, i + "  .... min{0}", commissionFee.getMinRange());
////                logger.log(Level.INFO, i + "  .... max{0}", commissionFee.getMaxRange());
//                i++;
//            }
         
            BigDecimal calculateGradComm = calculateGradComm(cumm, comFees, transaction.getAmount());
                BigDecimal etzPercent = transaction.getCorporate().getGradBankCommission().getEtzPercent();
                etzPercent = etzPercent==null?BigDecimal.ZERO:etzPercent;
                charges[0]=etzPercent.divide(new BigDecimal("100")).multiply(calculateGradComm);
                BigDecimal thirdParty1Percent = transaction.getCorporate().getGradBankCommission().getThirdPartyPercent1();
                thirdParty1Percent = thirdParty1Percent==null?BigDecimal.ZERO:thirdParty1Percent;
                charges[2] = thirdParty1Percent.divide(new BigDecimal("100")).multiply(calculateGradComm);
                BigDecimal thirdParty2Percent = transaction.getCorporate().getGradBankCommission().getThirdPartyPercent2();
                thirdParty2Percent = thirdParty2Percent==null?BigDecimal.ZERO:thirdParty2Percent;
                charges[3] = thirdParty2Percent.divide(new BigDecimal("100")).multiply(calculateGradComm);
               
                BigDecimal bankCommPercent = new BigDecimal("100").subtract(etzPercent.add(thirdParty1Percent).add(thirdParty2Percent));
                charges[1] = bankCommPercent.divide(new BigDecimal("100")).multiply(calculateGradComm);
                return charges;
            }
        
     
}
