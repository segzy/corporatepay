/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.Transaction;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class TransactionWorkflowFacade extends AbstractWorkflowFacade implements TransactionWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        Transaction transaction = (Transaction) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(0, new TaskObjectNameValuePair("Transaction Reference: ", "08" + transaction.getTransactionId(),
                null));
        nameValuePairs.add(1, new TaskObjectNameValuePair("Corporate ", transaction.getCorporate().getCorporateName(), null));
        nameValuePairs.add(2, new TaskObjectNameValuePair("Beneficiary Name ", transaction.getBeneficaryName(), null));
        nameValuePairs.add(3, new TaskObjectNameValuePair("Account Number ", transaction.getAccount().getAccountNumber(), transaction.getAccount().getTmpAccountNumber()));

        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
