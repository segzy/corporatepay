/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.setup.facade.*;
import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface TransactionFacadeLocal extends AbstractFacadeLocal<Transaction> {
//
    public void openOfflineTransactions(List<Transaction> transactions) throws Exception;
//    
//    public void initiatePayment(Payment t,String approvalPath) throws Exception;
//    
//    public Payment edit(Payment t) throws Exception;
    
    public void save(BigInteger transactionId, String account) throws Exception;
    
    public void finalizeApprove(Transaction t, ApprovalTask task) throws Exception;

}
