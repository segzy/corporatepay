/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.PaymentScheduler;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class SchedulerFacade extends AbstractFacade<PaymentScheduler> implements SchedulerFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public SchedulerFacade() {
        super(PaymentScheduler.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected PaymentScheduler edit(PaymentScheduler entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PaymentScheduler create(PaymentScheduler entity) throws Exception {
        PaymentScheduler create = super.create(entity);
        for(Transaction transaction: entity.getPayment().getTransactions()) {
//            if(transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PAID)) {
                Transaction t = new Transaction();
                transaction.setPaymentDate(new Date());
                transaction.setBankFeeAmount(BigDecimal.ZERO);
                transaction.setEtzFeeAmount(BigDecimal.ZERO);
                transaction.setThirdpartyFeeAmount(BigDecimal.ZERO);
                transaction.setThirdpartyFeeAmount2(BigDecimal.ZERO);
                BeanUtils.copyProperties(t, transaction);
                transaction.setPaymentDate(null);
                t.setTransactionId(null);
                t.setPaymentDate(null);
                t.setSchedule(create);
                t.setTrsnsactionStatus(AppConstants.TRANSACTION_CREATED_OK);
                t = em.merge(t);
                create.getTransactions().add(t);
//            }
        }
        entity.getPayment().setPaymentReference("SCHEDULE/" + entity.getPayment().getPaymentReference());
        em.merge(entity.getPayment());
        return create;
    }

    @Override
    public void refresh(PaymentScheduler paymentScheduler) throws Exception {
        PaymentScheduler find = find(paymentScheduler.getScheduleId());
        Set<Transaction> allPaymentTrans = paymentScheduler.getPayment().getTransactions();
           for(Transaction transaction: allPaymentTrans) {
  
           if((!find.getTransactions().contains(transaction)) && transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PAID) && transaction.getSchedule()!=null) {
                 Transaction t = new Transaction();
                transaction.setPaymentDate(new Date());
                BeanUtils.copyProperties(t, transaction);
                transaction.setPaymentDate(null);
                t.setTransactionId(null);
                t.setPaymentDate(null);
                t.setTrsnsactionStatus(AppConstants.TRANSACTION_CREATED_OK);
                t.setSchedule(find);
                t = em.merge(t);
                find.getTransactions().add(t);
           }
       }
    }

}
