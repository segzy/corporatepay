package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.core.web.AbstractBean;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.PaymentScheduler;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.model.SingleBeneficiaryPayment;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.model.UploadPayment;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.payment.facade.SchedulerFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryGroupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.TransactionTypeFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import com.etz.http.etc.HttpHost;
import com.etz.http.etc.TransCode;
import com.etz.http.etc.VCardRequest;
import com.etz.http.etc.XProcessor;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.selectoneradio.SelectOneRadio;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "paymentBean")
@SessionScoped
public class PaymentBean extends AbstractBean<Payment> {

    @EJB
    private PaymentFacadeLocal paymentFacadeLocal;

    private Payment selectedItem2;

    @EJB
    private BeneficiaryFacadeLocal beneficiaryFacadeLocal;

    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;

    @EJB
    private BeneficiaryGroupFacadeLocal beneficiaryGroupFacadeLocal;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;
    private List<BeneficiaryGroup> beneficiaryGroups;
    private BeneficiaryGroup beneficiaryGroup;
    private Beneficiary beneficary;
    private List banks;
    private String acctOption = "PM";

    @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;

    @EJB
    private TransactionTypeFacadeLocal transactionTypeFacadeLocal;

    @EJB
    private SchedulerFacadeLocal paymentScheduleFacadeLocal;
    private int focusIndex;
    private TransactionType transactionType;
    private List<TransactionType> transactionTypes;
    private byte[] upload;
    private String route;
    private List<Object> routes;
    private boolean header = true;
    private Set<Transaction> transactions;
    private String defaultBen;

    protected PaymentFacadeLocal getFacade() {
        return this.paymentFacadeLocal;
    }

    public PaymentBean() {
        super(Payment.class);
    }

    public List<Beneficiary> completeBeneficary(String query) {
        try {
            return this.beneficiaryFacadeLocal.findBeneficiaries(query);
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList();
    }

    public void handleGroupChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        try {
            Object value = ((UISelectOne) e.getComponent()).getValue();
            BeneficiaryGroup benGroup = (BeneficiaryGroup) value;
            List<Beneficiary> findBeneficaries = this.beneficiaryFacadeLocal.findBeneficaries(benGroup);
            log.info("beneficiaries " + findBeneficaries.size());
            ((SingleBeneficiaryPayment) this.selectedItem).setBeneficiaries(new HashSet());
            for (Beneficiary ben : findBeneficaries) {
                ((SingleBeneficiaryPayment) this.selectedItem).getBeneficiaries().add(ben);
            }
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            ((SingleBeneficiaryPayment) this.selectedItem).setBeneficiaries(new HashSet());
        }
    }

    public void onRowSelect(SelectEvent selectEvent) {
        try {
            setSelectedItem2((Payment) selectEvent.getObject());
            this.selectedItem2 = ((Payment) this.paymentFacadeLocal.find(this.selectedItem2.getId()));
            for (Transaction transaction : this.selectedItem2.getTransactions()) {
                switch (transaction.getTrsnsactionStatus()) {
                    case "1":
                        this.selectedItem2.setAmountProcessedOK(this.selectedItem2.getAmountProcessedOK().add(transaction.getAmount()));
                        this.selectedItem2.setProcessedCountOK(Integer.valueOf(this.selectedItem2.getProcessedCountOK().intValue() + 1));
                        break;
                    case "2":
                        this.selectedItem2.setAmountPendingApproval(this.selectedItem2.getAmountPendingApproval().add(transaction.getAmount()));
                        this.selectedItem2.setPendingApproval(Integer.valueOf(this.selectedItem2.getPendingApproval().intValue() + 1));
                        break;
                    case "3":
                        this.selectedItem2.setAmountPendingPayment(this.selectedItem2.getAmountPendingPayment().add(transaction.getAmount()));
                        this.selectedItem2.setPendingPayment(Integer.valueOf(this.selectedItem2.getPendingPayment().intValue() + 1));
                        break;
                    case "4":
                        this.selectedItem2.setAmountPaid(this.selectedItem2.getAmountPaid().add(transaction.getAmount()));
                        this.selectedItem2.setPaid(Integer.valueOf(this.selectedItem2.getPaid().intValue() + 1));
                }
            }

            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('transactionssummary').show();");
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void viewTransaction(Payment payment) {
        try {
            this.selectedItem2 = ((Payment) this.paymentFacadeLocal.find(payment.getId()));
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('transactions').show();");
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initiateWorkflow() {
        FacesMessage m = new FacesMessage();
        try {
            getFacade().initiatePayment((Payment) this.selectedItem, this.route);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payment Approval Initiated Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            nullifySelection();
        }
    }

    public void initiateWorkflow2() {
        FacesMessage m = new FacesMessage();
        try {
            getFacade().initiatePayment(this.selectedItem2, this.route);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payment Approval Initiated Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            nullifySelection2();
        }
    }

    public void markBeneficary(Beneficiary beneficiary) {
        this.beneficary = beneficiary;
        this.beneficary.setAmount(null);
        this.beneficary.setNarration(null);
    }

    public void initiateWorkflowLater() {
        nullifySelection();
    }

    public void initiateWorkflowLater2() {
        nullifySelection2();
    }

    public void nullifySelection() {
        if ((this.selectedItem instanceof SingleBeneficiaryPayment)) {
            this.selectedItem = new SingleBeneficiaryPayment();
        } else if ((this.selectedItem instanceof UploadPayment)) {
            this.selectedItem = new UploadPayment();
        }
    }

    public void nullifySelection2() {
        if ((this.selectedItem2 instanceof SingleBeneficiaryPayment)) {
            this.selectedItem2 = new SingleBeneficiaryPayment();
        } else if ((this.selectedItem2 instanceof UploadPayment)) {
            this.selectedItem2 = new UploadPayment();
        }
    }

    public void createSchedule() {
        FacesMessage m = new FacesMessage();
        try {
            PaymentScheduler paymentScheduler = new PaymentScheduler();
            paymentScheduler.setPayment(this.selectedItem2);
            paymentScheduler.setCorporate(getLoggedCorporate());
            paymentScheduler.setFlagStatus("CR");
            paymentScheduler.setScheduleId(this.selectedItem2.getPaymentReference());
            this.paymentScheduleFacadeLocal.create(paymentScheduler);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payment Schedule Created");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            nullifySelection2();
        }
    }

    public void onTabChange(TabChangeEvent event) {
        String tabTitle = event.getTab().getTitle();
        switch (tabTitle) {
            case "Pay Single/Multiple Beneficiary":
                this.beneficiaryGroup = new BeneficiaryGroup();
                this.beneficary = new Beneficiary();
                this.transactionType = new TransactionType();
                this.selectedItem = new SingleBeneficiaryPayment();
                this.focusIndex = 1;
                break;
            case "Pay Group Beneficiary":
                this.beneficiaryGroup = new BeneficiaryGroup();
                this.beneficary = new Beneficiary();
                this.transactionType = new TransactionType();
                this.selectedItem = new SingleBeneficiaryPayment();
                this.focusIndex = 2;
                break;
            case "Upload":
                initUpload();
                break;
        }
        this.selectedItem.setChargeBearer(defaultBeneficiary());
    }

    private void initUpload() {
        this.selectedItem = new UploadPayment();
        this.transactionType = new TransactionType();
        this.focusIndex = 0;
        if (this.transactionTypes == null) {
            try {
                Map map = new HashMap();
                map.put("upload", Boolean.valueOf(true));
                this.transactionTypes = this.transactionTypeFacadeLocal.findAll(map, false);
            } catch (Exception ex) {
                Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
                this.transactionTypes = new ArrayList();
            }
        }
    }

    public void save() {
        log.info("saving payment............");
        FacesMessage m = new FacesMessage();
        try {
            if ((this.selectedItem instanceof UploadPayment)) {
                ((UploadPayment) this.selectedItem).setUploadType(this.transactionType);
                if (this.upload == null) {
                    throw new Exception("No File Uploaded Yet");
                }
                ((Payment) this.selectedItem).setFlagStatus("PR");
                ((UploadPayment) this.selectedItem).setHeader(this.header);
                ((UploadPayment) this.selectedItem).setUpload(this.upload);
                ((UploadPayment) this.selectedItem).setUploaderContact(getLoggedUser().getEmail());
            } else if ((this.selectedItem instanceof SingleBeneficiaryPayment)) {
                Set beneficiaries = ((SingleBeneficiaryPayment) this.selectedItem).getBeneficiaries();
                if (((SingleBeneficiaryPayment) this.selectedItem).getBeneficiaries().isEmpty()) {
                    throw new Exception("There must be at least one beneficiary");
                }
                ((SingleBeneficiaryPayment) this.selectedItem).setBeneficiaries(beneficiaries);
            }
            Payment cPayment;
            if (((Payment) this.selectedItem).getPaymentReference() == null) {
                cPayment = (Payment) getFacade().create(this.selectedItem);
            } else {
                cPayment = getFacade().edit((Payment) this.selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payment Created Successfully");
            m.setDetail("Payment Reference: " + cPayment.getPaymentReference());
            FacesContext.getCurrentInstance().addMessage(null, m);
            this.header = true;
            this.upload = null;
            //RequestContext context = RequestContext.getCurrentInstance();
            if (this.focusIndex == 0) {
                // context.execute("PF('startWorkflow').show();");
                initiateWorkflowLater();
            } else if (this.focusIndex == 1) {
                initiateWorkflowLater();
                // context.execute("PF('gstartWorkflow').show();");
            } else if (this.focusIndex == 2) {
                initiateWorkflowLater();
            }
            selectedItem2 = null;
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void addSupplementaryFile() {
        FacesMessage m = new FacesMessage();
        try {
            if ((this.selectedItem2 instanceof UploadPayment)) {
                if (this.upload == null) {
                    throw new Exception("No File Uploaded Yet");
                }
                this.selectedItem2.setFlagStatus("PR");
                ((UploadPayment) this.selectedItem2).setHeader(Boolean.valueOf(this.header));
                ((UploadPayment) this.selectedItem2).setUpload(this.upload);
                ((UploadPayment) this.selectedItem2).setSuppUploadPath(((UploadPayment) this.selectedItem2).getUploadPath());
                ((UploadPayment) this.selectedItem2).setUploadPath(((Payment) this.paymentFacadeLocal.find(this.selectedItem2.getId())).getUploadPath());
                this.paymentFacadeLocal.createSupplementaryFile(this.selectedItem2);
                nullifySelection2();
                m.setSummary("Supplementary Payment File Saved");
                m.setDetail("Supplementary Payment File Saved");
                FacesContext.getCurrentInstance().addMessage(null, m);
                this.header = false;
                this.upload = null;
                initUpload();
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('suplementaryfile').hide();");
            }
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void addBeneficary() {
        ((SingleBeneficiaryPayment) this.selectedItem).getBeneficiaries().add(getBeneficary());
        this.beneficary = new Beneficiary();
    }

    public void doValidation() {
     
        String accountName = "";
        if (this.beneficary.getAccount() instanceof BankAccount) {
            accountName = doBankNamedEnquiry(this.beneficary.getAccount().getAccountNumber(), ((BankAccount) this.beneficary.getAccount()).getBank().getId());
        } else if (this.beneficary.getAccount() instanceof PocketMoneyAccount) {
            accountName = doPMNamedEnquiry(this.beneficary.getAccount().getAccountNumber());
        }
        if (accountName.isEmpty()) {
            FacesMessage m = new FacesMessage();
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("We are not able to fufill this request for now, We promise to deliver this in your later requests. Kindly provide the account name yourself to continue");
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
        this.beneficary.getAccount().setAccountName(accountName);
    }

    public String doPMNamedEnquiry(String accountNumber) {
           log.info("doing PM named enquiry......... " + accountNumber);
        String autoswitch_ip = ConfigurationUtil.instance().getAutoSwitchIP();
        String autoswitch_port = String.valueOf(ConfigurationUtil.instance().getAutoSwitchPort());
        XProcessor processor = new XProcessor();
        HttpHost host = new HttpHost();
        host.setServerAddress(autoswitch_ip);
        host.setPort(Integer.valueOf(autoswitch_port));
        String key = "123456";
        host.setSecureKey(key);

        VCardRequest request2 = new VCardRequest();
        request2.setOtherReference("CPAY2" + System.currentTimeMillis());

        String schemeCode = "700";
        request2.setMobileNumber(accountNumber);
        request2.setRequestType(TransCode.CARDQUERY);
        request2.setSchemeCode(schemeCode);

        VCardRequest response = null;
        try {
            response = processor.process(host, request2);
        } catch (Exception e) {
            log.log(Level.SEVERE, "AutoClient Error", e);
        }
        log.log(Level.INFO, "AutoSwitch Response code: {0}", response!=null?response.getResponse():null);
        String details;
        if (response != null && response.getResponse() == 0) {
            details = response.getFirstName() + " " + response.getLastName();
            details = details.substring(0, details.length() - 2).trim() + "--" + response.getControlId();
            if (details.contains("~")) {
                details = details.split("\\~")[0];
            }
        } else {
            details = "";
                        FacesMessage m = new FacesMessage();
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("We are not able to fufill this request for now, We promise to deliver this in your later requests. Kindly provide the account name yourself to continue");
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
        return details;
    }

    public String doBankNamedEnquiry(String accountNumber, String bankCode) {
           log.info("doing PM named enquiry......... " + accountNumber + "  " + bankCode);
        String autoswitch_ip = ConfigurationUtil.instance().getAutoSwitchIP();
        String autoswitch_port = String.valueOf(ConfigurationUtil.instance().getAutoSwitchPort());
        XProcessor processor = new XProcessor();
        HttpHost host = new HttpHost();
        host.setServerAddress(autoswitch_ip);
        host.setPort(Integer.valueOf(autoswitch_port));
        String key = "123456";
        host.setSecureKey(key);

        VCardRequest request2 = new VCardRequest();
        request2.setOtherReference("CPAY2" + System.currentTimeMillis());

        request2.setRequestType(TransCode.ACCOUNTQUERY);
        request2.setAccountNumber(accountNumber);
        request2.setSchemeCode(bankCode);

        VCardRequest response = null;

        try {
            response = processor.process(host, request2);
        } catch (Exception e) {
            log.log(Level.SEVERE, "AutoClient Error", e);
        }
        log.log(Level.INFO, "AutoSwitch Response code: {0}", response!=null?response.getResponse():null);

        if (response != null && response.getResponse() == 0) {
            log.log(Level.INFO, "AUTOSWITCH RESPONSE : {0} {1}", new Object[]{response.getLastName(), response.getFirstName()});
            return response.getLastName() + " " + response.getFirstName();
        } else {
            log.log(Level.SEVERE, "BANK ACCOUNTQUERY Failed: " + (response!=null?response.getResponse():"") + " " + (response!=null?response.getStatus():""));
                        FacesMessage m = new FacesMessage();
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("We are not able to fufill this request for now, We promise to deliver this in your later requests. Kindly provide the account name yourself to continue");
            FacesContext.getCurrentInstance().addMessage(null, m);
            return "";
        }

    }

    public void onDialogClose(CloseEvent event) {
        this.log.info("dialog close event.....");
        this.acctOption = "PM";
        this.beneficary = new Beneficiary();
        PocketMoneyAccount pm = new PocketMoneyAccount();
        pm.setType(AppConstants.POCKET_MONEY_ACCOUNT_TYPE);
        this.beneficary.setAccount(pm);
    }
    
    public void onDialogCloseTransaction(CloseEvent event) {
        this.transactions = new HashSet<>();
    }

    public void onDialogClose3(CloseEvent event) {
        nullifySelection2();
    }

    public void onDialogClose2(CloseEvent event) {
        this.transactions = new HashSet();
    }

    public void handleAcctOptionChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        String option = (String) ((SelectOneRadio) e.getComponent()).getValue();
        if (option.equals("PM")) {
            PocketMoneyAccount pm = new PocketMoneyAccount();
            pm.setType(AppConstants.POCKET_MONEY_ACCOUNT_TYPE);
            this.beneficary.setAccount(pm);
        } else if (option.equals("BN")) {
            this.beneficary.setAccount(new BankAccount());
            if (this.banks == null) {
                try {
                    this.banks = this.bankSetupFacadeLocal.findAll(new HashMap(), true, new String[]{"bankName"});
                } catch (Exception ex) {
                    Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
                    this.banks = new ArrayList();
                }
            }
        }
        this.acctOption = option;
    }

    public void createBeneficiary() {
        FacesMessage m = new FacesMessage();
        try {
            Beneficiary findBeneficiary = beneficiaryFacadeLocal.findBeneficiary(this.beneficary.getAccount());
            if (findBeneficiary != null) {
                if (beneficary.getBeneficiaryGroup() != null) {
//                 findBeneficiary.setBeneficiaryGroup(beneficiaryGroup);
                    findBeneficiary.getAccount().setAccountName(beneficary.getAccount().getAccountName());
                     findBeneficiary.getAccount().setAccountNumber(beneficary.getAccount().getAccountNumber());
                    findBeneficiary.setBeneficiaryGroup(beneficary.getBeneficiaryGroup());
                    beneficiaryFacadeLocal.save(findBeneficiary);
                    throw new Exception("Beneficiary already exist but has been added to group and updated accordingly");
                }
                                if (beneficary.getBeneficiaryEmail() != null) {
//                 findBeneficiary.setBeneficiaryGroup(beneficiaryGroup);
                    findBeneficiary.setBeneficiaryEmail(beneficary.getBeneficiaryEmail());
                      findBeneficiary.getAccount().setAccountName(beneficary.getAccount().getAccountName());
                     findBeneficiary.getAccount().setAccountNumber(beneficary.getAccount().getAccountNumber());
                    beneficiaryFacadeLocal.save(findBeneficiary);
                    throw new Exception("Beneficiary already exist but has being updated accordingly");
                }
                throw new Exception("Beneficiary already exist");
            }
            this.beneficary = ((Beneficiary) this.beneficiaryFacadeLocal.create(this.beneficary));
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Beneficiary Created Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            if ((this.beneficary.getAccount() instanceof BankAccount)) {
                this.beneficary.setAccount(new BankAccount());
            } else if ((this.beneficary.getAccount() instanceof PocketMoneyAccount)) {
                PocketMoneyAccount pm = new PocketMoneyAccount();
                pm.setType("P");
                this.beneficary.setAccount(pm);
            }
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('createBeneficiary').hide();");
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void createBeneficiaryGroup() {
        FacesMessage m = new FacesMessage();
        try {
            this.beneficiaryGroup = ((BeneficiaryGroup) this.beneficiaryGroupFacadeLocal.create(this.beneficiaryGroup));
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Beneficiary Group Created Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            this.beneficiaryGroup = new BeneficiaryGroup();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("PF('createBeneficiaryGroup').hide();");
            this.beneficary.setBeneficiaryGroup(this.beneficiaryGroup);
            this.beneficiaryGroups = null;
            getBeneficiaryGroups();
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void removeBeneficary(Beneficiary beneficiary) {
        ((SingleBeneficiaryPayment) this.selectedItem).getBeneficiaries().remove(beneficiary);
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            UploadedFile f = event.getFile();
            f.getContentType();
            this.upload = new byte[f.getInputstream().available()];
            f.getInputstream().read(this.upload);
            f.getInputstream().close();
            ((UploadPayment) this.selectedItem).setUploadPath(f.getFileName() + "_" + AppUtil.generateRandomPin());
            FacesMessage msg = new FacesMessage("File Upload", f.getFileName() + "Payment uploaded successfully.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Payment getSelectedItem() {
        if (this.selectedItem == null) {
            this.selectedItem = new SingleBeneficiaryPayment();
        }
        selectedItem.setChargeBearer(defaultBeneficiary());
        return this.selectedItem;
    }

    private String defaultBeneficiary() {
        if (defaultBen == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            List<Preferences> preferenceses = null;
            try {
                log.info("preferenceFacadeLocal:: " + preferenceFacadeLocal);
                preferenceses = preferenceFacadeLocal.findAll(map, true);
                log.info("preferences:::: " + preferenceses.size());
            } catch (Exception ex) {
                Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (preferenceses == null || preferenceses.isEmpty() || preferenceses.size() > 1) {
                defaultBen = AppConstants.CHARGE_BEARER_BENEFICIARY;
            } else {
                defaultBen = preferenceses.get(0).getBeneficiaryChargeBearer() ? AppConstants.CHARGE_BEARER_BENEFICIARY : AppConstants.CHARGE_BEARER_PAYER;
            }
        }
        return defaultBen;

    }

    public TransactionType getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public int getFocusIndex() {
        return this.focusIndex;
    }

    public void setFocusIndex(int focusIndex) {
        this.focusIndex = focusIndex;
    }

    public byte[] getUpload() {
        return this.upload;
    }

    public void setUpload(byte[] upload) {
        this.upload = upload;
    }

    public Beneficiary getBeneficary() {
        if (this.beneficary == null) {
            this.beneficary = new Beneficiary();
            PocketMoneyAccount pm = new PocketMoneyAccount();
            pm.setType(AppConstants.POCKET_MONEY_ACCOUNT_TYPE);
            this.beneficary.setAccount(pm);
        }
        return this.beneficary;
    }

    public void setBeneficary(Beneficiary beneficary) {
        this.beneficary = beneficary;
    }

    public List getBanks() {
        return this.banks;
    }

    public void setBanks(List banks) {
        this.banks = banks;
    }

    public String getAcctOption() {
        return this.acctOption;
    }

    public void setAcctOption(String acctOption) {
        this.acctOption = acctOption;
    }

    public List<BeneficiaryGroup> getBeneficiaryGroups() {
        if (this.beneficiaryGroups == null) {
            try {
                Map map = new HashMap();
                if (isCorporateUser()) {
                    map.put("corporate", getLoggedCorporate());
                } else {
                    map.put("creator", getLoggedUser().getId());
                    map.put("corporate", null);
                }
                this.beneficiaryGroups = this.beneficiaryGroupFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
                this.beneficiaryGroups = new ArrayList();
            }
        }
        return this.beneficiaryGroups;
    }

    public Set<Transaction> viewTransactions(TaskObjectNameValuePair taskObjectNameValuePair) {
        try {
            this.log.info("viewing transaction..........");
            this.log.log(Level.INFO, "... {0}", taskObjectNameValuePair.getTargetValue());
            this.transactions = ((Payment) this.paymentFacadeLocal.find(Long.valueOf(taskObjectNameValuePair.getTargetValue()))).getTransactions();
//            this.log.info("viewing transaction222..........");
//            RequestContext context = RequestContext.getCurrentInstance();
//            context.execute("PF('viewtransactions').show();");
//            this.log.info("viewing transaction333..........");
        } catch (Exception ex) {
            this.transactions = new HashSet();
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this.transactions;

    }

    public void setBeneficiaryGroups(List<BeneficiaryGroup> beneficiaryGroups) {
        this.beneficiaryGroups = beneficiaryGroups;
    }

    public BeneficiaryGroup getBeneficiaryGroup() {
        if (this.beneficiaryGroup == null) {
            this.beneficiaryGroup = new BeneficiaryGroup();
        }
        return this.beneficiaryGroup;
    }

    public void setBeneficiaryGroup(BeneficiaryGroup beneficiaryGroup) {
        this.beneficiaryGroup = beneficiaryGroup;
    }

    public String getRoute() {
        return this.route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public List<Object> getRoutes() {
        if (this.routes == null) {
            try {
                this.routes = this.approvalRouteFacadeLocal.findRouteAliases(Payment.class);
            } catch (Exception ex) {
                Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
                this.routes = new ArrayList();
            }
        }
        return this.routes;
    }

    public void setRoutes(List<Object> routes) {
        this.routes = routes;
    }

    public Payment getSelectedItem2() {
        return this.selectedItem2;
    }

    public void setSelectedItem2(Payment selectedItem2) {
        log.info("selected.....");
        this.selectedItem2 = selectedItem2;
        this.header = false;
        log.log(Level.INFO, "selected..... {0}", this.selectedItem2);
    }

    public List<TransactionType> getTransactionTypes() {
        return this.transactionTypes;
    }

    public void setTransactionTypes(List<TransactionType> TransactionTypes) {
        this.transactionTypes = TransactionTypes;
    }

    public void handleTransFileUpload(FileUploadEvent event) {
        try {
            boolean supplementary = (this.selectedItem2 != null) && ((this.selectedItem2 instanceof UploadPayment)) && (((UploadPayment) this.selectedItem2).getUploadType() != null);

            if ((!supplementary)
                    && (this.transactionType == null)) {
                FacesMessage m = new FacesMessage("Select a transaction type");
                m.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, m);
                return;
            }

            UploadedFile f = event.getFile();
            this.upload = new byte[f.getInputstream().available()];
            f.getInputstream().read(this.upload);
            f.getInputstream().close();
            if (supplementary) {
                ((UploadPayment) this.selectedItem2).setUploadPath(f.getFileName());
                log.info(" supuploaded tran:: " + ((UploadPayment) this.selectedItem2).getUploadPath());
            } 
            //else {
                ((UploadPayment) this.selectedItem).setUploadPath(f.getFileName());
                log.info("uploaded tran:: " + ((UploadPayment) this.selectedItem).getUploadPath());
           // }
            FacesMessage m = new FacesMessage(f.getFileName() + " uploaded successfully.", f.getFileName() + " uploaded successfully.");
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext.getCurrentInstance().addMessage(null, m);
            this.log.info("uploade....");
        } catch (IOException ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage("File is not valid for transaction type " + this.transactionType.getDescription());
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("growlUpload", msg);
        }
    }

    public boolean isHeader() {
        return this.header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public Set<Transaction> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void downloadErrorFile(Payment payment) {
        try {
            UploadPayment up = (UploadPayment) payment;
            String suppUploadPath = up.getSuppUploadPath();
            File file = new File(ConfigurationUtil.instance().getUploadDir() + File.separator + up.getUploadType().getId() + File.separator + "error" + File.separator + suppUploadPath);

            FileInputStream stream = new FileInputStream(file);
            byte[] b = new byte[stream.available()];
            this.log.log(Level.INFO, "size of byte {0}", Integer.valueOf(b.length));
            stream.read(b);
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext ec = fc.getExternalContext();
            ec.responseReset();
            String contentType = Files.probeContentType(file.toPath());
            this.log.log(Level.INFO, "content type {0}", contentType);
            ec.setResponseContentType(contentType);
            ec.setResponseHeader("Content-Disposition", "attachment; filename=" + suppUploadPath);
            ec.getResponseOutputStream().write(b);
            fc.responseComplete();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage(ex.getMessage());
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage(ex.getMessage());
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void generateUploadTemplate() {
        FileInputStream fileInputStream = null;
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.setResponseContentType("application/vnd.ms-excel");
            ec.setResponseHeader("Content-Disposition", "attachment; filename=upload_template.csv");
            fileInputStream = new FileInputStream(ConfigurationUtil.instance().getSampleUploadFile());
            byte[] excel = new byte[fileInputStream.available()];
            fileInputStream.read(excel);
            ec.getResponseOutputStream().write(excel);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage("Cannot download template at this time ");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("growlUpload", msg);
        } catch (IOException ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
                    FacesMessage msg = new FacesMessage("Cannot download template at this time ");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage("growlUpload", msg);
        } finally {
            try {
                if(fileInputStream!=null)
                fileInputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
