/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Settlement;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.dto.SettlementDto;
import com.etranzact.corporatepay.payment.facade.SettlementFacadeLocal;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.collections4.map.HashedMap;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "settlementBean")
@SessionScoped
public class SettlementBean extends AbstractBean<Settlement> {

    @EJB
    private TransactionFacadeLocal transactionFacadeLocal;
    @EJB
    private SettlementFacadeLocal settlementFacadeLocal;
    private List<SettlementDto> settlementDtos;

    @Override
    protected SettlementFacadeLocal getFacade() {
        return settlementFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public SettlementBean() {
        super(Settlement.class);
    }

    /**
     * @return the settlementDtos
     */
    public List<SettlementDto> getSettlementDtos() {
        SettlementTableBean settlementTableBean = (SettlementTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("settlementTableBean");
        if(settlementTableBean!=null) {
        List<Settlement> settlements = settlementTableBean.getAllList();
           log.info("settlement betweeen " + settlementTableBean.getFromDateFilter() + " and " + settlementTableBean.getToDateFilter());
        log.info("settlement list " + settlements.size());
        settlementTableBean.setDateAdjusted(false);
        if(settlements!=null) {
        settlementDtos = new ArrayList<>();
        for (Settlement settlement : settlements) {
            Map<String, Object> map = new HashedMap<>();
            map.put("sbatchId", settlement.getSbatchId());
            List<Transaction> transactions = null;
            try {
                transactions = transactionFacadeLocal.findAll(map, false);
                //log.info("transactions:: " + transactions.size());
            } catch (Exception ex) {
                Logger.getLogger(SettlementBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (transactions == null) {
                continue;
            }
            for (Transaction transaction : transactions) {
                SettlementDto settlementDto = new SettlementDto();
                settlementDto.setAccountNumber(transaction.getBeneficaryAccount());
                settlementDto.setBank(settlement.getSourceIssuerCode());
                settlementDto.setBeneficiaryName(transaction.getBeneficaryName());
                settlementDto.setCompanyName(settlement.getCorporate().getCorporateName());
                settlementDto.setCreated(AppUtil.formatDate(transaction.getPaymentDate()==null?transaction.getPaymentDate():transaction.getPayment().getDateAuthorized()==null?transaction.getDateCreated():transaction.getPayment().getDateAuthorized(), "yyyy-MM-dd HH:mm:ss"));
                settlementDto.setHostErrorCode(AppConstants.TRANSACTION_PAID.equals(transaction.getTrsnsactionStatus()) ? "000" : transaction.getResponseCode());
                settlementDto.setIssuerCode(settlement.getdIssuerCode());
                settlementDto.setNarration(transaction.getNarration());
                settlementDto.setPaymentId("08" + transaction.getPayment().getId().toString());
                settlementDto.setReference("08" + transaction.getTransactionId().toString());
                settlementDto.setTransAmount(transaction.getAmountf());
                settlementDtos.add(settlementDto);

            }
        }
         //log.info("settlementDtos:: " + settlementDtos.size());
        }
        }
        return settlementDtos;
    }
    


}
