/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "paymentTableBean")
@SessionScoped
public class PaymentTableBean extends AbstractTableBean<Payment> {

    @EJB
    protected PaymentFacadeLocal paymentFacadeLocal;
    protected String paymentRefFilter = "";
    protected Corporate corporateFilter;
    protected List<Corporate> corporates;

    @EJB
    protected CorporateSetupFacadeLocal corporateFacadeLocal;

    public PaymentTableBean() {
        super(Payment.class);
    }

    @Override
    protected PaymentFacadeLocal getFacade() {
        return paymentFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (!paymentRefFilter.isEmpty()) {
                  paymentRefFilter = paymentRefFilter.startsWith("08")?paymentRefFilter.substring(2):paymentRefFilter;
            properties.getAdditionalFilter().put("id=" + paymentRefFilter, null);
        }
        Corporate corporate = getLoggedCorporate();
        if (corporateFilter != null) {
            corporate = corporateFilter;
        }
        if (corporate != null) {
            properties.getAdditionalFilter().put("corporate", corporate);
        }
        Bank bank = getLoggedBank();
        if (bank != null && corporate == null) {
            properties.getAdditionalFilter().put("corporate.bank", bank);
        }
       // properties.getAdditionalFilter().put("flagStatus in ('CR','CRAPP','CRJ','PR','PROK','PRE')", null);
//          properties.getAdditionalFilter().put("flagStatus in ('" + AppConstants.PAYMENT_CLOSE + "','" + AppConstants.PAYMENT_HELD + "')", null);
                 properties.setFlagStatuses(new String[]{AppConstants.CREATED,AppConstants.CREATED_IN_APPROVAL,AppConstants.CREATED_REJECTED,AppConstants.PROCESSING,AppConstants.PROCESSED_OK,AppConstants.PROCESSED_WITH_ERRORS});
//        properties.getAdditionalFilter().put("corporate", getLoggedCorporate());
//        String[] flagStatuses = properties.getFlagStatuses();
//        flagStatuses[2] = AppConstants.PROCESSING;
//        properties.setFlagStatuses(new String[]{AppConstants.CREATED,
//            AppConstants.CREATED_IN_APPROVAL,AppConstants.CREATED_REJECTED,
//            AppConstants.PROCESSING,AppConstants.PROCESSED_OK,AppConstants.PROCESSED_WITH_ERRORS});
        return properties;
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        super.init();
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        try {
            Bank supportBank1 = getSupportBank();
            if (supportBank1 != null) {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("bank", supportBank1);
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
            } else {
                corporates = new ArrayList<>();
            }
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            corporates = new ArrayList<>();
        }

        return corporates;
    }

    @Override
    public Payment getTableRowData(String rowKey) {
        List<Payment> wrappedData = (List<Payment>) getLazyDataModel().getWrappedData();
        for (Payment t : wrappedData) {
            if (rowKey.equals(t.getId().toString())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Payment t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the paymentRefFilter
     */
    public String getPaymentRefFilter() {
        return paymentRefFilter;
    }

    /**
     * @param paymentRefFilter the paymentRefFilter to set
     */
    public void setPaymentRefFilter(String paymentRefFilter) {
        this.paymentRefFilter = paymentRefFilter;
    }

    /**
     * @return the corporateFilter
     */
    public Corporate getCorporateFilter() {
        return corporateFilter;
    }

    /**
     * @param corporateFilter the corporateFilter to set
     */
    public void setCorporateFilter(Corporate corporateFilter) {
        this.corporateFilter = corporateFilter;
    }

}
