/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Settlement;
import com.etranzact.corporatepay.payment.facade.SettlementFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "settlementTableBean")
@SessionScoped
public class SettlementTableBean extends AbstractTableBean<Settlement> {

    @EJB
    protected SettlementFacadeLocal settlementFacadeLocal;
    protected String settlementTypeFilter;

    @EJB
    protected CorporateSetupFacadeLocal corporateFacadeLocal;
    @EJB
    protected BankSetupFacadeLocal bankFacadeLocal;
    private Bank srcBankFilter;
    protected List<Bank> banks;
    private boolean dateAdjusted;

    public SettlementTableBean() {
        super(Settlement.class);
    }

    @Override
    protected SettlementFacadeLocal getFacade() {
        return settlementFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (srcBankFilter == null) {
            srcBankFilter = getSupportBank();
        }
        if (settlementTypeFilter != null && srcBankFilter != null) {
            if (AppConstants.REPORT_SETTLEMENT_INCOMING.equals(settlementTypeFilter)) {
                properties.getAdditionalFilter().put("s.settlementType in ('TS') and s.dIssuerCode='" + srcBankFilter.getId() + "' and s.sourceIssuerCode !='" + srcBankFilter.getId() + "' and s.sourceIssuerCode!=s.dIssuerCode", null);
            } else if (AppConstants.REPORT_SETTLEMENT_OUTGOING.equals(settlementTypeFilter)) {
                properties.getAdditionalFilter().put("s.settlementType in ('TS') and s.sourceIssuerCode='" + srcBankFilter.getId() + "' and s.dIssuerCode !='" + srcBankFilter.getId() + "' and s.sourceIssuerCode!=s.dIssuerCode", null);
            } else if (AppConstants.REPORT_SETTLEMENT_SAME.equals(settlementTypeFilter)) {
                properties.getAdditionalFilter().put("s.settlementType in ('TS') and s.sourceIssuerCode='" + srcBankFilter.getId() + "' and s.dIssuerCode ='" + srcBankFilter.getId() + "'", null);
            }
        } else if (settlementTypeFilter == null && srcBankFilter != null) {
            properties.getAdditionalFilter().put("s.settlementType in ('TS') and (s.sourceIssuerCode='" + srcBankFilter.getId() + "' or s.dIssuerCode ='" + srcBankFilter.getId() + "')", null);
        } else if (settlementTypeFilter != null && srcBankFilter == null) {

            if (AppConstants.REPORT_SETTLEMENT_SAME.equals(settlementTypeFilter)) {
                properties.getAdditionalFilter().put("s.settlementType in ('TS') and (s.sourceIssuerCode=s.dIssuerCode)", null);
            } else {
                properties.getAdditionalFilter().put("s.settlementType in ('TS') and s.sourceIssuerCode!=s.dIssuerCode", null);
            }

        }
        int settlementminus = 1;

        if (srcBankFilter != null) {
            if (srcBankFilter.isSpecialized()) {
                settlementminus = 2;
            }
        }
            Calendar cal;
//            if (fromDateFilter.getTime() == toDateFilter.getTime()) {
              log.info("dateAdjusted:: " + dateAdjusted);
            if(fromDateFilter!=null && toDateFilter!=null && !dateAdjusted) {
                setDateAdjusted(true);
                cal = Calendar.getInstance();
                cal.setTime(fromDateFilter);
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - settlementminus);
                cal.set(Calendar.HOUR_OF_DAY, 0);
                fromDateFilter = cal.getTime();
                cal = Calendar.getInstance();
                cal.setTime(toDateFilter);
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - settlementminus);
                cal.set(Calendar.HOUR_OF_DAY, 23);
                toDateFilter = cal.getTime();
            }
            
        //} 
        properties.setFlagStatuses(new String[0]);
        return properties;
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        reportPage = true;
        prefetch = false;

        super.init();
    }

    @Override
    public void refresh() {
        prefetch = true;
        dateRequired = true;
        super.init();
    }

    @Override
    public Settlement getTableRowData(String rowKey) {
        List<Settlement> wrappedData = (List<Settlement>) getLazyDataModel().getWrappedData();
        for (Settlement t : wrappedData) {
            if (rowKey.equals(t.getId().toString())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Settlement t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the settlementTypeFilter
     */
    public String getSettlementTypeFilter() {
        return settlementTypeFilter;
    }

    /**
     * @param settlementTypeFilter the settlementTypeFilter to set
     */
    public void setSettlementTypeFilter(String settlementTypeFilter) {
        this.settlementTypeFilter = settlementTypeFilter;
    }

    /**
     * @return the srcBankFilter
     */
    public Bank getSrcBankFilter() {
        return srcBankFilter;
    }

    /**
     * @param srcBankFilter the srcBankFilter to set
     */
    public void setSrcBankFilter(Bank srcBankFilter) {
        this.srcBankFilter = srcBankFilter;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null) {

            try {
                banks = bankFacadeLocal.findAll(new HashMap(), true, "bankName");
            } catch (Exception ex) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @param dateAdjusted the dateAdjusted to set
     */
    public void setDateAdjusted(boolean dateAdjusted) {
        this.dateAdjusted = dateAdjusted;
    }
    
    
}
