/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.SingleBeneficiaryPayment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.model.UploadPayment;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryGroupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "transactionBean")
@SessionScoped
public class TransactionBean extends AbstractBean<Transaction> {

    @EJB
    private PaymentFacadeLocal paymentFacadeLocal;
    @EJB
    private TransactionFacadeLocal transactionFacadeLocal;

    @Override
    protected TransactionFacadeLocal getFacade() {
        return transactionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public TransactionBean() {
        super(Transaction.class);
    }

    public void retryTransaction(Transaction transaction) {
        FacesMessage m = new FacesMessage();
        try {
            transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
            transactionFacadeLocal.create(transaction);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Retry initiated successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }
    
    public void modifyTransaction() {
         FacesMessage m = new FacesMessage();
        try {
            BigInteger transactionId = selectedItem.getTransactionId();
            String accountNumber = selectedItem.getAccount().getAccountNumber();
            getFacade().save(transactionId,accountNumber);
                   m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Record Modified and sent for approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
               Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
        
    }
}
