/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "offlineTransactionTableBean")
@SessionScoped
public class OfflineTransactionTableBean extends TransactionTableBean {

    private BigDecimal totalAmount = BigDecimal.ZERO;
    private String totalAmountF;

    @PostConstruct
    @Override
    public void init() {
        statusFilter = AppConstants.TRANSACTION_WAITING;
         prefetch=false;
        reportPage=true;
        super.init();
    }

//    @Override
//    public void refresh() {
//        setPrefetch(true);
//        super.refresh();
//    }
    
      @Override
    protected TableProperties getTableProperties() {
        TableProperties tableProperties = super.getTableProperties();
        tableProperties.getAdditionalFilter().put("t.sbatchId is not null", null);
        return tableProperties;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        for (Transaction transaction : allList) {
            totalAmount = totalAmount.add(transaction.getAmount());
        }
        return totalAmount;
    }

    /**
     * @return the totalAmountF
     */
    public String getTotalAmountF() {
        totalAmountF = AppUtil.formatAsAmount(getTotalAmount().toString());
        return totalAmountF;
    }

    public void openTransactions() {
        FacesMessage m = new FacesMessage();
        try {
            transactionFacadeLocal.openOfflineTransactions(allList);

            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("All Offline Transactions Opened Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(OfflineTransactionTableBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

}
