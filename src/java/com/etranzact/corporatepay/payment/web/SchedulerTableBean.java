/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.PaymentScheduler;
import com.etranzact.corporatepay.payment.facade.SchedulerFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "schedulerTableBean")
@SessionScoped
public class SchedulerTableBean extends AbstractTableBean<PaymentScheduler> {

    @EJB
    private SchedulerFacadeLocal paymentScheduleFacadeLocal;
    private String schedulerIdFilter = "";

    public SchedulerTableBean() {
        super(PaymentScheduler.class);
    }

    @Override
    protected SchedulerFacadeLocal getFacade() {
        return paymentScheduleFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        super.init();
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (!schedulerIdFilter.isEmpty()) {
            properties.getAdditionalFilter().put("scheduleId", getSchedulerIdFilter());
        }
        properties.getAdditionalFilter().put("corporate", getLoggedCorporate());
        return properties;
    }

    @Override
    public PaymentScheduler getTableRowData(String rowKey) {
        List<PaymentScheduler> wrappedData = (List<PaymentScheduler>) getLazyDataModel().getWrappedData();
        for (PaymentScheduler t : wrappedData) {
            if (rowKey.equals(t.getScheduleId())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(PaymentScheduler t) {
        return t.getScheduleId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the schedulerIdFilter
     */
    public String getSchedulerIdFilter() {
        return schedulerIdFilter;
    }

    /**
     * @param schedulerIdFilter the schedulerIdFilter to set
     */
    public void setSchedulerIdFilter(String schedulerIdFilter) {
        this.schedulerIdFilter = schedulerIdFilter;
    }

}
