/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Settlement;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.payment.facade.SettlementFacadeLocal;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "incomeTableBean")
@SessionScoped
public class IncomeTableBean extends AbstractTableBean<Settlement> {

    @EJB
    protected SettlementFacadeLocal settlementFacadeLocal;
    protected Corporate corporateFilter;
    private Bank bankFilter;
    private String settlementTypeFilter;
    private List<Bank> banks;
    protected List<Corporate> corporates;

    @EJB
    protected CorporateSetupFacadeLocal corporateFacadeLocal;
    @EJB
    protected BankSetupFacadeLocal bankFacadeLocal;

    public IncomeTableBean() {
        super(Settlement.class);
    }

    @Override
    protected SettlementFacadeLocal getFacade() {
        return settlementFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        Corporate corporate = getLoggedCorporate();
        if (corporateFilter != null) {
            corporate = corporateFilter;
        }
        if (corporate != null) {
            properties.getAdditionalFilter().put("corporate", corporate);
        }
        Bank bank = getLoggedBank();
        if (bank != null) {
            properties.getAdditionalFilter().put("settlementType in ('BS','TPS','TPS2')", null);
        }
        if (getBankFilter() != null) {
            bank = getBankFilter();
        }
        if (bank != null && corporate==null) {
            properties.getAdditionalFilter().put("corporate.bank='" + bank.getId() + "'", null);
        }
        if (isEtzUser()) {
            properties.getAdditionalFilter().put("settlementType in ('ES')", null);
        }
        if (getSettlementTypeFilter() != null) {
            properties.getAdditionalFilter().put("settlementType", getSettlementTypeFilter());
        }
           properties.getAdditionalFilter().put("corporate.applyCommission=true", null);
        properties.setFlagStatuses(new String[0]);
        return properties;
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
          reportPage=true;
        prefetch=false;
        super.init();
    }

    @Override
    public void refresh() {
        prefetch=true;
        super.init();
    }

    @Override
    public Settlement getTableRowData(String rowKey) {
        List<Settlement> wrappedData = (List<Settlement>) getLazyDataModel().getWrappedData();
        for (Settlement t : wrappedData) {
            if (rowKey.equals(t.getId().toString())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Settlement t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the corporateFilter
     */
    public Corporate getCorporateFilter() {
        return corporateFilter;
    }

    /**
     * @param corporateFilter the corporateFilter to set
     */
    public void setCorporateFilter(Corporate corporateFilter) {
        this.corporateFilter = corporateFilter;
    }

    public void handleBankChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        try {
            setBankFilter((Bank) ((UISelectOne) e.getComponent()).getValue());
            if (getBankFilter() != null) {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("bank", getBankFilter());
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
            } else {
                corporates = new ArrayList<>();
            }
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        if(isBankUser()) {
            try {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("bank", getLoggedBank());
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
            } catch (Exception ex) {
                Logger.getLogger(IncomeTableBean.class.getName()).log(Level.SEVERE, null, ex);
                corporates = new ArrayList<>();
            }
        }
        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("flagStatus", AppConstants.APPROVED);
            banks = bankFacadeLocal.findAll(map, true, "bankName");
            return banks;
        } catch (Exception ex) {
            Logger.getLogger(IncomeTableBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the bankFilter
     */
    public Bank getBankFilter() {
        return bankFilter;
    }

    /**
     * @param bankFilter the bankFilter to set
     */
    public void setBankFilter(Bank bankFilter) {
        this.bankFilter = bankFilter;
    }

    /**
     * @return the settlementTypeFilter
     */
    public String getSettlementTypeFilter() {
        return settlementTypeFilter;
    }

    /**
     * @param settlementTypeFilter the settlementTypeFilter to set
     */
    public void setSettlementTypeFilter(String settlementTypeFilter) {
        this.settlementTypeFilter = settlementTypeFilter;
    }

    
}
