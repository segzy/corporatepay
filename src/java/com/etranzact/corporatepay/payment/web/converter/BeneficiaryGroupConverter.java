/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web.converter;

import com.etranzact.corporatepay.setup.web.converter.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryGroupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("beneficaryGroupConverter")
public class BeneficiaryGroupConverter implements Converter {

    private static final Logger log = Logger.getLogger(BeneficiaryGroupConverter.class.getName());

    @Inject
    private BeneficiaryGroupFacadeLocal beneficaryGroupFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Map<String, Object> map = new HashMap<>();
            List<BeneficiaryGroup> beneficiaryGroups;
            try {
                beneficiaryGroups = beneficaryGroupFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(BeneficiaryGroupConverter.class.getName()).log(Level.SEVERE, null, ex);
                beneficiaryGroups = new ArrayList<>();
            }
            for (BeneficiaryGroup group : beneficiaryGroups) {
                if (value.equals(group.getId().toString())) {
                    return group;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof BeneficiaryGroup) {
            BeneficiaryGroup group = (BeneficiaryGroup) value;
            if (group.getId() == null) {
                return "";
            }
            return group.getId().toString();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
