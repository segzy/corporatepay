/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web.converter;

import com.etranzact.corporatepay.setup.web.converter.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("beneficiaryConverter")
public class BeneficiaryConverter implements Converter {

    private static final Logger log = Logger.getLogger(BeneficiaryConverter.class.getName());

    @Inject
    private BeneficiaryFacadeLocal beneficaryFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
            Beneficiary Beneficiary = null;
        if (value.trim().equals("")) {
            return Beneficiary;
        } else {
//            Map<String, Object> map = new HashMap<>();
//            List<Beneficiary> beneficaries;
//            try {
//                beneficaries = beneficaryFacadeLocal.findAll(map, true);
//            } catch (Exception ex) {
//                Logger.getLogger(BeneficiaryConverter.class.getName()).log(Level.SEVERE, null, ex);
//                beneficaries = new ArrayList<>();
//            }
       
            try {
           Beneficiary = beneficaryFacadeLocal.find(Long.valueOf(value));
            }catch (Exception ex) {
                Logger.getLogger(BeneficiaryConverter.class.getName()).log(Level.SEVERE, null, ex);
                Beneficiary = null;
           }
//            for (Beneficiary beneficiary : beneficaries) {
//                if (value.equals(beneficiary.getId().toString())) {
//                    return beneficiary;
//                }
//            }
        }
        return Beneficiary;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Beneficiary) {
            Beneficiary beneficiary = (Beneficiary) value;
            if(beneficiary.getId()!=null) {
            return beneficiary.getId().toString();
            }
            return "";
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
