/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.PaymentScheduler;
import com.etranzact.corporatepay.model.SchedulePayment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.payment.facade.SchedulerFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "schedulerBean")
@SessionScoped
public class SchedularBean extends AbstractBean<PaymentScheduler> {
    
    @EJB
    private SchedulerFacadeLocal paymentScheduleFacadeLocal;
    
    @EJB
    private PaymentFacadeLocal paymentFacadeLocal;
    
    @Override
    protected SchedulerFacadeLocal getFacade() {
        return paymentScheduleFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }
    
    public SchedularBean() {
        super(PaymentScheduler.class);
    }
    
    public void refresh(PaymentScheduler paymentScheduler) {
        FacesMessage m = new FacesMessage();
        try {
            paymentScheduleFacadeLocal.refresh(paymentScheduler);            
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payment Schedule Refreshed");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
        }
    }
    
    public void createPayment() {
        FacesMessage m = new FacesMessage();
        try {
            Set<Transaction> transactions = selectedItem.getTransactions();
            SchedulePayment payment = new SchedulePayment();
            payment.setTransactions(transactions);
            payment.setSchedulerId(selectedItem.getScheduleId());
            Payment create = paymentFacadeLocal.create(payment);            
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payment Created Successfully");
            m.setDetail("Payment Reference: " + create.getPaymentReference());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new PaymentScheduler();
        }
    }


    
    
}
