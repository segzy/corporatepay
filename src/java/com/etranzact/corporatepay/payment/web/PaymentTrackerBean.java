package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.AbstractBean;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApprovalTaskFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "paymentTrackerBean")
@SessionScoped
public class PaymentTrackerBean extends AbstractBean<ApprovalTask> {

    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;
    @EJB
    private ApprovalTaskFacadeLocal approvalTaskFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateFacadeLocal;
    @EJB
    private PaymentFacadeLocal paymentFacadeLocal;
    private Long paymentRefFilter;
    private Corporate corporateFilter;
    private List<ApprovalTask> approvalTasks;
    private List<Corporate> corporates;

    public void search() {
        try {

            String payRef = String.valueOf(paymentRefFilter);
            if (payRef.startsWith("08")) {
                payRef = payRef.substring(2);
            }
            Corporate loggedCorporate = getLoggedCorporate();
            if (loggedCorporate == null) {
                loggedCorporate = corporateFilter;
            }
            if (loggedCorporate == null) {
                Payment find = null;
                try {
                    find = paymentFacadeLocal.find(Long.valueOf(payRef));
                } catch (Exception e) {
                       Logger.getLogger(PaymentTrackerBean.class.getName()).log(Level.SEVERE, null, e);
                }
                if (find != null) {
                    loggedCorporate = find.getCorporate();
                }
            }
            List<ApprovalRoute> approvalRoutes = approvalRouteFacadeLocal.findRoute(Payment.class, loggedCorporate);
            int approvalRouteLenght = approvalRoutes.size();
            Map<String, Object> map = new HashMap<>();
            boolean filterCorp = false;
            if (corporateFilter != null && paymentRefFilter != null) {
//                map.put("targetObjectId='" + payRef + "' and approvalRoute.corporate.id=" + corporateFilter.getId(), null);
                   map.put("targetObjectId='" + payRef + "'", null);
                   filterCorp= true;
            } else if (corporateFilter == null && paymentRefFilter != null) {
                map.put("targetObjectId='" + payRef + "'", null);
            } else if (corporateFilter != null && paymentRefFilter == null) {
                   log.info("no payref but there is corporateFilter");
//                map.put("approvalRoute.corporate.id=" + corporateFilter.getId(), null);
                     filterCorp= true;
            }
            List<ApprovalTask> approvalTaskss = getFacade().findAll(map, false);
            if(filterCorp) {
            List<ApprovalTask> aptsk = new ArrayList<>();
            for(ApprovalTask approvalTask: approvalTaskss) {
                if(approvalTask.getApprovalRoute() instanceof CorporateApprovalRoute) {
                    if(((CorporateApprovalRoute)approvalTask.getApprovalRoute()).getCorporate().equals(corporateFilter)) {
                        aptsk.add(approvalTask);
                    }
                }
            }
             approvalTaskss = aptsk;
            }
           
            log.info("approvalTaskss gotten from query " + approvalTaskss.size());
            Map<String, List<ApprovalTask>> m = new HashMap<>();
            for (ApprovalTask approvalTask : approvalTaskss) {
                List<ApprovalTask> approvalTasksss = m.get(approvalTask.getTargetObjectId());
                if (approvalTasksss == null) {
                    approvalTasksss = new ArrayList<>();
                }
                approvalTasksss.add(approvalTask);
                log.info("adding approval task:: " + approvalTask);
                m.put(approvalTask.getTargetObjectId(), approvalTasksss);
            }
            approvalTasks = new ArrayList<>();
            for (Entry<String, List<ApprovalTask>> entrySet : m.entrySet()) {
                List<ApprovalTask> value = entrySet.getValue();
                int size = value.size();
                String paymentId = entrySet.getKey();
                log.info("for:: " + paymentId);
                log.info("size of tasks:: " + size);
                approvalTasks.addAll(value);
                log.info("lenght of approval route:: " + approvalRouteLenght);
                int uncreatedApprovalTask = approvalRouteLenght - size;
                log.info("current size of tasks:: " + approvalTasks.size());
                if (uncreatedApprovalTask != 0) {
                    for (int i = 0; i < uncreatedApprovalTask; i++) {

                        ApprovalTask approvalTask = new ApprovalTask();
                        ApprovalRoute get = approvalRoutes.get(size);
                        approvalTask.setApprovalRoute((CorporateApprovalRoute)get);
                        approvalTask.setTargetObjectId(paymentId);
                        approvalTasks.add(approvalTask);
                        log.info("added mirror task with level:: " + get.getLevel());
                        ++size;
                    }
                }
            }

            Comparator<ApprovalTask> approvalRouteComparable = new Comparator<ApprovalTask>() {

                @Override
                public int compare(ApprovalTask o1, ApprovalTask o2) {
                    return ( Long.valueOf(o1.getTargetObjectId()).intValue()
                            - Long.valueOf(o2.getTargetObjectId()).intValue());
                }
            };
            Collections.sort(approvalTasks, approvalRouteComparable);
//            approvalRouteComparable = new Comparator<ApprovalTask>() {
//
//                @Override
//                public int compare(ApprovalTask o1, ApprovalTask o2) {
//                    return (o1.getApprovalRoute().getLevel()
//                            .compareTo(o2.getApprovalRoute().getLevel()));
//                }
//            };
//            Collections.sort(approvalTasks, approvalRouteComparable);

        } catch (Exception ex) {
            Logger.getLogger(PaymentTrackerBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            for (ApprovalTask approvalTask : approvalTasks) {
                approvalTask.setTargetObjectId("08" + approvalTask.getTargetObjectId());
            }
            paymentRefFilter = null;
            corporateFilter = null;

        }
    }

    @Override
    protected ApprovalTaskFacadeLocal getFacade() {
        return this.approvalTaskFacadeLocal;
    }

    public PaymentTrackerBean() {
        super(ApprovalTask.class);
    }

    /**
     * @return the paymentRefFilter
     */
    public Long getPaymentRefFilter() {
        return paymentRefFilter;
    }

    /**
     * @param paymentRefFilter the paymentRefFilter to set
     */
    public void setPaymentRefFilter(Long paymentRefFilter) {
        this.paymentRefFilter = paymentRefFilter;
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        if (corporates == null) {
            Map m = new HashMap<>();
            try {
                corporates = corporateFacadeLocal.findAll(m, true, new String[]{"corporateName"});
            } catch (Exception ex) {
                Logger.getLogger(PaymentTrackerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return corporates;
    }

    /**
     * @return the corporateFilter
     */
    public Corporate getCorporateFilter() {
        return corporateFilter;
    }

    /**
     * @param corporateFilter the corporateFilter to set
     */
    public void setCorporateFilter(Corporate corporateFilter) {
        this.corporateFilter = corporateFilter;
    }

    /**
     * @return the approvalTasks
     */
    public List<ApprovalTask> getApprovalTasks() {
        return approvalTasks;
    }

    /**
     * @param approvalTasks the approvalTasks to set
     */
    public void setApprovalTasks(List<ApprovalTask> approvalTasks) {
        this.approvalTasks = approvalTasks;
    }

}
