/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.dto.SettlementDto;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "transactionSummaryTableBean")
@SessionScoped
public class TransactionSummaryTableBean extends PaymentTableBean {

       @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        prefetch=false;
        reportPage=true;
        super.init();
    }
    
      @Override
    public void refresh() {
         prefetch=true;
        super.init();
    }
    
        @Override
    protected TableProperties getTableProperties() {
         TableProperties tableProperties = super.getTableProperties();
         tableProperties.setFlagStatuses(new String[]{AppConstants.PAYMENT_CLOSE,AppConstants.PAYMENT_HELD,AppConstants.APPROVED});
        tableProperties.setFlagStatuses(new String[0]);
         return tableProperties;
    }

}
