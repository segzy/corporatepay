package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.AbstractBean;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import com.etranzact.corporatepay.model.CorporateBeneficiary;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.SingleBeneficiaryPayment;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.setup.facade.AccountFacade;
import com.etranzact.corporatepay.setup.facade.AccountFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BeneficiaryGroupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateBeneficiaryFacadeLocal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

@ManagedBean(name = "beneficiaryBean")
@SessionScoped
public class BeneficiaryBean extends AbstractBean<CorporateBeneficiary> {

    @EJB
    private CorporateBeneficiaryFacadeLocal corporateBeneficiaryFacadeLocal;

    @EJB
    private BeneficiaryFacadeLocal beneficiaryFacadeLocal;
    
        @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;
        
                @EJB
    private AccountFacadeLocal accountFacadeLocal;

    private List<CorporateBeneficiary> beneficiaries;
    private int focusIndex;
    private List<Bank> banks;

    @Override
    protected CorporateBeneficiaryFacadeLocal getFacade() {
        return this.corporateBeneficiaryFacadeLocal;
    }

    public BeneficiaryBean() {
        super(CorporateBeneficiary.class);
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        Account account = selectedItem.getAccount();
        if (account instanceof PocketMoneyAccount) {
            focusIndex = 0;
        } else if (account instanceof BankAccount) {
            focusIndex = 1;
        }
    }

    @Override
    public CorporateBeneficiary getSelectedItem() {
        if( this.selectedItem==null) {
        super.getSelectedItem();
        this.selectedItem.setAccount(new PocketMoneyAccount());
        return this.selectedItem;
        }
        return this.selectedItem;
    }

    public void onTabChange(TabChangeEvent event) {
        String tabTitle = event.getTab().getTitle();
        log.info("tabTitle: " + tabTitle);
        switch (tabTitle) {
            case "Pocket Moni Account":
                PocketMoneyAccount pm = new PocketMoneyAccount();
                pm.setType("P");
                this.selectedItem.setAccount(pm);
                focusIndex = 0;
                break;
            case "Bank Account":
                this.selectedItem.setAccount(new BankAccount());
                focusIndex = 1;
                break;
        }
    }
    
        public List getBanks() {
        try {
            this.banks = this.bankSetupFacadeLocal.findAll(new HashMap(), true, new String[]{"bankName"});
        } catch (Exception ex) {
            Logger.getLogger(BeneficiaryBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this.banks;
    }

    public void setBanks(List banks) {
        this.banks = banks;
    }

    /**
     * @return the beneficiaryGroups
     */
    public List<CorporateBeneficiary> getBeneficiaries() {
        try {
            Map<String, Object> m = new HashMap<>();
            m.put("corporate", getLoggedCorporate());
            setBeneficiaries(getFacade().findAll(m, true, new String[]{"id"}));
            return beneficiaries;
        } catch (Exception ex) {
            Logger.getLogger(BeneficiaryBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    public void createBeneficiary() {
        FacesMessage m = new FacesMessage();
        try {
//            Beneficiary findBeneficiary = beneficiaryFacadeLocal.findBeneficiary(selectedItem.getAccount());
               Beneficiary findBeneficiary = beneficiaryFacadeLocal.find(selectedItem.getId());
            if (findBeneficiary != null) {
                if (selectedItem.getId() !=null) {
                    findBeneficiary.setBeneficiaryPhone(selectedItem.getBeneficiaryPhone());
                    findBeneficiary.setBeneficiaryEmail(selectedItem.getBeneficiaryEmail());
                    Account account = accountFacadeLocal.find(selectedItem.getAccount().getId());
                    account.setAccountName(selectedItem.getAccount().getAccountName());
                    account.setAccountNumber(selectedItem.getAccount().getAccountNumber());
                    account=accountFacadeLocal.create(account);
                    findBeneficiary.setAccount(account);
                    
                    findBeneficiary.setBeneficiaryGroup(selectedItem.getBeneficiaryGroup());
                    beneficiaryFacadeLocal.save(findBeneficiary);
                    log.info("account name: " + findBeneficiary.getAccount().getAccountName());
                            log.info("account number: " + findBeneficiary.getAccount().getAccountNumber());
                    throw new Exception("Beneficiary already exist but has being updated accordingly");
                }
                throw new Exception("Beneficiary already exist");
            }
            this.selectedItem = ((CorporateBeneficiary) this.beneficiaryFacadeLocal.create(this.selectedItem));
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Beneficiary Created Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
                this.selectedItem = new CorporateBeneficiary();
            if ((this.selectedItem.getAccount() instanceof BankAccount)) {
             
                this.selectedItem.setAccount(new BankAccount());
            } else if ((this.selectedItem.getAccount() instanceof PocketMoneyAccount)) {
                PocketMoneyAccount pm = new PocketMoneyAccount();
                pm.setType("P");
                this.selectedItem.setAccount(pm);
            }
            log.info("corporate beneficiary............");
        } catch (Exception ex) {
            Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    /**
     * @param beneficiaries the beneficiaries to set
     */
    public void setBeneficiaries(List<CorporateBeneficiary> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    /**
     * @return the focusIndex
     */
    public int getFocusIndex() {
        return focusIndex;
    }

    /**
     * @param focusIndex the focusIndex to set
     */
    public void setFocusIndex(int focusIndex) {
        this.focusIndex = focusIndex;
    }

}
