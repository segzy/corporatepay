/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Settlement;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.payment.facade.SettlementFacadeLocal;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.event.CloseEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "settlementTableBean2")
@SessionScoped
public class SettlementTableBean2 extends AbstractTableBean<Settlement> {

    private Bank srcBankFilter;
    private List<Corporate> corporates;
    private List<Transaction> transactions = null;
    @EJB
    private TransactionFacadeLocal transactionFacadeLocal;
    private Corporate srcCorporateFilter;
    @EJB
    protected SettlementFacadeLocal settlementFacadeLocal;

    @EJB
    protected CorporateSetupFacadeLocal corporateFacadeLocal;
    @EJB
    protected BankSetupFacadeLocal bankFacadeLocal;
    protected List<Bank> banks;

    public SettlementTableBean2() {
        super(Settlement.class);
    }

    @Override
    protected SettlementFacadeLocal getFacade() {
        return settlementFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (getSrcBankFilter() == null) {
            setSrcBankFilter(getSupportBank());
        }
        if (srcCorporateFilter == null) {
            srcCorporateFilter = getSupportCorporate();
        }
//        if (getSrcBankFilter() != null) {
//            properties.getAdditionalFilter().put("s.settlementType in ('RS') and (s.sourceIssuerCode='" + getSrcBankFilter().getId() + "' or s.dIssuerCode ='" + getSrcBankFilter().getId() + "')", null);
//
//        }
        if (getSrcBankFilter() != null) {
            properties.getAdditionalFilter().put("corporate.bank", srcBankFilter);
        }
        if (srcCorporateFilter != null) {
            properties.getAdditionalFilter().put("corporate", srcCorporateFilter);
        }
        properties.getAdditionalFilter().put("s.settlementType in ('RS')", null);
        properties.setFlagStatuses(new String[0]);
        return properties;
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        reportPage = true;
        prefetch = false;

        super.init();
    }

    @Override
    public void refresh() {
        prefetch = true;
        dateRequired = true;
        super.init();
    }

    public void handleBankChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        try {
            setSrcBankFilter((Bank) ((UISelectOne) e.getComponent()).getValue());
            Map<String, Object> map = new HashMap<>();
            map.put("flagStatus", AppConstants.APPROVED);
            map.put("c.bank.id='" + getSrcBankFilter().getId() + "'", null);
            setCorporates(corporateFacadeLocal.findAll(map, true, "corporateName"));

        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null) {

            try {
                banks = bankFacadeLocal.findAll(new HashMap(), true, "bankName");
            } catch (Exception ex) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public void onDialogClose(CloseEvent event) {
        transactions = new ArrayList<>();
    }

    public void pullReversedTransactions(String sBatchId) {
        Map<String, Object> map = new HashMap<>();
        map.put("sbatchId='" + sBatchId + "'", null);
        map.put("transactionStatus in ('" + AppConstants.TRANSACTION_REVERSED + "')", null);
        try {
            setTransactions(transactionFacadeLocal.findAll(map, false));
            //log.info("transactions:: " + transactions.size());
        } catch (Exception ex) {
            Logger.getLogger(SettlementBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Settlement getTableRowData(String rowKey) {
        List<Settlement> wrappedData = (List<Settlement>) getLazyDataModel().getWrappedData();
        for (Settlement t : wrappedData) {
            if (rowKey.equals(t.getId().toString())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Settlement t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        Bank loggedBank = getLoggedBank();
        if (loggedBank != null) {
            try {
                Map<String, Object> map = new HashMap<>();
                map.put("bank", loggedBank);
                corporates = corporateFacadeLocal.findAll(map, true, new String[]{"corporateName"});
            } catch (Exception ex) {
                Logger.getLogger(SettlementTableBean2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }

    /**
     * @return the transactions
     */
    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * @param transactions the transactions to set
     */
    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * @return the srcCorporateFilter
     */
    public Corporate getSrcCorporateFilter() {
        return srcCorporateFilter;
    }

    /**
     * @param srcCorporateFilter the srcCorporateFilter to set
     */
    public void setSrcCorporateFilter(Corporate srcCorporateFilter) {
        this.srcCorporateFilter = srcCorporateFilter;
    }

    /**
     * @return the srcBankFilter
     */
    public Bank getSrcBankFilter() {
        return srcBankFilter;
    }

    /**
     * @param srcBankFilter the srcBankFilter to set
     */
    public void setSrcBankFilter(Bank srcBankFilter) {
        this.srcBankFilter = srcBankFilter;
    }

}
