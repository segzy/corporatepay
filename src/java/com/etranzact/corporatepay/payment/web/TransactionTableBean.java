/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payment.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "transactionTableBean")
@SessionScoped
public class TransactionTableBean extends AbstractTableBean<Transaction> {

    @EJB
    protected TransactionFacadeLocal transactionFacadeLocal;
    protected String paymentRefFilter = "";
    protected String transactionRefFilter = "";
    protected String narration = "";
    protected String accountNumber = "";
    protected String accountName = "";
    protected BigDecimal amount;
    protected Bank srcBankFilter;
    protected Corporate corporateFilter;
    protected String statusFilter2 = "";
    protected List<Bank> banks;
    protected List<Corporate> corporates;

    @EJB
    protected CorporateSetupFacadeLocal corporateFacadeLocal;
    @EJB
    protected BankSetupFacadeLocal bankFacadeLocal;

    public TransactionTableBean() {
        super(Transaction.class);
    }

    @Override
    protected TransactionFacadeLocal getFacade() {
        return transactionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (!paymentRefFilter.isEmpty()) {
            paymentRefFilter = paymentRefFilter.startsWith("08")?paymentRefFilter.substring(2):paymentRefFilter;
            properties.getAdditionalFilter().put("payment.id=" + paymentRefFilter, null);
        }
        if (!transactionRefFilter.isEmpty()) {
            String filter = "";
            String[] split = transactionRefFilter.split(",");
            for(String str: split) {
               str = str.startsWith("08")?str.substring(2):str;
               filter +=str + ",";
            }
            filter = filter.substring(0, filter.length()-1);
            properties.getAdditionalFilter().put("transactionId in (" + filter + ")", null);
        }
        if (!narration.isEmpty()) {
            properties.getAdditionalFilter().put("narration", getNarration());
        }
        if (getAmount() != null) {
            properties.getAdditionalFilter().put("amount", getAmount());
        }
        if (!accountNumber.isEmpty()) {
            properties.getAdditionalFilter().put("account.accountNumber", getAccountNumber());
        }
        if (!accountName.isEmpty()) {
            properties.getAdditionalFilter().put("account.accountName", getAccountName());
        }
        if (getStatusFilter() != null && !getStatusFilter().isEmpty()) {
            if (getStatusFilter().equals("3")) {
                properties.getAdditionalFilter().put("transactionStatus in (3,7,8,9)", null);
            } else {
                properties.getAdditionalFilter().put("transactionStatus", getStatusFilter());
            }
        } else {
            properties.getAdditionalFilter().put("transactionStatus in (3,4,5,6,7,8,9)", null);
        }
//           if (!statusFilter2.isEmpty()) {
//            properties.getAdditionalFilter().put("transactionStatus", getStatusFilter2());
//        }
        Bank bank = getLoggedBank();
        if (srcBankFilter != null) {
            bank = srcBankFilter;
        }
        Corporate corporate = getLoggedCorporate();
        if (corporateFilter != null) {
            corporate = corporateFilter;
        }
        if (bank != null && corporate == null) {
            properties.getAdditionalFilter().put("corporate.bank", bank);
        }
        if (corporate != null) {
            properties.getAdditionalFilter().put("corporate", corporate);
        }
        properties.getAdditionalFilter().put("t.schedule is null", null);
        properties.setFlagStatuses(new String[0]);
        return properties;
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";        
        prefetch = !reportPage;
        super.init();
    }

    @Override
    public void refresh() {
        prefetch = true;
        super.init();
    }

    @Override
    public Transaction getTableRowData(String rowKey) {
        List<Transaction> wrappedData = (List<Transaction>) getLazyDataModel().getWrappedData();
        for (Transaction t : wrappedData) {
            if (rowKey.equals(t.getTransactionId().toString())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public List<SelectItem> getStatusOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.TRANSACTION_PENDING_PAYMENT, AppConstants.TRANSACTION_PENDING_PAYMENT_TEXT));
        options.add(new SelectItem(AppConstants.TRANSACTION_PAID, AppConstants.TRANSACTION_PAID_TEXT));
        options.add(new SelectItem(AppConstants.TRANSACTION_FAILED, AppConstants.TRANSACTION_FAILED_TEXT));
        options.add(new SelectItem(AppConstants.TRANSACTION_STATUS_UNKNOWN, AppConstants.TRANSACTION_STATUSUNKNOWN_TEXT));
        options.add(new SelectItem(AppConstants.TRANSACTION_REVERSED, AppConstants.TRANSACTION_REVERSED_TEXT));
        return options;
    }

    public List<SelectItem> getStatusOptions2() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.TRANSACTION_CREATED_OK, AppConstants.TRANSACTION_CREATED_OK_TEXT));
        options.add(new SelectItem(AppConstants.TRANSACTION_PENDING_APPROVAL, AppConstants.TRANSACTION_PENDING_APPROVAL_TEXT));
        return options;
    }

    @Override
    public Object getTableKey(Transaction t) {
        return t.getTransactionId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the paymentRefFilter
     */
    public String getPaymentRefFilter() {
        return paymentRefFilter;
    }

    /**
     * @param paymentRefFilter the paymentRefFilter to set
     */
    public void setPaymentRefFilter(String paymentRefFilter) {
        this.paymentRefFilter = paymentRefFilter;
    }

    /**
     * @return the transactionRefFilter
     */
    public String getTransactionRefFilter() {
        return transactionRefFilter;
    }

    /**
     * @param transactionRefFilter the transactionRefFilter to set
     */
    public void setTransactionRefFilter(String transactionRefFilter) {
        this.transactionRefFilter = transactionRefFilter;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * @param accountName the accountName to set
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the srcBankFilter
     */
    public Bank getSrcBankFilter() {
        return srcBankFilter;
    }

    /**
     * @param srcBankFilter the srcBankFilter to set
     */
    public void setSrcBankFilter(Bank srcBankFilter) {
        this.srcBankFilter = srcBankFilter;
    }

    /**
     * @return the corporateFilter
     */
    public Corporate getCorporateFilter() {
        return corporateFilter;
    }

    /**
     * @param corporateFilter the corporateFilter to set
     */
    public void setCorporateFilter(Corporate corporateFilter) {
        this.corporateFilter = corporateFilter;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null) {

            try {
                banks = bankFacadeLocal.findAll(new HashMap(), true, "bankName");
                corporateFilter = null;
            } catch (Exception ex) {
                Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        try {
            if (srcBankFilter != null) {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("bank", srcBankFilter);
                corporates = corporateFacadeLocal.findAll(map, true, "corporateName");
            } else {
                corporates = new ArrayList<>();
            }
        } catch (Exception ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
            corporates = new ArrayList<>();
        }

        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }


}
