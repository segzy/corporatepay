/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_HOLDING_ACCOUNT", uniqueConstraints = {@UniqueConstraint(columnNames = {"CARD_NUMBER"})})
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class HoldingAccount implements Serializable  {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Long id;
    @Size(min=16, max=16)
    @Column(name="CARD_NUMBER")
    private String cardNumber;
    
    @Column(name="ACTIVE_STATUS")
    private Boolean active = Boolean.FALSE;
    
    @Column(name="TMP_CARD_NUMBER")
    private String tmpCardNumber;

    @Column(name="TMP_ACTIVE_STATUS")
    private Boolean tmpActive = Boolean.FALSE;

    @Column(name = "AUTHORIZED")
    private Boolean authorized = Boolean.FALSE;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATE_CREATED", updatable = false)
    private Date createdDate = new Date();
      @Column(name = "FLAG_STATUS")
    private String flagStatus;
         @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;
//                 @Column(name = "BANK_CODE")
//    private String bankCode;
         
           @ManyToOne
    @JoinColumn(name="CORPORATE_ID")
    private Corporate corporate = new Corporate();
    @ManyToOne
    @JoinColumn(name="TMP_CORPORATE_ID")
    private Corporate tmpCorporate;
         
              @Transient
    private String status;
         
         /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }


    public String getTmpCardNumber() {
        return tmpCardNumber;
    }

    public void setTmpCardNumber(String tmpCardNumber) {
        this.tmpCardNumber = tmpCardNumber;
    }


     

    public Boolean getAuthorized() {
        return authorized;
    }

    public void setAuthorized(Boolean authorized) {
        this.authorized = authorized;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getTmpActive() {
        return tmpActive;
    }

    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }
    
    public Corporate getCorporate() {
        return corporate;
    }

    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HoldingAccount other = (HoldingAccount) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HoldingAccount{" + "id=" + id + '}';
    }

//    /**
//     * @return the bankCode
//     */
//    public String getBankCode() {
//        return bankCode;
//    }
//
//    /**
//     * @param bankCode the bankCode to set
//     */
//    public void setBankCode(String bankCode) {
//        if(this.bankCode==null) {
//            if(this instanceof BankHoldingAccount) {
//                this.bankCode = ((BankHoldingAccount)this).getBank().getId();   
//            } 
//        }
//        this.bankCode = bankCode;
//    }

   

    
}
