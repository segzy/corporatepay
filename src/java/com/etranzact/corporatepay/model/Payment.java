/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PAYMENT")
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class Payment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "PAYMENT_GEN")
    @GenericGenerator(name = "PAYMENT_GEN", strategy = "enhanced-table", parameters = {
        @org.hibernate.annotations.Parameter(name = "table_name", value = "hibernate_sequence_payment")
    })
    @Column(name = "ID")
    private Long id;
    @Column(name = "PAYMENT_ID")
    private String paymentReference;
    @Column(name = "PAYMENT_DESC")
    private String paymentDescription;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @ManyToOne
    @JoinColumn(name = "SOURCE_CARD")
    private Card card;
    @Column(name = "SOURCE_CARD_NUMBER")
    private String sourceCardNumber;
    @Column(name = "PAYMENT_ALIAS")
    private String paymentAlias;
    @Column(name = "TMP_PAYMENT_ALIAS")
    private String tmpPaymentAlias;
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate = new Corporate();
    @Column(name = "CHARGE_BEARER")
    private String chargeBearer;
//    @ManyToOne
//    @JoinColumn(name = "BANK_COMMISSION_ID")
//    private BankCommission commissionFees = new BankCommission();
//
//    @ManyToOne
//    @JoinColumn(name = "TMP_BANK_COMMISSION_ID")
//    private BankCommission tmpCommissionFees;
    @Transient
    private String uploadPath;

    //@Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, mappedBy = "payment")
    @OrderColumn
    private Set<Transaction> transactions = new HashSet<>();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "AUTHORIZED_DATE")
    private Date dateAuthorized = new Date();
    @Column(name = "AUTHORIZED_BY")
    private String authorizedBy;
    @Transient
    private String dateCreatedStr;
    @Transient
    private String authorizedDateStr;
    @Column(name = "SCHEDULED")
    private boolean scheduled = Boolean.FALSE;
    @Transient
    private HoldingAccount holdingAccount;
    @Column(name = "HOLDINGACCT_ID")
    private Long holdingAccountId;
    @Column(name = "HOLDINGACCT_NUMBER")
    private String holdingAccountNumber;
    @Column(name = "TRIAL")
    private Integer trial = new Integer("0");
    @Transient
    private String status;
    @Transient
    private BigDecimal totalAmount = BigDecimal.ZERO;
    @Transient
    private BigDecimal amountCharge = BigDecimal.ZERO;
    @Transient
    private BigDecimal amountProcessedOK = BigDecimal.ZERO;
    @Transient
    private BigDecimal amountPendingApproval = BigDecimal.ZERO;
    @Transient
    private BigDecimal amountPendingPayment = BigDecimal.ZERO;
    @Transient
    private BigDecimal amountPaid = BigDecimal.ZERO;
    @Transient
    private String totalAmountf;
    @Transient
    private String amountChargef;
    @Transient
    private String amountProcessedOKf;
    @Transient
    private String amountPendingApprovalf;
    @Transient
    private String amountPendingPaymentf;
    @Transient
    private String amountPaidf;
    @Transient
    private Integer totalCount = Integer.parseInt("0");
    @Transient
    private Integer processedCountOK = Integer.parseInt("0");
    @Transient
    private Integer pendingApproval = Integer.parseInt("0");
    ;
    @Transient
    private Integer pendingPayment = Integer.parseInt("0");
    @Transient
    private Integer paid = Integer.parseInt("0");
    @Column(name = "INVALID_COUNT")
    private Integer invalidCount = new Integer("0");
    @Column(name = "DUPLICATE_COUNT")
    private Integer duplicateCount = new Integer("0");
    @Transient
    private List<Transaction> uploads;
    @Transient
    private List<Transaction> paidTrans;
    @Transient
    private List<Transaction> pendingTrans;
    @Column(name = "RESPONSE")
    private String response;

    /**
     * @return the status
     */
    public String getStatus() {
        setStatus(flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT
                                                : flagStatus.equals(AppConstants.CREATED_IN_APPROVAL) ? AppConstants.CREATED_IN_APPROVAL_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                                ? AppConstants.MODIFIED_REJECTED_TEXT : flagStatus.equals(AppConstants.PROCESSING) ? AppConstants.PROCESSING_TEXT : flagStatus.equals(AppConstants.PROCESSED_OK) ? AppConstants.PROCESSING_OK_TEXT
                                                                                : flagStatus.equals(AppConstants.PROCESSED_WITH_ERRORS) ? AppConstants.PROCESSING_WITH_ERRORS_TEXT : flagStatus.equals(AppConstants.PAYMENT_HELD) ? AppConstants.PAYMENT_HELD_TEXT : flagStatus.equals(AppConstants.PAYMENT_CLOSE) ? AppConstants.PAYMENT_CLOSE_TEXT : "" : "");
        return status;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the paymentReference
     */
    public String getPaymentReference() {
        String val = id == null ? null : id.toString();
        return val;
    }

    /**
     * @param paymentReference the paymentReference to set
     */
    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

//    /**
//     * @param commissionFees the commissionFees to set
//     */
//    public void setCommissionFees(BankCommission commissionFees) {
//        this.commissionFees = commissionFees;
//    }
//
//    /**
//     * @return the commissionFees
//     */
//    public BankCommission getCommissionFees() {
//        return commissionFees;
//    }
//
//    /**
//     * @return the tmpCommissionFees
//     */
//    public BankCommission getTmpCommissionFees() {
//        return tmpCommissionFees;
//    }
//
//    /**
//     * @param tmpCommissionFees the tmpCommissionFees to set
//     */
//    public void setTmpCommissionFees(BankCommission tmpCommissionFees) {
//        this.tmpCommissionFees = tmpCommissionFees;
//    }

    /**
     * @return the paymentAlias
     */
    public String getPaymentAlias() {
        return paymentAlias;
    }

    /**
     * @param paymentAlias the paymentAlias to set
     */
    public void setPaymentAlias(String paymentAlias) {
        this.paymentAlias = paymentAlias;
    }

    /**
     * @return the tmpPaymentAlias
     */
    public String getTmpPaymentAlias() {
        return tmpPaymentAlias;
    }

    /**
     * @param tmpPaymentAlias the tmpPaymentAlias to set
     */
    public void setTmpPaymentAlias(String tmpPaymentAlias) {
        this.tmpPaymentAlias = tmpPaymentAlias;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        if (totalAmount.compareTo(BigDecimal.ZERO) == 0) {
            for (Transaction transaction : transactions) {
                if (transaction.getSchedule() == null) {
                    totalAmount = totalAmount.add(transaction.getAmount());
                }
            }
        }
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the transactions
     */
    public Set<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * @param transactions the transactions to set
     */
    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * @return the totalCount
     */
    public int getTotalCount() {
        if (totalCount == 0) {
            for (Transaction transaction : transactions) {
                if (transaction.getSchedule() == null) {
                    totalCount = totalCount == null ? new Integer("0") : totalCount;
                    totalCount++;
                }
            }
        }
        return totalCount;
    }

    /**
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return the amountProcessedOK
     */
    public BigDecimal getAmountProcessedOK() {
        return amountProcessedOK;
    }

    /**
     * @param amountProcessedOK the amountProcessedOK to set
     */
    public void setAmountProcessedOK(BigDecimal amountProcessedOK) {
        this.amountProcessedOK = amountProcessedOK;
    }

    /**
     * @return the processedCountOK
     */
    public Integer getProcessedCountOK() {
        return processedCountOK;
    }

    /**
     * @param processedCountOK the processedCountOK to set
     */
    public void setProcessedCountOK(Integer processedCountOK) {
        this.processedCountOK = processedCountOK;
    }

    /**
     * @return the card
     */
    public Card getCard() {
        return card;
    }

    /**
     * @param card the card to set
     */
    public void setCard(Card card) {
        this.card = card;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the pendingApproval
     */
    public Integer getPendingApproval() {
        return pendingApproval;
    }

    /**
     * @param pendingApproval the pendingApproval to set
     */
    public void setPendingApproval(Integer pendingApproval) {
        this.pendingApproval = pendingApproval;
    }

    /**
     * @return the pendingPayment
     */
    public Integer getPendingPayment() {
        if (transactions.isEmpty()) {
            return transactions.size();
        }

        if (pendingPayment == 0) {
            for (Transaction transaction : transactions) {
                if (transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PENDING_PAYMENT) || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_FAILED)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PROCESSING)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_REVERSED)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_WAITING)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PENDING_APPROVAL)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PROCESSING)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_STATUS_UNKNOWN)) {
                    pendingPayment++;
                }
            }
        }
        return pendingPayment;
    }

    /**
     * @param pendingPayment the pendingPayment to set
     */
    public void setPendingPayment(Integer pendingPayment) {
        this.pendingPayment = pendingPayment;
    }

    /**
     * @return the paid
     */
    public Integer getPaid() {
        if (transactions.isEmpty()) {
            return transactions.size();
        }

        if (paid == 0) {
            for (Transaction transaction : transactions) {
                if (transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PAID)) {
                    paid++;
                }
            }
        }
        return paid;
    }

    /**
     * @param paid the paid to set
     */
    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    /**
     * @return the amountPendingApproval
     */
    public BigDecimal getAmountPendingApproval() {
        return amountPendingApproval;
    }

    /**
     * @param amountPendingApproval the amountPendingApproval to set
     */
    public void setAmountPendingApproval(BigDecimal amountPendingApproval) {
        this.amountPendingApproval = amountPendingApproval;
    }

    /**
     * @return the amountPendingPayment
     */
    public BigDecimal getAmountPendingPayment() {
        if (transactions.isEmpty()) {
            return BigDecimal.ZERO;
        }
        if (amountPendingPayment.compareTo(BigDecimal.ZERO) == 0) {
            for (Transaction transaction : transactions) {
                if (transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PENDING_PAYMENT)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_FAILED)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PROCESSING)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_REVERSED)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_WAITING)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PENDING_APPROVAL)
                        || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_STATUS_UNKNOWN)) {
                    amountPendingPayment = amountPendingPayment.add(transaction.getAmount());
                }
            }
        }
        return amountPendingPayment;
    }

    /**
     * @param amountPendingPayment the amountPendingPayment to set
     */
    public void setAmountPendingPayment(BigDecimal amountPendingPayment) {
        this.amountPendingPayment = amountPendingPayment;
    }

    /**
     * @return the amountPaid
     */
    public BigDecimal getAmountPaid() {
        if (transactions.isEmpty()) {
            return BigDecimal.ZERO;
        }
        if (amountPaid.compareTo(BigDecimal.ZERO) == 0) {
            for (Transaction transaction : transactions) {
                if (transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PAID)) {
                    amountPaid = amountPaid.add(transaction.getAmount());
                }
            }
        }
        return amountPaid;
    }

    /**
     * @param amountPaid the amountPaid to set
     */
    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    /**
     * @return the uploadPath
     */
    public String getUploadPath() {
        if (this instanceof UploadPayment) {
            uploadPath = ((UploadPayment) this).getUploadPath();
        }
        return uploadPath;
    }

    public String getUploadPath2() {
        return uploadPath;
    }

    /**
     * @return the scheduled
     */
    public boolean isScheduled() {
        return scheduled;
    }

    /**
     * @param scheduled the scheduled to set
     */
    public void setScheduled(boolean scheduled) {
        this.scheduled = scheduled;
    }

    /**
     * @return the dateCreatedStr
     */
    public String getDateCreatedStr() {
        dateCreatedStr = AppUtil.formatDate(dateCreated == null ? new Date() : dateCreated, "EEE, d MMM yyyy HH:mm:ss");
        return dateCreatedStr;
    }

    /**
     * @return the totalAmountf
     */
    public String getTotalAmountf() {
        totalAmountf = AppUtil.formatAsAmount(getTotalAmount().toString());
        return totalAmountf;
    }

    /**
     * @return the amountProcessedOKf
     */
    public String getAmountProcessedOKf() {
        amountProcessedOKf = AppUtil.formatAsAmount(amountProcessedOK.toString());
        return amountProcessedOKf;
    }

    /**
     * @return the amountPendingApprovalf
     */
    public String getAmountPendingApprovalf() {
        amountPendingApprovalf = AppUtil.formatAsAmount(amountPendingApproval.toString());
        return amountPendingApprovalf;
    }

    /**
     * @return the amountPendingPaymentf
     */
    public String getAmountPendingPaymentf() {
        amountPendingPaymentf = AppUtil.formatAsAmount(getAmountPendingPayment().toString());
        return amountPendingPaymentf;
    }

    /**
     * @return the amountPaidf
     */
    public String getAmountPaidf() {
        amountPaidf = AppUtil.formatAsAmount(getAmountPaid().toString());
        return amountPaidf;
    }

    @Override
    public String toString() {
        return id.toString();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the paymentDescription
     */
    public String getPaymentDescriptionn() {
        return paymentDescription;
    }

    /**
     * @return the paymentDescription
     */
    public String getPaymentDescription() {

        return paymentDescription == null || paymentDescription.isEmpty() ? "" : paymentDescription.startsWith("08") ? paymentDescription : id + "; " + paymentDescription;
    }

    /**
     * @param paymentDescription the paymentDescription to set
     */
    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    /**
     * @return the invalidCount
     */
    public Integer getInvalidCount() {
        return invalidCount;
    }

    /**
     * @param invalidCount the invalidCount to set
     */
    public void setInvalidCount(Integer invalidCount) {
        this.invalidCount = invalidCount;
    }

    /**
     * @return the duplicateCount
     */
    public Integer getDuplicateCount() {
        if (getTransactions().isEmpty()) {
            return duplicateCount;
        }
        Map<String, Integer> map = null;
        for (Transaction transaction : getTransactions()) {
            if (map == null) {
                map = new HashMap<>();
            }
            Integer count = map.get(transaction.getAccount().getAccountNumber());
            if (count == null) {
                map.put(transaction.getAccount().getAccountNumber(), new Integer("1"));
            } else {
                map.put(transaction.getAccount().getAccountNumber(), count + new Integer("1"));
            }
        }
        for (Transaction transaction : getTransactions()) {
            Integer count = map.get(transaction.getAccount().getAccountNumber());
            if (count < new Integer("2")) {
                map.remove(transaction.getAccount().getAccountNumber());
            }
        }
        duplicateCount = map.size();
        return duplicateCount;
    }

    /**
     * @param duplicateCount the duplicateCount to set
     */
    public void setDuplicateCount(Integer duplicateCount) {
        this.duplicateCount = duplicateCount;
    }

    /**
     * @return the uploads
     */
    public List<Transaction> getUploads() {
        if (transactions.isEmpty()) {
            return new ArrayList<>();
        }
        uploads = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_CREATED_OK) || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PENDING_APPROVAL)) {
                uploads.add(transaction);
            }
        }
        return uploads;
    }

    /**
     * @param uploads the uploads to set
     */
    public void setUploads(List<Transaction> uploads) {
        this.uploads = uploads;
    }

    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }

    /**
     * @return the chargeBearer
     */
    public String getChargeBearer() {
        return chargeBearer == null ? AppConstants.CHARGE_BEARER_BENEFICIARY : chargeBearer;
    }

    /**
     * @param chargeBearer the chargeBearer to set
     */
    public void setChargeBearer(String chargeBearer) {
        this.chargeBearer = chargeBearer;
    }

    /**
     * @return the dateAuthorized
     */
    public Date getDateAuthorized() {
        return dateAuthorized;
    }

    /**
     * @param dateAuthorized the dateAuthorized to set
     */
    public void setDateAuthorized(Date dateAuthorized) {
        this.dateAuthorized = dateAuthorized;
    }

    /**
     * @return the authorizedDateStr
     */
    public String getAuthorizedDateStr() {
        authorizedDateStr = dateAuthorized == null ? "" : AppUtil.formatDate(dateAuthorized, "EEE, d MMM yyyy HH:mm:ss");
        return authorizedDateStr;
    }

    /**
     * @return the authorizedBy
     */
    public String getAuthorizedBy() {
        return authorizedBy;
    }

    /**
     * @param authorizedBy the authorizedBy to set
     */
    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Payment other = (Payment) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    /**
     * @return the paidTrans
     */
    public List<Transaction> getPaidTrans() {
        if (transactions.isEmpty()) {
            return paidTrans = new ArrayList<>();
        }

        paidTrans = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PAID)) {
                System.out.println("paid trans: " + paidTrans);
                paidTrans.add(transaction);
            }
        }
        System.out.println("paid trans count: " + paidTrans.size());

        return paidTrans;
    }

    /**
     * @return the pendingTrans
     */
    public List<Transaction> getPendingTrans() {
        if (transactions.isEmpty()) {
            pendingTrans = new ArrayList<>();
        }
        pendingTrans = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PENDING_PAYMENT)
                    || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_FAILED)
                    || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PROCESSING)
                    || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_REVERSED)
                    || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_WAITING)
                    || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_PENDING_APPROVAL)
                    || transaction.getTrsnsactionStatus().equals(AppConstants.TRANSACTION_STATUS_UNKNOWN)) {
                System.out.println("paid trans: " + pendingTrans);
                pendingTrans.add(transaction);
            }
        }
        System.out.println("paid trans count: " + pendingTrans.size());
        return pendingTrans;
    }

    /**
     * @return the retrial
     */
    public Integer getTrial() {
        trial = trial == null ? new Integer("0") : trial;
        return trial;
    }

    /**
     * @param trial the retrial to set
     */
    public void setTrial(Integer trial) {
        this.trial = trial;
    }

    /**
     * @return the holdingAccountId
     */
    public Long getHoldingAccountId() {
        return holdingAccountId;
    }

    /**
     * @param holdingAccountId the holdingAccountId to set
     */
    public void setHoldingAccountId(Long holdingAccountId) {
        this.holdingAccountId = holdingAccountId;
    }

    /**
     * @return the holdingAccount
     */
    public HoldingAccount getHoldingAccount() {
        return holdingAccount;
    }

    /**
     * @param holdingAccount the holdingAccount to set
     */
    public void setHoldingAccount(HoldingAccount holdingAccount) {
        this.holdingAccount = holdingAccount;
    }

    /**
     * @return the amountCharge
     */
    public BigDecimal getAmountCharge() {
        if (amountCharge.compareTo(BigDecimal.ZERO) == 0) {
            for (Transaction transaction : transactions) {
                if (transaction.getSchedule() == null) {
                    amountCharge = amountCharge.add(transaction.getTotalFeeAmount());
                }
            }
        }
        return amountCharge;
    }

    /**
     * @param amountCharge the amountCharge to set
     */
    public void setAmountCharge(BigDecimal amountCharge) {
        this.amountCharge = amountCharge;
    }

    /**
     * @return the sourceCardNumber
     */
    public String getSourceCardNumber() {
        return sourceCardNumber;
    }

    /**
     * @param sourceCardNumber the sourceCardNumber to set
     */
    public void setSourceCardNumber(String sourceCardNumber) {
        this.sourceCardNumber = sourceCardNumber;
    }

    /**
     * @return the holdingAccountNumber
     */
    public String getHoldingAccountNumber() {
        return holdingAccountNumber;
    }

    /**
     * @param holdingAccountNumber the holdingAccountNumber to set
     */
    public void setHoldingAccountNumber(String holdingAccountNumber) {
        this.holdingAccountNumber = holdingAccountNumber;
    }

    /**
     * @return the amountChargef
     */
    public String getAmountChargef() {
        amountChargef = AppUtil.formatAsAmount(getAmountCharge().toString());
        return amountChargef;
    }

}
