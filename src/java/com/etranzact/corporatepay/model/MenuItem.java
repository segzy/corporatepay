/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_MENUITEM", uniqueConstraints = {@UniqueConstraint(columnNames = {"POSITION", "MENU_ID"})})
public class MenuItem implements Serializable, Comparable<MenuItem> {

    @Id
    @Column(name = "ID", length = 50)
    private String id;
    @Column(name = "LABEL", length = 50)
    private String label;
    @Column(name = "TOOL_TIP", length = 50)
    private String tooltip;
    @Column(name = "HREF", length = 50)
    private String href;
    @Column(name = "O_HREF", length = 50)
    private String ohref;
    @ManyToOne
    @JoinColumn(name = "MENU_ID")
    private Menu menu;
    @Column(name = "POSITION")
    private Integer position;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @ManyToMany(mappedBy = "resources")
    private Set<Role> roles = new HashSet<>();
                 @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();

    //@ManyToMany(mappedBy = "userMenus")
    //private Set<User> users = new HashSet<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getOhref() {
        return ohref;
    }

    public void setOhref(String ohref) {
        this.ohref = ohref;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    
    
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }

    @Override
    public boolean equals(Object object) {
        //return EqualsBuilder.reflectionEquals(this, obj); //fails security check,
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }

        if (object instanceof MenuItem) {
            MenuItem rhs = (MenuItem) object;
            return new EqualsBuilder()
                    .append(getId(), rhs.getId())
                    .isEquals();
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).
                append("id", getId()).
                append("description", getLabel()).
                append("url", getHref()).
                toString();
    }
    

//    @Override
//    public int compareTo(MenuItem o) {
//        return new CompareToBuilder().append(getId(), o.getId()).toComparison();
//    }
    
        @Override
    public int compareTo(MenuItem o) {
        return new CompareToBuilder().append(getPosition(), o.getPosition()).toComparison();
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

 
}
