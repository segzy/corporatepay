/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_APPROVALTASK")
//@Audited
public class ApprovalTask implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "APPROVAL_ROUTE_ID")
    private ApprovalRoute approvalRoute = new ApprovalRoute();
    @Column(name = "TARGET_OBJECT_ID")
    private String targetObjectId;
    @JoinColumn(name = "ACTOR_ID")
    @ManyToOne
    private Approver actor;
    @JoinColumn(name = "TASK_ACCESSOR")
    @ManyToOne
    private Approver taskAccessor;
    @Column(name = "COMMENT")
    private String comment;
    @Column(name = "APPROVAL_ACTION")
    private String approvalAction;
    @Column(name = "REQUEST_APPROVAL_TYPE")
    private String requestApprovalType;
    @Lob
    private byte[] document;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACTION_DATE")
    private Date actionDate;
    ////@Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(mappedBy = "approvalTask", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
    @Column(name = "CURRENT_ROUTE_ID")
    private Long currentRouteId;
    @Transient
    private String taskType;
    @Transient
    private String createdDateFormatted;
    @Transient
    private String actionDateFormatted;
    @Transient
    private String targetObjFormatted;
    @Transient
    private boolean cardDetailsRequired;
    @Transient
    private boolean tokenRequired;
        @Transient
    private boolean cardRequired;
    @Transient
    private String cardDetail;
      @Transient
    private String token;
         @Transient
    private String expMonth;
      @Transient
    private String expYear;
         @Transient
    private int appStage;

    @Transient
    private CorporateCard corporateCard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the approvalRoute
     */
    public ApprovalRoute getApprovalRoute() {
        return approvalRoute;
    }

    /**
     * @param approvalRoute the approvalRoute to set
     */
    public void setApprovalRoute(ApprovalRoute approvalRoute) {
        this.approvalRoute = approvalRoute;
    }

    /**
     * @return the targetObjectId
     */
    public String getTargetObjectId() {
        return targetObjectId;
    }

    /**
     * @param targetObjectId the targetObjectId to set
     */
    public void setTargetObjectId(String targetObjectId) {
        this.targetObjectId = targetObjectId;
    }

    /**
     * @return the document
     */
    public byte[] getDocument() {
        return document;
    }

    /**
     * @return the approvalAction
     */
    public String getApprovalAction() {
        approvalAction = approvalAction == null ? AppConstants.OPEN : approvalAction;
        return approvalAction;
    }

    /**
     * @param approvalAction the approvalAction to set
     */
    public void setApprovalAction(String approvalAction) {
        this.approvalAction = approvalAction;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the actionDate
     */
    public Date getActionDate() {
        return actionDate;
    }

    /**
     * @param actionDate the actionDate to set
     */
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    /**
     * @return the nameValuePairs
     */
    public List<TaskObjectNameValuePair> getNameValuePairs() {
        return nameValuePairs;
    }

    /**
     * @param nameValuePairs the nameValuePairs to set
     */
    public void setNameValuePairs(List<TaskObjectNameValuePair> nameValuePairs) {
        this.nameValuePairs = nameValuePairs;
    }

    /**
     * @return the actor
     */
    public Approver getActor() {
        return actor;
    }

    /**
     * @param actor the actor to set
     */
    public void setActor(Approver actor) {
        this.actor = actor;
    }

    /**
     * @return the taskAccessor
     */
    public Approver getTaskAccessor() {
        return taskAccessor;
    }

    /**
     * @param taskAccessor the taskAccessor to set
     */
    public void setTaskAccessor(Approver taskAccessor) {
        this.taskAccessor = taskAccessor;
    }

    /**
     * @param document the document to set
     */
    public void setDocument(byte[] document) {
        this.document = document;
    }

    /**
     * @return the taskType
     */
    public String getTaskType() {
        return taskType;
    }

    /**
     * @param taskType the taskType to set
     */
    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    /**
     * @return the requestApprovalType
     */
    public String getRequestApprovalType() {
        return requestApprovalType;
    }

    /**
     * @param requestApprovalType the requestApprovalType to set
     */
    public void setRequestApprovalType(String requestApprovalType) {
        this.requestApprovalType = requestApprovalType;
    }

    /**
     * @return the createdDateFormatted
     */
    public String getCreatedDateFormatted() {
        createdDateFormatted = AppUtil.formatDate(dateCreated, "EEE, d MMM yyyy HH:mm:ss");
        return createdDateFormatted;
    }

    /**
     * @param createdDateFormatted the createdDateFormatted to set
     */
    public void setCreatedDateFormatted(String createdDateFormatted) {
        this.createdDateFormatted = createdDateFormatted;
    }

    /**
     * @return the actionDateFormatted
     */
    public String getActionDateFormatted() {
        if(actionDate!=null) {
        actionDateFormatted = AppUtil.formatDate(actionDate, "EEE, d MMM yyyy HH:mm:ss");
        }
        return actionDateFormatted;
    }

    /**
     * @param actionDateFormatted the actionDateFormatted to set
     */
    public void setActionDateFormatted(String actionDateFormatted) {
        this.actionDateFormatted = actionDateFormatted;
    }

    /**
     * @return the targetObjFormatted
     */
    public String getTargetObjFormatted() {
        return targetObjFormatted;
    }

    /**
     * @param targetObjFormatted the targetObjFormatted to set
     */
    public void setTargetObjFormatted(String targetObjFormatted) {
        this.targetObjFormatted = targetObjFormatted;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApprovalTask other = (ApprovalTask) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApprovalTask{" + "id=" + id + '}';
    }

    /**
     * @return the cardDetailsRequired
     */
    public boolean isCardDetailsRequired() {
        return cardDetailsRequired;
    }

    /**
     * @param cardDetailsRequired the cardDetailsRequired to set
     */
    public void setCardDetailsRequired(boolean cardDetailsRequired) {
        this.cardDetailsRequired = cardDetailsRequired;
    }

    /**
     * @return the tokenRequired
     */
    public boolean isTokenRequired() {
        return tokenRequired;
    }

    /**
     * @param tokenRequired the tokenRequired to set
     */
    public void setTokenRequired(boolean tokenRequired) {
        this.tokenRequired = tokenRequired;
    }

    /**
     * @return the cardDetail
     */
    public String getCardDetail() {
        return cardDetail;
    }

    /**
     * @param cardDetail the cardDetail to set
     */
    public void setCardDetail(String cardDetail) {
        this.cardDetail = cardDetail;
    }



    /**
     * @return the cardRequired
     */
    public boolean isCardRequired() {
        return cardRequired;
    }

    /**
     * @param cardRequired the cardRequired to set
     */
    public void setCardRequired(boolean cardRequired) {
        this.cardRequired = cardRequired;
    }

    /**
     * @return the corporateCard
     */
    public CorporateCard getCorporateCard() {
        return corporateCard;
    }

    /**
     * @param corporateCard the corporateCard to set
     */
    public void setCorporateCard(CorporateCard corporateCard) {
        this.corporateCard = corporateCard;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the currentRouteId
     */
    public Long getCurrentRouteId() {
        return currentRouteId;
    }

    /**
     * @param currentRouteId the currentRouteId to set
     */
    public void setCurrentRouteId(Long currentRouteId) {
        this.currentRouteId = currentRouteId;
    }

    /**
     * @return the expMonth
     */
    public String getExpMonth() {
        return expMonth;
    }

    /**
     * @param expMonth the expMonth to set
     */
    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    /**
     * @return the expYear
     */
    public String getExpYear() {
        return expYear;
    }

    /**
     * @param expYear the expYear to set
     */
    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }

    /**
     * @return the appStage
     */
    public int getAppStage() {
        return appStage;
    }

    /**
     * @param appStage the appStage to set
     */
    public void setAppStage(int appStage) {
        this.appStage = appStage;
    }
   
    
    

}
