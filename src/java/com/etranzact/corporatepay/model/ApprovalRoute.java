/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_APPROVAL_ROUTE")
@Inheritance(strategy= InheritanceType.JOINED)
//@Audited
public class ApprovalRoute implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "APPROVAL_TYPE_ID")
       //@Audited(targetAuditMode = NOT_AUDITED)
    private ApprovalType approvalType = new ApprovalType();
    @ManyToOne
    @JoinColumn(name = "APPROVER")
    private Approver approver = new Approver();
    @Column(name = "LEVEL_NO")
    private Integer level;

    @Column(name = "TMP_LEVEL_NO")
    private Integer tmpLevel;

    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "DATE_CREATED", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated = new Date();
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
      @Column(name = "ROUTE_ALIAS")
    private String routeAlias;
    @Transient
    private String status;
        @Column(name = "MAX_LEVEL")
    private Boolean maxLevel = Boolean.FALSE;
        @Transient
        private ApprovalTask approvalTask;
     
    
                

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ApprovalType getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(ApprovalType approvalType) {
        this.approvalType = approvalType;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getTmpLevel() {
        return tmpLevel;
    }

    public void setTmpLevel(Integer tmpLevel) {
        this.tmpLevel = tmpLevel;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getTmpActive() {
        return tmpActive;
    }

    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the approver
     */
    public Approver getApprover() {
        return approver;
    }

    /**
     * @param approver the approver to set
     */
    public void setApprover(Approver approver) {
        this.approver = approver;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }

    /**
     * @return the maxLevel
     */
    public Boolean getMaxLevel() {
        return maxLevel;
    }

    /**
     * @param maxLevel the maxLevel to set
     */
    public void setMaxLevel(Boolean maxLevel) {
        this.maxLevel = maxLevel;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApprovalRoute other = (ApprovalRoute) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApprovalRoute{" + "id=" + id + '}';
    }

        /**
     * @return the status
     */
    public String getStatus() {
        status=flagStatus!=null?flagStatus.equals(AppConstants.APPROVED)?AppConstants.APPROVED_TEXT:
         flagStatus.equals(AppConstants.REJECTED)?AppConstants.REJECTED_TEXT:flagStatus.equals(AppConstants.CREATED)?
                AppConstants.CREATED_TEXT:flagStatus.equals(AppConstants.MODIFIED)?AppConstants.MODIFIED_TEXT:
                flagStatus.equals(AppConstants.CREATED_REJECTED)?AppConstants.CREATED_REJECTED_TEXT:flagStatus.equals(AppConstants.MODIFIED_REJECTED)?
                AppConstants.MODIFIED_REJECTED_TEXT:"":"";
        return status;
    }

    /**
     * @return the routeAlias
     */
    public String getRouteAlias() {
        return routeAlias;
    }

    /**
     * @param routeAlias the routeAlias to set
     */
    public void setRouteAlias(String routeAlias) {
        this.routeAlias = routeAlias;
    }

    /**
     * @return the approvalTask
     */
    public ApprovalTask getApprovalTask() {
        return approvalTask;
    }

    /**
     * @param approvalTask the approvalTask to set
     */
    public void setApprovalTask(ApprovalTask approvalTask) {
        this.approvalTask = approvalTask;
    }

    


}
