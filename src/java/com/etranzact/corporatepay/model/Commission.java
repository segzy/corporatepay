/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_GRADUATED_COMMISSION")
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class Commission implements Serializable {

    public Commission() {
        setCentral(Boolean.FALSE);
    }

    @Id
    @GeneratedValue
    @Column(name = "COMMISSION_ID")
    private Long id;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.FALSE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @Column(name = "STRUCTURE_NAME")
    private String structureName;
    //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_GRAD_COMMISSION_COMMISSIONFEE", joinColumns
            = @JoinColumn(name = "COMMISSION_ID"), inverseJoinColumns
            = @JoinColumn(name = "COMMISSION_FEE_ID"))
    private Set<CommissionFee> commissionFees = new HashSet<>();
    //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_GRAD_COMMISSION_COMMISSIONFEE", joinColumns
            = @JoinColumn(name = "COMMISSION_ID"), inverseJoinColumns
            = @JoinColumn(name = "COMMISSION_FEE_ID"))
    private Set<CommissionFee> tmpCommissionFees;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Column(name = "CENTRAL")
    private Boolean central = Boolean.FALSE;
    @ManyToOne
    @JoinColumn(name = "BANK_ID")
    //@Audited(targetAuditMode = NOT_AUDITED)
    private Bank bank = new Bank();
    @Column(name = "ETZ_PERCENT")
    private BigDecimal etzPercent;
    @Column(name = "THIRDPARTY1_PERCENT")
    private BigDecimal thirdPartyPercent1;
    @Column(name = "THIRDPARTY2_PERCENT")
    private BigDecimal thirdPartyPercent2;
    @Column(name = "TMP_ETZ_PERCENT")
    private BigDecimal tmpEtzPercent;
    @Column(name = "TMP_THIRDPARTY1_PERCENT")
    private BigDecimal tmpThirdPartyPercent1;
    @Column(name = "TMP_THIRDPARTY2_PERCENT")
    private BigDecimal tmpThirdPartyPercent2;
    @Transient
    private String status;
    @Column(name = "CALCULATION_MODE")
    private int calculationMode;
     @Column(name = "TMP_CALCULATION_MODE")
    private int tmpCalculationMode;

    /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    @Override
//    public int hashCode() {
//        return new HashCodeBuilder().append(getId()).toHashCode();
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        if (object == null) {
//            return false;
//        }
//        if (object == this) {
//            return true;
//        }
//        if (object instanceof Commission) {
//            Commission rhs = (Commission) object;
//            return new EqualsBuilder()
//                    .appendSuper(super.equals(object))
//                    .append(getId(), rhs.getId())
//                    .isEquals();
//        } else {
//            return false;
//        }
//
//    }
//
//    @Override
//    public String toString() {
//        return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).
//                append("id", getId()).
//                toString();
//    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Commission other = (Commission) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Commission{" + "id=" + id + '}';
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the commissionFees
     */
    public Set<CommissionFee> getCommissionFees() {
        return commissionFees;
    }

    /**
     * @param commissionFees the commissionFees to set
     */
    public void setCommissionFees(Set<CommissionFee> commissionFees) {
        this.commissionFees = commissionFees;
    }

    /**
     * @return the tmpCommissionFees
     */
    public Set<CommissionFee> getTmpCommissionFees() {
        return tmpCommissionFees;
    }

    /**
     * @param tmpCommissionFees the tmpCommissionFees to set
     */
    public void setTmpCommissionFees(Set<CommissionFee> tmpCommissionFees) {
        this.tmpCommissionFees = tmpCommissionFees;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the structureName
     */
    public String getStructureName() {
        return structureName;
    }

    /**
     * @param structureName the structureName to set
     */
    public void setStructureName(String structureName) {
        this.structureName = structureName;
    }

    /**
     * @return the etzPercent
     */
    public BigDecimal getEtzPercent() {
        return etzPercent;
    }

    /**
     * @param etzPercent the etzPercent to set
     */
    public void setEtzPercent(BigDecimal etzPercent) {
        this.etzPercent = etzPercent;
    }

    /**
     * @return the thirdPartyPercent1
     */
    public BigDecimal getThirdPartyPercent1() {
        return thirdPartyPercent1;
    }

    /**
     * @param thirdPartyPercent1 the thirdPartyPercent1 to set
     */
    public void setThirdPartyPercent1(BigDecimal thirdPartyPercent1) {
        this.thirdPartyPercent1 = thirdPartyPercent1;
    }

    /**
     * @return the thirdPartyPercent2
     */
    public BigDecimal getThirdPartyPercent2() {
        return thirdPartyPercent2;
    }

    /**
     * @param thirdPartyPercent2 the thirdPartyPercent2 to set
     */
    public void setThirdPartyPercent2(BigDecimal thirdPartyPercent2) {
        this.thirdPartyPercent2 = thirdPartyPercent2;
    }

    /**
     * @return the tmpEtzPercent
     */
    public BigDecimal getTmpEtzPercent() {
        return tmpEtzPercent;
    }

    /**
     * @param tmpEtzPercent the tmpEtzPercent to set
     */
    public void setTmpEtzPercent(BigDecimal tmpEtzPercent) {
        this.tmpEtzPercent = tmpEtzPercent;
    }

    /**
     * @return the tmpThirdPartyPercent1
     */
    public BigDecimal getTmpThirdPartyPercent1() {
        return tmpThirdPartyPercent1;
    }

    /**
     * @param tmpThirdPartyPercent1 the tmpThirdPartyPercent1 to set
     */
    public void setTmpThirdPartyPercent1(BigDecimal tmpThirdPartyPercent1) {
        this.tmpThirdPartyPercent1 = tmpThirdPartyPercent1;
    }

    /**
     * @return the tmpThirdPartyPercent2
     */
    public BigDecimal getTmpThirdPartyPercent2() {
        return tmpThirdPartyPercent2;
    }

    /**
     * @param tmpThirdPartyPercent2 the tmpThirdPartyPercent2 to set
     */
    public void setTmpThirdPartyPercent2(BigDecimal tmpThirdPartyPercent2) {
        this.tmpThirdPartyPercent2 = tmpThirdPartyPercent2;
    }

    /**
     * @return the calculationMode
     */
    public int getCalculationMode() {
        return calculationMode;
    }

    /**
     * @param calculationMode the calculationMode to set
     */
    public void setCalculationMode(int calculationMode) {
        this.calculationMode = calculationMode;
    }

    /**
     * @return the tmpCalculationMode
     */
    public int getTmpCalculationMode() {
        return tmpCalculationMode;
    }

    /**
     * @param tmpCalculationMode the tmpCalculationMode to set
     */
    public void setTmpCalculationMode(int tmpCalculationMode) {
        this.tmpCalculationMode = tmpCalculationMode;
    }
    
    
    

}
