/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_APPROVAL_GROUP")
@Inheritance(strategy= InheritanceType.JOINED)
//@Audited
public class ApprovalGroup extends Approver {

       //@Audited(targetAuditMode = NOT_AUDITED)
    @OrderColumn
    @JoinTable(name = "COP_APPROVALGROUP_USER", joinColumns =
    @JoinColumn(name = "APPROVALGROUP_ID"), inverseJoinColumns =
    @JoinColumn(name = "USER_ID"))
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<User> users = new HashSet<>();
           @Column(name="GROUP_ALIAS")
    private String groupAlias;
           //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_APPROVALGROUP_USER", joinColumns =
    @JoinColumn(name = "APPROVALGROUP_ID"), inverseJoinColumns =
    @JoinColumn(name = "USER_ID"))
    private Set<User> tmpUsers = new HashSet<>();
           @Column(name="TMP_GROUP_ALIAS")
    private String tmpGroupAlias;
               @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();
   @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;
        @Column(name="ACTIVE_STATUS")
    private Boolean active = Boolean.FALSE;
       @Column(name="TMP_ACTIVE_STATUS")
    private Boolean tmpActive = Boolean.FALSE;
     @Column(name = "FLAG_STATUS")
    private String flagStatus;
         @Transient
    private String status;
   
    @Override
    public String getIdentity() {
       return groupAlias + "(GROUP)"; 
    }
    
//    @Override
//    public int hashCode() {
//        return new HashCodeBuilder().append(getId()).toHashCode();
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        if (object == null) { return false; }
//        if (object == this) { return true; }
//
//        if (object instanceof ApprovalGroup) {
//            ApprovalGroup rhs = (ApprovalGroup) object;
//            return new EqualsBuilder()
//                    .append(getId(), rhs.getId())
//                    .isEquals();
//        }  else {
//            return false;
//        }
//    }
//
//    @Override
//    public String toString() {
//        return new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE).
//                append("id", getId()).
//                toString();
//    }
    
    

    /**
     * @return the users
     */
    public Set<User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(Set<User> users) {
        this.users = users;
    }

    /**
     * @return the groupAlias
     */
    public String getGroupAlias() {
        return groupAlias;
    }

    /**
     * @param groupAlias the groupAlias to set
     */
    public void setGroupAlias(String groupAlias) {
        this.groupAlias = groupAlias;
    }

    /**
     * @return the tmpUsers
     */
    public Set<User> getTmpUsers() {
        return tmpUsers;
    }

    /**
     * @param tmpUsers the tmpUsers to set
     */
    public void setTmpUsers(Set<User> tmpUsers) {
        this.tmpUsers = tmpUsers;
    }

    /**
     * @return the tmpGroupAlias
     */
    public String getTmpGroupAlias() {
        return tmpGroupAlias;
    }

    /**
     * @param tmpGroupAlias the tmpGroupAlias to set
     */
    public void setTmpGroupAlias(String tmpGroupAlias) {
        this.tmpGroupAlias = tmpGroupAlias;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        status=flagStatus!=null?flagStatus.equals(AppConstants.APPROVED)?AppConstants.APPROVED_TEXT:
         flagStatus.equals(AppConstants.REJECTED)?AppConstants.REJECTED_TEXT:flagStatus.equals(AppConstants.CREATED)?
                AppConstants.CREATED_TEXT:flagStatus.equals(AppConstants.MODIFIED)?AppConstants.MODIFIED_TEXT:
                flagStatus.equals(AppConstants.CREATED_REJECTED)?AppConstants.CREATED_REJECTED_TEXT:flagStatus.equals(AppConstants.MODIFIED_REJECTED)?
                AppConstants.MODIFIED_REJECTED_TEXT:"":"";
        return status;
    }
    
    

}
