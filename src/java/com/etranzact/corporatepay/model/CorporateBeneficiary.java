/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_CORPORATE_BENEFICIARY")
//@Audited
public class CorporateBeneficiary extends Beneficiary {


      @ManyToOne
    @JoinColumn(name="CORPORATE_ID")
    private Corporate corporate = new Corporate();
    
    @ManyToOne
    @JoinColumn(name="TMP_CORPORATE_ID")
    private Corporate tmpCorporate;

           public CorporateBeneficiary() {
         setCentral(Boolean.FALSE);
     }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

  

    
}
