/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_ACCOUNT")
@Inheritance(strategy = InheritanceType.JOINED)
//@Audited
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ACCOUNT_GEN")
    @GenericGenerator(name = "ACCOUNT_GEN", strategy = "enhanced-table", parameters = {
        @org.hibernate.annotations.Parameter(name = "table_name", value = "hibernate_sequence_acct")
    })
    @Column(name = "ID")
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Column(name = "ACCOUNT_NAME", nullable = false)
    protected String accountName;
    @Column(name = "ACCOUNT_NUMBER")
    protected String accountNumber;
     @Column(name = "TMP_ACCOUNT_NUMBER")
    private String tmpAccountNumber;
    @Transient
    private boolean defaultBank;
    @Transient
    protected String accountIdentity;
    @Transient
    private String bankName;
    @Transient
    private Bank bank;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

//    @Override
//    public String toString() {
//        return "Account{" + "id=" + id + '}';
//    }
    /**
     * @return the defaultBank
     */
    public boolean isDefaultBank() {
        return defaultBank;
    }

    /**
     * @param defaultBank the defaultBank to set
     */
    public void setDefaultBank(boolean defaultBank) {
        this.defaultBank = defaultBank;
    }

    /**
     * @return the accountIdentity
     */
    public String getAccountIdentity() {
        return accountIdentity;
    }

    /**
     * @return the accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * @param accountName the accountName to set
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        System.out.println("acct tostring");
        if (this instanceof BankAccount) {
            if (getId() != null) {
                accountIdentity = ((BankAccount) this).getBank().getBankName() + "/" + accountName + "/" + accountNumber;
//                  System.out.println("bank accountIdentity: " + accountIdentity);
            }
            return accountIdentity;
        } else if (this instanceof PocketMoneyAccount) {
            if (getId() != null) {
                accountIdentity = "Pocket Moni/" + accountName + "/" + accountNumber;
//                 System.out.println("pm accountIdentity: " + accountIdentity);
            }
            return accountIdentity;
        } else {
//            System.out.println("nothing");
            return "";
        }

    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        if(this instanceof BankAccount) {
            return ((BankAccount)this).getBank();
        }
        return bank;
    }

    /**
     * @return the tmpAccountNumber
     */
    public String getTmpAccountNumber() {
        return tmpAccountNumber;
    }

    /**
     * @param tmpAccountNumber the tmpAccountNumber to set
     */
    public void setTmpAccountNumber(String tmpAccountNumber) {
        this.tmpAccountNumber = tmpAccountNumber;
    }
    
    

}
