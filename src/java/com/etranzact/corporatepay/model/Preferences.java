/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PREFERENCES")
//@Audited
public class Preferences implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "PREFERECNE_ID")
    private Long id;
    @Column(name = "SESSION_TIME")
    private Integer sessionTime;
    @Column(name = "PAYROLL_MODE")
    private String payrollMode;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate = new Corporate();
    @JoinColumn(name = "BANK_ID")
    @ManyToOne
    private Bank bank;
    @Column(name = "SETTLEMENT_ACCOUNT")
    private String settlementAccount;
      @Column(name = "BENEFICIARY_CHARGE_BEARER")
    private Boolean beneficiaryChargeBearer=Boolean.TRUE;
          @Column(name = "THRESHOLD_PAYMENT_AMOUNT", length = 50)
      private String minimumPaymentAmount;
                    @Column(name = "THOLD_EMAILS", length = 500)
      private String minimumPaymentAmountEmails;
                                      @Column(name = "AUTH_EMAILS", length = 500)
      private String authorizationSupportEmails;
                    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Preferences other = (Preferences) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Preference{" + "id=" + getId() + '}';
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sessionTime
     */
    public Integer getSessionTime() {
        return sessionTime;
    }

    /**
     * @param sessionTime the sessionTime to set
     */
    public void setSessionTime(Integer sessionTime) {
        this.sessionTime = sessionTime;
    }

    /**
     * @return the payrollMode
     */
    public String getPayrollMode() {
        return payrollMode;
    }

    /**
     * @param payrollMode the payrollMode to set
     */
    public void setPayrollMode(String payrollMode) {
        this.payrollMode = payrollMode;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the settlementAccount
     */
    public String getSettlementAccount() {
        return settlementAccount;
    }

    /**
     * @param settlementAccount the settlementAccount to set
     */
    public void setSettlementAccount(String settlementAccount) {
        this.settlementAccount = settlementAccount;
    }

    /**
     * @return the beneficiaryChargeBearer
     */
    public Boolean getBeneficiaryChargeBearer() {
        return beneficiaryChargeBearer==null?Boolean.TRUE:beneficiaryChargeBearer;
    }

    /**
     * @param beneficiaryChargeBearer the beneficiaryChargeBearer to set
     */
    public void setBeneficiaryChargeBearer(Boolean beneficiaryChargeBearer) {
        this.beneficiaryChargeBearer = beneficiaryChargeBearer;
    }

    /**
     * @return the minimumPaymentAmount
     */
    public String getMinimumPaymentAmount() {
        return minimumPaymentAmount;
    }

    /**
     * @param minimumPaymentAmount the minimumPaymentAmount to set
     */
    public void setMinimumPaymentAmount(String minimumPaymentAmount) {
        this.minimumPaymentAmount = minimumPaymentAmount;
    }

    /**
     * @return the minimumPaymentAmountEmails
     */
    public String getMinimumPaymentAmountEmails() {
        return minimumPaymentAmountEmails;
    }

    /**
     * @param minimumPaymentAmountEmails the minimumPaymentAmountEmails to set
     */
    public void setMinimumPaymentAmountEmails(String minimumPaymentAmountEmails) {
        this.minimumPaymentAmountEmails = minimumPaymentAmountEmails;
    }

    /**
     * @return the authorizationSupportEmails
     */
    public String getAuthorizationSupportEmails() {
        return authorizationSupportEmails;
    }

    /**
     * @param authorizationSupportEmails the authorizationSupportEmails to set
     */
    public void setAuthorizationSupportEmails(String authorizationSupportEmails) {
        this.authorizationSupportEmails = authorizationSupportEmails;
    }

    
    
}
