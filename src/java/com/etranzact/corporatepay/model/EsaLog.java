package com.etranzact.corporatepay.model;

//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Emmanuel.Kpoudosu on 9/1/2014.
 */
@Entity
@Table(name="COP_ESA_LOG")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EsaLog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ESA_ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "ESA_USER_ID")
    private User user;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ESA_REQUEST_DATE")
    private Date date = new Date();
    @Column(name = "ESA_RESPONSE")
    private String response;
    @Enumerated(EnumType.STRING)
    @Column(name="ESA_REQUEST_TYPE")
    private EsaType esaType;
    @Column(name="ESA_MEDIUM")
    private String esaMedium;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public EsaType getEsaType() {
        return esaType;
    }

    public void setEsaType(EsaType esaType) {
        this.esaType = esaType;
    }

    public String getEsaMedium() {
        return esaMedium;
    }

    public void setEsaMedium(String esaMedium) {
        this.esaMedium = esaMedium;
    }
}
