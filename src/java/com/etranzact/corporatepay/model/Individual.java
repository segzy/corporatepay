/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_INDIVIDUAL")
//@Audited
public class Individual extends User {
       
        //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_INDIVIDUAL_CARD", joinColumns =
    @JoinColumn(name = "INDIVIDUAL_ID"), inverseJoinColumns =
    @JoinColumn(name = "CARD_ID"))
    private Set<Card> cards = new HashSet<>();
        @ManyToOne
        @JoinColumn(name = "CARD_ID")
    private Card defaultCard;
             //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_INDIVIDUAL_CARD", joinColumns =
    @JoinColumn(name = "INDIVIDUAL_ID"), inverseJoinColumns =
    @JoinColumn(name = "CARD_ID"))
    private Set<Card> tmpCards = new HashSet<>();
        @ManyToOne
        @JoinColumn(name = "TMP_CARD_ID")
    private Card tmpDefaultCard;

    /**
     * @return the cards
     */
    public Set<Card> getCards() {
        return cards;
    }

    /**
     * @param cards the cards to set
     */
    public void setCards(Set<Card> cards) {
        this.cards = cards;
    }

    /**
     * @return the defaultCard
     */
    public Card getDefaultCard() {
        return defaultCard;
    }

    /**
     * @param defaultCard the defaultCard to set
     */
    public void setDefaultCard(Card defaultCard) {
        this.defaultCard = defaultCard;
    }

    /**
     * @return the tmpCards
     */
    public Set<Card> getTmpCards() {
        return tmpCards;
    }

    /**
     * @param tmpCards the tmpCards to set
     */
    public void setTmpCards(Set<Card> tmpCards) {
        this.tmpCards = tmpCards;
    }

    /**
     * @return the tmpDefaultCard
     */
    public Card getTmpDefaultCard() {
        return tmpDefaultCard;
    }

    /**
     * @param tmpDefaultCard the tmpDefaultCard to set
     */
    public void setTmpDefaultCard(Card tmpDefaultCard) {
        this.tmpDefaultCard = tmpDefaultCard;
    }
        
        
    
}
