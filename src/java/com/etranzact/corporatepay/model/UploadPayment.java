/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_UPLOAD_PAYMENT")
//@Audited
public class UploadPayment extends Payment {

    @Column(name = "UPLOAD_PATH")
    private String uploadPath;
    @Column(name = "SUPPLEMENTARY_UPLOAD_PATH")
    private String suppUploadPath;
    @Column(name = "TMP_UPLOAD_PATH")
    private String tmpUploadPath;
    //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "UPLOAD_TYPE_ID")
    private TransactionType uploadType;
    @JoinColumn(name = "HEADER_ADDED")
    private Boolean header = Boolean.TRUE;
      @Column(name = "UPLOADER_CONTACT")
    private String uploaderContact;
    @Transient
    private byte[] upload;
    @Transient
    private Set<Beneficiary> beneficiaries = new HashSet<>();


    /**
     * @return the beneficiaries
     */
    public Set<Beneficiary> getBeneficiaries() {
        return beneficiaries;
    }

    /**
     * @param beneficiaries the beneficiaries to set
     */
    public void setBeneficiaries(Set<Beneficiary> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    /**
     * @return the uploadPath
     */
    @Override
    public String getUploadPath() {
        return uploadPath;
    }
    
       /**
     * @return the uploadPath
     */
    @Override
    public String getUploadPath2() {
        uploadPath = uploadPath!=null?uploadPath.split("_")[0]:"";
        return uploadPath;
    }

    /**
     * @param uploadPath the uploadPath to set
     */
    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    /**
     * @return the tmpUploadPath
     */
    public String getTmpUploadPath() {
        return tmpUploadPath;
    }

    /**
     * @param tmpUploadPath the tmpUploadPath to set
     */
    public void setTmpUploadPath(String tmpUploadPath) {
        this.tmpUploadPath = tmpUploadPath;
    }

    /**
     * @return the uploadType
     */
    public TransactionType getUploadType() {
        return uploadType;
    }

    /**
     * @param uploadType the uploadType to set
     */
    public void setUploadType(TransactionType uploadType) {
        this.uploadType = uploadType;
    }

    /**
     * @return the upload
     */
    public byte[] getUpload() {
        return upload;
    }

    /**
     * @param upload the upload to set
     */
    public void setUpload(byte[] upload) {
        this.upload = upload;
    }

    /**
     * @return the header
     */
    public Boolean getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Boolean header) {
        this.header = header;
    }

    /**
     * @return the suppUploadPath
     */
    public String getSuppUploadPath() {
        return suppUploadPath;
    }

    /**
     * @param suppUploadPath the suppUploadPath to set
     */
    public void setSuppUploadPath(String suppUploadPath) {
        this.suppUploadPath = suppUploadPath;
    }

    /**
     * @return the uploaderContact
     */
    public String getUploaderContact() {
        return uploaderContact;
    }

    /**
     * @param uploaderContact the uploaderContact to set
     */
    public void setUploaderContact(String uploaderContact) {
        this.uploaderContact = uploaderContact;
    }

    
}
