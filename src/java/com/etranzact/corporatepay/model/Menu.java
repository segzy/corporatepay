/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.apache.commons.lang3.builder.CompareToBuilder;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_MENU")
public class Menu implements Serializable, Comparable<Menu> {
    @Id
    @Column(name = "ID", length = 50)
    private String id;
    @Column(name = "LABEL", length = 50)
    private String label;
    @Column(name = "TOOL_TIP", length = 100)
    private String tooltip;
    @Column(name = "POSITION", unique = true)
    private Integer position;
    @Transient
    private List<MenuItem> items = new ArrayList<>();
        @ManyToOne
    @JoinColumn(name = "CP_SERVICE_ID")
    private CorporatePayService corporatePayService;
                     @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED")
    private Date dateCreated = new Date();
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public List<MenuItem> getItems() {
        return items;
    }

    public void setItems(List<MenuItem> items) {
        this.items = items;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.etranzact.model.Menu[ id=" + getId() + " ]";
    }

//    @Override
//    public int compareTo(Menu o) {
//        return new CompareToBuilder().append(getId(), o.getId()).toComparison();
//    }
    
        @Override
    public int compareTo(Menu o) {
        return new CompareToBuilder().append(getPosition(), o.getPosition()).toComparison();
    }

    /**
     * @return the corporatePayService
     */
    public CorporatePayService getCorporatePayService() {
        return corporatePayService;
    }

    /**
     * @param corporatePayService the corporatePayService to set
     */
    public void setCorporatePayService(CorporatePayService corporatePayService) {
        this.corporatePayService = corporatePayService;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
    
    
    
}
