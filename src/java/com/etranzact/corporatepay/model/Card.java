/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_CARD", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"CARD_NUMBER"})})
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class Card implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Size(min = 16, max = 16)
    @Column(name = "CARD_NUMBER")
    private String cardNumber;
    @Column(name = "CARD_DESC")
    private String description;
    @Column(name = "ACTIVE_STATUS")
    private Boolean active = Boolean.FALSE;

    @Column(name = "TMP_CARD_NUMBER")
    private String tmpCardNumber;
    @Column(name = "TMP_CARD_DESC")
    private String tmpDescription;

    @Column(name = "TMP_ACTIVE_STATUS")
    private Boolean tmpActive = Boolean.FALSE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_CREATED", updatable = false)
    private Date createdDate = new Date();
    @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;
    @Transient
    private String status;
     @Column(name = "PIN")
    private String cardPIN;
          @Size(min = 2,max = 2)
      @Column(name = "EXP_MONTH")
    private String expMonth;
          @Size(min = 4,max = 4)
      @Column(name = "EXP_YEAR")
    private String expYear;
      @Column(name = "TMP_EXP_MONTH")
    private String tmpExpMonth;
      @Column(name = "TMP_EXP_YEAR")
    private String tmpExpYear;
      @Transient
      private String accountBalance;

    /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTmpCardNumber() {
        return tmpCardNumber;
    }

    public void setTmpCardNumber(String tmpCardNumber) {
        this.tmpCardNumber = tmpCardNumber;
    }

    public String getTmpDescription() {
        return tmpDescription;
    }

    public void setTmpDescription(String tmpDescription) {
        this.tmpDescription = tmpDescription;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getTmpActive() {
        return tmpActive;
    }

    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }

    /**
     * @return the cardPIN
     */
    public String getCardPIN() {
        return cardPIN;
    }

    /**
     * @param cardPIN the cardPIN to set
     */
    public void setCardPIN(String cardPIN) {
        this.cardPIN = cardPIN;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    /**
     * @return the expMonth
     */
    public String getExpMonth() {
        return expMonth;
    }

    /**
     * @param expMonth the expMonth to set
     */
    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    /**
     * @return the expYear
     */
    public String getExpYear() {
        return expYear;
    }

    /**
     * @param expYear the expYear to set
     */
    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }


    /**
     * @return the tmpExpMonth
     */
    public String getTmpExpMonth() {
        return tmpExpMonth;
    }

    /**
     * @param tmpExpMonth the tmpExpMonth to set
     */
    public void setTmpExpMonth(String tmpExpMonth) {
        this.tmpExpMonth = tmpExpMonth;
    }

    /**
     * @return the tmpExpYear
     */
    public String getTmpExpYear() {
        return tmpExpYear;
    }

    /**
     * @param tmpExpYear the tmpExpYear to set
     */
    public void setTmpExpYear(String tmpExpYear) {
        this.tmpExpYear = tmpExpYear;
    }

    /**
     * @return the accountBalance
     */
    public String getAccountBalance() {
        return accountBalance;
    }

    /**
     * @param accountBalance the accountBalance to set
     */
    public void setAccountBalance(String accountBalance) {
        this.accountBalance = accountBalance;
    }
    
    


    
    

}
