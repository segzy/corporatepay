/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_SCHEDULE_PAYMENT")
//@Audited
public class SchedulePayment extends Payment {

    @Column(name = "SCHEDULE_ID", nullable = false)
    private String schedulerId;
        //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToOne
    @JoinColumn(name = "TRANSACTION_TYPE_ID")
    private TransactionType transactionType;

    /**
     * @return the schedulerId
     */
    public String getSchedulerId() {
        return schedulerId;
    }

    /**
     * @param schedulerId the schedulerId to set
     */
    public void setSchedulerId(String schedulerId) {
        this.schedulerId = schedulerId;
    }

    /**
     * @return the transactionType
     */
    public TransactionType getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

  

}
