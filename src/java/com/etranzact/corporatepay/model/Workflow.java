/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_WORKFLOW")
@Inheritance(strategy = InheritanceType.JOINED)
//@Audited
public class Workflow implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "APPROVAL_TYPE_ID")
    //@Audited(targetAuditMode = NOT_AUDITED)
    private ApprovalType approvalType = new ApprovalType();
    //@Audited(targetAuditMode = NOT_AUDITED)
    @OrderColumn
    @JoinTable(name = "COP_WORKFLOW_APPROVALROUTE", joinColumns
            = @JoinColumn(name = "WORKFLOW_ID"), inverseJoinColumns
            = @JoinColumn(name = "APPROVALROUTE_ID"))
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<ApprovalRoute> approvalRoutes = new HashSet<>();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
        @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;
    @Column(name = "ACTIVE_STATUS")
    private Boolean active = Boolean.FALSE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the approvalType
     */
    public ApprovalType getApprovalType() {
        return approvalType;
    }

    /**
     * @param approvalType the approvalType to set
     */
    public void setApprovalType(ApprovalType approvalType) {
        this.approvalType = approvalType;
    }

    /**
     * @return the approvalRoutes
     */
    public Set<ApprovalRoute> getApprovalRoutes() {
        return approvalRoutes;
    }

    /**
     * @param approvalRoutes the approvalRoutes to set
     */
    public void setApprovalRoutes(Set<ApprovalRoute> approvalRoutes) {
        this.approvalRoutes = approvalRoutes;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    
}
