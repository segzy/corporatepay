/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_POCKET_MONEY_ACCOUNT")
//@Audited
public class PocketMoneyAccount extends Account {

    @Column(name = "TMP_ACCOUNT_NUMBER")
    @Size(min = 11, max = 20)
    private String tmpAccountNumber;
     @Column(name = "ACCOUNT_TYPE")
    private String type;

    /**
     * @return the accountNumber
     */
    @Size(min = 11, max = 18)
    @Override
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @return the tmpAccountNumber
     */
    public String getTmpAccountNumber() {
        return tmpAccountNumber;
    }

    /**
     * @param tmpAccountNumber the tmpAccountNumber to set
     */
    public void setTmpAccountNumber(String tmpAccountNumber) {
        this.tmpAccountNumber = tmpAccountNumber;
    }

    /**
     * @return the accountIdentity
     */
    @Override
    public String getAccountIdentity() {
        if(getId() !=null) {
        accountIdentity = "Pocket Moni/" + accountName + "/" + accountNumber;
        }
        return accountIdentity;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    
          /**
     * @return the bankName
     */
        @Override
    public String getBankName() {
        if(getId()!=null) {
            return "POCKET MONI";
        }
        return null;
    }

}
