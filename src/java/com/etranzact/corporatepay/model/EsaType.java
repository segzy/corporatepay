package com.etranzact.corporatepay.model;

/**
 * Created by Emmanuel.Kpoudosu on 9/1/2014.
 */
public enum EsaType {
    REQUEST_CODE("Request Code"), VALIDATE_CODE("Validate Code"), REGISTER_USER("User Registration");

    private String esaType;

    private EsaType(String s) {
        esaType = s;
    }

    public String getEsaType() {
        return esaType;
    }
}
