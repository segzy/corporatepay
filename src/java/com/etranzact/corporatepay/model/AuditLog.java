/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_AUDIT_LOG")
public class AuditLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "ID")
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "CREATED")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated = new Date();
    
    private String dateCreatedStr;

    @Column(name = "LOGGED_USER")
    private String username;

    @Column(name = "ORGANIZATION")
    private String organization;

    @Column(name = "ROLE")
    private String userRole;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "TARGET")
    private String target;

    @Column(name = "AUDIT_TYPE")
    private String auditType;
    
       @Column(name = "BANK_CODE")
    private String bankCode;

              @Column(name = "CORPORATE_ID")
    private Long corporateId;
              
                 @Column(name = "CLIENT_IP")
    private String clientIP;
              
        @OneToMany(fetch = FetchType.EAGER)
    private List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuditLog)) {
            return false;
        }
        AuditLog other = (AuditLog) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.etranzact.model.AuditLog[ id=" + getId() + " ]";
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the organization
     */
    public String getOrganization() {
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    /**
     * @return the userRole
     */
    public String getUserRole() {
        return userRole;
    }

    /**
     * @param userRole the userRole to set
     */
    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return the auditType
     */
    public String getAuditType() {
        return auditType;
    }

    /**
     * @param auditType the auditType to set
     */
    public void setAuditType(String auditType) {
        this.auditType = auditType;
    }

    /**
     * @return the nameValuePairs
     */
    public List<TaskObjectNameValuePair> getNameValuePairs() {
        return nameValuePairs;
    }

    /**
     * @param nameValuePairs the nameValuePairs to set
     */
    public void setNameValuePairs(List<TaskObjectNameValuePair> nameValuePairs) {
        this.nameValuePairs = nameValuePairs;
    }

    /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the corporateId
     */
    public Long getCorporateId() {
        return corporateId;
    }

    /**
     * @param corporateId the corporateId to set
     */
    public void setCorporateId(Long corporateId) {
        this.corporateId = corporateId;
    }

    /**
     * @return the dateCreatedStr
     */
    public String getDateCreatedStr() {
        dateCreatedStr = AppUtil.formatDate(dateCreated, "EEE, d MMM yyyy HH:mm:ss");
        return dateCreatedStr;
    }

    /**
     * @return the clientIP
     */
    public String getClientIP() {
        return clientIP;
    }

    /**
     * @param clientIP the clientIP to set
     */
    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

   


  
    
}
