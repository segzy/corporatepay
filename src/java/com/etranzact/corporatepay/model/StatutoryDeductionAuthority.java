/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name="COP_STATUTORY_DEDUCTION_AUTHORITY")
@Inheritance(strategy= InheritanceType.JOINED)
//@Audited
public class StatutoryDeductionAuthority implements Serializable {
             @Id
    @Column(name="AUTH_CODE")
           @Size(min = 3, max = 6)
    private String authCode;
              @Column(name="AUTH_NAME")
    private String authName;
                      @Column(name="TMP_AUTH_NAME")
    private String tmpAuthName;
             @ManyToOne
        @JoinColumn(name = "BANK_ACCOUNT_ID")
    private BankAccount defaultAccount;
           @Column(name="CONTACT_EMAIL")
           @Email
    private String contactEmail;
    @ManyToOne
    @JoinColumn(name = "USER_ID")
      private User user;
           @ManyToOne
        @JoinColumn(name = "TMP_ACCOUNT_ID")
    private BankAccount tmpContactAccount;
           @Column(name="TMP_PFA_EMAIL")
    private String tmpContactEmail;
    @ManyToOne
    @JoinColumn(name = "TMP_USER_ID")
      private User tmpUser;
          @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
      @Column(name = "FLAG_STATUS")
    private String flagStatus;
         @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate = new Corporate();
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
      
          @Transient
    private String status;

    /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }


    /**
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * @param authCode the authCode to set
     */
    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    /**
     * @return the defaultAccount
     */
    public BankAccount getDefaultAccount() {
        return defaultAccount;
    }

    /**
     * @param defaultAccount the defaultAccount to set
     */
    public void setDefaultAccount(BankAccount defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    /**
     * @return the contactEmail
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * @param contactEmail the contactEmail to set
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the tmpContactAccount
     */
    public BankAccount getTmpContactAccount() {
        return tmpContactAccount;
    }

    /**
     * @param tmpContactAccount the tmpContactAccount to set
     */
    public void setTmpContactAccount(BankAccount tmpContactAccount) {
        this.tmpContactAccount = tmpContactAccount;
    }

    /**
     * @return the tmpContactEmail
     */
    public String getTmpContactEmail() {
        return tmpContactEmail;
    }

    /**
     * @param tmpContactEmail the tmpContactEmail to set
     */
    public void setTmpContactEmail(String tmpContactEmail) {
        this.tmpContactEmail = tmpContactEmail;
    }

    /**
     * @return the tmpUser
     */
    public User getTmpUser() {
        return tmpUser;
    }

    /**
     * @param tmpUser the tmpUser to set
     */
    public void setTmpUser(User tmpUser) {
        this.tmpUser = tmpUser;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the authName
     */
    public String getAuthName() {
        return authName;
    }

    /**
     * @param authName the authName to set
     */
    public void setAuthName(String authName) {
        this.authName = authName;
    }

    /**
     * @return the tmpAuthName
     */
    public String getTmpAuthName() {
        return tmpAuthName;
    }

    /**
     * @param tmpAuthName the tmpAuthName to set
     */
    public void setTmpAuthName(String tmpAuthName) {
        this.tmpAuthName = tmpAuthName;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.authCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StatutoryDeductionAuthority other = (StatutoryDeductionAuthority) obj;
        if (!Objects.equals(this.authCode, other.authCode)) {
            return false;
        }
        return true;
    }



   
    
}
