/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Digits;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_CORPORATE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"CORPORATE_NAME", "BANK_ID"})})
//@Audited
public class Corporate implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Column(name = "CORPORATE_NAME")
    private String corporateName;
    @Column(name = "TMP_CORPORATE_NAME")
    private String tmpCorporateName;
    @ManyToOne
    @JoinColumn(name = "BANK_ID")
    private Bank bank = new Bank();
    @ManyToOne
    @JoinColumn(name = "TMP_BANK_ID")
    private Bank tmpBank;
    @Email
    @Column(name = "CORPORATE_EMAIL")
    private String contactEmail;
    @Column(name = "TMP_CORPORATE_EMAIL")
    private String tmpContactEmail;
    @Column(name = "CORPORATE_STREET")
    private String contactStreet;
    @Column(name = "TMP_CORPORATE_STREET")
    private String tmpContactStreet;
    @Column(name = "CORPORATE_TOWN")
    private String contactTown;
    @Column(name = "TMP_CORPORATE_TOWN")
    private String tmpContactTown;
    @Column(name = "CORPORATE_STATE")
    private String contactState;
    @Column(name = "TMP_CORPORATE_STATE")
    private String tmpContactState;
//    @Digits(integer = 20, fraction = 0, message = "Mobile Phone must be digits and cannot be more than 20")
    @Column(name = "CORPORATE_PHONE")
    private String contactPhone;
    @Column(name = "TMP_CORPORATE_PHONE")
    private String tmpContactPhone;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @ManyToOne
    @JoinColumn(name = "GRAD_COMM")
    private Commission gradBankCommission;
        @ManyToOne
    @JoinColumn(name = "TMP_GRAD_COMM")
    private Commission tmpGradBankCommission;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_CORPORATE_CORPORATEPAY_SERVICE", joinColumns
            = @JoinColumn(name = "CORPORATE_ID"), inverseJoinColumns
            = @JoinColumn(name = "SERVICE_ID"))
    private List<CorporatePayService> corporatePayServices = new ArrayList<>();
    //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_TMP_CORPORATE_CORPORATEPAY_SERVICE", joinColumns
            = @JoinColumn(name = "CORPORATE_ID"), inverseJoinColumns
            = @JoinColumn(name = "SERVICE_ID"))
    private List<CorporatePayService> tmpCorporatePayServices = new ArrayList<>();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Column(name = "APPLY_COMMISSION")
    private Boolean applyCommission = Boolean.TRUE;
    @Column(name = "DISALLOW_OTHERACCOUNT")
    private Boolean disAllowOtherAccount = Boolean.TRUE;
    @Column(name = "ETZ_COMMISSION")
    private BigDecimal etzCommission;
    @Column(name = "BANK_COMMISSION_TYPE")
    private String bankCommissionType;
    @Column(name = "BANK_COMMISSION")
    private BigDecimal bankCommission;
    @Column(name = "BANK_COMMISSION_ACCOUNT")
    private String bankCommissionAccount;
    @Column(name = "THIRDPARTY_COMMISSION_TYPE")
    private String thirdPartyCommissionType;
    @Column(name = "THIRDPARTY_BANK_COMMISSION")
    private BigDecimal thirdPartyCommission;
    @Column(name = "THIRDPARTY_COMMISSION_ACCOUNT")
    private String thirdPartyCommissionAccount;
    @Column(name = "THIRDPARTY_COMMISSION_TYPE2")
    private String thirdPartyCommissionType2;
    @Column(name = "THIRDPARTY_BANK_COMMISSION2")
    private BigDecimal thirdPartyCommission2;
    @Column(name = "THIRDPARTY_COMMISSION_ACCOUNT2")
    private String thirdPartyCommissionAccount2;
    @Column(name = "ALLOW_FINAL_AUTH_CREATION")
    private Boolean allowFinalAuthCreation = Boolean.TRUE;
    @Column(name = "TMP_ETZ_COMMISSION")
    private BigDecimal tmpEtzCommission;
    @Column(name = "TMP_BANK_COMMISSION_TYPE")
    private String tmpBankCommissionType;
    @Column(name = "TMP_BANK_COMMISSION")
    private BigDecimal tmpBankCommission;
    @Column(name = "TMP_BANK_COMMISSION_ACCOUNT")
    private String tmpBankCommissionAccount;
    @Column(name = "TMP_THIRDPARTY_COMMISSION_TYPE")
    private String tmpThirdPartyCommissionType;
    @Column(name = "TMP_THIRDPARTY_BANK_COMMISSION")
    private BigDecimal tmpThirdPartyCommission;
    @Column(name = "TMP_THIRDPARTY_COMMISSION_ACCOUNT")
    private String tmpThirdPartyCommissionAccount;
    @Column(name = "TMP_THIRDPARTY_COMMISSION_TYPE2")
    private String tmpThirdPartyCommissionType2;
    @Column(name = "TMP_THIRDPARTY_BANK_COMMISSION2")
    private BigDecimal tmpThirdPartyCommission2;
    @Column(name = "TMP_THIRDPARTY_COMMISSION_ACCOUNT2")
    private String tmpThirdPartyCommissionAccount2;
    @Column(name = "TMP_ALLOW_FINAL_AUTH_CREATION")
    private Boolean tmpAllowFinalAuthCreation;
    @Column(name = "APPROVED_COMMISSION")
    private Boolean approveCommission = Boolean.TRUE;
    @Column(name = "ENABLE_RSA")
    private Boolean enableESA = Boolean.FALSE;
    @Column(name = "TMP_ENABLE_RSA")
    private Boolean tmpEnableESA;
    @Column(name = "COMMISSION_TYPE", length = 1)
    private String commissionType;
    @Column(name = "TMP_COMMISSION_TYPE", length = 1)
    private String tmpCommissionType;
    @Transient
    private String etzCommissionStr;
    @Transient
    private String bankCommissionStr;
    @Transient
    private String thirdPartyCommission1Str;
    @Transient
    private String thirdPartyCommission2Str;

    @Lob
    private byte[] logo;
    @Lob
    private byte[] tmpLogo;

    @Transient
    private String status;

    /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactStreet() {
        return contactStreet;
    }

    public void setContactStreet(String contactStreet) {
        this.contactStreet = contactStreet;
    }

    public String getContactTown() {
        return contactTown;
    }

    public void setContactTown(String contactTown) {
        this.contactTown = contactTown;
    }

    public String getContactState() {
        return contactState;
    }

    public void setContactState(String contactState) {
        this.contactState = contactState;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public byte[] getLogo() {
        return logo;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporatePayServices
     */
    public List<CorporatePayService> getCorporatePayServices() {
        return corporatePayServices;
    }

    /**
     * @param corporatePayServices the corporatePayServices to set
     */
    public void setCorporatePayServices(List<CorporatePayService> corporatePayServices) {
        this.corporatePayServices = corporatePayServices;
    }

    /**
     * @return the tmpCorporatePayServices
     */
    public List<CorporatePayService> getTmpCorporatePayServices() {
        return tmpCorporatePayServices;
    }

    /**
     * @param tmpCorporatePayServices the tmpCorporatePayServices to set
     */
    public void setTmpCorporatePayServices(List<CorporatePayService> tmpCorporatePayServices) {
        this.tmpCorporatePayServices = tmpCorporatePayServices;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Corporate other = (Corporate) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return corporateName;
    }

    /**
     * @return the tmpCorporateName
     */
    public String getTmpCorporateName() {
        return tmpCorporateName;
    }

    /**
     * @param tmpCorporateName the tmpCorporateName to set
     */
    public void setTmpCorporateName(String tmpCorporateName) {
        this.tmpCorporateName = tmpCorporateName;
    }

    /**
     * @return the tmpBank
     */
    public Bank getTmpBank() {
        return tmpBank;
    }

    /**
     * @param tmpBank the tmpBank to set
     */
    public void setTmpBank(Bank tmpBank) {
        this.tmpBank = tmpBank;
    }

    /**
     * @return the tmpContactEmail
     */
    public String getTmpContactEmail() {
        return tmpContactEmail;
    }

    /**
     * @param tmpContactEmail the tmpContactEmail to set
     */
    public void setTmpContactEmail(String tmpContactEmail) {
        this.tmpContactEmail = tmpContactEmail;
    }

    /**
     * @return the tmpContactStreet
     */
    public String getTmpContactStreet() {
        return tmpContactStreet;
    }

    /**
     * @param tmpContactStreet the tmpContactStreet to set
     */
    public void setTmpContactStreet(String tmpContactStreet) {
        this.tmpContactStreet = tmpContactStreet;
    }

    /**
     * @return the tmpContactTown
     */
    public String getTmpContactTown() {
        return tmpContactTown;
    }

    /**
     * @param tmpContactTown the tmpContactTown to set
     */
    public void setTmpContactTown(String tmpContactTown) {
        this.tmpContactTown = tmpContactTown;
    }

    /**
     * @return the tmpContactState
     */
    public String getTmpContactState() {
        return tmpContactState;
    }

    /**
     * @param tmpContactState the tmpContactState to set
     */
    public void setTmpContactState(String tmpContactState) {
        this.tmpContactState = tmpContactState;
    }

    /**
     * @return the tmpContactPhone
     */
    public String getTmpContactPhone() {
        return tmpContactPhone;
    }

    /**
     * @param tmpContactPhone the tmpContactPhone to set
     */
    public void setTmpContactPhone(String tmpContactPhone) {
        this.tmpContactPhone = tmpContactPhone;
    }

    /**
     * @return the tmpLogo
     */
    public byte[] getTmpLogo() {
        return tmpLogo;
    }

    /**
     * @param tmpLogo the tmpLogo to set
     */
    public void setTmpLogo(byte[] tmpLogo) {
        this.tmpLogo = tmpLogo;
    }

    /**
     * @return the applyCommission
     */
    public Boolean getApplyCommission() {
        applyCommission = applyCommission == null ? Boolean.TRUE : applyCommission;
        return applyCommission;
    }

    /**
     * @param applyCommission the applyCommission to set
     */
    public void setApplyCommission(Boolean applyCommission) {
        this.applyCommission = applyCommission;
    }

    /**
     * @return the disAllowOtherAccount
     */
    public Boolean getDisAllowOtherAccount() {
        disAllowOtherAccount = disAllowOtherAccount == null ? Boolean.TRUE : disAllowOtherAccount;
        return disAllowOtherAccount;
    }

    /**
     * @param disAllowOtherAccount the disAllowOtherAccount to set
     */
    public void setDisAllowOtherAccount(Boolean disAllowOtherAccount) {
        this.disAllowOtherAccount = disAllowOtherAccount;
    }

    /**
     * @return the etzCommission
     */
    public BigDecimal getEtzCommission() {
        return etzCommission;
    }

    /**
     * @param etzCommission the etzCommission to set
     */
    public void setEtzCommission(BigDecimal etzCommission) {
        this.etzCommission = etzCommission;
    }

    /**
     * @return the bankCommissionType
     */
    public String getBankCommissionType() {
        return bankCommissionType;
    }

    /**
     * @param bankCommissionType the bankCommissionType to set
     */
    public void setBankCommissionType(String bankCommissionType) {
        this.bankCommissionType = bankCommissionType;
    }

    /**
     * @return the bankCommission
     */
    public BigDecimal getBankCommission() {
        return bankCommission;
    }

    /**
     * @param bankCommission the bankCommission to set
     */
    public void setBankCommission(BigDecimal bankCommission) {
        this.bankCommission = bankCommission;
    }

    /**
     * @return the bankCommissionAccount
     */
    public String getBankCommissionAccount() {
        return bankCommissionAccount;
    }

    /**
     * @param bankCommissionAccount the bankCommissionAccount to set
     */
    public void setBankCommissionAccount(String bankCommissionAccount) {
        this.bankCommissionAccount = bankCommissionAccount;
    }

    /**
     * @return the thirdPartyCommissionType
     */
    public String getThirdPartyCommissionType() {
        return thirdPartyCommissionType;
    }

    /**
     * @param thirdPartyCommissionType the thirdPartyCommissionType to set
     */
    public void setThirdPartyCommissionType(String thirdPartyCommissionType) {
        this.thirdPartyCommissionType = thirdPartyCommissionType;
    }

    /**
     * @return the thirdPartyCommission
     */
    public BigDecimal getThirdPartyCommission() {
        return thirdPartyCommission;
    }

    /**
     * @param thirdPartyCommission the thirdPartyCommission to set
     */
    public void setThirdPartyCommission(BigDecimal thirdPartyCommission) {
        this.thirdPartyCommission = thirdPartyCommission;
    }

    /**
     * @return the thirdPartyCommissionAccount
     */
    public String getThirdPartyCommissionAccount() {
        return thirdPartyCommissionAccount;
    }

    /**
     * @param thirdPartyCommissionAccount the thirdPartyCommissionAccount to set
     */
    public void setThirdPartyCommissionAccount(String thirdPartyCommissionAccount) {
        this.thirdPartyCommissionAccount = thirdPartyCommissionAccount;
    }

    /**
     * @return the thirdPartyCommission2
     */
    public BigDecimal getThirdPartyCommission2() {
        return thirdPartyCommission2;
    }

    /**
     * @param thirdPartyCommission2 the thirdPartyCommission2 to set
     */
    public void setThirdPartyCommission2(BigDecimal thirdPartyCommission2) {
        this.thirdPartyCommission2 = thirdPartyCommission2;
    }

    /**
     * @return the thirdPartyCommissionAccount2
     */
    public String getThirdPartyCommissionAccount2() {
        return thirdPartyCommissionAccount2;
    }

    /**
     * @param thirdPartyCommissionAccount2 the thirdPartyCommissionAccount2 to
     * set
     */
    public void setThirdPartyCommissionAccount2(String thirdPartyCommissionAccount2) {
        this.thirdPartyCommissionAccount2 = thirdPartyCommissionAccount2;
    }

    /**
     * @return the thirdPartyCommissionType2
     */
    public String getThirdPartyCommissionType2() {
        return thirdPartyCommissionType2;
    }

    /**
     * @param thirdPartyCommissionType2 the thirdPartyCommissionType2 to set
     */
    public void setThirdPartyCommissionType2(String thirdPartyCommissionType2) {
        this.thirdPartyCommissionType2 = thirdPartyCommissionType2;
    }

    /**
     * @return the allowFinalAuthCreation
     */
    public Boolean getAllowFinalAuthCreation() {
        return allowFinalAuthCreation;
    }

    /**
     * @param allowFinalAuthCreation the allowFinalAuthCreation to set
     */
    public void setAllowFinalAuthCreation(Boolean allowFinalAuthCreation) {
        this.allowFinalAuthCreation = allowFinalAuthCreation;
    }

    /**
     * @return the tmpEtzCommission
     */
    public BigDecimal getTmpEtzCommission() {
        return tmpEtzCommission;
    }

    /**
     * @param tmpEtzCommission the tmpEtzCommission to set
     */
    public void setTmpEtzCommission(BigDecimal tmpEtzCommission) {
        this.tmpEtzCommission = tmpEtzCommission;
    }

    /**
     * @return the tmpBankCommissionType
     */
    public String getTmpBankCommissionType() {
        return tmpBankCommissionType;
    }

    /**
     * @param tmpBankCommissionType the tmpBankCommissionType to set
     */
    public void setTmpBankCommissionType(String tmpBankCommissionType) {
        this.tmpBankCommissionType = tmpBankCommissionType;
    }

    /**
     * @return the tmpBankCommission
     */
    public BigDecimal getTmpBankCommission() {
        return tmpBankCommission;
    }

    /**
     * @param tmpBankCommission the tmpBankCommission to set
     */
    public void setTmpBankCommission(BigDecimal tmpBankCommission) {
        this.tmpBankCommission = tmpBankCommission;
    }

    /**
     * @return the tmpBankCommissionAccount
     */
    public String getTmpBankCommissionAccount() {
        return tmpBankCommissionAccount;
    }

    /**
     * @param tmpBankCommissionAccount the tmpBankCommissionAccount to set
     */
    public void setTmpBankCommissionAccount(String tmpBankCommissionAccount) {
        this.tmpBankCommissionAccount = tmpBankCommissionAccount;
    }

    /**
     * @return the tmpThirdPartyCommissionType
     */
    public String getTmpThirdPartyCommissionType() {
        return tmpThirdPartyCommissionType;
    }

    /**
     * @param tmpThirdPartyCommissionType the tmpThirdPartyCommissionType to set
     */
    public void setTmpThirdPartyCommissionType(String tmpThirdPartyCommissionType) {
        this.tmpThirdPartyCommissionType = tmpThirdPartyCommissionType;
    }

    /**
     * @return the tmpThirdPartyCommission
     */
    public BigDecimal getTmpThirdPartyCommission() {
        return tmpThirdPartyCommission;
    }

    /**
     * @param tmpThirdPartyCommission the tmpThirdPartyCommission to set
     */
    public void setTmpThirdPartyCommission(BigDecimal tmpThirdPartyCommission) {
        this.tmpThirdPartyCommission = tmpThirdPartyCommission;
    }

    /**
     * @return the tmpThirdPartyCommissionAccount
     */
    public String getTmpThirdPartyCommissionAccount() {
        return tmpThirdPartyCommissionAccount;
    }

    /**
     * @param tmpThirdPartyCommissionAccount the tmpThirdPartyCommissionAccount
     * to set
     */
    public void setTmpThirdPartyCommissionAccount(String tmpThirdPartyCommissionAccount) {
        this.tmpThirdPartyCommissionAccount = tmpThirdPartyCommissionAccount;
    }

    /**
     * @return the tmpThirdPartyCommissionType2
     */
    public String getTmpThirdPartyCommissionType2() {
        return tmpThirdPartyCommissionType2;
    }

    /**
     * @param tmpThirdPartyCommissionType2 the tmpThirdPartyCommissionType2 to
     * set
     */
    public void setTmpThirdPartyCommissionType2(String tmpThirdPartyCommissionType2) {
        this.tmpThirdPartyCommissionType2 = tmpThirdPartyCommissionType2;
    }

    /**
     * @return the tmpThirdPartyCommission2
     */
    public BigDecimal getTmpThirdPartyCommission2() {
        return tmpThirdPartyCommission2;
    }

    /**
     * @param tmpThirdPartyCommission2 the tmpThirdPartyCommission2 to set
     */
    public void setTmpThirdPartyCommission2(BigDecimal tmpThirdPartyCommission2) {
        this.tmpThirdPartyCommission2 = tmpThirdPartyCommission2;
    }

    /**
     * @return the tmpThirdPartyCommissionAccount2
     */
    public String getTmpThirdPartyCommissionAccount2() {
        return tmpThirdPartyCommissionAccount2;
    }

    /**
     * @param tmpThirdPartyCommissionAccount2 the
     * tmpThirdPartyCommissionAccount2 to set
     */
    public void setTmpThirdPartyCommissionAccount2(String tmpThirdPartyCommissionAccount2) {
        this.tmpThirdPartyCommissionAccount2 = tmpThirdPartyCommissionAccount2;
    }

    /**
     * @return the tmpAllowFinalAuthCreation
     */
    public Boolean getTmpAllowFinalAuthCreation() {
        return tmpAllowFinalAuthCreation;
    }

    /**
     * @param tmpAllowFinalAuthCreation the tmpAllowFinalAuthCreation to set
     */
    public void setTmpAllowFinalAuthCreation(Boolean tmpAllowFinalAuthCreation) {
        this.tmpAllowFinalAuthCreation = tmpAllowFinalAuthCreation;
    }

    /**
     * @return the approveCommission
     */
    public Boolean getApproveCommission() {
        if (!getApplyCommission()) {
            return Boolean.TRUE;
        }
        if (approveCommission == null) {
            if (bank != null) {
                approveCommission = etzCommission.compareTo(bank.getEtzFee()) >= 0;
            }
        }
        return approveCommission;
    }

    /**
     * @param approveCommission the approveCommission to set
     */
    public void setApproveCommission(Boolean approveCommission) {
        this.approveCommission = approveCommission;
    }

    /**
     * @return the etzCommissionStr
     */
    public String getEtzCommissionStr() {
        if (etzCommission != null) {
            etzCommissionStr = "Flat " + AppUtil.formatAsAmount(etzCommission.toString());
        }
        return etzCommissionStr;
    }

    /**
     * @return the bankCommissionStr
     */
    public String getBankCommissionStr() {
        if (bankCommission != null && bankCommissionType != null) {
            bankCommissionStr = (AppConstants.AMOUNT_TYPE_FIXED.equals(bankCommissionType) ? "Flat " : " Percent") + AppUtil.formatAsAmount(bankCommission.toString());
        }
        return bankCommissionStr;
    }

    /**
     * @return the thirdPartyCommission1Str
     */
    public String getThirdPartyCommission1Str() {
        if (thirdPartyCommission != null && thirdPartyCommissionType != null) {
            thirdPartyCommission1Str = (AppConstants.AMOUNT_TYPE_FIXED.equals(thirdPartyCommissionType) ? "Flat " : " Percent") + AppUtil.formatAsAmount(thirdPartyCommission.toString());
        }
        return thirdPartyCommission1Str;
    }

    /**
     * @return the thirdPartyCommission2Str
     */
    public String getThirdPartyCommission2Str() {
        if (thirdPartyCommission2 != null && thirdPartyCommissionType2 != null) {
            thirdPartyCommission2Str = (AppConstants.AMOUNT_TYPE_FIXED.equals(thirdPartyCommissionType2) ? "Flat " : " Percent") + AppUtil.formatAsAmount(thirdPartyCommission2.toString());
        }
        return thirdPartyCommission2Str;
    }

    /**
     * @return the enableESA
     */
    public Boolean getEnableESA() {
        return enableESA == null ? Boolean.FALSE : enableESA;
    }

    /**
     * @param enableESA the enableESA to set
     */
    public void setEnableESA(Boolean enableESA) {
        this.enableESA = enableESA;
    }

    /**
     * @return the tmpEnableESA
     */
    public Boolean getTmpEnableESA() {
        return tmpEnableESA;
    }

    /**
     * @param tmpEnableESA the tmpEnableESA to set
     */
    public void setTmpEnableESA(Boolean tmpEnableESA) {
        this.tmpEnableESA = tmpEnableESA;
    }

    /**
     * @return the commissionType
     */
    public String getCommissionType() {
        return commissionType == null ? "0" : commissionType;
    }

    /**
     * @param commissionType the commissionType to set
     */
    public void setCommissionType(String commissionType) {
        this.commissionType = commissionType;
    }

    /**
     * @return the tmpCommissionType
     */
    public String getTmpCommissionType() {
        return tmpCommissionType;
    }

    /**
     * @param tmpCommissionType the tmpCommissionType to set
     */
    public void setTmpCommissionType(String tmpCommissionType) {
        this.tmpCommissionType = tmpCommissionType;
    }

    /**
     * @return the gradBankCommission
     */
    public Commission getGradBankCommission() {
        return gradBankCommission;
    }

    /**
     * @param gradBankCommission the gradBankCommission to set
     */
    public void setGradBankCommission(Commission gradBankCommission) {
        this.gradBankCommission = gradBankCommission;
    }

    /**
     * @return the tmpGradBankCommission
     */
    public Commission getTmpGradBankCommission() {
        return tmpGradBankCommission;
    }

    /**
     * @param tmpGradBankCommission the tmpGradBankCommission to set
     */
    public void setTmpGradBankCommission(Commission tmpGradBankCommission) {
        this.tmpGradBankCommission = tmpGradBankCommission;
    }
    
    

}
