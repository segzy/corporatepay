/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_SETTLEMENT", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"BATCH_ID", "DEST_ISSUER_CODE", "TYPE"})})
//@Audited
public class Settlement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "SETTLEMENT_GEN")
    @GenericGenerator(name = "SETTLEMENT_GEN", strategy = "enhanced-table", parameters = {
        @org.hibernate.annotations.Parameter(name = "table_name", value = "hibernate_sequence_settlement")
    })
    @Column(name = "ID")
    private Long Id;
    @Column(name = "S_BATCH_ID", nullable = false)
    private String sbatchId;
    @Column(name = "BATCH_ID", nullable = false)
    private String batchId;
    @Column(name = "DEST_ISSUER_CODE", nullable = false)
    private String destIssuerCode;
    @Column(name = "SOURCE_ISSUER_CODE", nullable = false)
    private String sourceIssuerCode;
    @Column(name = "TYPE", nullable = false)
    private String settlementType;
    @Column(name = "DEBIT", nullable = false)
    private String debit;
    @Column(name = "CREDIT", nullable = false)
    private String credit;
    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Column(name = "TRANS_COUNT", nullable = true)
    private Integer transCount;
    @Column(name = "NARRATION", nullable = false)
    private String narration;
    @Column(name = "TRANS_CODE", nullable = false)
    private String transCode = "N";
    @Column(name = "UNIQUE_TRANS_ID", nullable = false)
    private String uniqueTransId;
    @ManyToOne
    @JoinColumn(name = "CORPORATE_ID")
    private Corporate corporate;
    @Column(name = "D_ISSUER_CODE", nullable = false)
    private String dIssuerCode;
    @Transient
    private String amountf;
    @Transient
    private String owner;
    @Transient
     private String bank;
          @Transient
     private String destBank;
           @Transient
             private String dateCreatedStr;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.batchId);
        hash = 47 * hash + Objects.hashCode(this.destIssuerCode);
        hash = 47 * hash + Objects.hashCode(this.settlementType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Settlement other = (Settlement) obj;
        if (!Objects.equals(this.batchId, other.batchId)) {
            return false;
        }
        if (!Objects.equals(this.destIssuerCode, other.destIssuerCode)) {
            return false;
        }
        if (!Objects.equals(this.settlementType, other.settlementType)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Settlement{" + "sbatchId=" + sbatchId + ", batchId=" + batchId + ", issuerCode=" + destIssuerCode + ", settlementType=" + settlementType + ", debit=" + debit + ", credit=" + credit + ", amount=" + amount + ", dateCreated=" + dateCreated + '}';
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the sbatchId
     */
    public String getSbatchId() {
        return sbatchId;
    }

    /**
     * @param sbatchId the sbatchId to set
     */
    public void setSbatchId(String sbatchId) {
        this.sbatchId = sbatchId;
    }

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the issuerCode
     */
    public String getDestIssuerCode() {
        return destIssuerCode;
    }

    /**
     * @param issuerCode the issuerCode to set
     */
    public void setDestIssuerCode(String issuerCode) {
        this.destIssuerCode = issuerCode;
    }

    /**
     * @return the settlementType
     */
    public String getSettlementType() {
        return settlementType;
    }

    /**
     * @param settlementType the settlementType to set
     */
    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    /**
     * @return the debit
     */
    public String getDebit() {
        return debit;
    }

    /**
     * @param debit the debit to set
     */
    public void setDebit(String debit) {
        this.debit = debit;
    }

    /**
     * @return the credit
     */
    public String getCredit() {
        return credit;
    }

    /**
     * @param credit the credit to set
     */
    public void setCredit(String credit) {
        this.credit = credit;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the sourceIssuerCode
     */
    public String getSourceIssuerCode() {
        return sourceIssuerCode;
    }

    /**
     * @param sourceIssuerCode the sourceIssuerCode to set
     */
    public void setSourceIssuerCode(String sourceIssuerCode) {
        this.sourceIssuerCode = sourceIssuerCode;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the transCount
     */
    public Integer getTransCount() {
        return transCount;
    }

    /**
     * @param transCount the transCount to set
     */
    public void setTransCount(Integer transCount) {
        this.transCount = transCount;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the transCode
     */
    public String getTransCode() {
        return transCode;
    }

    /**
     * @param transCode the transCode to set
     */
    public void setTransCode(String transCode) {
        this.transCode = transCode;
    }

    /**
     * @return the uniqueTransId
     */
    public String getUniqueTransId() {
        return uniqueTransId;
    }

    /**
     * @param uniqueTransId the uniqueTransId to set
     */
    public void setUniqueTransId(String uniqueTransId) {
        this.uniqueTransId = uniqueTransId;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the dIssuerCode
     */
    public String getdIssuerCode() {
        return dIssuerCode;
    }

    /**
     * @param dIssuerCode the dIssuerCode to set
     */
    public void setdIssuerCode(String dIssuerCode) {
        this.dIssuerCode = dIssuerCode;
    }

    /**
     * @return the amountf
     */
    public String getAmountf() {
        amountf = ConfigurationUtil.formatAsAmount(amount.toString());
        return amountf;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        if (settlementType.equals(AppConstants.ETZ_COMMISION_SETTLEMENT)) {
            owner = AppConstants.ETZ_COMMISSION_TYPE;
        } else if (settlementType.equals(AppConstants.BANK_COMMISION_SETTLEMENT)) {
            owner = AppConstants.BANK_COMMISSION_TYPE;
        } else if (settlementType.equals(AppConstants.THIRDPARTY_COMMISION_SETTLEMENT)) {
            owner = AppConstants.THIRDPARTY_COMMISSION_TYPE;
        }else if (settlementType.equals(AppConstants.THIRDPARTY_COMMISION_SETTLEMENT2)) {
            owner = AppConstants.THIRDPARTY_COMMISSION_TYPE2;
        }
        return owner;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        bank=corporate==null?"":corporate.getBank().getBankName();
        return bank;
    }

    /**
     * @return the destBank
     */
    public String getDestBank() {
        return destBank;
    }

    /**
     * @param destBank the destBank to set
     */
    public void setDestBank(String destBank) {
        this.destBank = destBank;
    }

    /**
     * @return the dateCreatedStr
     */
    public String getDateCreatedStr() {
        dateCreatedStr = AppUtil.formatDate(dateCreated, "EEE, d MMM yyyy HH:mm:ss");
        return dateCreatedStr;
    }

    
    
}
