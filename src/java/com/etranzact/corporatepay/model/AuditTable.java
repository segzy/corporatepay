/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.listener.AuditTableListener;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.ModifiedEntityNames;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "AUDIT_REVISION_LOG")
//@RevisionEntity(AuditTableListener.class)
public class AuditTable implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "ID")
    @Id
    @GeneratedValue
    @RevisionNumber
    private Long id;

    @Column(name = "REVISION_TIMESTAMP")
    @RevisionTimestamp
    private Date timestamp;

    @Transient
    private String timeStampStr;

    @Column(name = "USER_ID")
    private Long username;

    @JoinColumn(name = "USER")
    @ManyToOne
    private User user;

    @Column(name = "USER_TYPE")
    private String userType;

    @Column(name = "USER_ORG")
    private String userOrg;

//    @Transient
//    private String entityName;
    @Column(name = "ENTITY_ID")
    private String entityId;

    @Column(name = "ENTITY_NAME", length = 550)
    private String entityName;

    @Transient
    private String entityFocus;

    @Column(name = "REVISION_TYPE")
    private String revisionType;
    
    
    @Column(name = "IDENTITY_DATATYPE")
    private String identityDataType;

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "country", column = @Column(name = "AUDIT_COUNTRY")),
        @AttributeOverride(name = "ip", column = @Column(name = "AUDIT_IP")),
        @AttributeOverride(name = "city", column = @Column(name = "AUDIT_CITY")),
        @AttributeOverride(name = "latitude", column = @Column(name = "AUDIT_LATITUDE")),
        @AttributeOverride(name = "longitude", column = @Column(name = "AUDIT_LONGITUDE"))
    })
    private Location location;

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "REVCHANGES", joinColumns = @JoinColumn(name = "REV"))
    @Column(name = "ENTITYNAME")
    @ModifiedEntityNames
    private Set<String> modifiedEntityNames;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Long getUsername() {
        return username;
    }

    public void setUsername(Long username) {
        this.username = username;
    }

    public Set<String> getModifiedEntityNames() {
        return modifiedEntityNames;
    }

    public void setModifiedEntityNames(Set<String> modifiedEntityNames) {
        this.modifiedEntityNames = modifiedEntityNames;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuditTable)) {
            return false;
        }
        AuditTable other = (AuditTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.etranzact.model.AuditTable[ id=" + id + " ]";
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return the timeStampStr
     */
    public String getTimeStampStr() {
        timeStampStr = AppUtil.formatDate(timestamp == null ? new Date() : timestamp, "EEE, d MMM yyyy HH:mm:ss");
        return timeStampStr;
    }

    /**
     * @return the userOrg
     */
    public String getUserOrg() {
        return userOrg;
    }

    /**
     * @param userOrg the userOrg to set
     */
    public void setUserOrg(String userOrg) {
        this.userOrg = userOrg;
    }

    /**
     * @return the entityName
     */
    public String getEntityName() {
//        if(modifiedEntityNames!=null && !modifiedEntityNames.isEmpty()) {
//            List<String> ls = new ArrayList<>();
//            ls.addAll(modifiedEntityNames);
//            setEntityName(ls.get(0));
//        }
       
//        System.out.println("split:: " + split.length);
        return entityName;
    }

    /**
     * @return the entityFocus
     */
    public String getEntityFocus() {
        return entityFocus;
    }

    /**
     * @param entityFocus the entityFocus to set
     */
    public void setEntityFocus(String entityFocus) {
        this.entityFocus = entityFocus;
    }

    /**
     * @return the revisionType
     */
    public String getRevisionType() {
        return revisionType;
    }

    /**
     * @param revisionType the revisionType to set
     */
    public void setRevisionType(String revisionType) {
        this.revisionType = revisionType;
    }

//    public static void main(String[] args) {
//        String modifiedEntityNames = "com.etranzact.ccjc.BankUser";
//        String[] split = modifiedEntityNames.split("\\.");
//          modifiedEntityNames = split[split.length - 1];
//        System.out.println("split:: " + modifiedEntityNames);
//    }
    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @return the identityDataType
     */
    public String getIdentityDataType() {
        return identityDataType;
    }

    /**
     * @param identityDataType the identityDataType to set
     */
    public void setIdentityDataType(String identityDataType) {
        this.identityDataType = identityDataType;
    }

    
}
