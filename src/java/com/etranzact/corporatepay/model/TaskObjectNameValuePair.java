/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Entity
////@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "COP_TASK_OBJECT")
public class TaskObjectNameValuePair implements Serializable {

    @ManyToOne
    private ApprovalTask approvalTask;

    private String name;
    private String targetValue;
     private String targetTmpValue;

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long Id;

    public TaskObjectNameValuePair() {

    }

    public TaskObjectNameValuePair(String name, String targetValue,String tmpValue) {
        this.name = name;
        this.targetValue = targetValue;
        this.targetTmpValue = tmpValue;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the targetValue
     */
    public String getTargetValue() {
        return targetValue;
    }

    /**
     * @param targetValue the targetValue to set
     */
    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the approvalTask
     */
    public ApprovalTask getApprovalTask() {
        return approvalTask;
    }

    /**
     * @param approvalTask the approvalTask to set
     */
    public void setApprovalTask(ApprovalTask approvalTask) {
        this.approvalTask = approvalTask;
    }

    /**
     * @return the targetTmpValue
     */
    public String getTargetTmpValue() {
        return targetTmpValue;
    }

    /**
     * @param targetTmpValue the targetTmpValue to set
     */
    public void setTargetTmpValue(String targetTmpValue) {
        this.targetTmpValue = targetTmpValue;
    }

//    @Override
//    public int hashCode() {
//        int hash = 7;
//        hash = 19 * hash + Objects.hashCode(this.Id);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final TaskObjectNameValuePair other = (TaskObjectNameValuePair) obj;
//        if (!Objects.equals(this.Id, other.Id)) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "TaskObjectNameValuePair{" + "Id=" + Id + '}';
    }
    
    

}
