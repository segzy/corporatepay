/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import com.etranzact.corporatepay.util.encryption.BasicCrypto;
import com.etranzact.corporatepay.util.encryption.Encrypted;
import com.etz.security.util.Cryptographer;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_USER")
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class User extends Approver implements Serializable {

    @ManyToMany(mappedBy = "users")
    private List<ApprovalGroup> approvalGroups;
    private static final Cryptographer crpt = new Cryptographer();

    @Column(name = "USERNAME", unique = true, nullable = false)
    private String username;
//    @Column(name = "PASSWORD", nullable = false)
    @Transient
    private String password;
    @Column(name = "PASSWORD", nullable = false)
    private String passwordHash;
    @Column(name = "SCRET_KEY", nullable = false)
    private String securityKey;
    @JoinColumn(name = "ROLE_ID")
    @ManyToOne
    private Role role = new Role();
    @JoinColumn(name = "TMP_ROLE_ID")
    @ManyToOne
    private Role tmpRole;
    @Column(name = "FIRSTNAME")
    private String firstName;
    @Column(name = "LASTNAME")
    private String lastName;
    @Column(name = "TMP_FIRSTNAME")
    private String tmpFirstName;
    @Column(name = "TMP_LASTNAME")
    private String tmpLastName;
    @Email(message = "Malformed email address")
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "TMP_EMAIL")
    private String tmpEmail;

    @Column(name = "MOBILE_PHONE", length = 200)
    @Digits(integer = 20, fraction = 0, message = "Mobile Phone must be digits and cannot be more than 20")
    private String mobilePhone;
    @Column(name = "TMP_MOBILE_PHONE", length = 200)
    private String tmpMobilePhone;

//    @Column(name = "CHANGE_PASSWORD")
    @Transient
    private Boolean changePassword = Boolean.FALSE;
    //   @Column(name = "PASS_EXPIRE")
    @Transient
    private Boolean passwordExpired = Boolean.FALSE;

    @Column(name = "PASS_LOCK_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passwordLockDate;

    @Column(name = "PASS_DATE_EXPIRE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passwordDateExpired = new Date();

    @Column(name = "LAST_ACCESS_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastAccessTime = new Date();

    @Transient
    private String lastAccessTimeStr;

    @Transient
    private Date loginTime;

//    @Column(name = "USER_DISABLED")
    @Transient
    private Boolean userDisabled = Boolean.FALSE;
//    @Column(name = "USER_LOCKED")
    @Transient
    private Boolean userLocked = Boolean.FALSE;

    @Column(name = "PASSWORD_MISSED")
    private Integer passwordMissed = 0;

    @Column(name = "DAY_1", length = 1)
    private String monday = AppConstants.TURNED_ON;
    @Transient
    private Boolean tmonday = Boolean.FALSE;
    @Column(name = "DAY_2", length = 1)
    private String tuesday = AppConstants.TURNED_ON;
    @Transient
    private Boolean ttuesday = Boolean.FALSE;
    @Column(name = "DAY_3", length = 1)
    private String wednesday = AppConstants.TURNED_ON;
    @Transient
    private Boolean twednesday = Boolean.FALSE;
    @Column(name = "DAY_4", length = 1)
    private String thursday = AppConstants.TURNED_ON;
    @Transient
    private Boolean tthursday = Boolean.FALSE;
    @Column(name = "DAY_5", length = 1)
    private String friday = AppConstants.TURNED_ON;
    @Transient
    private Boolean tfriday = Boolean.FALSE;
    @Column(name = "DAY_6", length = 1)
    private String saturday = AppConstants.TURNED_ON;
    @Transient
    private Boolean tsaturday = Boolean.FALSE;
    @Column(name = "DAY_7", length = 1)
    private String sunday = AppConstants.TURNED_ON;
    @Transient
    private Boolean tsunday = Boolean.FALSE;

    @Column(name = "TMP_DAY_1", length = 1)
    private String tmpMonday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_2", length = 1)
    private String tmpTuesday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_3", length = 1)
    private String tmpWednesday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_4", length = 1)
    private String tmpThursday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_5", length = 1)
    private String tmpFriday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_6", length = 1)
    private String tmpSaturday = AppConstants.TURNED_OFF;
    @Column(name = "TMP_DAY_7", length = 1)
    private String tmpSunday = AppConstants.TURNED_OFF;

    @Column(name = "FROM_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromTimeAccess;
    @Column(name = "TO_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toTimeAccess;

    @Column(name = "TMP_FROM_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tmpFromTimeAccess;
    @Column(name = "TMP_TO_TIME_ACCESS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tmpToTimeAccess;
    @Column(name = "ACTIVE_STATUS")
    private Boolean active = Boolean.FALSE;
    @Column(name = "TMP_ACTIVE_STATUS")
    private Boolean tmpActive = Boolean.FALSE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_CREATED", updatable = false)
    private Date createdDate = new Date();
    //@Transient
    @Column(name = "BANK_CODE", updatable = false)
    private String bankCode;
    @Column(name = "CENTRAL")
    private Boolean central = Boolean.TRUE;

    @Column(name = "TOKEN_USERNAME")
    private String tokenUserName;
    @Column(name = "TOKEN_GROUP")
    private String tokenUserGroup = "Etranzact";
    @Column(name = "TOKEN_STATUS", length = 1)
    private String tokenActive = "0";
    @Column(name = "TOKEN_SERIAL_NUMBER")
    private String tokenSerialNumber;

    @Column(name = "TMP_TOKEN_USERNAME")
    private String tmpTokenUserName;
    @Column(name = "TMP_TOKEN_GROUP")
    private String tmpTokenUserGroup;
    @Column(name = "TMP_TOKEN_SERIAL_NUMBER")
    private String tmpTokenSerialNumber;
          @Column(name = "ENABLE_RSA")
    private Boolean enableESA = Boolean.FALSE;

    @Transient
    private String status;
       @Transient
    private Date resetRequestTime;
              @Transient
    private Boolean pendingToken = Boolean.FALSE;

    /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }

    @Override
    public String getIdentity() {
        return getFirstName() + " " + getLastName() + "/" + getUsername() + " (USER)";
    }

   
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        this.passwordHash = crpt.doMd5Hash(StringUtils.join(new String[]{StringUtils.trim(this.getUsername()), StringUtils.trim(password)}));
    }

    public void setPasswordHash(String password) {
        this.passwordHash = password;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getTmpRole() {
        return tmpRole;
    }

    public void setTmpRole(Role tmpRole) {
        this.tmpRole = tmpRole;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTmpFirstName() {
        return tmpFirstName;
    }

    public void setTmpFirstName(String tmpFirstName) {
        this.tmpFirstName = tmpFirstName;
    }

    public String getTmpLastName() {
        return tmpLastName;
    }

    public void setTmpLastName(String tmpLastName) {
        this.tmpLastName = tmpLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTmpEmail() {
        return tmpEmail;
    }

    public void setTmpEmail(String tmpEmail) {
        this.tmpEmail = tmpEmail;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getTmpMobilePhone() {
        return tmpMobilePhone;
    }

    public void setTmpMobilePhone(String tmpMobilePhone) {
        this.tmpMobilePhone = tmpMobilePhone;
    }

    public Boolean getChangePassword() {
        changePassword = getPasswordExpired();
        return changePassword;
    }

    public void setChangePassword(Boolean changePassword) {
        this.changePassword = changePassword;
    }

    public Boolean getPasswordExpired() {
        passwordExpired = Calendar.getInstance().getTimeInMillis() > getPasswordDateExpired().getTime();
        return passwordExpired;
    }

    public void setPasswordExpired(Boolean passwordExpired) {
        this.passwordExpired = passwordExpired;
    }

    public Boolean getUserDisabled() {
        userDisabled = !getActive();
        return userDisabled;
    }

    public void setUserDisabled(Boolean userDisabled) {
        this.userDisabled = userDisabled;
    }

    public Boolean getUserLocked() {
        userLocked = getPasswordMissed() >= ConfigurationUtil.instance().getLoginRetrialAllowed();
        return userLocked;
    }

    public void setUserLocked(Boolean userLocked) {
        this.userLocked = userLocked;
    }

    public Integer getPasswordMissed() {
        return passwordMissed;
    }

    public void setPasswordMissed(Integer passwordMissed) {
        this.passwordMissed = passwordMissed;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
//        if(monday!=null) 
//        monday = tmonday ? AppConstants.TURNED_OFF : AppConstants.TURNED_ON;
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
//        tuesday = ttuesday ? AppConstants.TURNED_OFF : AppConstants.TURNED_ON;
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
//        wednesday = twednesday ? AppConstants.TURNED_OFF : AppConstants.TURNED_ON;
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
//        thursday = tthursday ? AppConstants.TURNED_OFF : AppConstants.TURNED_ON;
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
//        friday = tfriday ? AppConstants.TURNED_OFF : AppConstants.TURNED_ON;
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
//        saturday = tsaturday ? AppConstants.TURNED_OFF : AppConstants.TURNED_ON;
        this.saturday = saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
//        sunday = tsunday ? AppConstants.TURNED_OFF : AppConstants.TURNED_ON;
        this.sunday = sunday;
    }

    public String getTmpMonday() {
        return tmpMonday;
    }

    public void setTmpMonday(String tmpMonday) {
        this.tmpMonday = tmpMonday;
    }

    public String getTmpTuesday() {
        return tmpTuesday;
    }

    public void setTmpTuesday(String tmpTuesday) {
        this.tmpTuesday = tmpTuesday;
    }

    public String getTmpWednesday() {
        return tmpWednesday;
    }

    public void setTmpWednesday(String tmpWednesday) {
        this.tmpWednesday = tmpWednesday;
    }

    public String getTmpThursday() {
        return tmpThursday;
    }

    public void setTmpThursday(String tmpThursday) {
        this.tmpThursday = tmpThursday;
    }

    public String getTmpFriday() {
        return tmpFriday;
    }

    public void setTmpFriday(String tmpFriday) {
        this.tmpFriday = tmpFriday;
    }

    public String getTmpSaturday() {
        return tmpSaturday;
    }

    public void setTmpSaturday(String tmpSaturday) {
        this.tmpSaturday = tmpSaturday;
    }

    public String getTmpSunday() {
        return tmpSunday;
    }

    public void setTmpSunday(String tmpSunday) {
        this.tmpSunday = tmpSunday;
    }

    public Date getFromTimeAccess() {
        return fromTimeAccess;
    }

    public void setFromTimeAccess(Date fromTimeAccess) {
        this.fromTimeAccess = fromTimeAccess;
    }

    public Date getToTimeAccess() {
        return toTimeAccess;
    }

    public void setToTimeAccess(Date toTimeAccess) {
        this.toTimeAccess = toTimeAccess;
    }

    public Date getTmpFromTimeAccess() {
        return tmpFromTimeAccess;
    }

    public void setTmpFromTimeAccess(Date tmpFromTimeAccess) {
        this.tmpFromTimeAccess = tmpFromTimeAccess;
    }

    public Date getTmpToTimeAccess() {
        return tmpToTimeAccess;
    }

    public void setTmpToTimeAccess(Date tmpToTimeAccess) {
        this.tmpToTimeAccess = tmpToTimeAccess;
    }

    public Date getPasswordLockDate() {
        return passwordLockDate;
    }

    public void setPasswordLockDate(Date passwordLockDate) {
        this.passwordLockDate = passwordLockDate;
    }

    public Date getPasswordDateExpired() {
        return passwordDateExpired;
    }

    public void setPasswordDateExpired(Date passwordDateExpired) {
        this.passwordDateExpired = passwordDateExpired;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the approvalGroups
     */
    public List<ApprovalGroup> getApprovalGroups() {
        return approvalGroups;
    }

    /**
     * @param approvalGroups the approvalGroups to set
     */
    public void setApprovalGroups(List<ApprovalGroup> approvalGroups) {
        this.approvalGroups = approvalGroups;
    }

//    /**
//     * @return the bankCode
//     */
//    public String getBankCode() {
//        bankCode=bankCode==null || !bankCode.equals("1000")?this instanceof BankUser?((BankUser)this).getBank().getId():this instanceof CorporateUser?((CorporateUser)this).getCorporate().getBank().getId():bankCode:bankCode;
//        return bankCode;
//    }
    
       /**
     * @return the bankCode
     */
    public String getBankCode() {
        return bankCode;
    }


    /**
     * @param bankCode the bankCode to set
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    /**
     * @return the central
     */
    public Boolean getCentral() {
        return central;
    }

    /**
     * @param central the central to set
     */
    public void setCentral(Boolean central) {
        this.central = central;
    }

    /**
     * @return the tmonday
     */
    public Boolean getTmonday() {
        tmonday = monday.equals(AppConstants.TURNED_ON);
        return tmonday;
    }

    public Boolean getTTmonday() {
        return tmonday;
    }

    /**
     * @param tmonday the tmonday to set
     */
    public void setTmonday(Boolean tmonday) {
        this.tmonday = tmonday;
    }

    /**
     * @return the ttuesday
     */
    public Boolean getTtuesday() {
        ttuesday = tuesday.equals(AppConstants.TURNED_ON);
        return ttuesday;
    }

    public Boolean getTTtuesday() {
        return ttuesday;
    }

    /**
     * @param ttuesday the ttuesday to set
     */
    public void setTtuesday(Boolean ttuesday) {
        this.ttuesday = ttuesday;
    }

    /**
     * @return the twednesday
     */
    public Boolean getTwednesday() {
        twednesday = wednesday.equals(AppConstants.TURNED_ON);
        return twednesday;
    }

    public Boolean getTTwednesday() {
        return twednesday;
    }

    /**
     * @param twednesday the twednesday to set
     */
    public void setTwednesday(Boolean twednesday) {
        this.twednesday = twednesday;
    }

    /**
     * @return the tthursday
     */
    public Boolean getTthursday() {
        tthursday = thursday.equals(AppConstants.TURNED_ON);
        return tthursday;
    }

    public Boolean getTTthursday() {
        return tthursday;
    }

    /**
     * @param tthursday the tthursday to set
     */
    public void setTthursday(Boolean tthursday) {
        this.tthursday = tthursday;
    }

    /**
     * @return the tfriday
     */
    public Boolean getTfriday() {
        tfriday = friday.equals(AppConstants.TURNED_ON);
        return tfriday;
    }

    public Boolean getTTfriday() {
        return tfriday;
    }

    /**
     * @param tfriday the tfriday to set
     */
    public void setTfriday(Boolean tfriday) {
        this.tfriday = tfriday;
    }

    /**
     * @return the tsaturday
     */
    public Boolean getTsaturday() {
        tsaturday = saturday.equals(AppConstants.TURNED_ON);
        return tsaturday;
    }

    public Boolean getTTsaturday() {
        return tfriday;
    }

    /**
     * @param tsaturday the tsaturday to set
     */
    public void setTsaturday(Boolean tsaturday) {
        this.tsaturday = tsaturday;
    }

    /**
     * @return the tsunday
     */
    public Boolean getTsunday() {
        tsunday = sunday.equals(AppConstants.TURNED_ON);
        return tsunday;
    }

    public Boolean getTTsunday() {
        return tsunday;
    }

    /**
     * @param tsunday the tsunday to set
     */
    public void setTsunday(Boolean tsunday) {
        this.tsunday = tsunday;
    }

    public static void main(String[] args) {
//        try {
//            Field f = User.class.getDeclaredField("id");
//            System.out.println("f " + f);
//
//        } catch (NoSuchFieldException ex) {
//            try {
//                String superName = User.class.getSuperclass().getName();
//                System.out.println("superName " + superName);
//                Field ff = User.class.getSuperclass().getDeclaredField("id");
//                System.out.println("ff " + ff);
//            } catch (NoSuchFieldException exx) {
//
//            } catch (SecurityException exx) {
//                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        String doMd5Hash = crpt.doMd5Hash(StringUtils.
//                join(new String[]{StringUtils.trim(" thtoshinowo"), StringUtils.trim("Oshinowo123")}));
//          String doMd5Hash = crpt.doMd5Hash(StringUtils.
//                join(new String[]{StringUtils.trim("      Nwakoh"), StringUtils.trim("Nwakoh123")}));
//              String doMd5Hash = crpt.doMd5Hash(StringUtils.
//                join(new String[]{StringUtils.trim("2857"), StringUtils.trim("Nwakoh123")}));
//           String doMd5Hash = crpt.doMd5Hash(StringUtils.
//                join(new String[]{StringUtils.trim("7697"), StringUtils.trim("Nwakoh123")}));
//         String doMd5Hash = crpt.doMd5Hash(StringUtils.
//                join(new String[]{StringUtils.trim("admin1@nipc.com"), StringUtils.trim("Nwakoh123")}));
                 String doMd5Hash = crpt.doMd5Hash(StringUtils.
                join(new String[]{StringUtils.trim("guinessfauth@guiness.com"), StringUtils.trim("Guinessfauth123")}));
        
        System.out.println(" " + doMd5Hash);
    }

    /**
     * @return the lastAccessTime
     */
    public Date getLastAccessTime() {
        return lastAccessTime;
    }

    /**
     * @param lastAccessTime the lastAccessTime to set
     */
    public void setLastAccessTime(Date lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    /**
     * @return the loginTime
     */
    public Date getLoginTime() {
        return loginTime;
    }

    /**
     * @param loginTime the loginTime to set
     */
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * @param lastAccessTimeStr the lastAccessTimeStr to set
     */
    public void setLastAccessTimeStr(String lastAccessTimeStr) {
        this.lastAccessTimeStr = lastAccessTimeStr;
    }

    /**
     * @return the lastAccessTimeStr
     */
    public String getLastAccessTimeStr() {
        lastAccessTimeStr = AppUtil.formatDate(lastAccessTime == null ? new Date() : lastAccessTime, "EEE, d MMM yyyy HH:mm:ss");
        return lastAccessTimeStr;
    }

    public String getTokenUserName() {
        tokenUserName = new BasicCrypto().decrypt(tokenUserName);
        return tokenUserName;
    }

    public void setTokenUserName(String tokenUserName) {
        this.tokenUserName = new BasicCrypto().encrypt(tokenUserName);
    }

    public String getTokenUserGroup() {
        tokenUserGroup = new BasicCrypto().decrypt(tokenUserGroup);
        return tokenUserGroup;
    }

    public void setTokenUserGroup(String tokenUserGroup) {
        this.tokenUserGroup =  new BasicCrypto().encrypt(tokenUserGroup);
    }

    public String getTokenActive() {
        return tokenActive;
    }

    public void setTokenActive(String tokenActive) {
        this.tokenActive = tokenActive;
    }

    public String getTokenSerialNumber() {
        tokenSerialNumber = new BasicCrypto().decrypt(tokenSerialNumber);
        return tokenSerialNumber;
    }

    public void setTokenSerialNumber(String tokenSerialNumber) {
        this.tokenSerialNumber = new BasicCrypto().encrypt(tokenSerialNumber);
    }

    /**
     * @return the tmpTokenUserName
     */
    public String getTmpTokenUserName() {
        tmpTokenUserName = new BasicCrypto().decrypt(tmpTokenUserName);
        return tmpTokenUserName;
    }

    /**
     * @param tmpTokenUserName the tmpTokenUserName to set
     */
    public void setTmpTokenUserName(String tmpTokenUserName) {
        this.tmpTokenUserName = new BasicCrypto().encrypt(tmpTokenUserName);
    }

    /**
     * @return the tmpTokenUserGroup
     */
    public String getTmpTokenUserGroup() {
        tmpTokenUserGroup = new BasicCrypto().decrypt(tmpTokenUserGroup);
        return tmpTokenUserGroup;
    }

    /**
     * @param tmpTokenUserGroup the tmpTokenUserGroup to set
     */
    public void setTmpTokenUserGroup(String tmpTokenUserGroup) {
        this.tmpTokenUserGroup = new BasicCrypto().encrypt(tmpTokenUserGroup);
    }

    /**
     * @return the tmpTokenSerialNumber
     */
    public String getTmpTokenSerialNumber() {
        tmpTokenSerialNumber = new BasicCrypto().decrypt(tmpTokenSerialNumber);
        return tmpTokenSerialNumber;
    }

    /**
     * @param tmpTokenSerialNumber the tmpTokenSerialNumber to set
     */
    public void setTmpTokenSerialNumber(String tmpTokenSerialNumber) {
        this.tmpTokenSerialNumber = new BasicCrypto().encrypt(tmpTokenSerialNumber);
    }

    /**
     * @return the resetRequestTime
     */
    public Date getResetRequestTime() {
        return resetRequestTime;
    }

    /**
     * @param resetRequestTime the resetRequestTime to set
     */
    public void setResetRequestTime(Date resetRequestTime) {
        this.resetRequestTime = resetRequestTime;
    }

    /**
     * @return the enableESA
     */
    public Boolean getEnableESA() {
        return enableESA;
    }

    /**
     * @param enableESA the enableESA to set
     */
    public void setEnableESA(Boolean enableESA) {
        this.enableESA = enableESA;
    }

    /**
     * @return the pendingToken
     */
    public Boolean getPendingToken() {
        return pendingToken;
    }

    /**
     * @param pendingToken the pendingToken to set
     */
    public void setPendingToken(Boolean pendingToken) {
        this.pendingToken = pendingToken;
    }

    
    
}
