/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PAYMENT_SCHEDULER")
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class PaymentScheduler implements Serializable {

    @Id
    @Column(name = "SCHEDULE_ID")
    private String scheduleId;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate = new Corporate();
    //@Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, mappedBy = "schedule")
    @OrderColumn
    private Set<Transaction> transactions = new HashSet<>();
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @JoinColumn(name = "PAYMENT_ID")
    @ManyToOne
    private Payment payment = new Payment();
    @Transient
    private String status;
    @Transient
    private int totalCount;
    @Transient
    private BigDecimal totalAmount = BigDecimal.ZERO;
    @Transient
    private String totalAmountf;

    /**
     * @return the status
     */
    public String getStatus() {
        setStatus(getFlagStatus() != null ? getFlagStatus().equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : getFlagStatus().equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : getFlagStatus().equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : getFlagStatus().equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : getFlagStatus().equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT
                                                : getFlagStatus().equals(AppConstants.CREATED_IN_APPROVAL) ? AppConstants.CREATED_IN_APPROVAL_TEXT : getFlagStatus().equals(AppConstants.MODIFIED_REJECTED)
                                                                ? AppConstants.MODIFIED_REJECTED_TEXT : getFlagStatus().equals(AppConstants.PROCESSING) ? AppConstants.PROCESSING_TEXT : getFlagStatus().equals(AppConstants.PROCESSED_OK) ? AppConstants.PROCESSING_OK_TEXT
                                                                                : getFlagStatus().equals(AppConstants.PROCESSED_WITH_ERRORS) ? AppConstants.PROCESSING_WITH_ERRORS_TEXT : "" : "");
        return status;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the scheduleId
     */
    public String getScheduleId() {
        return scheduleId;
    }

    /**
     * @param scheduleId the scheduleId to set
     */
    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the transactions
     */
    public Set<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * @param transactions the transactions to set
     */
    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the payment
     */
    public Payment getPayment() {
        return payment;
    }

    /**
     * @param payment the payment to set
     */
    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the totalCount
     */
    public int getTotalCount() {
        totalCount = transactions.size();
        return totalCount;
    }

    /**
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        for (Transaction transaction : transactions) {
            totalAmount = totalAmount.add(transaction.getAmount());
        }
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the totalAmountf
     */
    public String getTotalAmountf() {
        totalAmountf = AppUtil.formatAsAmount(getTotalAmount().toString());
        return totalAmountf;
    }

}
