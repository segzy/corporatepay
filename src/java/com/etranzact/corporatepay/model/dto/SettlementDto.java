/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model.dto;

import java.io.Serializable;

/**
 *
 * @author oluwasegun.idowu
 */
public class SettlementDto implements Serializable{
    
    private String paymentId;
    private String companyName;
    private String bank;
    private String hostErrorCode;
    private String reference;
    private String issuerCode;
    private String accountNumber;
    private String transAmount;
    private String beneficiaryName;
    private String narration;
    private String created;

    /**
     * @return the paymentId
     */
    public String getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(String bank) {
        this.bank = bank;
    }

    /**
     * @return the hostErrorCode
     */
    public String getHostErrorCode() {
        return hostErrorCode;
    }

    /**
     * @param hostErrorCode the hostErrorCode to set
     */
    public void setHostErrorCode(String hostErrorCode) {
        this.hostErrorCode = hostErrorCode;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the issuerCode
     */
    public String getIssuerCode() {
        return issuerCode;
    }

    /**
     * @param issuerCode the issuerCode to set
     */
    public void setIssuerCode(String issuerCode) {
        this.issuerCode = issuerCode;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the transAmount
     */
    public String getTransAmount() {
        return transAmount;
    }

    /**
     * @param transAmount the transAmount to set
     */
    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    /**
     * @return the beneficiaryName
     */
    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    /**
     * @param beneficiaryName the beneficiaryName to set
     */
    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the created
     */
    public String getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName the companyName to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    
}
