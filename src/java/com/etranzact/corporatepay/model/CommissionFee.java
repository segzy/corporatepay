/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_GRADUATED_COMMISSION_FEE")
public class CommissionFee implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "COMMISSION_FEE_ID", length = 6)
    private Long id;
    @Digits(integer = 20, fraction = 2, message = "from amount must be a valid amount")
    @Column(name = "MIN_RANGE")
    private String minRange;
    @Column(name = "TMP_MIN_RANGE")
    private String tmpMinRange;
    @Digits(integer = 20, fraction = 2, message = "to amount must be a valid amount")
    @Column(name = "MAX_RANGE")
    private String maxRange;
    @Column(name = "TMP_MAX_RANGE")
    private String tmpMaxRange;
    @Column(name = "AMOUNT_TYPE")
    private String amountType;
    @Column(name = "TMP_AMOUNT_TYPE")
    private String tmpAmountType;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "TMP_AMOUNT")
    private BigDecimal tmpAmount;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Transient
    private String commissionOwner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the minRange
     */
    public String getMinRange() {
        return minRange;
    }

    /**
     * @param minRange the minRange to set
     */
    public void setMinRange(String minRange) {
        this.minRange = minRange;
    }

    /**
     * @return the maxRange
     */
    public String getMaxRange() {
        return maxRange;
    }

    /**
     * @param maxRange the maxRange to set
     */
    public void setMaxRange(String maxRange) {
        this.maxRange = maxRange;
    }

    /**
     * @return the amountType
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * @param amountType the amountType to set
     */
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CommissionFee other = (CommissionFee) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CommissionFee{" + "id=" + id + '}';
    }

    /**
     * @return the tmpMinRange
     */
    public String getTmpMinRange() {
        return tmpMinRange;
    }

    /**
     * @param tmpMinRange the tmpMinRange to set
     */
    public void setTmpMinRange(String tmpMinRange) {
        this.tmpMinRange = tmpMinRange;
    }

    /**
     * @return the tmpMaxRange
     */
    public String getTmpMaxRange() {
        return tmpMaxRange;
    }

    /**
     * @param tmpMaxRange the tmpMaxRange to set
     */
    public void setTmpMaxRange(String tmpMaxRange) {
        this.tmpMaxRange = tmpMaxRange;
    }

    /**
     * @return the tmpAmountType
     */
    public String getTmpAmountType() {
        return tmpAmountType;
    }

    /**
     * @param tmpAmountType the tmpAmountType to set
     */
    public void setTmpAmountType(String tmpAmountType) {
        this.tmpAmountType = tmpAmountType;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }

    /**
     * @return the commissionOwner
     */
    public String getCommissionOwner() {
        commissionOwner = commissionOwner==null?AppConstants.BANK_COMMISSION_TYPE:commissionOwner;
        return commissionOwner;
    }

    /**
     * @param commissionOwner the commissionOwner to set
     */
    public void setCommissionOwner(String commissionOwner) {
        this.commissionOwner = commissionOwner;
    }
    
    

}
