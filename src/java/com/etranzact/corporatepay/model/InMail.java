/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_INMAIL")
public class InMail implements Serializable {

    @Id
    @Column(name = "ID", length = 6)
    @GeneratedValue
    private Long id;
    @Column(name = "MESSAGE_CODE")
    private String messageCode;
    @Column(name = "CLAZZ", length = 30)
    private String clazz;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Column(name = "OBJECT_ID")
    private String targetObjectId;
     @Column(name = "STATUS")
    private String status;
       @Transient
    private String message;
         @Transient
       private String createdDateFormatted;
                @Transient
       private String subject;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the messageCode
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * @param messageCode the messageCode to set
     */
    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    /**
     * @return the clazz
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * @param clazz the clazz to set
     */
    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the targetObjectId
     */
    public String getTargetObjectId() {
        return targetObjectId;
    }

    /**
     * @param targetObjectId the targetObjectId to set
     */
    public void setTargetObjectId(String targetObjectId) {
        this.targetObjectId = targetObjectId;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        status = status == null?AppConstants.OPEN:AppConstants.CLOSED;
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the createdDateFormatted
     */
    public String getCreatedDateFormatted() {
        createdDateFormatted = AppUtil.formatDate(dateCreated, "EEE, d MMM yyyy HH:mm:ss");
        return createdDateFormatted;
    }

    /**
     * @param createdDateFormatted the createdDateFormatted to set
     */
    public void setCreatedDateFormatted(String createdDateFormatted) {
        this.createdDateFormatted = createdDateFormatted;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        subject = messageCode.equals(AppConstants.NEW_USER_MESSAGE_CODE)?AppConstants.NEW_USER_MESSAGE_SUBJECT:"";
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InMail other = (InMail) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InMail{" + "id=" + id + '}';
    }

  

   
}
