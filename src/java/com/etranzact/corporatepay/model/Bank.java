/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.model;

import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_ISSUER", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ISSUER_NAME", "BANK_IP"})})
//@Audited
public class Bank implements Serializable {

    @Id
    @Size(min = 3, max = 10)
    @Column(name = "ISSUER_CODE")
    private String id;
    @Column(name = "ISSUER_NAME", nullable = false)
    private String bankName;
    @Column(name = "TMP_ISSUER_NAME")
    private String tmpBankName;
    @Column(name = "BANK_IP", nullable = false)
    private String bankIp = "172.0.0.1";
    @Column(name = "TMP_BANK_IP")
    private String tmpBankIp;
    @Email
    @Column(name = "BANK_EMAIL")
    private String bankEmail;
    @Column(name = "TMP_BANK_EMAIL")
    private String tmpBankEmail;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.FALSE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Column(name = "PREF_SESSION_TIMEOUT")
    private Integer timeOutPreference;
    @Transient
    private String status;
    @Column(name = "TSS_ACCOUNT")
    private String tssAccount;
    @Column(name = "TMP_TSS_ACCOUNT")
    private String tmpTssAccount;
    @Column(name = "DB_IP")
    private String bankLocalIP;
    @Column(name = "DB_PORT")
    private String bankLocalPort;
    @Column(name = "DB_NAME")
    private String bankLocalName;
    @Column(name = "DB_USERNAME")
    private String bankLocalUserName;
    @Column(name = "DB_PASSWORD")
    private String bankLocalPassword;
    @Column(name = "PAYMENT_THREAD")
    private String bankLocalPaymentThread;
    @Column(name = "PAY_MODE")
    private String payMode;
       @Column(name = "SETTLEMENT_MODE")
    private String settlementMode;
             @Column(name = "TMP_SETTLEMENT_MODE")
    private String tmpSettlementMode;
    @Column(name = "etzFee")
    private BigDecimal etzFee;
    @Column(name = "TMP_Etz_Fee")
    private BigDecimal tmpEtzFee;
       @Column(name = "Etz_Percentage")
    private BigDecimal etzPercentage;
    @Column(name = "TMP_Etz_Percentage")
    private BigDecimal tmpEtzPercentage;
    @Column(name = "ACCOUNT_LENGHT")
    private Integer accountLenght = 10;
    @Column(name = "MFB_BANK")
    private Boolean mfbBank = Boolean.FALSE;
    @JoinColumn(name = "AFFLIATE_BANK")
    @ManyToOne
    private Bank affliateBank;
    @Column(name = "AFFLIATE_BANK_ACCOUNT")
    private String affliateBankAccount;
    @JoinColumn(name = "TMP_AFFLIATE_BANK")
    @ManyToOne
    private Bank tmpAffliateBank;
    @Column(name = "TMP_AFFLIATE_BANK_ACCOUNT")
    private String tmpAffliateBankAccount;
    @Column(name = "TMP_MFB_BANK")
    private Boolean tmpmfbBank;
    @Column(name = "ENABLE_RSA")
    private Boolean enableESA = Boolean.FALSE;
    @Column(name = "TMP_ENABLE_RSA")
    private Boolean tmpEnableESA;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankIp() {
        return bankIp;
    }

    public void setBankIp(String bankIp) {
        this.bankIp = bankIp;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bank other = (Bank) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return bankName;
    }

    /**
     * @return the bankEmail
     */
    public String getBankEmail() {
        return bankEmail;
    }

    /**
     * @param bankEmail the bankEmail to set
     */
    public void setBankEmail(String bankEmail) {
        this.bankEmail = bankEmail;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the tmpBankName
     */
    public String getTmpBankName() {
        return tmpBankName;
    }

    /**
     * @param tmpBankName the tmpBankName to set
     */
    public void setTmpBankName(String tmpBankName) {
        this.tmpBankName = tmpBankName;
    }

    /**
     * @return the tmpBankIp
     */
    public String getTmpBankIp() {
        return tmpBankIp;
    }

    /**
     * @param tmpBankIp the tmpBankIp to set
     */
    public void setTmpBankIp(String tmpBankIp) {
        this.tmpBankIp = tmpBankIp;
    }

    /**
     * @return the tmpBankEmail
     */
    public String getTmpBankEmail() {
        return tmpBankEmail;
    }

    /**
     * @param tmpBankEmail the tmpBankEmail to set
     */
    public void setTmpBankEmail(String tmpBankEmail) {
        this.tmpBankEmail = tmpBankEmail;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }

    /**
     * @return the timeOutPreference
     */
    public Integer getTimeOutPreference() {
        return timeOutPreference;
    }

    /**
     * @param timeOutPreference the timeOutPreference to set
     */
    public void setTimeOutPreference(Integer timeOutPreference) {
        this.timeOutPreference = timeOutPreference;
    }

    /**
     * @return the tssAccount
     */
    public String getTssAccount() {
        return tssAccount;
    }

    /**
     * @param tssAccount the tssAccount to set
     */
    public void setTssAccount(String tssAccount) {
        this.tssAccount = tssAccount;
    }

    /**
     * @return the bankLocalIP
     */
    public String getBankLocalIP() {
        return bankLocalIP;
    }

    /**
     * @param bankLocalIP the bankLocalIP to set
     */
    public void setBankLocalIP(String bankLocalIP) {
        this.bankLocalIP = bankLocalIP;
    }

    /**
     * @return the bankLocalPort
     */
    public String getBankLocalPort() {
        return bankLocalPort;
    }

    /**
     * @param bankLocalPort the bankLocalPort to set
     */
    public void setBankLocalPort(String bankLocalPort) {
        this.bankLocalPort = bankLocalPort;
    }

    /**
     * @return the bankLocalName
     */
    public String getBankLocalName() {
        return bankLocalName;
    }

    /**
     * @param bankLocalName the bankLocalName to set
     */
    public void setBankLocalName(String bankLocalName) {
        this.bankLocalName = bankLocalName;
    }

    /**
     * @return the bankLocalUserName
     */
    public String getBankLocalUserName() {
        return bankLocalUserName;
    }

    /**
     * @param bankLocalUserName the bankLocalUserName to set
     */
    public void setBankLocalUserName(String bankLocalUserName) {
        this.bankLocalUserName = bankLocalUserName;
    }

    /**
     * @return the bankLocalPassword
     */
    public String getBankLocalPassword() {
        return bankLocalPassword;
    }

    /**
     * @param bankLocalPassword the bankLocalPassword to set
     */
    public void setBankLocalPassword(String bankLocalPassword) {
        this.bankLocalPassword = bankLocalPassword;
    }

    /**
     * @return the bankLocalPaymentThread
     */
    public String getBankLocalPaymentThread() {
        return bankLocalPaymentThread;
    }

    /**
     * @param bankLocalPaymentThread the bankLocalPaymentThread to set
     */
    public void setBankLocalPaymentThread(String bankLocalPaymentThread) {
        this.bankLocalPaymentThread = bankLocalPaymentThread;
    }

    /**
     * @return the payMode
     */
    public String getPayMode() {
        return payMode;
    }

    /**
     * @param payMode the payMode to set
     */
    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    /**
     * @return the tmpTssAccount
     */
    public String getTmpTssAccount() {
        return tmpTssAccount;
    }

    /**
     * @param tmpTssAccount the tmpTssAccount to set
     */
    public void setTmpTssAccount(String tmpTssAccount) {
        this.tmpTssAccount = tmpTssAccount;
    }

    /**
     * @return the accountLenght
     */
    public Integer getAccountLenght() {
        return accountLenght;
    }

    /**
     * @param accountLenght the accountLenght to set
     */
    public void setAccountLenght(Integer accountLenght) {
        this.accountLenght = accountLenght;
    }

    /**
     * @return the tmc
     */
    public boolean isTmc() {
        return payMode != null && payMode.equals("0");
    }

    /**
     * @return the tmc
     */
    public boolean isPM() {
        return payMode != null && payMode.equals("1");
    }

    /**
     * @return the tmc
     */
    public boolean isInsertion() {
        return payMode != null && payMode.equals("2");
    }
    
      /**
     * @return the tmc
     */
    public boolean isSpecialized() {
        return settlementMode != null &&  settlementMode.equals("1");
    }
    
         /**
     * @return the tmc
     */
    public boolean isNormal() {
        return settlementMode == null || settlementMode.equals("0");
    }


    /**
     * @return the mfbBank
     */
    public Boolean getMfbBank() {
        mfbBank = mfbBank == null ? Boolean.FALSE : mfbBank;
        return mfbBank;
    }

    /**
     * @param mfbBank the mfbBank to set
     */
    public void setMfbBank(Boolean mfbBank) {
        this.mfbBank = mfbBank;
    }

    /**
     * @return the affliateBank
     */
    public Bank getAffliateBank() {
        return affliateBank;
    }

    /**
     * @param affliateBank the affliateBank to set
     */
    public void setAffliateBank(Bank affliateBank) {
        this.affliateBank = affliateBank;
    }

    /**
     * @return the affliateBankAccount
     */
    public String getAffliateBankAccount() {
        return affliateBankAccount;
    }

    /**
     * @param affliateBankAccount the affliateBankAccount to set
     */
    public void setAffliateBankAccount(String affliateBankAccount) {
        this.affliateBankAccount = affliateBankAccount;
    }

    /**
     * @return the etzFee
     */
    public BigDecimal getEtzFee() {
        etzFee = etzFee == null ? ConfigurationUtil.instance().getEtzMinCommission() : etzFee;
        return etzFee;
    }

    /**
     * @param etzFee the etzFee to set
     */
    public void setEtzFee(BigDecimal etzFee) {
        this.etzFee = etzFee;
    }

    /**
     * @return the tmpEtzFee
     */
    public BigDecimal getTmpEtzFee() {
        return tmpEtzFee;
    }

    /**
     * @param tmpEtzFee the tmpEtzFee to set
     */
    public void setTmpEtzFee(BigDecimal tmpEtzFee) {
        this.tmpEtzFee = tmpEtzFee;
    }

    /**
     * @return the tmpAffliateBank
     */
    public Bank getTmpAffliateBank() {
        return tmpAffliateBank;
    }

    /**
     * @param tmpAffliateBank the tmpAffliateBank to set
     */
    public void setTmpAffliateBank(Bank tmpAffliateBank) {
        this.tmpAffliateBank = tmpAffliateBank;
    }

    /**
     * @return the tmpAffliateBankAccount
     */
    public String getTmpAffliateBankAccount() {
        return tmpAffliateBankAccount;
    }

    /**
     * @param tmpAffliateBankAccount the tmpAffliateBankAccount to set
     */
    public void setTmpAffliateBankAccount(String tmpAffliateBankAccount) {
        this.tmpAffliateBankAccount = tmpAffliateBankAccount;
    }

    /**
     * @return the tmpmfbBank
     */
    public Boolean getTmpmfbBank() {
        return tmpmfbBank;
    }

    /**
     * @param tmpmfbBank the tmpmfbBank to set
     */
    public void setTmpmfbBank(Boolean tmpmfbBank) {
        this.tmpmfbBank = tmpmfbBank;
    }

    /**
     * @return the enableESA
     */
    public Boolean getEnableESA() {
        return enableESA==null?Boolean.FALSE:enableESA;
    }

    /**
     * @param enableESA the enableESA to set
     */
    public void setEnableESA(Boolean enableESA) {
        this.enableESA = enableESA;
    }

    /**
     * @return the tmpEnableESA
     */
    public Boolean getTmpEnableESA() {
        return tmpEnableESA;
    }

    /**
     * @param tmpEnableESA the tmpEnableESA to set
     */
    public void setTmpEnableESA(Boolean tmpEnableESA) {
        this.tmpEnableESA = tmpEnableESA;
    }

    /**
     * @return the settlementMode
     */
    public String getSettlementMode() {
        return settlementMode;
    }

    /**
     * @param settlementMode the settlementMode to set
     */
    public void setSettlementMode(String settlementMode) {
        this.settlementMode = settlementMode;
    }

    /**
     * @return the tmpSettlementMode
     */
    public String getTmpSettlementMode() {
        return tmpSettlementMode;
    }

    /**
     * @param tmpSettlementMode the tmpSettlementMode to set
     */
    public void setTmpSettlementMode(String tmpSettlementMode) {
        this.tmpSettlementMode = tmpSettlementMode;
    }

    /**
     * @return the etzPercentage
     */
    public BigDecimal getEtzPercentage() {
        return etzPercentage;
    }

    /**
     * @param etzPercentage the etzPercentage to set
     */
    public void setEtzPercentage(BigDecimal etzPercentage) {
        this.etzPercentage = etzPercentage;
    }

    /**
     * @return the tmpEtzPercentage
     */
    public BigDecimal getTmpEtzPercentage() {
        return tmpEtzPercentage;
    }

    /**
     * @param tmpEtzPercentage the tmpEtzPercentage to set
     */
    public void setTmpEtzPercentage(BigDecimal tmpEtzPercentage) {
        this.tmpEtzPercentage = tmpEtzPercentage;
    }

    
}
