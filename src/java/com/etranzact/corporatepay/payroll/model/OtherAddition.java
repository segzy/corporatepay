/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_OTHER_ADDITION", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ADDITION_NAME", "CORPORATE_ID"})})
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class OtherAddition implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "ADDITION_ID")
    private Long Id;
    @Column(name = "ADDITION_NAME")
    private String additionName;
    @Column(name = "TMP_ADDITION_NAME")
    private String tmpAdditionName;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
    @Column(name = "FIGURES_STATIC")
    private Boolean figuresStatic = Boolean.FALSE;
    @Column(name = "TMP_FIGURES_STATIC")
    private Boolean tmpFiguresStatic = Boolean.FALSE;
    //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_PR_OTHERADDITION_PAYITEM", joinColumns
            = @JoinColumn(name = "OTHERADDITION_ID"), inverseJoinColumns
            = @JoinColumn(name = "PAYITEM_ID"))
    private List<PayItem> refStatutoryPayItems = new ArrayList<>();
       @JoinColumn(name = "ADDITION_TYPE")
    @ManyToOne
    private AdditionType additionType;

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the figuresStatic
     */
    public Boolean getFiguresStatic() {
        return figuresStatic;
    }

    /**
     * @param figuresStatic the figuresStatic to set
     */
    public void setFiguresStatic(Boolean figuresStatic) {
        this.figuresStatic = figuresStatic;
    }

    /**
     * @return the tmpFiguresStatic
     */
    public Boolean getTmpFiguresStatic() {
        return tmpFiguresStatic;
    }

    /**
     * @param tmpFiguresStatic the tmpFiguresStatic to set
     */
    public void setTmpFiguresStatic(Boolean tmpFiguresStatic) {
        this.tmpFiguresStatic = tmpFiguresStatic;
    }

    /**
     * @return the additionName
     */
    public String getAdditionName() {
        return additionName;
    }

    /**
     * @param additionName the additionName to set
     */
    public void setAdditionName(String additionName) {
        this.additionName = additionName;
    }

    /**
     * @return the tmpAdditionName
     */
    public String getTmpAdditionName() {
        return tmpAdditionName;
    }

    /**
     * @param tmpAdditionName the tmpAdditionName to set
     */
    public void setTmpAdditionName(String tmpAdditionName) {
        this.tmpAdditionName = tmpAdditionName;
    }

    /**
     * @return the refStatutoryPayItems
     */
    public List<PayItem> getRefStatutoryPayItems() {
        return refStatutoryPayItems;
    }

    /**
     * @param refStatutoryPayItems the refStatutoryPayItems to set
     */
    public void setRefStatutoryPayItems(List<PayItem> refStatutoryPayItems) {
        this.refStatutoryPayItems = refStatutoryPayItems;
    }

    /**
     * @return the additionType
     */
    public AdditionType getAdditionType() {
        return additionType;
    }

    /**
     * @param additionType the additionType to set
     */
    public void setAdditionType(AdditionType additionType) {
        this.additionType = additionType;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.Id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OtherAddition other = (OtherAddition) obj;
        if (!Objects.equals(this.Id, other.Id)) {
            return false;
        }
        return true;
    }
    
    

}
