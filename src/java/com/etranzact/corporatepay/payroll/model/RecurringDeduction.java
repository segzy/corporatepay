/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_RECURRING_DEDUCTION")
//@Audited
public class RecurringDeduction extends OtherDeduction {

    @Column(name = "NO_OF_OCCURRENCES")
    private int noOfOcurrences;
    @Column(name = "TMP_NO_OF_OCCURRENCES")
    private int tmpNoOfOcurrences;
    @Column(name = "INDEFINITE")
    private Boolean indefinite = Boolean.FALSE;
    @Column(name = "TMP_INDEFINITE")
    private Boolean tmpIndefinite = Boolean.FALSE;
    @Column(name = "AMOUNT_TYPE")
    private String amountType;
    @Column(name = "TMP_AMOUNT_TYPE")
    private String tmpAmountType;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "TMP_AMOUNT")
    private BigDecimal tmpAmount;

    /**
     * @return the noOfOcurrences
     */
    public int getNoOfOcurrences() {
        return noOfOcurrences;
    }

    /**
     * @param noOfOcurrences the noOfOcurrences to set
     */
    public void setNoOfOcurrences(int noOfOcurrences) {
        this.noOfOcurrences = noOfOcurrences;
    }

    /**
     * @return the tmpNoOfOcurrences
     */
    public int getTmpNoOfOcurrences() {
        return tmpNoOfOcurrences;
    }

    /**
     * @param tmpNoOfOcurrences the tmpNoOfOcurrences to set
     */
    public void setTmpNoOfOcurrences(int tmpNoOfOcurrences) {
        this.tmpNoOfOcurrences = tmpNoOfOcurrences;
    }

    /**
     * @return the indefinite
     */
    public Boolean getIndefinite() {
        return indefinite;
    }

    /**
     * @param indefinite the indefinite to set
     */
    public void setIndefinite(Boolean indefinite) {
        this.indefinite = indefinite;
    }

    /**
     * @return the tmpIndefinite
     */
    public Boolean getTmpIndefinite() {
        return tmpIndefinite;
    }

    /**
     * @param tmpIndefinite the tmpIndefinite to set
     */
    public void setTmpIndefinite(Boolean tmpIndefinite) {
        this.tmpIndefinite = tmpIndefinite;
    }

    /**
     * @return the amountType
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * @param amountType the amountType to set
     */
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    /**
     * @return the tmpAmountType
     */
    public String getTmpAmountType() {
        return tmpAmountType;
    }

    /**
     * @param tmpAmountType the tmpAmountType to set
     */
    public void setTmpAmountType(String tmpAmountType) {
        this.tmpAmountType = tmpAmountType;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }

}
