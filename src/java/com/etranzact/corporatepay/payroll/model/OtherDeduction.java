/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_OTHER_DEDUCTION", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"DEDUCTION_NAME", "CORPORATE_ID"})})
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class OtherDeduction implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "DEDUCTION_ID")
    private Long Id;
    @Column(name = "DEDUCTION_NAME")
    private String deductionName;
    @Column(name = "TMP_DEDUCTION_NAME")
    private String tmpDeductionName;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "DEDUCTION_TYPE")
    @ManyToOne
    protected DeductionType deductionType;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
    @Column(name = "FIGURES_STATIC")
    private Boolean figuresStatic = Boolean.FALSE;
    @Column(name = "TMP_FIGURES_STATIC")
    private Boolean tmpFiguresStatic = Boolean.FALSE;
      //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_PR_OTHERDEDUCTION_PAYITEM", joinColumns
            = @JoinColumn(name = "OTHERDEDUCTION_ID"), inverseJoinColumns
            = @JoinColumn(name = "PAYITEM_ID"))
    private List<PayItem> refStatutoryPayItems = new ArrayList<>();

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the deductionName
     */
    public String getDeductionName() {
        return deductionName;
    }

    /**
     * @param deductionName the deductionName to set
     */
    public void setDeductionName(String deductionName) {
        this.deductionName = deductionName;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the figuresStatic
     */
    public Boolean getFiguresStatic() {
        return figuresStatic;
    }

    /**
     * @param figuresStatic the figuresStatic to set
     */
    public void setFiguresStatic(Boolean figuresStatic) {
        this.figuresStatic = figuresStatic;
    }

    /**
     * @return the tmpDeductionName
     */
    public String getTmpDeductionName() {
        return tmpDeductionName;
    }

    /**
     * @param tmpDeductionName the tmpDeductionName to set
     */
    public void setTmpDeductionName(String tmpDeductionName) {
        this.tmpDeductionName = tmpDeductionName;
    }

    /**
     * @return the tmpFiguresStatic
     */
    public Boolean getTmpFiguresStatic() {
        return tmpFiguresStatic;
    }

    /**
     * @param tmpFiguresStatic the tmpFiguresStatic to set
     */
    public void setTmpFiguresStatic(Boolean tmpFiguresStatic) {
        this.tmpFiguresStatic = tmpFiguresStatic;
    }

 

    /**
     * @return the deductionType
     */
    public DeductionType getDeductionType() {
        return deductionType;
    }

    /**
     * @param deductionType the deductionType to set
     */
    public void setDeductionType(DeductionType deductionType) {
        deductionType = new DeductionType();
        if (this instanceof OneTimeDeduction) {
            deductionType.setId("#ONE_TIME");
        } else if (this instanceof RecurringDeduction) {
            deductionType.setId("#RECURRING");
        } else if (this instanceof LoanDeduction) {
            deductionType.setId("#LOAN");
        }
        this.deductionType = deductionType;
    }

    /**
     * @return the refStatutoryPayItems
     */
    public List<PayItem> getRefStatutoryPayItems() {
        return refStatutoryPayItems;
    }

    /**
     * @param refStatutoryPayItems the refStatutoryPayItems to set
     */
    public void setRefStatutoryPayItems(List<PayItem> refStatutoryPayItems) {
        this.refStatutoryPayItems = refStatutoryPayItems;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.Id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OtherDeduction other = (OtherDeduction) obj;
        if (!Objects.equals(this.Id, other.Id)) {
            return false;
        }
        return true;
    }

    
    
}
