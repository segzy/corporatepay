/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.*;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
////@Audited
@Table(name = "COP_ADDITION_TYPE")
public class AdditionType implements Serializable {

    @Id
    @Column(name = "ID", length = 15)
    private String id;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "IS_ACTIVE")
    private Boolean active;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

   
    
    

}
