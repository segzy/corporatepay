/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKERSCATEGORYGROUP")
public class WorkersCategoryGroup implements Serializable {
    @ManyToOne
    private WorkersCategory workersCategory;

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Column(name = "GROUP_LEVEL")
    private Integer groupLevel;
     @Column(name = "STEP_NO")
    private Integer stepNo = 1;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    //@Audited(targetAuditMode = NOT_AUDITED)
    
    @OneToMany(mappedBy = "workersCategoryGroup", fetch = FetchType.LAZY)
    private List<WorkersCategoryGroupStep> workersCategoryGroupSteps = new ArrayList<>();
     @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the groupLevel
     */
    public Integer getGroupLevel() {
        return groupLevel;
    }

    /**
     * @param groupLevel the groupLevel to set
     */
    public void setGroupLevel(Integer groupLevel) {
        this.groupLevel = groupLevel;
    }

    /**
     * @return the workersCategoryGroupSteps
     */
    public List<WorkersCategoryGroupStep> getWorkersCategoryGroupSteps() {
        return workersCategoryGroupSteps;
    }

    /**
     * @param workersCategoryGroupSteps the workersCategoryGroupSteps to set
     */
    public void setWorkersCategoryGroupSteps(List<WorkersCategoryGroupStep> workersCategoryGroupSteps) {
        this.workersCategoryGroupSteps = workersCategoryGroupSteps;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the stepNo
     */
    public Integer getStepNo() {
        return stepNo;
    }

    /**
     * @param stepNo the stepNo to set
     */
    public void setStepNo(Integer stepNo) {
        this.stepNo = stepNo;
    }

    /**
     * @return the workersCategory
     */
    public WorkersCategory getWorkersCategory() {
        return workersCategory;
    }

    /**
     * @param workersCategory the workersCategory to set
     */
    public void setWorkersCategory(WorkersCategory workersCategory) {
        this.workersCategory = workersCategory;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkersCategoryGroup other = (WorkersCategoryGroup) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    

}
