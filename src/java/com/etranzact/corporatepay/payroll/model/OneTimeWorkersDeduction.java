/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_ONETIME_WORKERS_DEDUCTION")
//@Audited
public class OneTimeWorkersDeduction extends WorkersSalaryDeduction {

    @Column(name = "AMOUNT_TYPE")
    private String amountType;
    @Column(name = "TMP_AMOUNT_TYPE")
    private String tmpAmountType;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "TMP_AMOUNT")
    private BigDecimal tmpAmount;
    @Transient
    private String amountTypeLabel;
        @Transient
    private String tmpAmountTypeLabel;
    
    

    /**
     * @return the amountType
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * @param amountType the amountType to set
     */
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    /**
     * @return the tmpAmountType
     */
    public String getTmpAmountType() {
        return tmpAmountType;
    }

    /**
     * @param tmpAmountType the tmpAmountType to set
     */
    public void setTmpAmountType(String tmpAmountType) {
        this.tmpAmountType = tmpAmountType;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }

    /**
     * @return the amountTypeLabel
     */
    public String getAmountTypeLabel() {
           amountTypeLabel = amountType==null?"":amountType.equals(AppConstants.AMOUNT_TYPE_FIXED)?"Fixed":"Percent";
        return amountTypeLabel;
    }

    /**
     * @return the tmpAmountTypeLabel
     */
    public String getTmpAmountTypeLabel() {
        tmpAmountTypeLabel = tmpAmountType==null?"":tmpAmountType.equals(AppConstants.AMOUNT_TYPE_FIXED)?"Fixed":"Percent";
        return tmpAmountTypeLabel;
    }

    
}
