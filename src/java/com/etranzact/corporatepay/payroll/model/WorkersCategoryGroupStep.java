/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.util.AppConstants;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKERSCATEGORYGROUPSTEP")
public class WorkersCategoryGroupStep implements Serializable {
    @ManyToOne
    private WorkersCategoryGroup workersCategoryGroup;

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Column(name = "GROUPSTEP_LEVEL")
    private Integer groupStepLevel = 1;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
         //@Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @OrderColumn
    @JoinTable(name = "COP_PR_WCGSTEP_PAYCOMPONENT", joinColumns
            = @JoinColumn(name = "WCGSTEP_ID"), inverseJoinColumns
            = @JoinColumn(name = "PAYCOMPONENT_ID"))
    private Set<StatutoryPayComponent> statutoryPayComponents = new HashSet<>();
         @Transient
         private String name;
            @Column(name = "FLAG_STATUS")
    private String flagStatus;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the groupStepLevel
     */
    public Integer getGroupStepLevel() {
        return groupStepLevel;
    }

    /**
     * @param groupStepLevel the groupStepLevel to set
     */
    public void setGroupStepLevel(Integer groupStepLevel) {
        this.groupStepLevel = groupStepLevel;
    }

    /**
     * @return the statutoryPayComponents
     */
    public Set<StatutoryPayComponent> getStatutoryPayComponents() {
        return statutoryPayComponents;
    }

    /**
     * @param statutoryPayComponents the statutoryPayComponents to set
     */
    public void setStatutoryPayComponents(Set<StatutoryPayComponent> statutoryPayComponents) {
        this.statutoryPayComponents = statutoryPayComponents;
    }

    /**
     * @return the workersCategoryGroup
     */
    public WorkersCategoryGroup getWorkersCategoryGroup() {
        return workersCategoryGroup;
    }

    /**
     * @param workersCategoryGroup the workersCategoryGroup to set
     */
    public void setWorkersCategoryGroup(WorkersCategoryGroup workersCategoryGroup) {
        this.workersCategoryGroup = workersCategoryGroup;
    }

    /**
     * @return the name
     */
    public String getName() {
        name = workersCategoryGroup.getWorkersCategory().getGroupNaming() 
                + " " + workersCategoryGroup.getGroupLevel() + " " + workersCategoryGroup.getWorkersCategory().getGroupStepNaming() + 
                " " + this.getGroupStepLevel();
        return name;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

  
       @Transient
      private String status;
      
        /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED_NOT_SETUP)
                                                        ? AppConstants.CREATED_NOT_SETUP_TEXT : flagStatus.equals(AppConstants.SETUP_COMPLETED)
                                                        ? AppConstants.SETUP_COMPLETED_TEXT:flagStatus.equals(AppConstants.SET_UP_IN_PROGRESS)
                                                        ? AppConstants.SET_UP_IN_PROGRESS_TEXT: AppConstants.CREATED_NOT_SETUP_TEXT : AppConstants.CREATED_NOT_SETUP_TEXT;
        return status;
    }

    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkersCategoryGroupStep other = (WorkersCategoryGroupStep) obj;
        if (!Objects.equals(this.id, other.getId())) {
            return false;
        }
        return true;
    }

    
    
}
