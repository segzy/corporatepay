/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_LOAN_WORKERS_DEDUCTION")
////@Audited
public class LoanWorkersDeduction extends  WorkersSalaryDeduction {

    
          @Column(name="PRINCIPAL_AMOUNT")
     private BigDecimal amount;
                 @Column(name="TMP_PRINCIPAL_AMOUNT")
     private BigDecimal tmpAmount;
              @Column(name="DEDUCTION_AMOUNT")
     private BigDecimal deductionAmount;
                      @Column(name="TMP_DEDUCTION_AMOUNT")
     private BigDecimal tmpDeductionAmount;
                                @Column(name="PAID_AMOUNT")
     private BigDecimal paidAmount;

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }

    /**
     * @return the deductionAmount
     */
    public BigDecimal getDeductionAmount() {
        return deductionAmount;
    }

    /**
     * @param deductionAmount the deductionAmount to set
     */
    public void setDeductionAmount(BigDecimal deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    /**
     * @return the tmpDeductionAmount
     */
    public BigDecimal getTmpDeductionAmount() {
        return tmpDeductionAmount;
    }

    /**
     * @param tmpDeductionAmount the tmpDeductionAmount to set
     */
    public void setTmpDeductionAmount(BigDecimal tmpDeductionAmount) {
        this.tmpDeductionAmount = tmpDeductionAmount;
    }

    /**
     * @return the paidAmount
     */
    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    /**
     * @param paidAmount the paidAmount to set
     */
    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }
                      
                      
   
}
