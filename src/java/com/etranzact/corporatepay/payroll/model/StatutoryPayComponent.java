/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_PAYCOMPONENT")
//@Audited
public class StatutoryPayComponent implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "COMPONENT_ID")
    private Long Id;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @JoinColumn(name = "PAYITEM_ID")
    @ManyToOne
    private PayItem payItem;
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
    @Column(name = "AMOUNT_TYPE")
    private String amountType;
    @Column(name = "TMP_AMOUNT_TYPE")
    private String tmpAmountType;
    @Column(name = "AMOUNT")
    private BigDecimal amount;
    @Column(name = "TMP_AMOUNT")
    private BigDecimal tmpAmount;
    @JoinColumn(name = "GROUPSTEP_ID")
    @ManyToOne
    private WorkersCategoryGroupStep workersCategoryGroupStep;

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the payItem
     */
    public PayItem getPayItem() {
        return payItem;
    }

    /**
     * @param payItem the payItem to set
     */
    public void setPayItem(PayItem payItem) {
        this.payItem = payItem;
    }

    /**
     * @return the amountType
     */
    public String getAmountType() {
        return amountType;
    }

    /**
     * @param amountType the amountType to set
     */
    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    /**
     * @return the tmpAmountType
     */
    public String getTmpAmountType() {
        return tmpAmountType;
    }

    /**
     * @param tmpAmountType the tmpAmountType to set
     */
    public void setTmpAmountType(String tmpAmountType) {
        this.tmpAmountType = tmpAmountType;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the tmpAmount
     */
    public BigDecimal getTmpAmount() {
        return tmpAmount;
    }

    /**
     * @param tmpAmount the tmpAmount to set
     */
    public void setTmpAmount(BigDecimal tmpAmount) {
        this.tmpAmount = tmpAmount;
    }

    /**
     * @return the workersCategoryGroupStep
     */
    public WorkersCategoryGroupStep getWorkersCategoryGroupStep() {
        return workersCategoryGroupStep;
    }

    /**
     * @param workersCategoryGroupStep the workersCategoryGroupStep to set
     */
    public void setWorkersCategoryGroupStep(WorkersCategoryGroupStep workersCategoryGroupStep) {
        this.workersCategoryGroupStep = workersCategoryGroupStep;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.Id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StatutoryPayComponent other = (StatutoryPayComponent) obj;
        if (!Objects.equals(this.Id, other.Id)) {
            return false;
        }
        return true;
    }

    
    
}
