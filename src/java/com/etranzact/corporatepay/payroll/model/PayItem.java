/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_PAYITEM", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"PAYITEM_NAME","CORPORATE_ID"})})
//@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public class PayItem implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "PAYITEM_ID")
    private Long Id;
    @Column(name = "PAYITEM_NAME")
    private String payItemname;
    @Column(name = "TMP_PAYITEM_NAME")
    private String tmpPayItemname;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
    @Column(name = "IS_BASIC")
    private Boolean basic = Boolean.FALSE;

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

 

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

 

    /**
     * @return the basic
     */
    public Boolean getBasic() {
        return basic;
    }

    /**
     * @param basic the basic to set
     */
    public void setBasic(Boolean basic) {
        this.basic = basic;
    }

    /**
     * @return the payItemname
     */
    public String getPayItemname() {
        return payItemname;
    }

    /**
     * @param payItemname the payItemname to set
     */
    public void setPayItemname(String payItemname) {
        this.payItemname = payItemname;
    }

    /**
     * @return the tmpPayItemname
     */
    public String getTmpPayItemname() {
        return tmpPayItemname;
    }

    /**
     * @param tmpPayItemname the tmpPayItemname to set
     */
    public void setTmpPayItemname(String tmpPayItemname) {
        this.tmpPayItemname = tmpPayItemname;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.payItemname);
        hash = 23 * hash + Objects.hashCode(this.corporate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PayItem other = (PayItem) obj;
        if (!Objects.equals(this.payItemname, other.payItemname)) {
            return false;
        }
        if (!Objects.equals(this.corporate, other.corporate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return payItemname;
    }

    
   
}
