/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKERSCATEGORY")
public class WorkersCategory implements Serializable {

    @Id
    @Size(min = 6,max = 30)
    @Column(name = "ID", length = 30)
    private String id;
    @Column(name = "CATEGORY_DESC")
    private String description;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.TRUE;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive = Boolean.TRUE;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @Column(name = "GROUP_NUMENCLATURE")
    private String groupNaming = "Grade";
    @Column(name = "TMP_GROUP_NUMENCLATURE")
    private String tmpGroupNaming;
      @Column(name = "GROUP_STEP_NUMENCLATURE")
    private String groupStepNaming = "Step";
      @Column(name = "TMP_GROUPSTEP_NUMENCLATURE")
    private String tmpGroupStepNaming;
    @JoinColumn(name = "CORPORATE_ID")
    @ManyToOne
    private Corporate corporate;
    @JoinColumn(name = "TMP_CORPORATE_ID")
    @ManyToOne
    private Corporate tmpCorporate;
      @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Transient
    private String dateCreatedStr;
      //@Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(mappedBy = "workersCategory", fetch = FetchType.EAGER)
    private List<WorkersCategoryGroup> workersCategoryGroups = new ArrayList<>();
      @Transient
      private String status;
      
        /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED_NOT_SETUP)
                                                        ? AppConstants.CREATED_NOT_SETUP_TEXT : flagStatus.equals(AppConstants.SETUP_COMPLETED)
                                                        ? AppConstants.SETUP_COMPLETED_TEXT:flagStatus.equals(AppConstants.SET_UP_IN_PROGRESS)
                                                        ? AppConstants.SET_UP_IN_PROGRESS_TEXT: "" : "";
        return status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the tmpCorporate
     */
    public Corporate getTmpCorporate() {
        return tmpCorporate;
    }

    /**
     * @param tmpCorporate the tmpCorporate to set
     */
    public void setTmpCorporate(Corporate tmpCorporate) {
        this.tmpCorporate = tmpCorporate;
    }

    /**
     * @return the groupNaming
     */
    public String getGroupNaming() {
        return groupNaming;
    }

    /**
     * @param groupNaming the groupNaming to set
     */
    public void setGroupNaming(String groupNaming) {
        this.groupNaming = groupNaming;
    }

    /**
     * @return the tmpGroupNaming
     */
    public String getTmpGroupNaming() {
        return tmpGroupNaming;
    }

    /**
     * @param tmpGroupNaming the tmpGroupNaming to set
     */
    public void setTmpGroupNaming(String tmpGroupNaming) {
        this.tmpGroupNaming = tmpGroupNaming;
    }

    /**
     * @return the workersCategoryGroups
     */
    public List<WorkersCategoryGroup> getWorkersCategoryGroups() {
        return workersCategoryGroups;
    }

    /**
     * @param workersCategoryGroups the workersCategoryGroups to set
     */
    public void setWorkersCategoryGroups(List<WorkersCategoryGroup> workersCategoryGroups) {
        this.workersCategoryGroups = workersCategoryGroups;
    }

    /**
     * @return the groupStepNaming
     */
    public String getGroupStepNaming() {
        return groupStepNaming;
    }

    /**
     * @param groupStepNaming the groupStepNaming to set
     */
    public void setGroupStepNaming(String groupStepNaming) {
        this.groupStepNaming = groupStepNaming;
    }

    /**
     * @return the tmpGroupStepNaming
     */
    public String getTmpGroupStepNaming() {
        return tmpGroupStepNaming;
    }

    /**
     * @param tmpGroupStepNaming the tmpGroupStepNaming to set
     */
    public void setTmpGroupStepNaming(String tmpGroupStepNaming) {
        this.tmpGroupStepNaming = tmpGroupStepNaming;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the dateCreatedStr
     */
    public String getDateCreatedStr() {
               dateCreatedStr = AppUtil.formatDate(dateCreated == null ? new Date() : dateCreated, "EEE, d MMM yyyy HH:mm:ss");
        return dateCreatedStr;
    }

    @Override
    public String toString() {
        return "WorkersCategory{" + "id=" + id + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WorkersCategory other = (WorkersCategory) obj;
          if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    

    
}
