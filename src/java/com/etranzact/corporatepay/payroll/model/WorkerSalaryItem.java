/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKER_SALARY_ITEM")
//@Audited
public class WorkerSalaryItem implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "SALARY_ID")
    private Long Id;
    @ManyToOne
    @JoinColumn(name = "WORKER_ID")
    private Worker worker;
    @ManyToOne
    @JoinColumn(name = "PAYROLL_PERIOD")
    private PayrollPeriod payrollPeriod;
    @Column(name = "AMOUNT")
    private BigDecimal amount = new BigDecimal("0.0");
    @Column(name = "item_id")
    private Long itemId;
    @Column(name = "item_type")
    private String itemType;
        @Column(name = "item_name")
    private String itemName;
    @Column(name = "pay_effect")
    private Integer effect;
    

    /**
     * @return the payrollPeriod
     */
    public PayrollPeriod getPayrollPeriod() {
        return payrollPeriod;
    }

    /**
     * @param payrollPeriod the payrollPeriod to set
     */
    public void setPayrollPeriod(PayrollPeriod payrollPeriod) {
        this.payrollPeriod = payrollPeriod;
    }

    /**
     * @return the Id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     * @return the worker
     */
    public Worker getWorker() {
        return worker;
    }

    /**
     * @param worker the worker to set
     */
    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the itemId
     */
    public Long getItemId() {
        return itemId;
    }

    /**
     * @param itemId the itemId to set
     */
    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    /**
     * @return the itemType
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return the effect
     */
    public Integer getEffect() {
        return effect;
    }

    /**
     * @param effect the effect to set
     */
    public void setEffect(Integer effect) {
        this.effect = effect;
    }

    /**
     * @return the itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * @param itemName the itemName to set
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    
   
}
