/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.model;

import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateBeneficiary;
import com.etranzact.corporatepay.model.PFA;
import com.etranzact.corporatepay.model.Tax;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.envers.Audited;
import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

/**
 *
 * @author Emmanuel.Kpoudosu
 */
@Entity
@Table(name = "COP_PR_WORKER")
//@Audited
public class Worker implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;
    @Column(name = "STAFF_ID")
    private String staffId;
       @Column(name = "GROSS_SALARY")
    private BigDecimal grossSalary;
    @ManyToOne
    @JoinColumn(name = "WORKERS_CATEGORY_ID")
    //@Audited(targetAuditMode = NOT_AUDITED)
    private WorkersCategory workersCategory;
    @ManyToOne
    @JoinColumn(name = "WORKERS_CATEGORY_GROUPSTEP_ID")
    //@Audited(targetAuditMode = NOT_AUDITED)
    private WorkersCategoryGroupStep workersCategoryGroupStep;
    @ManyToOne
    @JoinColumn(name = "TMP_WORKERS_CATEGORY_ID")
    //@Audited(targetAuditMode = NOT_AUDITED)
    private WorkersCategory tmpWorkersCategory;
    @ManyToOne
    @JoinColumn(name = "TMP_WORKERS_CATEGORY_GROUPSTEP_ID")
    //@Audited(targetAuditMode = NOT_AUDITED)
    private WorkersCategoryGroupStep tmpWorkersCategoryGroupStep;
    @ManyToOne
    @JoinColumn(name = "BENEFICIARY_ID")
    private CorporateBeneficiary corporateBeneficiary = new CorporateBeneficiary();
    @ManyToOne
    @JoinColumn(name = "TMP_BENEFICIARY_ID")
    private CorporateBeneficiary tmpCorporateBeneficiary;
    @Column(name = "ACTIVE")
    private Boolean active = Boolean.FALSE;
    @Column(name = "F_NAME")
    private String firstName;
    @Column(name = "L_NAME")
    private String lastName;
    @Column(name = "TMP_F_NAME")
    private String tmpFirstName;
    @Column(name = "TMP_L_NAME")
    private String tmpLastName;
    @Column(name = "TMP_ACTIVE")
    private Boolean tmpActive;
    @Column(name = "FLAG_STATUS")
    private String flagStatus;
    @JoinColumn(name = "PAYEE_AUTH_ID")
    @ManyToOne
    private Tax tax;
    @JoinColumn(name = "TMP_PAYEE_AUTH_ID")
    @ManyToOne
    private Tax tmpTax;
    @JoinColumn(name = "PFA_AUTH_ID")
    @ManyToOne
    private PFA pfa;
    @JoinColumn(name = "TMP_PFA_AUTH_ID")
    @ManyToOne
    private PFA tmpPfa;
    @Transient
    private String dateCreatedStr;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED")
    private Date dateCreated = new Date();
    @Column(name = "W_STATE")
    private String wstate;
      @Column(name = "TMP_WSTATE")
    private String tmpWstate;
    @Transient
    private String stateLabel;
    
        @Transient
    private String status;

    /**
     * @return the status
     */
    public String getStatus() {
        status = flagStatus != null ? flagStatus.equals(AppConstants.APPROVED) ? AppConstants.APPROVED_TEXT
                : flagStatus.equals(AppConstants.REJECTED) ? AppConstants.REJECTED_TEXT : flagStatus.equals(AppConstants.CREATED)
                                ? AppConstants.CREATED_TEXT : flagStatus.equals(AppConstants.MODIFIED) ? AppConstants.MODIFIED_TEXT
                                        : flagStatus.equals(AppConstants.CREATED_REJECTED) ? AppConstants.CREATED_REJECTED_TEXT : flagStatus.equals(AppConstants.MODIFIED_REJECTED)
                                                        ? AppConstants.MODIFIED_REJECTED_TEXT : "" : "";
        return status;
    }

    /**
     * @return the dateCreatedStr
     */
    public String getDateCreatedStr() {
        dateCreatedStr = AppUtil.formatDate(getDateCreated() == null ? new Date() : getDateCreated(), "EEE, d MMM yyyy HH:mm:ss");
        return dateCreatedStr;
    }

    /**
     * @return the workersCategory
     */
    public WorkersCategory getWorkersCategory() {
        return workersCategory;
    }

    /**
     * @param workersCategory the workersCategory to set
     */
    public void setWorkersCategory(WorkersCategory workersCategory) {
        this.workersCategory = workersCategory;
    }

    /**
     * @return the active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return the tmpActive
     */
    public Boolean getTmpActive() {
        return tmpActive;
    }

    /**
     * @param tmpActive the tmpActive to set
     */
    public void setTmpActive(Boolean tmpActive) {
        this.tmpActive = tmpActive;
    }

    /**
     * @return the flagStatus
     */
    public String getFlagStatus() {
        return flagStatus;
    }

    /**
     * @param flagStatus the flagStatus to set
     */
    public void setFlagStatus(String flagStatus) {
        this.flagStatus = flagStatus;
    }

    /**
     * @return the tmpWorkersCategory
     */
    public WorkersCategory getTmpWorkersCategory() {
        return tmpWorkersCategory;
    }

    /**
     * @param tmpWorkersCategory the tmpWorkersCategory to set
     */
    public void setTmpWorkersCategory(WorkersCategory tmpWorkersCategory) {
        this.tmpWorkersCategory = tmpWorkersCategory;
    }

   

    /**
     * @return the tax
     */
    public Tax getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(Tax tax) {
        this.tax = tax;
    }

    /**
     * @return the tmpTax
     */
    public Tax getTmpTax() {
        return tmpTax;
    }

    /**
     * @param tmpTax the tmpTax to set
     */
    public void setTmpTax(Tax tmpTax) {
        this.tmpTax = tmpTax;
    }

    /**
     * @return the pfa
     */
    public PFA getPfa() {
        return pfa;
    }

    /**
     * @param pfa the pfa to set
     */
    public void setPfa(PFA pfa) {
        this.pfa = pfa;
    }

    /**
     * @return the tmpPfa
     */
    public PFA getTmpPfa() {
        return tmpPfa;
    }

    /**
     * @param tmpPfa the tmpPfa to set
     */
    public void setTmpPfa(PFA tmpPfa) {
        this.tmpPfa = tmpPfa;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the corporateBeneficiary
     */
    public CorporateBeneficiary getCorporateBeneficiary() {
        return corporateBeneficiary;
    }

    /**
     * @param corporateBeneficiary the corporateBeneficiary to set
     */
    public void setCorporateBeneficiary(CorporateBeneficiary corporateBeneficiary) {
        this.corporateBeneficiary = corporateBeneficiary;
    }

    /**
     * @return the workersCategoryGroupStep
     */
    public WorkersCategoryGroupStep getWorkersCategoryGroupStep() {
        return workersCategoryGroupStep;
    }

    /**
     * @param workersCategoryGroupStep the workersCategoryGroupStep to set
     */
    public void setWorkersCategoryGroupStep(WorkersCategoryGroupStep workersCategoryGroupStep) {
        this.workersCategoryGroupStep = workersCategoryGroupStep;
    }

    /**
     * @return the tmpWorkersCategoryGroupStep
     */
    public WorkersCategoryGroupStep getTmpWorkersCategoryGroupStep() {
        return tmpWorkersCategoryGroupStep;
    }

    /**
     * @param tmpWorkersCategoryGroupStep the tmpWorkersCategoryGroupStep to set
     */
    public void setTmpWorkersCategoryGroupStep(WorkersCategoryGroupStep tmpWorkersCategoryGroupStep) {
        this.tmpWorkersCategoryGroupStep = tmpWorkersCategoryGroupStep;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the tmpFirstName
     */
    public String getTmpFirstName() {
        return tmpFirstName;
    }

    /**
     * @param tmpFirstName the tmpFirstName to set
     */
    public void setTmpFirstName(String tmpFirstName) {
        this.tmpFirstName = tmpFirstName;
    }

    /**
     * @return the tmpLastName
     */
    public String getTmpLastName() {
        return tmpLastName;
    }

    /**
     * @param tmpLastName the tmpLastName to set
     */
    public void setTmpLastName(String tmpLastName) {
        this.tmpLastName = tmpLastName;
    }

    /**
     * @return the staffId
     */
    public String getStaffId() {
        return staffId;
    }

    /**
     * @param staffId the staffId to set
     */
    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    /**
     * @return the tmpCorporateBeneficiary
     */
    public CorporateBeneficiary getTmpCorporateBeneficiary() {
        return tmpCorporateBeneficiary;
    }

    /**
     * @param tmpCorporateBeneficiary the tmpCorporateBeneficiary to set
     */
    public void setTmpCorporateBeneficiary(CorporateBeneficiary tmpCorporateBeneficiary) {
        this.tmpCorporateBeneficiary = tmpCorporateBeneficiary;
    }

  

    /**
     * @return the stateLabel
     */
    public String getStateLabel() {
        stateLabel = wstate==null?"":wstate.equals(AppConstants.SUSPEND_WORKER)?"Suspended":wstate.equals(AppConstants.TERMINATE_WORKER)?"Terminated":"";
        return stateLabel;
    }

   
    /**
     * @return the wstate
     */
    public String getWstate() {
        return wstate;
    }

    /**
     * @param wstate the wstate to set
     */
    public void setWstate(String wstate) {
        this.wstate = wstate;
    }

    /**
     * @return the tmpWstate
     */
    public String getTmpWstate() {
        return tmpWstate;
    }

    /**
     * @param tmpWstate the tmpWstate to set
     */
    public void setTmpWstate(String tmpWstate) {
        this.tmpWstate = tmpWstate;
    }

    /**
     * @return the grossSalary
     */
    public BigDecimal getGrossSalary() {
        return grossSalary;
    }

    /**
     * @param grossSalary the grossSalary to set
     */
    public void setGrossSalary(BigDecimal grossSalary) {
        this.grossSalary = grossSalary;
    }



}
