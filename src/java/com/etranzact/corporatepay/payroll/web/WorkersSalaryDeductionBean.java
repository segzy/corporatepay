/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.model.LoanDeduction;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OtherDeduction;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.RecurringAddition;
import com.etranzact.corporatepay.payroll.model.RecurringDeduction;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersAddition;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.event.CloseEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "workersSalaryDeductionBean")
@SessionScoped
public class WorkersSalaryDeductionBean extends AbstractBean<WorkersSalaryDeduction> {

    @EJB
    private WorkersSalaryDeductionFacadeLocal workersSalaryDeductionFacadeLocal;
    private List<? extends OtherDeduction> otherDeductions;
    private List<? extends WorkersSalaryDeduction> workersSalaryDeductions;

    @Override
    protected WorkersSalaryDeductionFacadeLocal getFacade() {
        return workersSalaryDeductionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public WorkersSalaryDeductionBean() {
        super(WorkersSalaryDeduction.class);
    }
    
     public void onDialogClose(CloseEvent event) {
         selectedItem = new WorkersSalaryDeduction();
         otherDeductions = new ArrayList<>();
         workersSalaryDeductions = new ArrayList<>();
     }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            OtherDeduction otherDeduction = selectedItem.getOtherDeduction();
            PayrollPeriod incluPayrollPeriod = selectedItem.getIncluPayrollPeriod();
            Worker worker = selectedItem.getWorker();
            if(selectedItem.getId()==null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                getFacade().create(selectedItem);
            } else {
                  selectedItem.setFlagStatus(AppConstants.MODIFIED);
                getFacade().edit(selectedItem); 
            }
          Map  map = new HashMap<>();
        map.put("worker", selectedItem.getWorker());
        List<WorkersSalaryDeduction> result;
     
        try {
             result = workersSalaryDeductionFacadeLocal.findAll(map);
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            result = new ArrayList<>();
        }
        
        if(selectedItem instanceof RecurringWorkersDeduction) {
                     List<RecurringWorkersDeduction> recurringWorkersDeductions = new ArrayList<>();
           for(WorkersSalaryDeduction workersSalaryDeduction: result) {
               if(!(workersSalaryDeduction instanceof RecurringWorkersDeduction)) {
                   continue;
               }
               recurringWorkersDeductions.add((RecurringWorkersDeduction)workersSalaryDeduction);
           }
           workersSalaryDeductions = recurringWorkersDeductions;
           selectedItem = new RecurringWorkersDeduction();
           selectedItem.setOtherDeduction(otherDeduction);
           selectedItem.setIncluPayrollPeriod(incluPayrollPeriod);
           selectedItem.setWorker(worker);
        } else if(selectedItem instanceof OneTimeWorkersDeduction) {
                     List<OneTimeWorkersDeduction> oneTimeWorkersDeductions = new ArrayList<>();
           for(WorkersSalaryDeduction workersSalaryDeduction: result) {
               if(!(workersSalaryDeduction instanceof OneTimeWorkersDeduction)) {
                   continue;
               }
               oneTimeWorkersDeductions.add((OneTimeWorkersDeduction)workersSalaryDeduction);
           }
           workersSalaryDeductions = oneTimeWorkersDeductions;
                           selectedItem = new OneTimeWorkersDeduction();
                              selectedItem.setOtherDeduction(otherDeduction);
           selectedItem.setIncluPayrollPeriod(incluPayrollPeriod);
           selectedItem.setWorker(worker);
        } else if(selectedItem instanceof LoanWorkersDeduction) {
                     List<LoanWorkersDeduction> loanWorkersDeductions = new ArrayList<>();
           for(WorkersSalaryDeduction workersSalaryDeduction: result) {
               if(!(workersSalaryDeduction instanceof LoanWorkersDeduction)) {
                   continue;
               }
               loanWorkersDeductions.add((LoanWorkersDeduction)workersSalaryDeduction);
           }
           workersSalaryDeductions = loanWorkersDeductions;
                    selectedItem = new LoanWorkersDeduction();
                       selectedItem.setOtherDeduction(otherDeduction);
           selectedItem.setIncluPayrollPeriod(incluPayrollPeriod);
             selectedItem.setWorker(worker);
        }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Worker Salary Deduction Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
          
        } catch (Exception ex) {
            Logger.getLogger(WorkersSalaryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }
    
       public void deductionTypeChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        Object input = ((UISelectOne) e.getComponent()).getValue();
        log.info("selected ......... " + input);
            log.info("selected ......... " + input);
        if (input instanceof RecurringDeduction) {
            ((RecurringWorkersDeduction) selectedItem).setAmount(((RecurringDeduction) input).getAmount());
            ((RecurringWorkersDeduction) selectedItem).setAmountType(((RecurringDeduction) input).getAmountType());
            ((RecurringWorkersDeduction) selectedItem).setIndefinite(((RecurringDeduction) input).getIndefinite());
            ((RecurringWorkersDeduction) selectedItem).setNoOfOcurrences(((RecurringDeduction) input).getNoOfOcurrences());
        } else if (input instanceof OneTimeDeduction) {
            ((OneTimeWorkersDeduction) selectedItem).setAmount(((OneTimeDeduction) input).getAmount());
            ((OneTimeWorkersDeduction) selectedItem).setAmountType(((OneTimeDeduction) input).getAmountType());
        } else if (input instanceof LoanDeduction) {
            ((LoanWorkersDeduction) selectedItem).setAmount(((LoanDeduction) input).getAmount());
            ((LoanWorkersDeduction) selectedItem).setDeductionAmount(((LoanDeduction) input).getDeductionAmount());
        }
    }
    
    public void nullifySelection() {
            OtherDeduction otherDeduction = selectedItem.getOtherDeduction();
            PayrollPeriod incluPayrollPeriod = selectedItem.getIncluPayrollPeriod();
            Worker worker = selectedItem.getWorker();
             if(selectedItem instanceof RecurringWorkersDeduction) {
                selectedItem = new RecurringWorkersDeduction();
            } else if(selectedItem instanceof OneTimeWorkersDeduction) {
                selectedItem = new OneTimeWorkersDeduction();
            } else if(selectedItem instanceof LoanWorkersDeduction) {
                selectedItem = new LoanWorkersDeduction();
            }
               selectedItem.setIncluPayrollPeriod(incluPayrollPeriod);
                selectedItem.setWorker(worker);
                selectedItem.setOtherDeduction(otherDeduction);
    }
    
    public void doDeductionStatusChange() {
        selectedItem.setActive(!selectedItem.getActive());
        save();
    }

    /**
     * @return the otherDeductions
     */
    public List<? extends OtherDeduction> getOtherDeductions() {
        return otherDeductions;
    }

    /**
     * @param otherDeductions the otherDeductions to set
     */
    public void setOtherDeductions(List<? extends OtherDeduction> otherDeductions) {
        this.otherDeductions = otherDeductions;
    }

    /**
     * @return the workersSalaryDeductions
     */
    public List<? extends WorkersSalaryDeduction> getWorkersSalaryDeductions() {
        return workersSalaryDeductions;
    }

    /**
     * @param workersSalaryDeductions the workersSalaryDeductions to set
     */
    public void setWorkersSalaryDeductions(List<? extends WorkersSalaryDeduction> workersSalaryDeductions) {
        this.workersSalaryDeductions = workersSalaryDeductions;
    }

    /**
     * @param workersSalaryDeductionFacadeLocal the workersSalaryDeductionFacadeLocal to set
     */
    public void setWorkersSalaryDeductionFacadeLocal(WorkersSalaryDeductionFacadeLocal workersSalaryDeductionFacadeLocal) {
        this.workersSalaryDeductionFacadeLocal = workersSalaryDeductionFacadeLocal;
    }

    

}
