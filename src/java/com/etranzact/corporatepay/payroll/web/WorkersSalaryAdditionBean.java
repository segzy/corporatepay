/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryAdditionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OtherAddition;
import com.etranzact.corporatepay.payroll.model.OtherDeduction;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.RecurringAddition;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersAddition;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryAddition;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.event.CloseEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "workersSalaryAdditionBean")
@SessionScoped
public class WorkersSalaryAdditionBean extends AbstractBean<WorkersSalaryAddition> {

    @EJB
    private WorkersSalaryAdditionFacadeLocal workersSalaryAdditionFacadeLocal;
    private List<? extends OtherAddition> otherAdditions;
    private List<? extends WorkersSalaryAddition> workersSalaryAdditions;

    @Override
    protected WorkersSalaryAdditionFacadeLocal getFacade() {
        return workersSalaryAdditionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public WorkersSalaryAdditionBean() {
        super(WorkersSalaryAddition.class);
    }

    public void onDialogClose(CloseEvent event) {
        selectedItem = new WorkersSalaryAddition();
        otherAdditions = new ArrayList<>();
        workersSalaryAdditions = new ArrayList<>();
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            OtherAddition otherAddition = selectedItem.getOtherAddition();
            PayrollPeriod incluPayrollPeriod = selectedItem.getIncluPayrollPeriod();
            Worker worker = selectedItem.getWorker();
            log.info("worker::::: " + worker);
            if (selectedItem.getId() == null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                getFacade().create(selectedItem);
            } else {
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                getFacade().edit(selectedItem);
            }
            Map map = new HashMap<>();
            map.put("worker", selectedItem.getWorker());
            List<WorkersSalaryAddition> result;

            try {
                result = workersSalaryAdditionFacadeLocal.findAll(map);
            } catch (Exception ex) {
                Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
                result = new ArrayList<>();
            }

            if (selectedItem instanceof RecurringWorkersAddition) {
                List<RecurringWorkersAddition> recurringWorkersAdditions = new ArrayList<>();
                for (WorkersSalaryAddition workersSalaryDeduction : result) {
                    if (!(workersSalaryDeduction instanceof RecurringWorkersAddition)) {
                        continue;
                    }
                    recurringWorkersAdditions.add((RecurringWorkersAddition) workersSalaryDeduction);
                }
                workersSalaryAdditions = recurringWorkersAdditions;
                selectedItem = new RecurringWorkersAddition();
                selectedItem.setOtherAddition(otherAddition);
                selectedItem.setIncluPayrollPeriod(incluPayrollPeriod);
                selectedItem.setWorker(worker);
            } else if (selectedItem instanceof OneTimeWorkersAddition) {
                List<OneTimeWorkersAddition> oneTimeWorkersAdditions = new ArrayList<>();
                for (WorkersSalaryAddition workersSalaryAddition : result) {
                    if (!(workersSalaryAddition instanceof OneTimeWorkersAddition)) {
                        continue;
                    }
                    oneTimeWorkersAdditions.add((OneTimeWorkersAddition) workersSalaryAddition);
                }
                workersSalaryAdditions = oneTimeWorkersAdditions;
                selectedItem = new OneTimeWorkersAddition();
                selectedItem.setOtherAddition(otherAddition);
                selectedItem.setIncluPayrollPeriod(incluPayrollPeriod);
                selectedItem.setWorker(worker);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Worker Salary Addition Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);

        } catch (Exception ex) {
            Logger.getLogger(WorkersSalaryAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    public void nullifySelection() {
        OtherAddition otherAddition = selectedItem.getOtherAddition();
        PayrollPeriod incluPayrollPeriod = selectedItem.getIncluPayrollPeriod();
        Worker worker = selectedItem.getWorker();
        if (selectedItem instanceof RecurringWorkersAddition) {
            selectedItem = new RecurringWorkersAddition();
        } else if (selectedItem instanceof OneTimeWorkersAddition) {
            selectedItem = new OneTimeWorkersAddition();
        }
        selectedItem.setIncluPayrollPeriod(incluPayrollPeriod);
        selectedItem.setWorker(worker);
        selectedItem.setOtherAddition(otherAddition);
    }

    public void additionTypeChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        Object input = ((UISelectOne) e.getComponent()).getValue();
        if (input instanceof RecurringAddition) {
            ((RecurringWorkersAddition) selectedItem).setAmount(((RecurringAddition) input).getAmount());
            ((RecurringWorkersAddition) selectedItem).setAmountType(((RecurringAddition) input).getAmountType());
            ((RecurringWorkersAddition) selectedItem).setIndefinite(((RecurringAddition) input).getIndefinite());
            ((RecurringWorkersAddition) selectedItem).setNoOfOcurrences(((RecurringAddition) input).getNoOfOcurrences());
        } else if (input instanceof OneTimeAddition) {
            ((OneTimeWorkersAddition) selectedItem).setAmount(((OneTimeAddition) input).getAmount());
            ((OneTimeWorkersAddition) selectedItem).setAmountType(((OneTimeAddition) input).getAmountType());
        }
    }

    public void doAdditionStatusChange() {
        selectedItem.setActive(!selectedItem.getActive());
        save();
    }

    /**
     * @return the otherDeductions
     */
    public List<? extends OtherAddition> getOtherAdditions() {
        return otherAdditions;
    }

    /**
     * @param otherAdditions the otherDeductions to set
     */
    public void setOtherAdditions(List<? extends OtherAddition> otherAdditions) {
        this.otherAdditions = otherAdditions;
    }

    /**
     * @return the workersSalaryDeductions
     */
    public List<? extends WorkersSalaryAddition> getWorkersSalaryAdditions() {
        return workersSalaryAdditions;
    }

    /**
     * @param workersSalaryAdditions the workersSalaryAdditions to set
     */
    public void setWorkersSalaryAdditions(List<? extends WorkersSalaryAddition> workersSalaryAdditions) {
        this.workersSalaryAdditions = workersSalaryAdditions;
    }

    /**
     * @param workersSalaryAdditionFacadeLocal the
     * workersSalaryAdditionFacadeLocal to set
     */
    public void setWorkersSalaryAdditionFacadeLocal(WorkersSalaryAdditionFacadeLocal workersSalaryAdditionFacadeLocal) {
        this.workersSalaryAdditionFacadeLocal = workersSalaryAdditionFacadeLocal;
    }

}
