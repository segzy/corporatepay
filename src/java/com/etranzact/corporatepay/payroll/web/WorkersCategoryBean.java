/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.facade.PayItemFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.PayrollPeriodFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.StatutoryPayComponentFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryGroupFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryGroupStepFacadeLocal;
import com.etranzact.corporatepay.payroll.model.PayItem;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.StatutoryPayComponent;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroup;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroupStep;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "workersCategoryBean")
@SessionScoped
public class WorkersCategoryBean extends AbstractBean<WorkersCategory> {

    @EJB
    private WorkersCategoryFacadeLocal workersCategoryFacadeLocal;
    @EJB
    private WorkersCategoryGroupFacadeLocal workersCategoryGroupFacadeLocal;
    @EJB
    private WorkersCategoryGroupStepFacadeLocal workersCategoryGroupStepFacadeLocal;
    @EJB
    private StatutoryPayComponentFacadeLocal statutoryPayComponentFacadeLocal;
    @EJB
    private PayItemFacadeLocal payItemFacadeLocal;
    private WorkersCategoryGroup workersCategoryGroup;
    private WorkersCategoryGroupStep workersCategoryGroupStep;
    private List<WorkersCategoryGroupStep> workersCategoryGroupSteps;
    private Set<StatutoryPayComponent> statPaycomponents;
    private PayItem payItem;
    private static final String MONTHLY_BASIC = "Monthly Basic";

    @Override
    protected WorkersCategoryFacadeLocal getFacade() {
        return workersCategoryFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public WorkersCategoryBean() {
        super(WorkersCategory.class);
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            if (selectedItem.getFlagStatus() == null) {
                Map<String, Object> map = new HashMap<>();
                User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(AppConstants.LOGIN_USER);
                if (user instanceof CorporateUser) {
                    map.put("corporate", ((CorporateUser) user).getCorporate());
                }
                map.put("id", selectedItem.getId());
                List<WorkersCategory> workersCategorys = workersCategoryFacadeLocal.findAll(map, true);
                if (!workersCategorys.isEmpty()) {
                    throw new Exception("Workers Category Exist");
                }
            }
            selectedItem.setCorporate(getLoggedCorporate());
            selectedItem.setFlagStatus(AppConstants.CREATED_NOT_SETUP);
            if (selectedItem.getFlagStatus() == null) {
                getFacade().create(selectedItem);
            } else {
                getFacade().create(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Workers Category Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new WorkersCategory();
        }

    }

    public void removeCategoryGroup(WorkersCategoryGroup workersCategoryGroup) {
        FacesMessage m = new FacesMessage();
        try {
            workersCategoryGroupFacadeLocal.remove(workersCategoryGroup);
            setSelectedItem(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Workers Category Group removed");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void saveCategoryGroup() {
        FacesMessage m = new FacesMessage();
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("groupLevel", workersCategoryGroup.getGroupLevel());
            map.put("corporate", getLoggedCorporate());
            map.put("workersCategory", selectedItem);
            List<WorkersCategoryGroup> ls = workersCategoryGroupFacadeLocal.findAll(map, true);
            if (!ls.isEmpty()) {
                throw new Exception("Group already exist");
            }
            workersCategoryGroup.setCorporate(getLoggedCorporate());
            workersCategoryGroup.setWorkersCategory(selectedItem);
            WorkersCategoryGroup wcCategoryGroup = workersCategoryGroupFacadeLocal.create(workersCategoryGroup);
            for (int i = 1; i <= workersCategoryGroup.getStepNo(); i++) {
                WorkersCategoryGroupStep step = new WorkersCategoryGroupStep();
                step.setGroupStepLevel(i);
                step.setWorkersCategoryGroup(wcCategoryGroup);
                workersCategoryGroupStepFacadeLocal.create(step);
            }
            selectedItem.setFlagStatus(AppConstants.SET_UP_IN_PROGRESS);
            selectedItem = workersCategoryFacadeLocal.create(selectedItem);
            selectedItem.getWorkersCategoryGroups().add(wcCategoryGroup);

            setSelectedItem(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Workers Category Group added");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            workersCategoryGroup = new WorkersCategoryGroup();
        }

    }

    public void onCloseWCGroupDialog(CloseEvent event) {
        selectedItem = new WorkersCategory();
    }

    /**
     * @return the workersCategoryGroup
     */
    public WorkersCategoryGroup getWorkersCategoryGroup() {
        workersCategoryGroup = workersCategoryGroup == null ? new WorkersCategoryGroup() : workersCategoryGroup;
        return workersCategoryGroup;
    }

    /**
     * @param workersCategoryGroup the workersCategoryGroup to set
     */
    public void setWorkersCategoryGroup(WorkersCategoryGroup workersCategoryGroup) {
        this.workersCategoryGroup = workersCategoryGroup;
    }

    /**
     * @param selected
     *
     */
    @Override
    public void setSelectedItem(WorkersCategory selected) {
        try {
            if (selected != null) {
                selectedItem = workersCategoryFacadeLocal.find(selected.getId());
                log.log(Level.INFO, "selectedItem: {0}", selectedItem);
                List<WorkersCategoryGroup> workersCategoryGroups = selectedItem.getWorkersCategoryGroups();

                Comparator<WorkersCategoryGroup> c = new Comparator<WorkersCategoryGroup>() {

                    @Override
                    public int compare(WorkersCategoryGroup o1, WorkersCategoryGroup o2) {
                        return o1.getGroupLevel() - o2.getGroupLevel();
                    }
                };
                Collections.sort(workersCategoryGroups, c);
                selectedItem.setWorkersCategoryGroups(workersCategoryGroups);
                for (WorkersCategoryGroup workersCategoryGroup : workersCategoryGroups) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("workersCategoryGroup", workersCategoryGroup);
                    workersCategoryGroup.setWorkersCategoryGroupSteps(workersCategoryGroupStepFacadeLocal.findAll(map, true));
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the workersCategoryGroupSteps
     */
    public List<WorkersCategoryGroupStep> getWorkersCategoryGroupSteps() {
        try {
            if (selectedItem.getId() != null) {
                selectedItem = workersCategoryFacadeLocal.find(selectedItem.getId());
                List<WorkersCategoryGroup> workersCategoryGroups = selectedItem.getWorkersCategoryGroups();
                //   log.log(Level.INFO, "workersCategoryGroups: {0}", workersCategoryGroups.size());
                Comparator<WorkersCategoryGroup> c = new Comparator<WorkersCategoryGroup>() {

                    @Override
                    public int compare(WorkersCategoryGroup o1, WorkersCategoryGroup o2) {
                        return o1.getGroupLevel() - o2.getGroupLevel();
                    }
                };
                Collections.sort(workersCategoryGroups, c);
                selectedItem.setWorkersCategoryGroups(workersCategoryGroups);
                workersCategoryGroupSteps = new ArrayList<>();
                for (WorkersCategoryGroup workersCategoryGroup : workersCategoryGroups) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("workersCategoryGroup", workersCategoryGroup);
                    workersCategoryGroupSteps.addAll(workersCategoryGroupStepFacadeLocal.findAll(map, true));
                }
                return workersCategoryGroupSteps;
            }
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return workersCategoryGroupSteps;
    }
    
    
        /**
     * @return the workersCategoryGroupSteps
     */
    public List<WorkersCategoryGroupStep> getWorkersCategoryGroupSteps(WorkersCategoryGroup workersCategoryGroup) {
        try {
        
                    Map<String, Object> map = new HashMap<>();
                    map.put("workersCategoryGroup", workersCategoryGroup);
                    return workersCategoryGroupStepFacadeLocal.findAll(map, true);
         
            
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
              return new ArrayList<>();
        }
      
    }

    /**
     * @return the workersCategoryGroupStep
     */
    public WorkersCategoryGroupStep getWorkersCategoryGroupStep() {
        return workersCategoryGroupStep;
    }

    /**
     * @param workersCategoryGroupStep the workersCategoryGroupStep to set
     */
    public void setWorkersCategoryGroupStep(WorkersCategoryGroupStep workersCategoryGroupStep) {
        this.workersCategoryGroupStep = workersCategoryGroupStep;
        if (this.workersCategoryGroupStep != null) {
            this.workersCategoryGroupStep.setStatutoryPayComponents(getStatPaycomponents(this.workersCategoryGroupStep));
        }

    }

    /**
     * @param wcategoryGroupStep
     * @return the statPaycomponents
     */
    public Set<StatutoryPayComponent> getStatPaycomponents(WorkersCategoryGroupStep wcategoryGroupStep) {

        try {
            Map<String, Object> map = new HashMap<>();
            map.put("workersCategoryGroupStep", wcategoryGroupStep);
            statPaycomponents = new HashSet<>();
            statPaycomponents.addAll(statutoryPayComponentFacadeLocal.findAll(map, true));
            map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            List<PayItem> payItems = payItemFacadeLocal.findAll(map, true);
            // log.log(Level.INFO, "payitems found:: {0}", payItems.size());
            long idds = 0l;
            outer:
            for (PayItem payItem : payItems) {
                for (StatutoryPayComponent payComponent : statPaycomponents) {
                    if (payItem.equals(payComponent.getPayItem())) {
                        //      log.log(Level.INFO, "payitem:: {0} alread in....", payItem.getPayItemname());
                        continue outer;
                    }
                }
                StatutoryPayComponent payComponent = new StatutoryPayComponent();
                idds -= 1;
                payComponent.setId(idds);
                payComponent.setFlagStatus(AppConstants.APPROVED);
                payComponent.setCorporate(getLoggedCorporate());
                payComponent.setPayItem(payItem);
                payComponent.setWorkersCategoryGroupStep(wcategoryGroupStep);
                statPaycomponents.add(payComponent);
            }
            return statPaycomponents;
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
            statPaycomponents = new HashSet<>();
        }
        return statPaycomponents;
    }

    /**
     * @param statPaycomponents the statPaycomponents to set
     */
    public void setStatPaycomponents(Set<StatutoryPayComponent> statPaycomponents) {
        this.statPaycomponents = statPaycomponents;
    }

    /**
     * @return the PayItem
     */
    public PayItem getPayItem() {
        return payItem;
    }

    /**
     * @param PayItem the PayItem to set
     */
    public void setPayItem(PayItem PayItem) {
        this.payItem = PayItem;
    }

    public void doAddPayItem() {
        this.payItem = new PayItem();
        this.payItem.setCorporate(getLoggedCorporate());
        this.payItem.setFlagStatus(AppConstants.APPROVED);
    }

    public void addPayItem() {
        FacesMessage m = new FacesMessage();
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            map.put("basic", Boolean.TRUE);
            List<PayItem> payItems = payItemFacadeLocal.findAll(map, true);
            if (payItems.isEmpty()) {
                PayItem basicItem = new PayItem();
                basicItem.setBasic(Boolean.TRUE);
                basicItem.setCorporate(getLoggedCorporate());
                basicItem.setFlagStatus(AppConstants.APPROVED);
                basicItem.setPayItemname(MONTHLY_BASIC);
                payItemFacadeLocal.create(basicItem);
            }
            map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            map.put("payItemname", payItem.getPayItemname());
            boolean payItemExist = !payItemFacadeLocal.findAll(map, true).isEmpty();
            if (payItemExist) {
                throw new Exception("Pay Item already exist");
            }
            payItemFacadeLocal.create(payItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Pay Item Added");
            FacesContext.getCurrentInstance().addMessage(null, m);
            doAddPayItem();
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public List<String> completePayItem(String query) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            map.put("payItemname", query);
            List<PayItem> findAll = payItemFacadeLocal.findAll(map, true);
            List<String> strings = new ArrayList<>();
            for (PayItem payItem : findAll) {
                strings.add(payItem.getPayItemname());
            }
            return strings;
        } catch (Exception ex) {
            Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

//    public void onCellEdit(CellEditEvent event) {
//          Object oldValue = event.getOldValue();
//        Object newValue = event.getNewValue();
//        log.info("old value " + oldValue);
//              log.info("new value " + newValue);
//                   log.info("column name " + event.getColumn().getHeaderText());
//                        log.info("field name " + event.getColumn().getField());
//                             log.info("index " + event.getRowIndex());
//           if(newValue != null && !newValue.equals(oldValue)) {
//               if(newValue instanceof String) {
//                   log.info("column " + event.getColumn().getHeaderText());
//                   if(event.getColumn().getHeaderText().equals("Amount Type")) {
//                       
//                   }
//               } 
//           }
//    }
    public void saveStepTable() {
        FacesMessage m = new FacesMessage();
        boolean exception = false;
        try {
            for (StatutoryPayComponent payComponent : workersCategoryGroupStep.getStatutoryPayComponents()) {
                if (payComponent.getAmount() == null || StringUtils.isEmpty(payComponent.getAmountType())) {
                    continue;
                }
                if (payComponent.getId() < 0l) {
                    payComponent.setId(null);
                }
              
                payComponent.setFlagStatus(AppConstants.APPROVED);
                if (payComponent.getAmount()==null || payComponent.getPayItem().getPayItemname().equals(MONTHLY_BASIC)
                        && payComponent.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT)) {
                    exception = true;

                } 
//                else {
//                    System.out.println("saving......");
//                    statutoryPayComponentFacadeLocal.create(payComponent);
//                }
            }
                workersCategoryGroupStep.setFlagStatus(exception?AppConstants.SET_UP_IN_PROGRESS:AppConstants.SETUP_COMPLETED);
                workersCategoryGroupStepFacadeLocal.create(workersCategoryGroupStep);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(exception ? "Changes Saved excluding but with exceptions" : "All changes Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception e) {
              Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, e);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    public void finalizeGroupSetUp() {
        FacesMessage m = new FacesMessage();
        try {
            for (WorkersCategoryGroupStep categoryGroupStep : workersCategoryGroupSteps) {
                categoryGroupStep = workersCategoryGroupStepFacadeLocal.find(categoryGroupStep.getId());
                if (categoryGroupStep.getFlagStatus()==null||categoryGroupStep.getFlagStatus().equals(AppConstants.CREATED_NOT_SETUP)) {
                    throw new Exception("One or More Steps has not being populated with values");
                }
            }
            selectedItem.setFlagStatus(AppConstants.SETUP_COMPLETED);
            getFacade().create(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Group SetUp Completed");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception e) {
              Logger.getLogger(WorkersCategoryBean.class.getName()).log(Level.SEVERE, null, e);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

}
