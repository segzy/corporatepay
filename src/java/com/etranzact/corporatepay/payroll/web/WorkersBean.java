package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.web.AbstractBean;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.CorporateBeneficiary;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payment.web.PaymentBean;
import com.etranzact.corporatepay.payroll.facade.OtherAdditionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.OtherDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.PayrollPeriodFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryAdditionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersSalaryDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.model.LoanDeduction;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OtherAddition;
import com.etranzact.corporatepay.payroll.model.OtherDeduction;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.RecurringAddition;
import com.etranzact.corporatepay.payroll.model.RecurringDeduction;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersAddition;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryAddition;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.component.selectoneradio.SelectOneRadio;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

@ManagedBean(name = "workersBean")
@SessionScoped
public class WorkersBean extends AbstractBean<Worker> {

    @EJB
    private WorkersFacadeLocal workersFacadeLocal;

    @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;

    @EJB
    private OtherDeductionFacadeLocal otherDeductionFacadeLocal;

    @EJB
    private WorkersSalaryDeductionFacadeLocal workersSalaryDeductionFacadeLocal;

    @EJB
    private WorkersSalaryAdditionFacadeLocal workersSalaryAdditionFacadeLocal;

    @EJB
    private OtherAdditionFacadeLocal otherAdditionFacadeLocal;

    @EJB
    private PayrollPeriodFacadeLocal payrollPeriodFacadeLocal;
    private String acctOption = "PM";
    private List<Bank> banks;

    protected WorkersFacadeLocal getFacade() {
        return this.workersFacadeLocal;
    }

    public WorkersBean() {
        super(Worker.class);
    }

    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        try {
            Map m = new HashMap();

            WorkersTableBean workersTableBean = (WorkersTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workersTableBean");

            workersTableBean.setWorkersCategoryChanged(true);
            workersTableBean.setWorkersCategoryFilter(((Worker) this.selectedItem).getWorkersCategory());
            if ((((Worker) this.selectedItem).getCorporateBeneficiary().getAccount() instanceof BankAccount)) {
                this.acctOption = "BN";
                if (getBanks() == null) {
                    try {
                        setBanks(this.bankSetupFacadeLocal.findAll(new HashMap(), true, new String[]{"bankName"}));
                    } catch (Exception ex) {
                        Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
                        setBanks(new ArrayList());
                    }
                }
            } else if ((((Worker) this.selectedItem).getCorporateBeneficiary().getAccount() instanceof PocketMoneyAccount)) {
                this.acctOption = "PM";
            }
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void doValidation() {
        String accountNumber = ((Worker) this.selectedItem).getCorporateBeneficiary().getAccount().getAccountNumber();
        ((Worker) this.selectedItem).getCorporateBeneficiary().getAccount().setAccountName(accountNumber);
    }

    public void handleAcctOptionChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        String option = (String) ((SelectOneRadio) e.getComponent()).getValue();
        if (option.equals("PM")) {
            PocketMoneyAccount pm = new PocketMoneyAccount();
            pm.setType("P");
            ((Worker) this.selectedItem).getCorporateBeneficiary().setAccount(pm);
        } else if (option.equals("BN")) {
            ((Worker) this.selectedItem).getCorporateBeneficiary().setAccount(new BankAccount());
            if (getBanks() == null) {
                try {
                    setBanks(this.bankSetupFacadeLocal.findAll(new HashMap(), true, new String[]{"bankName"}));
                } catch (Exception ex) {
                    Logger.getLogger(PaymentBean.class.getName()).log(Level.SEVERE, null, ex);
                    setBanks(new ArrayList());
                }
            }
        }
        this.acctOption = option;
    }

    public void doWorkerDeductionAction(String deductionType) {
        if ((!((Worker) this.selectedItem).getFlagStatus().equals("AP")) && (!((Worker) this.selectedItem).getFlagStatus().equals("MRJ"))) {
            FacesMessage msg = new FacesMessage();
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary("Cannot Perform Action on Worker: Worker currently undergoing an approval process");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
         if (this.selectedItem.getWstate()!=null && !((Worker) this.selectedItem).getWstate().equals(AppConstants.REINSTATE_WORKER)) {
                FacesMessage msg = new FacesMessage();
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary("Cannot Perform Action on Worker: Worker's appointment has been " + (this.selectedItem.getWstate().equals(AppConstants.SUSPEND_WORKER)?"Suspended":"Terminated"));
            FacesContext.getCurrentInstance().addMessage(null, msg); 
            return;
         }
        WorkersSalaryDeductionBean workersSalaryDeductionBean = (WorkersSalaryDeductionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workersSalaryDeductionBean");
        if (workersSalaryDeductionBean == null) {
            workersSalaryDeductionBean = new WorkersSalaryDeductionBean();
            workersSalaryDeductionBean.setWorkersSalaryDeductionFacadeLocal(this.workersSalaryDeductionFacadeLocal);
        }
        Map m = new HashMap();
        m.put("corporate", getLoggedCorporate());
        List<OtherDeduction> otherDeductions;
        try {
            otherDeductions = this.otherDeductionFacadeLocal.findAll(m, new String[0]);
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            otherDeductions = new ArrayList();
        }
        m = new HashMap();
        m.put("worker", this.selectedItem);
        List<WorkersSalaryDeduction> workersSalaryDeduction;
        try {
            workersSalaryDeduction = this.workersSalaryDeductionFacadeLocal.findAll(m, new String[0]);
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            workersSalaryDeduction = new ArrayList();
        }
        this.log.info("size of all salary deduction " + workersSalaryDeduction.size());
        PayrollPeriod currPayrollPeriod;
        try {
            m = new HashMap();
            m.put("corporate", getLoggedCorporate());
            m.put("flagStatus", "PIP");
            List payrollInProgressPeriods = this.payrollPeriodFacadeLocal.findAll(m, new String[0]);
            if (payrollInProgressPeriods.isEmpty()) {
                throw new Exception("No Current Payroll Period Exist");
            }
            if (payrollInProgressPeriods.size() > 1) {
                throw new Exception("Error determining Current Payroll Period: please contact the Administrator");
            }
            currPayrollPeriod = (PayrollPeriod) payrollInProgressPeriods.get(0);
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage();
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        RequestContext context = RequestContext.getCurrentInstance();
        switch (deductionType) {
            case AppConstants.ONE_TIME_DEDUCTION:
                OneTimeWorkersDeduction oneTimeWorkersDeduction = new OneTimeWorkersDeduction();
                oneTimeWorkersDeduction.setWorker((Worker) this.selectedItem);
                oneTimeWorkersDeduction.setIncluPayrollPeriod(currPayrollPeriod);
                List oneTimeDeductions = new ArrayList();
                for (OtherDeduction otherDeduction : otherDeductions) {
                    if ((otherDeduction instanceof OneTimeDeduction)) {
                        oneTimeDeductions.add((OneTimeDeduction) otherDeduction);
                    }
                }
                List oneTimeWorkersDeductions = new ArrayList();
                for (WorkersSalaryDeduction workersSalaryDeduction1 : workersSalaryDeduction) {
                    if ((workersSalaryDeduction1 instanceof OneTimeWorkersDeduction)) {
                        oneTimeWorkersDeductions.add((OneTimeWorkersDeduction) workersSalaryDeduction1);
                    }
                }
                this.log.info("one time deductions size " + oneTimeWorkersDeductions.size());
                workersSalaryDeductionBean.setSelectedItem(oneTimeWorkersDeduction);
                workersSalaryDeductionBean.setOtherDeductions(oneTimeDeductions);
                workersSalaryDeductionBean.setWorkersSalaryDeductions(oneTimeWorkersDeductions);

                context.execute("PF('onetime-deduction').show();");
                break;
            case AppConstants.RECURRING_DEDUCTION:
                RecurringWorkersDeduction recurringWorkersDeduction = new RecurringWorkersDeduction();
                recurringWorkersDeduction.setWorker((Worker) this.selectedItem);
                recurringWorkersDeduction.setIncluPayrollPeriod(currPayrollPeriod);
                List recurringDeductions = new ArrayList();
                for (OtherDeduction otherDeduction : otherDeductions) {
                    if ((otherDeduction instanceof RecurringDeduction)) {
                        recurringDeductions.add((RecurringDeduction) otherDeduction);
                    }
                }
                List recurringWorkersDeductions = new ArrayList();
                for (WorkersSalaryDeduction workersSalaryDeduction1 : workersSalaryDeduction) {
                    if ((workersSalaryDeduction1 instanceof RecurringWorkersDeduction)) {
                        recurringWorkersDeductions.add((RecurringWorkersDeduction) workersSalaryDeduction1);
                    }
                }
                workersSalaryDeductionBean.setSelectedItem(recurringWorkersDeduction);
                workersSalaryDeductionBean.setOtherDeductions(recurringDeductions);
                workersSalaryDeductionBean.setWorkersSalaryDeductions(recurringWorkersDeductions);
                context = RequestContext.getCurrentInstance();
                context.execute("PF('recurring-deduction').show();");
                break;
            case AppConstants.LOAN:
                List loanDeductions = new ArrayList();
                LoanWorkersDeduction loanWorkersDeduction = new LoanWorkersDeduction();
                loanWorkersDeduction.setWorker((Worker) this.selectedItem);
                loanWorkersDeduction.setIncluPayrollPeriod(currPayrollPeriod);
                for (OtherDeduction otherDeduction : otherDeductions) {
                    if ((otherDeduction instanceof LoanDeduction)) {
                        loanDeductions.add((LoanDeduction) otherDeduction);
                    }
                }
                List loanWorkersDeductions = new ArrayList();
                for (WorkersSalaryDeduction workersSalaryDeduction1 : workersSalaryDeduction) {
                    if ((workersSalaryDeduction1 instanceof LoanWorkersDeduction)) {
                        loanWorkersDeductions.add((LoanWorkersDeduction) workersSalaryDeduction1);
                    }
                }
                workersSalaryDeductionBean.setSelectedItem(loanWorkersDeduction);
                workersSalaryDeductionBean.setWorkersSalaryDeductions(loanWorkersDeductions);
                workersSalaryDeductionBean.setOtherDeductions(loanDeductions);
                context.execute("PF('loan-deduction').show();");
        }

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("workersSalaryDeductionBean", workersSalaryDeductionBean);
    }

    public void doWorkerAdditionAction(String additionType) {
        if ((!((Worker) this.selectedItem).getFlagStatus().equals("AP")) && (!((Worker) this.selectedItem).getFlagStatus().equals("MRJ"))) {
            FacesMessage msg = new FacesMessage();
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary("Cannot Perform Action on Worker: Worker currently undergoing an approval process");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
            if (this.selectedItem.getWstate()!=null && !((Worker) this.selectedItem).getWstate().equals(AppConstants.REINSTATE_WORKER)) {
                FacesMessage msg = new FacesMessage();
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary("Cannot Perform Action on Worker: Worker's appointment has been " + (this.selectedItem.getWstate().equals(AppConstants.SUSPEND_WORKER)?"Suspended":"Terminated"));
            FacesContext.getCurrentInstance().addMessage(null, msg); 
            return;
         }
        WorkersSalaryAdditionBean workersSalaryAdditionBean = (WorkersSalaryAdditionBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("workersSalaryAdditionBean");
        if (workersSalaryAdditionBean == null) {
            workersSalaryAdditionBean = new WorkersSalaryAdditionBean();
            workersSalaryAdditionBean.setWorkersSalaryAdditionFacadeLocal(this.workersSalaryAdditionFacadeLocal);
        }
        Map m = new HashMap();
        m.put("corporate", getLoggedCorporate());
        List<OtherAddition> otherAdditions;
        try {
            otherAdditions = this.otherAdditionFacadeLocal.findAll(m, new String[0]);
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            otherAdditions = new ArrayList();
        }
        m = new HashMap();
        m.put("worker", this.selectedItem);
        List<WorkersSalaryAddition> workersSalaryAddition;
        try {
            workersSalaryAddition = this.workersSalaryAdditionFacadeLocal.findAll(m, new String[0]);
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            workersSalaryAddition = new ArrayList();
        }
        this.log.info("size of all salary addition " + workersSalaryAddition.size());
        PayrollPeriod currPayrollPeriod;
        try {
            m = new HashMap();
            m.put("corporate", getLoggedCorporate());
            m.put("flagStatus", "PIP");
            List payrollInProgressPeriods = this.payrollPeriodFacadeLocal.findAll(m, new String[0]);
            if (payrollInProgressPeriods.isEmpty()) {
                throw new Exception("No Current Payroll Period Exist");
            }
            if (payrollInProgressPeriods.size() > 1) {
                throw new Exception("Error determining Current Payroll Period: please contact the Administrator");
            }
            currPayrollPeriod = (PayrollPeriod) payrollInProgressPeriods.get(0);
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage();
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return;
        }
        RequestContext context = RequestContext.getCurrentInstance();
        switch (additionType) {
            case AppConstants.ONE_TIME_ADDITION:
                OneTimeWorkersAddition oneTimeWorkersAddition = new OneTimeWorkersAddition();
                oneTimeWorkersAddition.setWorker((Worker) this.selectedItem);
                oneTimeWorkersAddition.setIncluPayrollPeriod(currPayrollPeriod);
                List oneTimeAdditions = new ArrayList();
                for (OtherAddition otherAddition : otherAdditions) {
                    if ((otherAddition instanceof OneTimeAddition)) {
                        oneTimeAdditions.add((OneTimeAddition) otherAddition);
                    }
                }
                List oneTimeWorkersAdditions = new ArrayList();
                for (WorkersSalaryAddition workersSalaryAddition1 : workersSalaryAddition) {
                    if ((workersSalaryAddition1 instanceof OneTimeWorkersAddition)) {
                        oneTimeWorkersAdditions.add((OneTimeWorkersAddition) workersSalaryAddition1);
                    }
                }
                this.log.info("one time additions size " + oneTimeWorkersAdditions.size());
                workersSalaryAdditionBean.setSelectedItem(oneTimeWorkersAddition);
                workersSalaryAdditionBean.setOtherAdditions(oneTimeAdditions);
                workersSalaryAdditionBean.setWorkersSalaryAdditions(oneTimeWorkersAdditions);

                context.execute("PF('onetime-addition').show();");
                break;
            case AppConstants.RECURRING_ADDITION:
                RecurringWorkersAddition recurringWorkersAddition = new RecurringWorkersAddition();
                recurringWorkersAddition.setWorker((Worker) this.selectedItem);
                recurringWorkersAddition.setIncluPayrollPeriod(currPayrollPeriod);
                List recurringAdditions = new ArrayList();
                for (OtherAddition otherAddition : otherAdditions) {
                    if ((otherAddition instanceof RecurringAddition)) {
                        recurringAdditions.add((RecurringAddition) otherAddition);
                    }
                }
                List recurringWorkersAdditions = new ArrayList();
                for (WorkersSalaryAddition workersSalaryAddition1 : workersSalaryAddition) {
                    if ((workersSalaryAddition1 instanceof RecurringWorkersAddition)) {
                        recurringWorkersAdditions.add((RecurringWorkersAddition) workersSalaryAddition1);
                    }
                }
                this.log.info("recurring time additions size " + recurringWorkersAdditions.size());
                workersSalaryAdditionBean.setSelectedItem(recurringWorkersAddition);
                workersSalaryAdditionBean.setOtherAdditions(recurringAdditions);
                workersSalaryAdditionBean.setWorkersSalaryAdditions(recurringWorkersAdditions);
                context = RequestContext.getCurrentInstance();
                context.execute("PF('recurring-addition').show();");
        }

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("workersSalaryAdditionBean", workersSalaryAdditionBean);
    }

    public void doWorkerAction(String actionType) {
          RequestContext context = RequestContext.getCurrentInstance();
        switch (actionType) {
            case AppConstants.SUSPEND_WORKER:
                selectedItem.setActive(Boolean.FALSE);
                selectedItem.setWstate(AppConstants.SUSPEND_WORKER);
                System.out.println("set worker suspended");
                 context.execute("PF('suspendWorker').show();");
                break;

            case AppConstants.TERMINATE_WORKER:
                            selectedItem.setActive(Boolean.FALSE);
                selectedItem.setWstate(AppConstants.TERMINATE_WORKER);
                 context.execute("PF('terminateWorker').show();");
                break;

            case AppConstants.REINSTATE_WORKER:
                             selectedItem.setActive(Boolean.TRUE);
                selectedItem.setWstate(AppConstants.REINSTATE_WORKER);
                 context.execute("PF('reinstateWorker').show();");
                break;
        }
    }
    
    public void nullifyStateChange() {
      selectedItem.setActive(!selectedItem.getActive());
      selectedItem.setWstate(null);
      System.out.println("nullify wstate...");
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            ((Worker) this.selectedItem).getCorporateBeneficiary().setCorporate(getLoggedCorporate());

            ((Worker) this.selectedItem).getCorporateBeneficiary().setCreator(getLoggedUser().getId());
            if (((Worker) this.selectedItem).getId() == null) {
                ((Worker) this.selectedItem).setFlagStatus("CR");
                getFacade().create(this.selectedItem);
            } else {
                if ((!((Worker) this.selectedItem).getFlagStatus().equals("AP")) && (!((Worker) this.selectedItem).getFlagStatus().equals("MRJ")) && (!((Worker) this.selectedItem).getFlagStatus().equals("CRJ"))) {
                    throw new Exception("Cannot Save Worker: Worker currently undergoing an approval process");
                }
                System.out.println("wstate b4 saving " + this.selectedItem.getWstate());
                ((Worker) this.selectedItem).setFlagStatus("MD");
                getFacade().edit((Worker) this.selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Worker Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
            this.selectedItem = new Worker();
            PocketMoneyAccount pm = new PocketMoneyAccount();
            pm.setType("P");
            ((Worker) this.selectedItem).getCorporateBeneficiary().setAccount(pm);
            this.acctOption = "PM";
        } catch (Exception ex) {
            Logger.getLogger(WorkersBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }
    
     

    public String getAcctOption() {
        return this.acctOption;
    }

    public void setAcctOption(String acctOption) {
        this.acctOption = acctOption;
    }

    public List<Bank> getBanks() {
        return this.banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public Worker getSelectedItem() {
        if (this.selectedItem == null) {
            this.selectedItem = new Worker();
            PocketMoneyAccount pm = new PocketMoneyAccount();
            pm.setType("P");
            ((Worker) this.selectedItem).getCorporateBeneficiary().setAccount(pm);
        }
        return (Worker) this.selectedItem;
    }
}
