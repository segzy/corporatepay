/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.payroll.facade.PayItemFacadeLocal;
import com.etranzact.corporatepay.payroll.model.PayItem;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "payItemTableBean")
@SessionScoped
public class PayItemTableBean extends AbstractTableBean<PayItem> {

    @EJB
    private PayItemFacadeLocal payItemFacadeLocal;

    public PayItemTableBean() {
        super(PayItem.class);
    }

    @Override
    protected PayItemFacadeLocal getFacade() {
        return payItemFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public PayItem getTableRowData(String rowKey) {
        List<PayItem> wrappedData = (List<PayItem>) getLazyDataModel().getWrappedData();
        for (PayItem t : wrappedData) {
            if (rowKey.equals(t.getId())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(PayItem t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        return new TableProperties();
    }

}
