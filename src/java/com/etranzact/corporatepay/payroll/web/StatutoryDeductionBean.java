/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.payroll.facade.PayItemFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.StatutoryDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.model.PayItem;
import com.etranzact.corporatepay.payroll.model.StatutoryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import java.io.CharArrayReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "statutoryDeductionBean")
@SessionScoped
public class StatutoryDeductionBean extends AbstractBean<StatutoryDeduction> {

    @EJB
    private StatutoryDeductionFacadeLocal statutoryDeductionFacadeLocal;
    @EJB
    private PayItemFacadeLocal payItemFacadeLocal;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;
    private List<PayItem> payItems;
    private Preferences preference;
    private boolean completePayroll;

    @Override
    protected StatutoryDeductionFacadeLocal getFacade() {
        return statutoryDeductionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public StatutoryDeductionBean() {
        super(StatutoryDeduction.class);
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        try {
            selectedItem = statutoryDeductionFacadeLocal.find(selectedItem.getId());
        } catch (Exception ex) {
            Logger.getLogger(StatutoryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            if (selectedItem.getId() == null) {
                Map<String, Object> map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("statDeductionType", selectedItem.getStatDeductionType());
                if (!getFacade().findAll(map, true).isEmpty()) {
                    throw new Exception(selectedItem.getStatDeductionType() + " already exist");
                }
           
            }
            if (selectedItem.getAmountType() != null && selectedItem.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT) && selectedItem.getRefStatutoryPayItems().isEmpty()) {
                throw new Exception("There must be Dependent pay items for percent types ");
            }
            if (selectedItem.getAmountType() != null && selectedItem.getAmount() == null) {
                throw new Exception("Amount Field is mandatory ");
            }
            if (selectedItem.getAmountType() == null && selectedItem.getAmount() != null) {
                throw new Exception("Amount Type Field is mandatory ");
            }
            selectedItem.setCorporate(getLoggedCorporate());
            selectedItem.setFlagStatus(AppConstants.APPROVED);
            getFacade().create(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Statutory Deduction Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(StatutoryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new StatutoryDeduction();
        }

    }

    /**
     * @return the payItems
     */
    public List<PayItem> getPayItems() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            payItems = payItemFacadeLocal.findAll(map, true);
            return payItems;
        } catch (Exception ex) {
            Logger.getLogger(StatutoryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    /**
     * @param payItems the payItems to set
     */
    public void setPayItems(List<PayItem> payItems) {
        this.payItems = payItems;
    }

    /**
     * @return the preference
     */
    public Preferences getPreference() {
        if(preference==null) {
            try { 
                preference = preferenceFacadeLocal.getCorporatePreferences(getLoggedCorporate());
            } catch (Exception ex) {
                Logger.getLogger(StatutoryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return preference;
    }

    /**
     * @return the completePayroll
     */
    public boolean isCompletePayroll() {
        completePayroll = getPreference()!=null?getPreference().getPayrollMode().equals(AppConstants.COMPLETE_PAYROLL):completePayroll;
        return completePayroll;
    }
    
   

}
