/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.payroll.facade.DeductionTypeFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.OtherDeductionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.PayItemFacadeLocal;
import com.etranzact.corporatepay.payroll.model.DeductionType;
import com.etranzact.corporatepay.payroll.model.LoanDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeDeduction;
import com.etranzact.corporatepay.payroll.model.OtherDeduction;
import com.etranzact.corporatepay.payroll.model.PayItem;
import com.etranzact.corporatepay.payroll.model.RecurringDeduction;
import com.etranzact.corporatepay.payroll.model.StatutoryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "otherDeductionBean")
@SessionScoped
public class OtherDeductionBean extends AbstractBean<OtherDeduction> {

    @EJB
    private OtherDeductionFacadeLocal otherDeductionFacadeLocal;
    @EJB
    private DeductionTypeFacadeLocal deductionTypeFacadeLocal;
    @EJB
    private PayItemFacadeLocal payItemFacadeLocal;
    private List<DeductionType> deductionTypes;
    private List<OtherDeduction> otherDeductions;
    private DeductionType deductionType;
    private String deductionTypeId;
    private String deductionName;
    private List<PayItem> payItems;
    private LoanDeduction loanDeduction;
    private OneTimeDeduction oneTimeDeduction;
    private RecurringDeduction recurringDeduction;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;
    private Preferences preference;
    private boolean completePayroll;

    @Override
    protected OtherDeductionFacadeLocal getFacade() {
        return otherDeductionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public OtherDeductionBean() {
        super(OtherDeduction.class);
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        Object object = selectEvent.getObject();
        if (object instanceof OtherDeduction) {
            super.onRowSelect(selectEvent);
            try {
                selectedItem = getFacade().find(selectedItem.getId());
                if (selectedItem instanceof LoanDeduction) {
                    loanDeduction = (LoanDeduction) selectedItem;
                } else if (selectedItem instanceof RecurringDeduction) {
                    recurringDeduction = (RecurringDeduction) selectedItem;
                } else if (selectedItem instanceof OneTimeDeduction) {
                    oneTimeDeduction = (OneTimeDeduction) selectedItem;
                }
                deductionName = selectedItem.getDeductionName();
            } catch (Exception ex) {
                Logger.getLogger(OtherDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (object instanceof DeductionType) {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            setDeductionType((DeductionType) object);
            deductionTypeId = deductionType.getId();

            map.put("deductionType", deductionType);
            initOtherDeduction();
            try {
                otherDeductions = otherDeductionFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(OtherDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
                otherDeductions = new ArrayList<>();
            }
        }
    }

    private void initOtherDeduction() {
        switch (deductionType.getId()) {
            case "#ONE_TIME":
                oneTimeDeduction = new OneTimeDeduction();
                oneTimeDeduction.setDeductionType(deductionType);
                oneTimeDeduction.setCorporate(getLoggedCorporate());
                break;
            case "#RECURRING":
                recurringDeduction = new RecurringDeduction();
                recurringDeduction.setDeductionType(deductionType);
                recurringDeduction.setCorporate(getLoggedCorporate());
                break;
            case "#LOAN":
                loanDeduction = new LoanDeduction();
                loanDeduction.setDeductionType(deductionType);
                loanDeduction.setCorporate(getLoggedCorporate());
                break;
        }

    }

    public void saveLoan() {
        FacesMessage m = new FacesMessage();
        try {
            deductionType = deductionTypeFacadeLocal.find(deductionTypeId);
            log.log(Level.INFO, "saving loan {0}", deductionType);
            loanDeduction.setFlagStatus(AppConstants.APPROVED);
            if (loanDeduction.getId() == null || (loanDeduction.getId() != null && !loanDeduction.getDeductionName().equalsIgnoreCase(deductionName))) {
                Map<String, Object> map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("deductionName='" + loanDeduction.getDeductionName() + "'", null);
                if (!getFacade().findAll(map, true).isEmpty()) {
                    throw new Exception(loanDeduction.getDeductionName() + " already exist");
                }
            }
            otherDeductions.add(getFacade().create(loanDeduction));
            log.log(Level.INFO, "saved loan {0}", deductionType);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Loan Deduction Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            initOtherDeduction();
        } catch (Exception ex) {
            Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    public void saveOneTime() {
        FacesMessage m = new FacesMessage();
        try {
            deductionType = deductionTypeFacadeLocal.find(deductionTypeId);
            oneTimeDeduction.setFlagStatus(AppConstants.APPROVED);
            if (oneTimeDeduction.getId() == null || (oneTimeDeduction.getId() != null && !oneTimeDeduction.getDeductionName().equalsIgnoreCase(deductionName))) {
                Map<String, Object> map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("deductionName='" + oneTimeDeduction.getDeductionName() + "'", null);
                if (!getFacade().findAll(map, true).isEmpty()) {
                    throw new Exception(oneTimeDeduction.getDeductionName() + " already exist");
                }
            }
            if (oneTimeDeduction.getAmountType() != null && oneTimeDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT) && oneTimeDeduction.getRefStatutoryPayItems().isEmpty()) {
                throw new Exception("There must be Dependent pay items for percent types ");
            }
            if (oneTimeDeduction.getAmountType() != null && oneTimeDeduction.getAmount() == null) {
                throw new Exception("Amount Field is mandatory ");
            }
            if (oneTimeDeduction.getAmountType() == null && oneTimeDeduction.getAmount() != null) {
                throw new Exception("Amount Type Field is mandatory ");
            }
            otherDeductions.add(getFacade().create(oneTimeDeduction));
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("One Time Deduction Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            initOtherDeduction();
        } catch (Exception ex) {
            Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    public void saveRecurring() {
        FacesMessage m = new FacesMessage();
        try {
            deductionType = deductionTypeFacadeLocal.find(deductionTypeId);
            recurringDeduction.setFlagStatus(AppConstants.APPROVED);
            log.log(Level.INFO, "saving recurring {0}", deductionType);
            if (recurringDeduction.getId() == null || (recurringDeduction.getId() != null && !recurringDeduction.getDeductionName().equalsIgnoreCase(deductionName))) {
                Map<String, Object> map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("deductionName='" + recurringDeduction.getDeductionName() + "'", null);
                if (!getFacade().findAll(map, true).isEmpty()) {
                    throw new Exception(recurringDeduction.getDeductionName() + " already exist");
                }
            }
            if (recurringDeduction.getAmountType() != null && recurringDeduction.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT) && recurringDeduction.getRefStatutoryPayItems().isEmpty()) {
                throw new Exception("There must be Dependent pay items for percent types ");
            }
            if (recurringDeduction.getAmountType() != null && recurringDeduction.getAmount() == null) {
                throw new Exception("Amount Field is mandatory ");
            }
            if (recurringDeduction.getAmountType() == null && recurringDeduction.getAmount() != null) {
                throw new Exception("Amount Type Field is mandatory ");
            }
            otherDeductions.add(getFacade().create(recurringDeduction));
            log.log(Level.INFO, "saved recurring {0}", deductionType);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Recurring Deduction Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            initOtherDeduction();
        } catch (Exception ex) {
            Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    /**
     * @return the deductionTypes
     */
    public List<DeductionType> getDeductionTypes() {
        if (deductionTypes == null) {
            try {
                deductionTypes = deductionTypeFacadeLocal.findAll(new HashMap<String, Object>(), true);
            } catch (Exception ex) {
                Logger.getLogger(OtherDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
                deductionTypes = new ArrayList<>();
            }
        }
        return deductionTypes;
    }

    /**
     * @return the otherDeductions
     */
    public List<OtherDeduction> getOtherDeductions() {
        return otherDeductions;
    }

    /**
     * @return the deductionType
     */
    public DeductionType getDeductionType() {
        return deductionType;
    }

    /**
     * @param deductionType the deductionType to set
     */
    public void setDeductionType(DeductionType deductionType) {
        this.deductionType = deductionType;
    }

    /**
     * @return the payItems
     */
    public List<PayItem> getPayItems() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            payItems = payItemFacadeLocal.findAll(map, true);
            return payItems;
        } catch (Exception ex) {
            Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    /**
     * @param payItems the payItems to set
     */
    public void setPayItems(List<PayItem> payItems) {
        this.payItems = payItems;
    }

    /**
     * @return the loanDeduction
     */
    public LoanDeduction getLoanDeduction() {
        return loanDeduction;
    }

    /**
     * @param loanDeduction the loanDeduction to set
     */
    public void setLoanDeduction(LoanDeduction loanDeduction) {
        this.loanDeduction = loanDeduction;
    }

    /**
     * @return the oneTimeDeduction
     */
    public OneTimeDeduction getOneTimeDeduction() {
        return oneTimeDeduction;
    }

    /**
     * @param oneTimeDeduction the oneTimeDeduction to set
     */
    public void setOneTimeDeduction(OneTimeDeduction oneTimeDeduction) {
        this.oneTimeDeduction = oneTimeDeduction;
    }

    /**
     * @return the recurringDeduction
     */
    public RecurringDeduction getRecurringDeduction() {
        return recurringDeduction;
    }

    /**
     * @param recurringDeduction the recurringDeduction to set
     */
    public void setRecurringDeduction(RecurringDeduction recurringDeduction) {
        this.recurringDeduction = recurringDeduction;
    }

    /**
     * @return the preference
     */
    public Preferences getPreference() {
        if (preference == null) {
            try {
                preference = preferenceFacadeLocal.getCorporatePreferences(getLoggedCorporate());
            } catch (Exception ex) {
                Logger.getLogger(StatutoryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return preference;
    }

    /**
     * @return the completePayroll
     */
    public boolean isCompletePayroll() {
        completePayroll = getPreference() != null ? getPreference().getPayrollMode().equals(AppConstants.COMPLETE_PAYROLL) : completePayroll;
        return completePayroll;
    }

}
