/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.payroll.facade.AdditionTypeFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.OtherAdditionFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.PayItemFacadeLocal;
import com.etranzact.corporatepay.payroll.model.AdditionType;
import com.etranzact.corporatepay.payroll.model.LoanDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeAddition;
import com.etranzact.corporatepay.payroll.model.OtherAddition;
import com.etranzact.corporatepay.payroll.model.OtherDeduction;
import com.etranzact.corporatepay.payroll.model.PayItem;
import com.etranzact.corporatepay.payroll.model.RecurringAddition;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "otherAdditionBean")
@SessionScoped
public class OtherAdditionBean extends AbstractBean<OtherAddition> {

    @EJB
    private OtherAdditionFacadeLocal otherAdditionFacadeLocal;
    @EJB
    private AdditionTypeFacadeLocal additionTypeFacadeLocal;
    @EJB
    private PayItemFacadeLocal payItemFacadeLocal;
    private List<AdditionType> additionTypes;
    private List<OtherAddition> otherAdditions;
    private AdditionType additionType;
    private String additionTypeId;
    private String additionName;
    private List<PayItem> payItems;
    private LoanDeduction loanDeduction;
    private OneTimeAddition oneTimeAddition;
    private RecurringAddition recurringAddition;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;
    private Preferences preference;
    private boolean completePayroll;

    @Override
    protected OtherAdditionFacadeLocal getFacade() {
        return otherAdditionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public OtherAdditionBean() {
        super(OtherAddition.class);
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        Object object = selectEvent.getObject();
        if (object instanceof OtherDeduction) {
            super.onRowSelect(selectEvent);
            try {
                selectedItem = getFacade().find(selectedItem.getId());
                if (selectedItem instanceof RecurringAddition) {
                    setRecurringAddition((RecurringAddition) selectedItem);
                } else if (selectedItem instanceof OneTimeAddition) {
                    setOneTimeAddition((OneTimeAddition) selectedItem);
                }
                additionName = selectedItem.getAdditionName();
            } catch (Exception ex) {
                Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (object instanceof AdditionType) {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            setAdditionType((AdditionType) object);
            additionTypeId = getAdditionType().getId();
            initOtherAddition();
            map.put("additionType", getAdditionType());
            try {
                setOtherAdditions(otherAdditionFacadeLocal.findAll(map, true));
            } catch (Exception ex) {
                Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
                setOtherAdditions(new ArrayList<OtherAddition>());
            }
        }
    }

    private void initOtherAddition() {
        switch (getAdditionType().getId()) {
            case "#ONE_TIME":
                setOneTimeAddition(new OneTimeAddition());
                getOneTimeAddition().setAdditionType(getAdditionType());
                getOneTimeAddition().setCorporate(getLoggedCorporate());
                break;
            case "#RECURRING":
                setRecurringAddition(new RecurringAddition());
                getRecurringAddition().setAdditionType(getAdditionType());
                getRecurringAddition().setCorporate(getLoggedCorporate());
                break;
        }

    }

    public void saveOneTime() {
        FacesMessage m = new FacesMessage();
        try {
            additionType = additionTypeFacadeLocal.find(additionTypeId);
            oneTimeAddition.setFlagStatus(AppConstants.APPROVED);
            if (oneTimeAddition.getId() == null || (oneTimeAddition.getId() != null && !oneTimeAddition.getAdditionName().equalsIgnoreCase(additionName))) {
                Map<String, Object> map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("additionName='" + oneTimeAddition.getAdditionName() + "'", null);
                if (!getFacade().findAll(map, true).isEmpty()) {
                    throw new Exception(oneTimeAddition.getAdditionName() + " already exist");
                }
            }
            if (oneTimeAddition.getAmountType() != null && oneTimeAddition.getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT) && oneTimeAddition.getRefStatutoryPayItems().isEmpty()) {
                throw new Exception("There must be Dependent pay items for percent types ");
            }
            if (oneTimeAddition.getAmountType() != null && oneTimeAddition.getAmount() == null) {
                throw new Exception("Amount Field is mandatory ");
            }
            if (oneTimeAddition.getAmountType() == null && oneTimeAddition.getAmount() != null) {
                throw new Exception("Amount Type Field is mandatory ");
            }
            otherAdditions.add(getFacade().create(oneTimeAddition));
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("One Time Deduction Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            initOtherAddition();
        } catch (Exception ex) {
            Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    public void saveRecurring() {
        FacesMessage m = new FacesMessage();
        try {
            additionType = additionTypeFacadeLocal.find(additionTypeId);
            getRecurringAddition().setFlagStatus(AppConstants.APPROVED);
            log.log(Level.INFO, "saving addition {0}", additionType);
            if (getRecurringAddition().getId() == null || (getRecurringAddition().getId() != null && !recurringAddition.getAdditionName().equalsIgnoreCase(additionName))) {
                Map<String, Object> map = new HashMap<>();
                map.put("corporate", getLoggedCorporate());
                map.put("additionName='" + getRecurringAddition().getAdditionName() + "'", null);
                if (!getFacade().findAll(map, true).isEmpty()) {
                    throw new Exception(getRecurringAddition().getAdditionName() + " already exist");
                }
            }
            if (getRecurringAddition().getAmountType() != null && getRecurringAddition().getAmountType().equals(AppConstants.AMOUNT_TYPE_PERCENT) && getRecurringAddition().getRefStatutoryPayItems().isEmpty()) {
                throw new Exception("There must be Dependent pay items for percent types ");
            }
            if (getRecurringAddition().getAmountType() != null && getRecurringAddition().getAmount() == null) {
                throw new Exception("Amount Field is mandatory ");
            }
            if (getRecurringAddition().getAmountType() == null && getRecurringAddition().getAmount() != null) {
                throw new Exception("Amount Type Field is mandatory ");
            }
            otherAdditions.add(getFacade().create(getRecurringAddition()));
            log.log(Level.INFO, "saved recurring {0}", additionType);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Recurring Deduction Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
            initOtherAddition();
        } catch (Exception ex) {
            Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    /**
     * @return the deductionTypes
     */
    public List<AdditionType> getAdditionTypes() {
        if (additionTypes == null) {
            try {
                setAdditionTypes(additionTypeFacadeLocal.findAll(new HashMap<String, Object>(), true));
            } catch (Exception ex) {
                Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
                setAdditionTypes(new ArrayList<AdditionType>());
            }
        }
        return additionTypes;
    }

    /**
     * @return the payItems
     */
    public List<PayItem> getPayItems() {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            payItems = payItemFacadeLocal.findAll(map, true);
            return payItems;
        } catch (Exception ex) {
            Logger.getLogger(OtherAdditionBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    /**
     * @param payItems the payItems to set
     */
    public void setPayItems(List<PayItem> payItems) {
        this.payItems = payItems;
    }

    /**
     * @return the loanDeduction
     */
    public LoanDeduction getLoanDeduction() {
        return loanDeduction;
    }

    /**
     * @param loanDeduction the loanDeduction to set
     */
    public void setLoanDeduction(LoanDeduction loanDeduction) {
        this.loanDeduction = loanDeduction;
    }

    /**
     * @return the additionName
     */
    public String getAdditionName() {
        return additionName;
    }

    /**
     * @return the oneTimeAddition
     */
    public OneTimeAddition getOneTimeAddition() {
        return oneTimeAddition;
    }

    /**
     * @param oneTimeAddition the oneTimeAddition to set
     */
    public void setOneTimeAddition(OneTimeAddition oneTimeAddition) {
        this.oneTimeAddition = oneTimeAddition;
    }

    /**
     * @return the recurringAddition
     */
    public RecurringAddition getRecurringAddition() {
        return recurringAddition;
    }

    /**
     * @param recurringAddition the recurringAddition to set
     */
    public void setRecurringAddition(RecurringAddition recurringAddition) {
        this.recurringAddition = recurringAddition;
    }

    /**
     * @return the otherAdditions
     */
    public List<OtherAddition> getOtherAdditions() {
        return otherAdditions;
    }

    /**
     * @param otherAdditions the otherAdditions to set
     */
    public void setOtherAdditions(List<OtherAddition> otherAdditions) {
        this.otherAdditions = otherAdditions;
    }

    /**
     * @param additionTypes the additionTypes to set
     */
    public void setAdditionTypes(List<AdditionType> additionTypes) {
        this.additionTypes = additionTypes;
    }

    /**
     * @return the additionType
     */
    public AdditionType getAdditionType() {
        return additionType;
    }

    /**
     * @param additionType the additionType to set
     */
    public void setAdditionType(AdditionType additionType) {
        this.additionType = additionType;
    }

    /**
     * @return the preference
     */
    public Preferences getPreference() {
        if (preference == null) {
            try {
                preference = preferenceFacadeLocal.getCorporatePreferences(getLoggedCorporate());
            } catch (Exception ex) {
                Logger.getLogger(StatutoryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return preference;
    }

    /**
     * @return the completePayroll
     */
    public boolean isCompletePayroll() {
        completePayroll = getPreference() != null ? getPreference().getPayrollMode().equals(AppConstants.COMPLETE_PAYROLL) : completePayroll;
        return completePayroll;
    }

}
