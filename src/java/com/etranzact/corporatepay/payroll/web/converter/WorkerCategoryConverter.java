/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web.converter;

import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryFacadeLocal;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("workersCategoryConverter")
public class WorkerCategoryConverter implements Converter {
    
    private static final Logger log = Logger.getLogger(WorkerCategoryConverter.class.getName());
    
    @Inject
    private WorkersCategoryFacadeLocal workersCategoryFacadeLocal;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Map<String, Object> map = new HashMap<>();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            map.put("corporate", ((CorporateUser) loggedInUser).getCorporate());
            List<WorkersCategory> workersCategorys;
            try {
                workersCategorys = workersCategoryFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(WorkerCategoryConverter.class.getName()).log(Level.SEVERE, null, ex);
                workersCategorys = new ArrayList<>();
            }
            for (WorkersCategory workersCategory : workersCategorys) {
                if (value.equals(workersCategory.getId())) {
                    return workersCategory;
                }
            }
        }
        return null;
    }
    
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof WorkersCategory) {
            WorkersCategory workersCategory = (WorkersCategory) value;
            String workerCategoryId = workersCategory.getId();
            return workerCategoryId;
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
