/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web.converter;

import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryGroupStepFacadeLocal;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroupStep;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("workersCategoryGroupStepConverter")
public class WorkerCategoryGroupStepConverter implements Converter {

    private static final Logger log = Logger.getLogger(WorkerCategoryGroupStepConverter.class.getName());

    @Inject
    private WorkersCategoryGroupStepFacadeLocal workersCategoryGroupStepFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Map<String, Object> map = new HashMap<>();
            List<WorkersCategoryGroupStep> workersCategoryGroupSteps;
            try {
                workersCategoryGroupSteps = workersCategoryGroupStepFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(WorkerCategoryGroupStepConverter.class.getName()).log(Level.SEVERE, null, ex);
                workersCategoryGroupSteps = new ArrayList<>();
            }
            for (WorkersCategoryGroupStep workersCategoryGroupStep : workersCategoryGroupSteps) {
                if (value.equals(workersCategoryGroupStep.getId().toString())) {
                    return workersCategoryGroupStep;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof WorkersCategoryGroupStep) {
            WorkersCategoryGroupStep workersCategoryGroupStep = (WorkersCategoryGroupStep) value;
            return workersCategoryGroupStep.getId().toString();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
