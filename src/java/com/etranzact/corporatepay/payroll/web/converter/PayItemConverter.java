/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web.converter;

import com.etranzact.corporatepay.setup.web.converter.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.CorporatePayService;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.facade.PayItemFacadeLocal;
import com.etranzact.corporatepay.payroll.model.PayItem;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporatepayServiceFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("payitemConverter")
public class PayItemConverter implements Converter {

    private static final Logger log = Logger.getLogger(CorporateConverter.class.getName());

    @Inject
    private PayItemFacadeLocal payItemFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Map<String, Object> map = new HashMap<>();
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            map.put("corporate", ((CorporateUser) loggedInUser).getCorporate());
            List<PayItem> payItems;
            try {
                payItems = payItemFacadeLocal.findAll(map, false);
            } catch (Exception ex) {
                Logger.getLogger(PayItemConverter.class.getName()).log(Level.SEVERE, null, ex);
                payItems = new ArrayList<>();
            }
            for (PayItem payItem : payItems) {
                if (value.equals(payItem.getId().toString())) {
                    return payItem;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof PayItem) {
            PayItem payItem = (PayItem) value;
            if (payItem.getId() == null) {
                return "";
            }
            return payItem.getId().toString();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
