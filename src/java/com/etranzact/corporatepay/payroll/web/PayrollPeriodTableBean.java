/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.payroll.facade.PayrollPeriodFacadeLocal;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "payrollPeriodTableBean")
@SessionScoped
public class PayrollPeriodTableBean extends AbstractTableBean<PayrollPeriod> {

    @EJB
    private PayrollPeriodFacadeLocal payrollPeriodFacadeLocal;
    private String payrollPeriodId = "";
    private String payrollPeriodDesc = "";

    public PayrollPeriodTableBean() {
        super(PayrollPeriod.class);
    }

    @Override
    protected PayrollPeriodFacadeLocal getFacade() {
        return payrollPeriodFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        super.init();
    }
    
    @Override
        public List<SelectItem> getStatusOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.PAYROLL_IN_PROGRESS, AppConstants.PAYROLL_IN_PROGRESS_TEXT));
        options.add(new SelectItem(AppConstants.PAYROLL_GENERATED, AppConstants.PAYROLL_GENERATED_TEXT));
        options.add(new SelectItem(AppConstants.PAYROLL_ROLLEDBACK, AppConstants.PAYROLL_ROLLEDBACK_TEXT));
        options.add(new SelectItem(AppConstants.PAYROLL_APPROVED, AppConstants.PAYROLL_APPROVED_TEXT));
        return options;
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!payrollPeriodId.isEmpty()) {
            properties.getAdditionalFilter().put("tag", getPayrollPeriodId());
        }
        if (!payrollPeriodDesc.isEmpty()) {
            properties.getAdditionalFilter().put("payrollPeriodDesc", getPayrollPeriodDesc());
        }
        return properties;
    }

    @Override
    public PayrollPeriod getTableRowData(String rowKey) {
        List<PayrollPeriod> wrappedData = (List<PayrollPeriod>) getLazyDataModel().getWrappedData();
        for (PayrollPeriod t : wrappedData) {
            if (rowKey.equals(t.getTag())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(PayrollPeriod t) {
        return t.getTag(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the payrollPeriodId
     */
    public String getPayrollPeriodId() {
        return payrollPeriodId;
    }

    /**
     * @param payrollPeriodId the payrollPeriodId to set
     */
    public void setPayrollPeriodId(String payrollPeriodId) {
        this.payrollPeriodId = payrollPeriodId;
    }

    /**
     * @return the payrollPeriodDesc
     */
    public String getPayrollPeriodDesc() {
        return payrollPeriodDesc;
    }

    /**
     * @param payrollPeriodDesc the payrollPeriodDesc to set
     */
    public void setPayrollPeriodDesc(String payrollPeriodDesc) {
        this.payrollPeriodDesc = payrollPeriodDesc;
    }

}
