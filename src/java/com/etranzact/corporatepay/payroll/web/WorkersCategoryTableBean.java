/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.payroll.facade.PayrollPeriodFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryFacadeLocal;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "workersCategoryTableBean")
@SessionScoped
public class WorkersCategoryTableBean extends AbstractTableBean<WorkersCategory> {

    @EJB
    private WorkersCategoryFacadeLocal workersCategoryFacadeLocal;
    private String categoryName = "";

    public WorkersCategoryTableBean() {
        super(WorkersCategory.class);
    }

    @Override
    protected WorkersCategoryFacadeLocal getFacade() {
        return workersCategoryFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }
    
       @Override
        public List<SelectItem> getStatusOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(AppConstants.CREATED_NOT_SETUP, AppConstants.CREATED_NOT_SETUP_TEXT));
        options.add(new SelectItem(AppConstants.SET_UP_IN_PROGRESS, AppConstants.SET_UP_IN_PROGRESS_TEXT));
        options.add(new SelectItem(AppConstants.SETUP_COMPLETED, AppConstants.SETUP_COMPLETED_TEXT));
        return options;
    }

    @PostConstruct
    @Override
    public void init() {
        dateField = "dateCreated";
        super.init();
    }
    

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!categoryName.isEmpty()) {
            properties.getAdditionalFilter().put("id", getCategoryName());
        }
        return properties;
    }

    @Override
    public WorkersCategory getTableRowData(String rowKey) {
        List<WorkersCategory> wrappedData = (List<WorkersCategory>) getLazyDataModel().getWrappedData();
        for (WorkersCategory t : wrappedData) {
            if (rowKey.equals(t.getId())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(WorkersCategory t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * @param categoryName the categoryName to set
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    

}
