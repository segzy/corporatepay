/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.web;

import com.etranzact.corporatepay.core.facade.PreferenceFacadeLocal;
import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.PFA;
import com.etranzact.corporatepay.model.Preferences;
import com.etranzact.corporatepay.model.Tax;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryGroupFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersCategoryGroupStepFacadeLocal;
import com.etranzact.corporatepay.payroll.facade.WorkersFacadeLocal;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroup;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroupStep;
import com.etranzact.corporatepay.setup.facade.PFASetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.TaxOfficeSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "workersTableBean")
@SessionScoped
public class WorkersTableBean extends AbstractTableBean<Worker> {

    @EJB
    private WorkersFacadeLocal workersFacadeLocal;
    @EJB
    private WorkersCategoryFacadeLocal workersCategoryFacadeLocal;
    @EJB
    private WorkersCategoryGroupFacadeLocal workersCategoryGroupFacadeLocal;
    @EJB
    private WorkersCategoryGroupStepFacadeLocal workersCategoryGroupStepFacadeLocal;
    @EJB
    private PFASetupFacadeLocal pfaSetupFacadeLocal;
    @EJB
    private TaxOfficeSetupFacadeLocal taxOfficeSetupFacadeLocal;
    @EJB
    private PreferenceFacadeLocal preferenceFacadeLocal;
    private Preferences preference;
    private boolean completePayroll;
    private List<WorkersCategory> workersCategorys;
    private List<WorkersCategoryGroup> workersCategoryGroups;
    private List<WorkersCategoryGroupStep> workersCategoryGroupSteps;
    private List<PFA> pfas;
    private List<Tax> taxs;
    private String firstNameFilter = "";
    private String lastNameFilter = "";
    private WorkersCategory workersCategoryFilter;
    private WorkersCategoryGroup workersCategoryGroupFilter;
    private WorkersCategoryGroupStep workersCategoryGroupStepFilter;
    private PFA pfaFilter;
    private Tax taxFilter;
    private boolean workersCategoryChanged;
    private boolean workersCategoryGroupChanged;

    public WorkersTableBean() {
        super(Worker.class);
    }

    @Override
    protected WorkersFacadeLocal getFacade() {
        return workersFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void handleCategoryChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        workersCategoryFilter = (WorkersCategory) ((UISelectOne) e.getComponent()).getValue();
        setWorkersCategoryChanged(true);
    }

    public void handleCategoryGroupChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        workersCategoryGroupFilter = (WorkersCategoryGroup) ((UISelectOne) e.getComponent()).getValue();
        workersCategoryGroupChanged = true;
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (isCorporateUser()) {
            properties.getAdditionalFilter().put("corporateBeneficiary.corporate", getLoggedCorporate());
//             properties.getAdditionalFilter().put("corporate", getLoggedCorporate());
        }
        if (getWorkersCategoryGroupFilter() != null) {
            properties.getAdditionalFilter().put("workersCategoryGroupStep.workersCategoryGroup", workersCategoryGroupStepFilter.getWorkersCategoryGroup());
        }
        if (workersCategoryGroupStepFilter != null) {
            properties.getAdditionalFilter().put("workersCategoryGroupStep", workersCategoryGroupStepFilter);
        }
        if (workersCategoryFilter != null) {
            properties.getAdditionalFilter().put("workersCategory", workersCategoryFilter);
        }
        if (taxFilter != null) {
            properties.getAdditionalFilter().put("tax", taxFilter);
        }
        if (pfaFilter != null) {
            properties.getAdditionalFilter().put("pfa", pfaFilter);
        }
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!lastNameFilter.isEmpty()) {
            properties.getAdditionalFilter().put("lastName", getLastNameFilter());
        }
        if (!firstNameFilter.isEmpty()) {
            properties.getAdditionalFilter().put("firstName", getFirstNameFilter());
        }

        return properties;
    }

    @Override
    public Worker getTableRowData(String rowKey) {
        List<Worker> wrappedData = (List<Worker>) getLazyDataModel().getWrappedData();
        for (Worker t : wrappedData) {
            if (rowKey.equals(String.valueOf(t.getId()))) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Worker t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the firstNameFilter
     */
    public String getFirstNameFilter() {
        return firstNameFilter;
    }

    /**
     * @param firstNameFilter the firstNameFilter to set
     */
    public void setFirstNameFilter(String firstNameFilter) {
        this.firstNameFilter = firstNameFilter;
    }

    /**
     * @return the lastNameFilter
     */
    public String getLastNameFilter() {
        return lastNameFilter;
    }

    /**
     * @param lastNameFilter the lastNameFilter to set
     */
    public void setLastNameFilter(String lastNameFilter) {
        this.lastNameFilter = lastNameFilter;
    }

    /**
     * @return the workersCategorys
     */
    public List<WorkersCategory> getWorkersCategorys() {
        if (workersCategorys == null) {
            Map<String, Object> m = new HashMap<>();
            m.put("corporate", getLoggedCorporate());
            try {
                workersCategorys = workersCategoryFacadeLocal.findAll(m, true);
            } catch (Exception ex) {
                Logger.getLogger(WorkersTableBean.class.getName()).log(Level.SEVERE, null, ex);
                workersCategorys = new ArrayList<>();
            }
        }

        return workersCategorys;
    }

    /**
     * @param workersCategorys the workersCategorys to set
     */
    public void setWorkersCategorys(List<WorkersCategory> workersCategorys) {
        this.workersCategorys = workersCategorys;
    }

    /**
     * @return the workersCategoryGroupSteps
     */
    public List<WorkersCategoryGroupStep> getWorkersCategoryGroupSteps() {
        try {
            if (workersCategoryChanged || workersCategoryGroupChanged) {
                Map<String, Object> m = new HashMap<>();
                if (workersCategoryGroupFilter != null) {
//                    log.log(Level.INFO, "workersCategoryGroupFilter {0}", workersCategoryGroupFilter);
//                    m.put("corporate", getLoggedCorporate());
                    m.put("workersCategoryGroup", workersCategoryGroupFilter);
                    workersCategoryGroupSteps = workersCategoryGroupStepFacadeLocal.findAll(m, true, new String[]{"groupStepLevel"});
                    log.log(Level.INFO, "workersCategoryGroupSteps: {0}", workersCategoryGroupSteps.size());
                } else {
                    workersCategoryGroupSteps = new ArrayList<>();
//                    log.log(Level.INFO, "workersCategoryGroupFilterr {0}", workersCategoryGroupFilter);
                    m.put("corporate", getLoggedCorporate());
                    m.put("workersCategory", workersCategoryFilter);
                    List<WorkersCategoryGroup> workersCategoryGroupss = workersCategoryGroupFacadeLocal.findAll(m, true, new String[]{"groupLevel"});
                    for (WorkersCategoryGroup categoryGroup : workersCategoryGroupss) {
                        m = new HashMap<>();
                        m.put("workersCategoryGroup", categoryGroup);
                        workersCategoryGroupSteps.addAll(workersCategoryGroupStepFacadeLocal.findAll(m, true, new String[]{"groupStepLevel"}));
//                        log.log(Level.INFO, "workersCategoryGroupStepss: {0}", workersCategoryGroupSteps.size());
                    }
                }

            }
//            try {
//                workersCategoryGroupSteps = workersCategoryGroupStepFacadeLocal.findAll(m, true);
//            } catch (Exception ex) {
//                Logger.getLogger(WorkersTableBean.class.getName()).log(Level.SEVERE, null, ex);
//                workersCategoryGroupSteps = new ArrayList<>();
//            }
        } catch (Exception ex) {
            Logger.getLogger(WorkersTableBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            setWorkersCategoryChanged(false);
            workersCategoryGroupChanged = false;
        }

//        }
        return workersCategoryGroupSteps;
    }

    /**
     * @param workersCategoryGroupSteps the workersCategoryGroupSteps to set
     */
    public void setWorkersCategoryGroupSteps(List<WorkersCategoryGroupStep> workersCategoryGroupSteps) {
        this.workersCategoryGroupSteps = workersCategoryGroupSteps;
    }

    /**
     * @return the workersCategoryFilter
     */
    public WorkersCategory getWorkersCategoryFilter() {
        return workersCategoryFilter;
    }

    /**
     * @param workersCategoryFilter the workersCategoryFilter to set
     */
    public void setWorkersCategoryFilter(WorkersCategory workersCategoryFilter) {
        this.workersCategoryFilter = workersCategoryFilter;
    }

    /**
     * @return the workersCategoryGroupStepFilter
     */
    public WorkersCategoryGroupStep getWorkersCategoryGroupStepFilter() {

        return workersCategoryGroupStepFilter;
    }

    /**
     * @param workersCategoryGroupStepFilter the workersCategoryGroupStepFilter
     * to set
     */
    public void setWorkersCategoryGroupStepFilter(WorkersCategoryGroupStep workersCategoryGroupStepFilter) {
        this.workersCategoryGroupStepFilter = workersCategoryGroupStepFilter;
    }

    /**
     * @return the workersCategoryGroups
     */
    public List<WorkersCategoryGroup> getWorkersCategoryGroups() {
        if (workersCategoryChanged) {
            Map<String, Object> m = new HashMap<>();
            m.put("corporate", getLoggedCorporate());

            m.put("workersCategory", workersCategoryFilter);

            try {
                workersCategoryGroups = workersCategoryGroupFacadeLocal.findAll(m, true, new String[]{"groupLevel"});
            } catch (Exception ex) {
                Logger.getLogger(WorkersTableBean.class.getName()).log(Level.SEVERE, null, ex);
                workersCategoryGroups = new ArrayList<>();
            }
        } else {
            workersCategoryGroups = new ArrayList<>();
        } 
          setWorkersCategoryChanged(false);      
        return workersCategoryGroups;
    }

    /**
     * @param workersCategoryGroups the workersCategoryGroups to set
     */
    public void setWorkersCategoryGroups(List<WorkersCategoryGroup> workersCategoryGroups) {
        this.workersCategoryGroups = workersCategoryGroups;
    }

    /**
     * @return the pfas
     */
    public List<PFA> getPfas() {
        if (pfas == null) {
            Map<String, Object> m = new HashMap<>();
            m.put("corporate", getLoggedCorporate());
            try {
                pfas = pfaSetupFacadeLocal.findAll(m, true);
            } catch (Exception ex) {
                Logger.getLogger(WorkersTableBean.class.getName()).log(Level.SEVERE, null, ex);
                pfas = new ArrayList<>();
            }
        }
        return pfas;
    }

    /**
     * @param pfas the pfas to set
     */
    public void setPfas(List<PFA> pfas) {
        this.pfas = pfas;
    }

    /**
     * @return the taxs
     */
    public List<Tax> getTaxs() {
        if (taxs == null) {
            Map<String, Object> m = new HashMap<>();
            m.put("corporate", getLoggedCorporate());
            try {
                taxs = taxOfficeSetupFacadeLocal.findAll(m, true);
            } catch (Exception ex) {
                Logger.getLogger(WorkersTableBean.class.getName()).log(Level.SEVERE, null, ex);
                taxs = new ArrayList<>();
            }
        }
        return taxs;
    }

    /**
     * @param taxs the taxs to set
     */
    public void setTaxs(List<Tax> taxs) {
        this.taxs = taxs;
    }

    /**
     * @return the pfaFilter
     */
    public PFA getPfaFilter() {
        return pfaFilter;
    }

    /**
     * @param pfaFilter the pfaFilter to set
     */
    public void setPfaFilter(PFA pfaFilter) {
        this.pfaFilter = pfaFilter;
    }

    /**
     * @return the taxFilter
     */
    public Tax getTaxFilter() {
        return taxFilter;
    }

    /**
     * @param taxFilter the taxFilter to set
     */
    public void setTaxFilter(Tax taxFilter) {
        this.taxFilter = taxFilter;
    }

    /**
     * @return the workersCategoryGroupFilter
     */
    public WorkersCategoryGroup getWorkersCategoryGroupFilter() {
        return workersCategoryGroupFilter;
    }

    /**
     * @param workersCategoryGroupFilter the workersCategoryGroupFilter to set
     */
    public void setWorkersCategoryGroupFilter(WorkersCategoryGroup workersCategoryGroupFilter) {
        this.workersCategoryGroupFilter = workersCategoryGroupFilter;
    }

    /**
     * @return the preference
     */
    public Preferences getPreference() {
        if (preference == null) {
            try {
                preference = preferenceFacadeLocal.getCorporatePreferences(getLoggedCorporate());
            } catch (Exception ex) {
                Logger.getLogger(StatutoryDeductionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return preference;
    }

    /**
     * @return the completePayroll
     */
    public boolean isCompletePayroll() {
        completePayroll = getPreference() != null ? getPreference().getPayrollMode().equals(AppConstants.COMPLETE_PAYROLL) : completePayroll;
        return completePayroll;
    }

    /**
     * @param workersCategoryChanged the workersCategoryChanged to set
     */
    public void setWorkersCategoryChanged(boolean workersCategoryChanged) {
        this.workersCategoryChanged = workersCategoryChanged;
    }

   
    
    

}
