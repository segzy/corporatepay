/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersSalaryDeductionFacade extends AbstractFacade<WorkersSalaryDeduction> implements WorkersSalaryDeductionFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private WorkersSalaryDeductionWorkflowFacadeLocal workersSalaryDeductionWorkflowFacadeLocal;

    public WorkersSalaryDeductionFacade() {
        super(WorkersSalaryDeduction.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public WorkersSalaryDeduction create(WorkersSalaryDeduction entity) throws Exception {
        entity.setCorporate(getLoggedCorporate());
        WorkersSalaryDeduction create = super.create(entity);
        workersSalaryDeductionWorkflowFacadeLocal.startWorkflowCorporate(WorkersSalaryDeduction.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, getLoggedCorporate(), null);
        return create;
    }

    @Override
    public WorkersSalaryDeduction edit(WorkersSalaryDeduction entity) throws Exception {
        WorkersSalaryDeduction workersSalaryDeduction = find(entity.getId());
        if (workersSalaryDeduction instanceof OneTimeWorkersDeduction) {
            ((OneTimeWorkersDeduction) workersSalaryDeduction).setTmpAmount(((OneTimeWorkersDeduction) entity).getAmount());
            ((OneTimeWorkersDeduction) workersSalaryDeduction).setTmpAmountType(((OneTimeWorkersDeduction) entity).getAmountType());
        } else if (workersSalaryDeduction instanceof RecurringWorkersDeduction) {
            ((RecurringWorkersDeduction) workersSalaryDeduction).setTmpAmount(((RecurringWorkersDeduction) entity).getAmount());
            ((RecurringWorkersDeduction) workersSalaryDeduction).setTmpAmountType(((RecurringWorkersDeduction) entity).getAmountType());
            ((RecurringWorkersDeduction) workersSalaryDeduction).setTmpIndefinite(((RecurringWorkersDeduction) entity).getIndefinite());
            ((RecurringWorkersDeduction) workersSalaryDeduction).setTmpNoOfOcurrences(((RecurringWorkersDeduction) entity).getNoOfOcurrences());
        } else if (workersSalaryDeduction instanceof LoanWorkersDeduction) {
            ((LoanWorkersDeduction) workersSalaryDeduction).setTmpAmount(((LoanWorkersDeduction) entity).getAmount());
            ((LoanWorkersDeduction) workersSalaryDeduction).setTmpDeductionAmount(((LoanWorkersDeduction) entity).getDeductionAmount());
        }
        workersSalaryDeduction.setTmpActive(entity.getActive());
        workersSalaryDeduction.setFlagStatus(entity.getFlagStatus());
        WorkersSalaryDeduction create = super.create(workersSalaryDeduction);
        workersSalaryDeductionWorkflowFacadeLocal.startWorkflowCorporate(WorkersSalaryDeduction.class, entity.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, getLoggedCorporate(), null);
        return create; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finalizeApprove(WorkersSalaryDeduction t, ApprovalTask task) throws Exception {
        log.info("finalizin worker's deduction....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            t.setActive(t.getTmpActive());
            if (t instanceof RecurringWorkersDeduction) {
                ((RecurringWorkersDeduction) t).setAmount(((RecurringWorkersDeduction) t).getTmpAmount());
                ((RecurringWorkersDeduction) t).setAmountType(((RecurringWorkersDeduction) t).getTmpAmountType());
                ((RecurringWorkersDeduction) t).setIndefinite(((RecurringWorkersDeduction) t).getTmpIndefinite());
                ((RecurringWorkersDeduction) t).setNoOfOcurrences(((RecurringWorkersDeduction) t).getTmpNoOfOcurrences());
                ((RecurringWorkersDeduction) t).setTmpAmount(null);
                ((RecurringWorkersDeduction) t).setTmpAmountType(null);
                ((RecurringWorkersDeduction) t).setTmpIndefinite(null);
                ((RecurringWorkersDeduction) t).setTmpNoOfOcurrences(0);
            } else if (t instanceof OneTimeWorkersDeduction) {
                ((OneTimeWorkersDeduction) t).setAmount(((OneTimeWorkersDeduction) t).getTmpAmount());
                ((OneTimeWorkersDeduction) t).setAmountType(((OneTimeWorkersDeduction) t).getTmpAmountType());
                ((OneTimeWorkersDeduction) t).setTmpAmount(null);
                ((OneTimeWorkersDeduction) t).setTmpAmountType(null);
            } else if (t instanceof LoanWorkersDeduction) {
                ((LoanWorkersDeduction) t).setTmpAmount(null);
                ((LoanWorkersDeduction) t).setTmpDeductionAmount(null);
            }
            t.setTmpActive(null);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
    }

}
