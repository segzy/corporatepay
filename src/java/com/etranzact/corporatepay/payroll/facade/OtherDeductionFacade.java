/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.payroll.model.OtherDeduction;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class OtherDeductionFacade extends AbstractFacade<OtherDeduction> implements OtherDeductionFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public OtherDeductionFacade() {
        super(OtherDeduction.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OtherDeduction edit(OtherDeduction entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 
    
}
