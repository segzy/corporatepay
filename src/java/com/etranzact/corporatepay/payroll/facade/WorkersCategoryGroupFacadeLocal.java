/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.payment.facade.*;
import com.etranzact.corporatepay.setup.facade.*;
import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroup;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface WorkersCategoryGroupFacadeLocal extends AbstractFacadeLocal<WorkersCategoryGroup> {

     public void remove(WorkersCategoryGroup wp) throws Exception;

}
