/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroup;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroupStep;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersCategoryGroupFacade extends AbstractFacade<WorkersCategoryGroup> implements WorkersCategoryGroupFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public WorkersCategoryGroupFacade() {
        super(WorkersCategoryGroup.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected WorkersCategoryGroup edit(WorkersCategoryGroup entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(WorkersCategoryGroup wp) throws Exception {
        WorkersCategoryGroup categoryGroup = find(wp.getId());
        for(WorkersCategoryGroupStep categoryGroupStep: categoryGroup.getWorkersCategoryGroupSteps()) {
            em.remove(categoryGroupStep);
        }
        em.remove(categoryGroup);
    }


    
}
