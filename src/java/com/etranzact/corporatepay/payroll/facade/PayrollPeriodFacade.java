/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.payment.facade.*;
import com.etranzact.corporatepay.setup.facade.*;
import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.core.facade.ParametersFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.PaymentScheduler;
import com.etranzact.corporatepay.model.SchedulePayment;
import com.etranzact.corporatepay.model.SingleBeneficiaryPayment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.model.UploadPayment;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import com.etz.security.util.Cryptographer;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class PayrollPeriodFacade extends AbstractFacade<PayrollPeriod> implements PayrollPeriodFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public PayrollPeriodFacade() {
        super(PayrollPeriod.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PayrollPeriod create(PayrollPeriod entity) throws Exception {
        List<WorkersCategory> workersCategorys=null;
        for (WorkersCategory category : entity.getWorkersCategories()) {
            log.info("category::: " + category.getId());
            if(workersCategorys==null) {
              workersCategorys = new ArrayList<>();
            }
            workersCategorys.add(em.merge(category));
        }
        entity.setWorkersCategories(workersCategorys);
        return super.create(entity);
    }

    @Override
    public PayrollPeriod edit(PayrollPeriod entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
