/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.payroll.model.PayrollPeriod;
import com.etranzact.corporatepay.payroll.model.WorkersCategory;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersCategoryFacade extends AbstractFacade<WorkersCategory> implements WorkersCategoryFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public WorkersCategoryFacade() {
        super(WorkersCategory.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public WorkersCategory edit(WorkersCategory entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(WorkersCategory t, WorkersCategoryGroup wp) throws Exception {
        WorkersCategory category = find(t.getId());
        category.getWorkersCategoryGroups().remove(wp);
    }

    
}
