/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroup;
import com.etranzact.corporatepay.payroll.model.WorkersCategoryGroupStep;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersCategoryGroupStepFacade extends AbstractFacade<WorkersCategoryGroupStep> implements WorkersCategoryGroupStepFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public WorkersCategoryGroupStepFacade() {
        super(WorkersCategoryGroupStep.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected WorkersCategoryGroupStep edit(WorkersCategoryGroupStep entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
