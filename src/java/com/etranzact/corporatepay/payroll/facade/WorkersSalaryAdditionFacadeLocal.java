/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryAddition;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface WorkersSalaryAdditionFacadeLocal extends AbstractFacadeLocal<WorkersSalaryAddition> {

    
        public WorkersSalaryAddition edit(WorkersSalaryAddition entity) throws Exception;

    public void finalizeApprove(WorkersSalaryAddition t, ApprovalTask task) throws Exception;

}
