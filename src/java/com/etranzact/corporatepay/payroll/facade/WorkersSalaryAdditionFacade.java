/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersAddition;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryAddition;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersSalaryAdditionFacade extends AbstractFacade<WorkersSalaryAddition> implements WorkersSalaryAdditionFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private WorkersSalaryAdditionWorkflowFacadeLocal workersSalaryAdditionWorkflowFacadeLocal;

    public WorkersSalaryAdditionFacade() {
        super(WorkersSalaryAddition.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public WorkersSalaryAddition create(WorkersSalaryAddition entity) throws Exception {
        entity.setCorporate(getLoggedCorporate());
        WorkersSalaryAddition create = super.create(entity);
        workersSalaryAdditionWorkflowFacadeLocal.startWorkflowCorporate(WorkersSalaryAddition.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, getLoggedCorporate(), null);
        return create;
    }

    @Override
    public WorkersSalaryAddition edit(WorkersSalaryAddition entity) throws Exception {
        WorkersSalaryAddition workersSalaryAddition = find(entity.getId());
        if (workersSalaryAddition instanceof OneTimeWorkersAddition) {
            ((OneTimeWorkersAddition) workersSalaryAddition).setTmpAmount(((OneTimeWorkersAddition) entity).getAmount());
            ((OneTimeWorkersAddition) workersSalaryAddition).setTmpAmountType(((OneTimeWorkersAddition) entity).getAmountType());
        } else if (workersSalaryAddition instanceof RecurringWorkersAddition) {
            ((RecurringWorkersAddition) workersSalaryAddition).setTmpAmount(((RecurringWorkersAddition) entity).getAmount());
            ((RecurringWorkersAddition) workersSalaryAddition).setTmpAmountType(((RecurringWorkersAddition) entity).getAmountType());
            ((RecurringWorkersAddition) workersSalaryAddition).setTmpIndefinite(((RecurringWorkersAddition) entity).getIndefinite());
            ((RecurringWorkersAddition) workersSalaryAddition).setTmpNoOfOcurrences(((RecurringWorkersAddition) entity).getNoOfOcurrences());
        }
        workersSalaryAddition.setTmpActive(entity.getActive());
        workersSalaryAddition.setFlagStatus(entity.getFlagStatus());
        WorkersSalaryAddition create = super.create(workersSalaryAddition);
        workersSalaryAdditionWorkflowFacadeLocal.startWorkflowCorporate(WorkersSalaryAddition.class, entity.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, getLoggedCorporate(), null);
        return create; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finalizeApprove(WorkersSalaryAddition t, ApprovalTask task) throws Exception {
        log.info("finalizin worker's addition....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            t.setActive(t.getTmpActive());
             if (t instanceof RecurringWorkersAddition) {
                ((RecurringWorkersAddition) t).setAmount(((RecurringWorkersAddition) t).getTmpAmount());
                ((RecurringWorkersAddition) t).setAmountType(((RecurringWorkersAddition) t).getTmpAmountType());
                ((RecurringWorkersAddition) t).setIndefinite(((RecurringWorkersAddition) t).getTmpIndefinite());
                ((RecurringWorkersAddition) t).setNoOfOcurrences(((RecurringWorkersAddition) t).getTmpNoOfOcurrences());
                ((RecurringWorkersAddition) t).setTmpAmount(null);
                ((RecurringWorkersAddition) t).setTmpAmountType(null);
                ((RecurringWorkersAddition) t).setTmpIndefinite(null);
                ((RecurringWorkersAddition) t).setTmpNoOfOcurrences(0);
            }  else  if (t instanceof OneTimeWorkersAddition) {
                ((OneTimeWorkersAddition) t).setAmount(((OneTimeWorkersAddition) t).getTmpAmount());
                ((OneTimeWorkersAddition) t).setAmountType(((OneTimeWorkersAddition) t).getTmpAmountType());
                ((OneTimeWorkersAddition) t).setTmpAmount(null);
                ((OneTimeWorkersAddition) t).setTmpAmountType(null);
            }
            t.setTmpActive(null);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
    }

}
