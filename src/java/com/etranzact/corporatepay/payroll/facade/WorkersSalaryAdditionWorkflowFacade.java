/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersAddition;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersAddition;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryAddition;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersSalaryAdditionWorkflowFacade extends AbstractWorkflowFacade implements WorkersSalaryAdditionWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        WorkersSalaryAddition workerSalaryAddition = (WorkersSalaryAddition) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(0, new TaskObjectNameValuePair("Worker: ", workerSalaryAddition.getWorker().getFirstName() + " " + workerSalaryAddition.getWorker().getLastName() + " (" + workerSalaryAddition.getWorker().getStaffId() + ")", null));
        nameValuePairs.add(1, new TaskObjectNameValuePair("Payroll Period: ", workerSalaryAddition.getIncluPayrollPeriod().getTag(), null));
        nameValuePairs.add(2, new TaskObjectNameValuePair("Addition: ", workerSalaryAddition.getOtherAddition().getAdditionName(), null));
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
             if (workerSalaryAddition instanceof RecurringWorkersAddition) {
                if (!((RecurringWorkersAddition) workerSalaryAddition).getAmountType().equals(((RecurringWorkersAddition) workerSalaryAddition).getTmpAmountType())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Amount Type: ", ((RecurringWorkersAddition) workerSalaryAddition).getAmountTypeLabel(),
                            ((RecurringWorkersAddition) workerSalaryAddition).getTmpAmountTypeLabel()));
                }
                if (!((RecurringWorkersAddition) workerSalaryAddition).getAmount().equals(((RecurringWorkersAddition) workerSalaryAddition).getTmpAmount())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((RecurringWorkersAddition) workerSalaryAddition).getAmount().toString(), "###,###.###"),
                            AppUtil.formatAsAmount(((RecurringWorkersAddition) workerSalaryAddition).getTmpAmount().toString(), "###,###.###")));
                }
                if (!((RecurringWorkersAddition) workerSalaryAddition).getIndefinite().equals(((RecurringWorkersAddition) workerSalaryAddition).getTmpIndefinite())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Indefinite: ", ((RecurringWorkersAddition) workerSalaryAddition).getIndefinite() ? "Indefinite" : "Finite",
                            ((RecurringWorkersAddition) workerSalaryAddition).getTmpIndefinite() ? "Indefinite" : "Finite"));
                }
                if (((RecurringWorkersAddition) workerSalaryAddition).getNoOfOcurrences() != ((RecurringWorkersAddition) workerSalaryAddition).getTmpNoOfOcurrences()) {
                    nameValuePairs.add(new TaskObjectNameValuePair("No of Payroll Period: ", String.valueOf(((RecurringWorkersAddition) workerSalaryAddition).getNoOfOcurrences()),
                            String.valueOf(((RecurringWorkersAddition) workerSalaryAddition).getTmpNoOfOcurrences())));
                }
                if (!workerSalaryAddition.getActive().equals(workerSalaryAddition.getTmpActive())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Deduction Status: ", workerSalaryAddition.getActive() ? "Active" : "Inactive",
                            workerSalaryAddition.getTmpActive() ? "Active" : "Inactive"));
                }
            } else if (workerSalaryAddition instanceof OneTimeWorkersAddition) {
                if (!((OneTimeWorkersAddition) workerSalaryAddition).getAmountType().equals(((OneTimeWorkersAddition) workerSalaryAddition).getTmpAmountType())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Amount Type: ", ((OneTimeWorkersAddition) workerSalaryAddition).getAmountTypeLabel(),
                            ((OneTimeWorkersAddition) workerSalaryAddition).getTmpAmountTypeLabel()));
                }
                if (!((OneTimeWorkersAddition) workerSalaryAddition).getAmount().equals(((OneTimeWorkersAddition) workerSalaryAddition).getTmpAmount())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((OneTimeWorkersAddition) workerSalaryAddition).getAmount().toString(), "###,###.###"),
                            AppUtil.formatAsAmount(((OneTimeWorkersAddition) workerSalaryAddition).getTmpAmount().toString(), "###,###.###")));
                }
                  if (!workerSalaryAddition.getActive().equals(workerSalaryAddition.getTmpActive())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Deduction Status: ", workerSalaryAddition.getActive() ? "Active" : "Inactive",
                            workerSalaryAddition.getTmpActive() ? "Active" : "Inactive"));
                }
            } 

        } else {
            if (workerSalaryAddition instanceof OneTimeWorkersAddition) {
                nameValuePairs.add(3, new TaskObjectNameValuePair("Amount Type: ", ((OneTimeWorkersAddition) workerSalaryAddition).getAmountTypeLabel(),
                        null));
                nameValuePairs.add(4, new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((OneTimeWorkersAddition) workerSalaryAddition).getAmount().toString()),
                        null));
            } else if (workerSalaryAddition instanceof RecurringWorkersAddition) {
                nameValuePairs.add(3, new TaskObjectNameValuePair("Amount Type: ", ((RecurringWorkersAddition) workerSalaryAddition).getAmountTypeLabel(),
                        null));
                nameValuePairs.add(4, new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((RecurringWorkersAddition) workerSalaryAddition).getAmount().toString()),
                        null));
                nameValuePairs.add(5, new TaskObjectNameValuePair("Indefinite: ", ((RecurringWorkersAddition) workerSalaryAddition).getIndefinite() ? "Indefinite" : "Definite",
                        null));
                nameValuePairs.add(6, new TaskObjectNameValuePair("No Of Occurrences: ", String.valueOf(((RecurringWorkersAddition) workerSalaryAddition).getNoOfOcurrences()),
                        null));
            }
        }

        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
