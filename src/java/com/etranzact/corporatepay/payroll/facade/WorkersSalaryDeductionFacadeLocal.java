/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface WorkersSalaryDeductionFacadeLocal extends AbstractFacadeLocal<WorkersSalaryDeduction> {

    
        public WorkersSalaryDeduction edit(WorkersSalaryDeduction entity) throws Exception;

    public void finalizeApprove(WorkersSalaryDeduction t, ApprovalTask task) throws Exception;

}
