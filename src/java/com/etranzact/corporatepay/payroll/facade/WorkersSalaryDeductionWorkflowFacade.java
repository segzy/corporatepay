/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.payroll.model.LoanWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.OneTimeWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.RecurringWorkersDeduction;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.payroll.model.WorkersSalaryDeduction;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersSalaryDeductionWorkflowFacade extends AbstractWorkflowFacade implements WorkersSalaryDeductionWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        WorkersSalaryDeduction workerSalaryDeduction = (WorkersSalaryDeduction) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(0, new TaskObjectNameValuePair("Worker: ", workerSalaryDeduction.getWorker().getFirstName() + " " + workerSalaryDeduction.getWorker().getLastName() + " (" + workerSalaryDeduction.getWorker().getStaffId() + ")", null));
        nameValuePairs.add(1, new TaskObjectNameValuePair("Payroll Period: ", workerSalaryDeduction.getIncluPayrollPeriod().getTag(), null));
        nameValuePairs.add(2, new TaskObjectNameValuePair("Deduction: ", workerSalaryDeduction.getOtherDeduction().getDeductionName(), null));
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
             if (workerSalaryDeduction instanceof RecurringWorkersDeduction) {
                if (!((RecurringWorkersDeduction) workerSalaryDeduction).getAmountType().equals(((RecurringWorkersDeduction) workerSalaryDeduction).getTmpAmountType())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Amount Type: ", ((RecurringWorkersDeduction) workerSalaryDeduction).getAmountTypeLabel(),
                            ((RecurringWorkersDeduction) workerSalaryDeduction).getTmpAmountTypeLabel()));
                }
                if (!((RecurringWorkersDeduction) workerSalaryDeduction).getAmount().equals(((RecurringWorkersDeduction) workerSalaryDeduction).getTmpAmount())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((RecurringWorkersDeduction) workerSalaryDeduction).getAmount().toString(), "###,###.###"),
                            AppUtil.formatAsAmount(((RecurringWorkersDeduction) workerSalaryDeduction).getTmpAmount().toString(), "###,###.###")));
                }
                if (!((RecurringWorkersDeduction) workerSalaryDeduction).getIndefinite().equals(((RecurringWorkersDeduction) workerSalaryDeduction).getTmpIndefinite())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Indefinite: ", ((RecurringWorkersDeduction) workerSalaryDeduction).getIndefinite() ? "Indefinite" : "Finite",
                            ((RecurringWorkersDeduction) workerSalaryDeduction).getTmpIndefinite() ? "Indefinite" : "Finite"));
                }
                if (((RecurringWorkersDeduction) workerSalaryDeduction).getNoOfOcurrences() != ((RecurringWorkersDeduction) workerSalaryDeduction).getTmpNoOfOcurrences()) {
                    nameValuePairs.add(new TaskObjectNameValuePair("No of Payroll Period: ", String.valueOf(((RecurringWorkersDeduction) workerSalaryDeduction).getNoOfOcurrences()),
                            String.valueOf(((RecurringWorkersDeduction) workerSalaryDeduction).getTmpNoOfOcurrences())));
                }
                if (!workerSalaryDeduction.getActive().equals(workerSalaryDeduction.getTmpActive())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Deduction Status: ", workerSalaryDeduction.getActive() ? "Active" : "Inactive",
                            workerSalaryDeduction.getTmpActive() ? "Active" : "Inactive"));
                }
            } else if (workerSalaryDeduction instanceof OneTimeWorkersDeduction) {
                if (!((OneTimeWorkersDeduction) workerSalaryDeduction).getAmountType().equals(((OneTimeWorkersDeduction) workerSalaryDeduction).getTmpAmountType())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Amount Type: ", ((OneTimeWorkersDeduction) workerSalaryDeduction).getAmountTypeLabel(),
                            ((OneTimeWorkersDeduction) workerSalaryDeduction).getTmpAmountTypeLabel()));
                }
                if (!((OneTimeWorkersDeduction) workerSalaryDeduction).getAmount().equals(((OneTimeWorkersDeduction) workerSalaryDeduction).getTmpAmount())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((OneTimeWorkersDeduction) workerSalaryDeduction).getAmount().toString(), "###,###.###"),
                            AppUtil.formatAsAmount(((OneTimeWorkersDeduction) workerSalaryDeduction).getTmpAmount().toString(), "###,###.###")));
                }
                  if (!workerSalaryDeduction.getActive().equals(workerSalaryDeduction.getTmpActive())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Deduction Status: ", workerSalaryDeduction.getActive() ? "Active" : "Inactive",
                            workerSalaryDeduction.getTmpActive() ? "Active" : "Inactive"));
                }
            }  else if (workerSalaryDeduction instanceof LoanWorkersDeduction) {
                if (!((LoanWorkersDeduction) workerSalaryDeduction).getAmount().equals(((LoanWorkersDeduction) workerSalaryDeduction).getTmpAmount())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Loan Principal Amount: ", AppUtil.formatAsAmount(((LoanWorkersDeduction) workerSalaryDeduction).getAmount().toString(), "###,###.###"),
                            AppUtil.formatAsAmount(((LoanWorkersDeduction) workerSalaryDeduction).getTmpAmount().toString(), "###,###.###")));
                }
                if (!((LoanWorkersDeduction) workerSalaryDeduction).getDeductionAmount().equals(((LoanWorkersDeduction) workerSalaryDeduction).getTmpDeductionAmount())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Loan Deduction Amount: ", AppUtil.formatAsAmount(((LoanWorkersDeduction) workerSalaryDeduction).getDeductionAmount().toString(), "###,###.###"),
                            AppUtil.formatAsAmount(((LoanWorkersDeduction) workerSalaryDeduction).getTmpDeductionAmount().toString(), "###,###.###")));
                }
                  if (!workerSalaryDeduction.getActive().equals(workerSalaryDeduction.getTmpActive())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Deduction Status: ", workerSalaryDeduction.getActive() ? "Active" : "Inactive",
                            workerSalaryDeduction.getTmpActive() ? "Active" : "Inactive"));
                }
            }

        } else {
            if (workerSalaryDeduction instanceof OneTimeWorkersDeduction) {
                nameValuePairs.add(3, new TaskObjectNameValuePair("Amount Type: ", ((OneTimeWorkersDeduction) workerSalaryDeduction).getAmountTypeLabel(),
                        null));
                nameValuePairs.add(4, new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((OneTimeWorkersDeduction) workerSalaryDeduction).getAmount().toString()),
                        null));
            } else if (workerSalaryDeduction instanceof RecurringWorkersDeduction) {
                nameValuePairs.add(3, new TaskObjectNameValuePair("Amount Type: ", ((RecurringWorkersDeduction) workerSalaryDeduction).getAmountTypeLabel(),
                        null));
                nameValuePairs.add(4, new TaskObjectNameValuePair("Value: ", AppUtil.formatAsAmount(((RecurringWorkersDeduction) workerSalaryDeduction).getAmount().toString()),
                        null));
                nameValuePairs.add(5, new TaskObjectNameValuePair("Indefinite: ", ((RecurringWorkersDeduction) workerSalaryDeduction).getIndefinite() ? "Indefinite" : "Definite",
                        null));
                nameValuePairs.add(6, new TaskObjectNameValuePair("No Of Occurrences: ", String.valueOf(((RecurringWorkersDeduction) workerSalaryDeduction).getNoOfOcurrences()),
                        null));
            }else if (workerSalaryDeduction instanceof LoanWorkersDeduction) {
                nameValuePairs.add(3, new TaskObjectNameValuePair("Principal Amount: ", AppUtil.formatAsAmount(((LoanWorkersDeduction) workerSalaryDeduction).getAmount().toString()),
                        null));
                nameValuePairs.add(4, new TaskObjectNameValuePair("Deduction Amount: ", AppUtil.formatAsAmount(((LoanWorkersDeduction) workerSalaryDeduction).getDeductionAmount().toString()),
                        null));
            }
        }

        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
