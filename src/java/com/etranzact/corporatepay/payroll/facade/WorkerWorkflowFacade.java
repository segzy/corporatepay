/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkerWorkflowFacade extends AbstractWorkflowFacade implements WorkerWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        Worker worker = (Worker) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new TaskObjectNameValuePair("Staff Id: ", worker.getStaffId(), null));
        
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            if (!worker.getFirstName().equals(worker.getTmpFirstName())) {
                nameValuePairs.add(new TaskObjectNameValuePair("First Name: ", worker.getFirstName(),
                        worker.getTmpFirstName()));
            }
            if (!worker.getLastName().equals(worker.getTmpLastName())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Last Name: ", worker.getLastName(),
                        worker.getTmpLastName()));
            }
            if (!worker.getWorkersCategory().equals(worker.getTmpWorkersCategory())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Worker's Category: ", worker.getWorkersCategory().getId(),
                        worker.getTmpWorkersCategory().getId()));
            }
            if (!worker.getWorkersCategoryGroupStep().equals(worker.getTmpWorkersCategoryGroupStep())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Worker's Salary Position: ", worker.getWorkersCategory().getGroupNaming() + " " + worker.getWorkersCategoryGroupStep().getWorkersCategoryGroup().getGroupLevel() + "  " + worker.getWorkersCategory().getGroupStepNaming() + " " + worker.getWorkersCategoryGroupStep().getGroupStepLevel(),
                        worker.getTmpWorkersCategory().getGroupNaming() + " " + worker.getTmpWorkersCategoryGroupStep().getWorkersCategoryGroup().getGroupLevel() + "  " + worker.getTmpWorkersCategory().getGroupStepNaming() + " " + worker.getTmpWorkersCategoryGroupStep().getGroupStepLevel()));
            }
            if (!worker.getTax().equals(worker.getTmpTax())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Tax Office: ", worker.getTax().getAuthName(),
                        worker.getTmpTax().getAuthName()));
            }
            if (!worker.getPfa().equals(worker.getTmpPfa())) {
                nameValuePairs.add(new TaskObjectNameValuePair("PFA: ", worker.getPfa().getAuthName(),
                        worker.getTmpPfa().getAuthName()));
            }
            if (!worker.getCorporateBeneficiary().getAccount().getAccountNumber().equals(worker.getTmpCorporateBeneficiary().getAccount().getAccountNumber())) {
                String currAcct = "";
                String newAcct = "";
                if (worker.getCorporateBeneficiary().getAccount() instanceof BankAccount) {
                    BankAccount bankAccount = (BankAccount) worker.getCorporateBeneficiary().getAccount();
                    currAcct = bankAccount.getAccountIdentity();
                } else if (worker.getCorporateBeneficiary().getAccount() instanceof PocketMoneyAccount) {
                    PocketMoneyAccount pocketMoneyAccount = (PocketMoneyAccount) worker.getCorporateBeneficiary().getAccount();
                    currAcct = pocketMoneyAccount.getAccountIdentity();
                }

                if (worker.getTmpCorporateBeneficiary().getAccount() instanceof BankAccount) {
                    BankAccount bankAccount = (BankAccount) worker.getTmpCorporateBeneficiary().getAccount();
                    newAcct = bankAccount.getAccountIdentity();
                } else if (worker.getTmpCorporateBeneficiary().getAccount() instanceof PocketMoneyAccount) {
                    PocketMoneyAccount pocketMoneyAccount = (PocketMoneyAccount) worker.getTmpCorporateBeneficiary().getAccount();
                    newAcct = pocketMoneyAccount.getAccountIdentity();
                }
                nameValuePairs.add(new TaskObjectNameValuePair("Account Details: ", currAcct,
                        newAcct));
              
            }
               nameValuePairs.add(new TaskObjectNameValuePair("Workers Status? ", worker.getActive()?"Active":"InActive",
                        worker.getTmpActive()?"Active":"InActive"));
               if(worker.getTmpWstate()!=null) {
                  nameValuePairs.add(new TaskObjectNameValuePair("Request? ", worker.getTmpWstate().equals(AppConstants.SUSPEND_WORKER)?"Request to Suspend Worker ":worker.getTmpWstate().equals(AppConstants.TERMINATE_WORKER)?"Request to Terminate Worker":"Request to Reinstate Worker",
                        null));   
               }
        } else {
            nameValuePairs.add(0, new TaskObjectNameValuePair("First Name: ", worker.getFirstName(),
                    null));
            nameValuePairs.add(1, new TaskObjectNameValuePair("Last Name: ", worker.getLastName(),
                    null));
            nameValuePairs.add(2, new TaskObjectNameValuePair("Worker's Category: ", worker.getWorkersCategory().getId(),
                    null));
            nameValuePairs.add(3, new TaskObjectNameValuePair("Worker's Salary Position: ", worker.getWorkersCategory().getGroupNaming() + " " + worker.getWorkersCategoryGroupStep().getWorkersCategoryGroup().getGroupLevel() + "  " + worker.getWorkersCategory().getGroupStepNaming() + " " + worker.getWorkersCategoryGroupStep().getGroupStepLevel(),
                    null));
            nameValuePairs.add(4, new TaskObjectNameValuePair("Tax Office: ", worker.getTax().getAuthName(),
                    null));
            nameValuePairs.add(5, new TaskObjectNameValuePair("PFA: ", worker.getPfa().getAuthName(),
                    null));
                 String currAcct = "";
                if (worker.getCorporateBeneficiary().getAccount() instanceof BankAccount) {
                    BankAccount bankAccount = (BankAccount) worker.getCorporateBeneficiary().getAccount();
                    currAcct = bankAccount.getAccountIdentity();
                } else if (worker.getCorporateBeneficiary().getAccount() instanceof PocketMoneyAccount) {
                    PocketMoneyAccount pocketMoneyAccount = (PocketMoneyAccount) worker.getCorporateBeneficiary().getAccount();
                    currAcct = pocketMoneyAccount.getAccountIdentity();
                }
                   nameValuePairs.add(6, new TaskObjectNameValuePair("Account: ", currAcct,
                    null));
        }

        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
