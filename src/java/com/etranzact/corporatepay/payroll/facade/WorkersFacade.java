/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.payroll.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payroll.model.Worker;
import com.etranzact.corporatepay.util.AppConstants;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkersFacade extends AbstractFacade<Worker> implements WorkersFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private WorkerWorkflowFacadeLocal workerWorkflowFacadeLocal;

    public WorkersFacade() {
        super(Worker.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Worker create(Worker entity) throws Exception {
        entity.getCorporateBeneficiary().setAccount(em.merge(entity.getCorporateBeneficiary().getAccount()));
        entity.setCorporateBeneficiary(em.merge(entity.getCorporateBeneficiary()));
        Worker create = super.create(entity);
        if(create.getStaffId()==null || create.getStaffId().isEmpty()) {
          create.setStaffId(create.getId().toString());
        }
        workerWorkflowFacadeLocal.startWorkflowCorporate(Worker.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, getLoggedCorporate(), null);
        return create;
    }

    @Override
    public Worker edit(Worker entity) throws Exception {
        Worker dbWorker = find(entity.getId());
        dbWorker.setTmpTax(entity.getTax());
        dbWorker.setTmpCorporateBeneficiary(entity.getCorporateBeneficiary());
        dbWorker.setTmpFirstName(entity.getFirstName());
        dbWorker.setTmpLastName(entity.getLastName());
        dbWorker.setTmpPfa(entity.getPfa());
        dbWorker.setTmpWorkersCategory(entity.getWorkersCategory());
        dbWorker.setTmpWorkersCategoryGroupStep(entity.getWorkersCategoryGroupStep());
        dbWorker.setTmpActive(entity.getActive());
        dbWorker.setTmpWstate(entity.getWstate());
        dbWorker.setFlagStatus(entity.getFlagStatus());
        Worker create = super.create(dbWorker);
        workerWorkflowFacadeLocal.startWorkflowCorporate(Worker.class, entity.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, getLoggedCorporate(), null);
        return create; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finalizeApprove(Worker t, ApprovalTask task) throws Exception {
        log.info("finalizin worker....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            t.setActive(t.getTmpActive());
            t.setTax(t.getTmpTax());
            t.setCorporateBeneficiary(t.getTmpCorporateBeneficiary());
            t.setFirstName(t.getTmpFirstName());
            t.setLastName(t.getTmpLastName());
            t.setPfa(t.getTmpPfa());
            t.setWorkersCategory(t.getTmpWorkersCategory());
            t.setWorkersCategoryGroupStep(t.getTmpWorkersCategoryGroupStep());
            t.setWstate(t.getTmpWstate());
            t.setTmpCorporateBeneficiary(null);
            t.setTmpFirstName(null);
            t.setTmpLastName(null);
            t.setTmpPfa(null);
            t.setTmpWstate(null);
            t.setTmpWorkersCategory(null);
            t.setTmpWorkersCategoryGroupStep(null);
            t.setTmpActive(null);
            t.setTmpTax(null);
        } else {
            t.setActive(Boolean.TRUE); 
        }
         t.setFlagStatus(status);
        
    }

}
