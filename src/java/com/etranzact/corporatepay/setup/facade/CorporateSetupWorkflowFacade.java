/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporatePayService;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class CorporateSetupWorkflowFacade extends AbstractWorkflowFacade implements CorporateSetupWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        Corporate corporate = (Corporate) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        String services = "";
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            if (!corporate.getCorporateName().equals(corporate.getTmpCorporateName())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Corporate Name: ", corporate.getCorporateName(),
                        corporate.getTmpCorporateName()));
            }
            if (!corporate.getContactEmail().equals(corporate.getTmpContactEmail())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Contact Email: ", corporate.getContactEmail(),
                        corporate.getTmpContactEmail()));
            }
            if (!corporate.getContactPhone().equals(corporate.getTmpContactPhone())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Contact Phone: ", corporate.getContactPhone(),
                        corporate.getTmpContactPhone()));
            }
            if (corporate.getContactState() != null && !corporate.getContactState().equals(corporate.getTmpContactState())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Contact State: ", corporate.getContactState(),
                        corporate.getTmpContactState()));
            }
            if (corporate.getContactStreet() != null && !corporate.getContactStreet().equals(corporate.getTmpContactStreet())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Contact Street: ", corporate.getContactStreet(),
                        corporate.getTmpContactStreet()));
            }
            if (corporate.getContactTown() != null && !corporate.getContactTown().equals(corporate.getTmpContactTown())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Contact Town: ", corporate.getContactTown(),
                        corporate.getTmpContactTown()));
            }
            if (corporate.getAllowFinalAuthCreation() != null && !corporate.getAllowFinalAuthCreation().equals(corporate.getTmpAllowFinalAuthCreation())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Allow Corporate to Create Final Authorizer: ", corporate.getAllowFinalAuthCreation().toString(),
                        corporate.getTmpAllowFinalAuthCreation().toString()));
            }
            if (corporate.getEnableESA() != null && !corporate.getEnableESA().equals(corporate.getTmpEnableESA())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Enable Token Operations: ", corporate.getEnableESA().toString(),
                        corporate.getTmpEnableESA().toString()));
            }
            if (corporate.getTmpCommissionType() != null && !corporate.getCommissionType().equals(corporate.getTmpCommissionType())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Commission Type: ", corporate.getCommissionType().equals("0") ? "Regular Commission" : "Graguated Commission",
                        corporate.getTmpCommissionType().equals("0") ? "Regular Commission" : "Graguated Commission"));
            }
            boolean regularComm = corporate.getTmpCommissionType().equals("0");
            if (!regularComm) {
                nameValuePairs.add(new TaskObjectNameValuePair("Graduated Fee Structure: ", corporate.getGradBankCommission() != null ? corporate.getGradBankCommission().getStructureName() : "",
                        corporate.getTmpGradBankCommission() != null ? corporate.getTmpGradBankCommission().getStructureName() : ""));
            }
            if (regularComm && corporate.getEtzCommission() != null && corporate.getEtzCommission().compareTo(corporate.getTmpEtzCommission()) != 0) {
                nameValuePairs.add(new TaskObjectNameValuePair("Etranzact Commission: ", corporate.getEtzCommission().toString(),
                        corporate.getTmpEtzCommission().toString()));
            }
            if (regularComm && corporate.getBankCommission() != null && corporate.getBankCommission().compareTo(corporate.getTmpBankCommission()) != 0) {
                nameValuePairs.add(new TaskObjectNameValuePair("Bank Commission: ", corporate.getBankCommission().toString(),
                        corporate.getTmpBankCommission().toString()));
            }
            if (regularComm && corporate.getBankCommissionType() != null && !corporate.getBankCommissionType().equals(corporate.getTmpBankCommissionType())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Bank Commission Amount Type: ", corporate.getBankCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent",
                        corporate.getTmpBankCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent"));
            }
            if (corporate.getBankCommissionAccount() != null && !corporate.getBankCommissionAccount().equals(corporate.getTmpBankCommissionAccount())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Bank Commission Account: ", corporate.getBankCommissionAccount(),
                        corporate.getTmpBankCommissionAccount()));
            }
            if (regularComm && corporate.getThirdPartyCommission() != null && corporate.getThirdPartyCommission().compareTo(corporate.getTmpThirdPartyCommission()) != 0) {
                nameValuePairs.add(new TaskObjectNameValuePair("ThirdParty1 Commission: ", corporate.getThirdPartyCommission().toString(),
                        corporate.getTmpThirdPartyCommission().toString()));
            }
            if (regularComm && corporate.getThirdPartyCommissionType() != null && !corporate.getThirdPartyCommissionType().equals(corporate.getTmpThirdPartyCommissionType())) {
                nameValuePairs.add(new TaskObjectNameValuePair("ThirdParty1 Commission Amount Type: ", corporate.getThirdPartyCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent",
                        corporate.getTmpThirdPartyCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent"));
            }
            if (corporate.getThirdPartyCommissionAccount() != null && !corporate.getThirdPartyCommissionAccount().equals(corporate.getTmpThirdPartyCommissionAccount())) {
                nameValuePairs.add(new TaskObjectNameValuePair("ThirdParty1 Commission Account: ", corporate.getThirdPartyCommissionAccount(),
                        corporate.getTmpThirdPartyCommissionAccount()));
            }
            if (regularComm && corporate.getThirdPartyCommission2() != null && corporate.getThirdPartyCommission2().compareTo(corporate.getTmpThirdPartyCommission2()) != 0) {
                nameValuePairs.add(new TaskObjectNameValuePair("ThirdParty2 Commission: ", corporate.getThirdPartyCommission2().toString(),
                        corporate.getTmpThirdPartyCommission2().toString()));
            }
            if (regularComm && corporate.getThirdPartyCommissionType2() != null && !corporate.getThirdPartyCommissionType2().equals(corporate.getTmpThirdPartyCommissionType2())) {
                nameValuePairs.add(new TaskObjectNameValuePair("ThirdParty2 Commission Amount Type: ", corporate.getThirdPartyCommissionType2().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent",
                        corporate.getTmpThirdPartyCommissionType2().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent"));
            }
            if (corporate.getThirdPartyCommissionAccount2() != null && !corporate.getThirdPartyCommissionAccount2().equals(corporate.getTmpThirdPartyCommissionAccount2())) {
                nameValuePairs.add(new TaskObjectNameValuePair("ThirdParty2 Commission Account: ", corporate.getThirdPartyCommissionAccount2(),
                        corporate.getTmpThirdPartyCommissionAccount2()));
            }
            for (CorporatePayService service : corporate.getTmpCorporatePayServices()) {
                services += service.getServiceDesc() + ", ";
            }

        } else {
            boolean regularComm = corporate.getCommissionType().equals("0");
            nameValuePairs.add(0, new TaskObjectNameValuePair("Corporate Name: ", corporate.getCorporateName(),
                    null));
            nameValuePairs.add(1, new TaskObjectNameValuePair("Contact Email: ", corporate.getContactEmail(),
                    null));
            nameValuePairs.add(2, new TaskObjectNameValuePair("Contact Phone: ", corporate.getContactPhone(),
                    null));
            nameValuePairs.add(3, new TaskObjectNameValuePair("Contact State: ", corporate.getContactState(),
                    null));
            nameValuePairs.add(4, new TaskObjectNameValuePair("Contact Street: ", corporate.getContactStreet(),
                    null));
            nameValuePairs.add(5, new TaskObjectNameValuePair("Contact Town: ", corporate.getContactTown(),
                    null));
            nameValuePairs.add(6, new TaskObjectNameValuePair("Allow Corporate to Create Final Authorizer: ", corporate.getAllowFinalAuthCreation().toString(),
                    null));
            nameValuePairs.add(7, new TaskObjectNameValuePair("Enable Token Operations: ", corporate.getEnableESA().toString(),
                    null));
            if (regularComm) {
                nameValuePairs.add(8, new TaskObjectNameValuePair("Etranzact Commission: ", corporate.getEtzCommission().toString(),
                        null));
                nameValuePairs.add(9, new TaskObjectNameValuePair("Bank Commission: ", corporate.getBankCommission().toString(),
                        null));
                nameValuePairs.add(10, new TaskObjectNameValuePair("Bank Commission Amount type: ", corporate.getBankCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent",
                        null));

                nameValuePairs.add(11, new TaskObjectNameValuePair("ThirdParty1 Commission: ", corporate.getThirdPartyCommission().toString(),
                        null));
                nameValuePairs.add(12, new TaskObjectNameValuePair("ThirdParty1 Commission Amount type: ", corporate.getThirdPartyCommissionType().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent",
                        null));

                nameValuePairs.add(13, new TaskObjectNameValuePair("ThirdParty2 Commission: ", corporate.getThirdPartyCommission2().toString(),
                        null));
                nameValuePairs.add(14, new TaskObjectNameValuePair("ThirdParty2 Commission Amount type: ", corporate.getThirdPartyCommissionType2().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Fixed" : "Percent",
                        null));

            } else {
                nameValuePairs.add(8, new TaskObjectNameValuePair("Graduated Fee Structure: ", corporate.getGradBankCommission().getStructureName(),
                        null));
            }
            nameValuePairs.add(nameValuePairs.size(), new TaskObjectNameValuePair("Bank Commission Account: ", corporate.getBankCommissionAccount(),
                    null));
            nameValuePairs.add(nameValuePairs.size(), new TaskObjectNameValuePair("ThirdParty1 Commission Account: ", corporate.getThirdPartyCommissionAccount(),
                    null));
            nameValuePairs.add(nameValuePairs.size(), new TaskObjectNameValuePair("ThirdParty2 Commission Account: ", corporate.getThirdPartyCommissionAccount2(),
                    null));
            for (CorporatePayService service : corporate.getCorporatePayServices()) {
                services += service.getServiceDesc() + ", ";
            }
        }

        if (!services.isEmpty()) {
            nameValuePairs.add(nameValuePairs.size(), new TaskObjectNameValuePair("Subscribed Additional Services: ", services.substring(0, services.length() - 2),
                    null));
        }
        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
