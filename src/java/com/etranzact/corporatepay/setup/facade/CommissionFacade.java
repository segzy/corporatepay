/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.CommissionFee;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class CommissionFacade extends AbstractFacade<Commission> implements CommissionFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @EJB
    private CommissionWorkflowFacadeLocal commissionWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    public CommissionFacade() {
        super(Commission.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Commission create(Commission entity) throws Exception {
        Set<CommissionFee> fees = new HashSet<>();
        for (CommissionFee fee : entity.getCommissionFees()) {
            fee.setId(null);
            CommissionFee merge = em.merge(fee);
            fees.add(merge);
        }
        entity.setCommissionFees(fees);
        Commission create = super.create(entity);
        commissionWorkflowFacadeLocal.startWorkflow(Commission.class, create.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return create;
    }

    @Override
    public Commission edit(Commission entity) throws Exception {
        Commission commission = find(entity.getId());
        commission.setTmpEtzPercent(entity.getEtzPercent());
        commission.setTmpThirdPartyPercent1(entity.getThirdPartyPercent1());
        commission.setTmpThirdPartyPercent2(entity.getThirdPartyPercent2());
        commission.setTmpCalculationMode(entity.getCalculationMode());
        Set<CommissionFee> commissionFees = new HashSet<>();
        for (CommissionFee commissionFee : entity.getCommissionFees()) {
            CommissionFee merge;
            if (commissionFee.getId() < 0) {
                commissionFee.setId(null);
                commissionFee.setTmpAmount(commissionFee.getAmount());
                commissionFee.setTmpAmountType(commissionFee.getAmountType());
                commissionFee.setTmpMaxRange(commissionFee.getMaxRange());
                commissionFee.setTmpMinRange(commissionFee.getMinRange());
                merge = em.merge(commissionFee);
                commissionFees.add(merge);
            } else {
                merge = em.find(CommissionFee.class, commissionFee.getId());
                merge.setTmpAmount(commissionFee.getAmount());
                merge.setTmpAmountType(commissionFee.getAmountType());
                merge.setTmpMaxRange(commissionFee.getMaxRange());
                merge.setTmpMinRange(commissionFee.getMinRange());
                commissionFees.add(merge);
            }
        }
        commission.setFlagStatus(entity.getFlagStatus());
        commission.setTmpCommissionFees(commissionFees);
        commission.setTmpActive(entity.getActive());
        em.flush();
        commissionWorkflowFacadeLocal.startWorkflow(Commission.class, commission.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return commission;

    }

    @Override
    public void finalizeApprove(Commission t, ApprovalTask task) throws Exception {
        log.info("finalizin Commkission....");
        String flagStatus = t.getFlagStatus();
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
              if(task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            for (CommissionFee fee : t.getTmpCommissionFees()) {
                fee.setAmount(fee.getTmpAmount());
                fee.setAmountType(fee.getTmpAmountType());
                fee.setMaxRange(fee.getTmpMaxRange());
                fee.setMinRange(fee.getTmpMinRange());
                fee.setTmpAmount(null);
                fee.setTmpAmountType(null);
                fee.setTmpMaxRange(null);
                fee.setTmpMinRange(null);
            }
            t.setCommissionFees(t.getTmpCommissionFees());
            t.setTmpCommissionFees(null);
            t.setTmpActive(null);
        }
        t.setEtzPercent(t.getTmpEtzPercent());
        t.setThirdPartyPercent1(t.getTmpThirdPartyPercent1());
        t.setThirdPartyPercent2(t.getTmpThirdPartyPercent2());
        t.setCalculationMode(t.getTmpCalculationMode());
        t.setTmpEtzPercent(null);
        t.setTmpThirdPartyPercent1(null);
        t.setTmpThirdPartyPercent2(null);
        t.setTmpCalculationMode(0);
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
        Map map = new HashMap();
        map.put("u.role.id in ('003')", null);
        List<User> blincopies = userSetupFacadeLocal.findAll(map, true);
        String[] bcopies = new String[blincopies.size()];
        for (int i = 0; i < blincopies.size(); i++) {
            bcopies[i] = blincopies.get(i).getEmail();
        }
        map = new HashMap<>();
        map.put("u.role.administrativeRole=true", null);
        map.put("central", true);
        List<User> findAll = userSetupFacadeLocal.findAll(map, true);
        String[] recepients = new String[findAll.size()];
        for (int i = 0; i < findAll.size(); i++) {
            recepients[i] = findAll.get(i).getEmail();
        }
        if (flagStatus.equals(AppConstants.MODIFIED)) {
            MailUtil.getInstance().sendEditMail2(task, t, recepients, t.getStructureName(), bcopies);
        } else {
            MailUtil.getInstance().sendCreationMail2(task, t, recepients, t.getStructureName(), bcopies);
        }
    }

    @Override
    public Commission save(Commission entity) throws Exception {
       return em.merge(entity);
    }

}
