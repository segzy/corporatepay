/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalTask;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface ApprovalGroupFacadeLocal extends AbstractFacadeLocal<ApprovalGroup>{

       ApprovalGroup edit(ApprovalGroup entity) throws Exception;
       
    public void finalizeApprove(ApprovalGroup t, ApprovalTask task) throws Exception;
    
}
