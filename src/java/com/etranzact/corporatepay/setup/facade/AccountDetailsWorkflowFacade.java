/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class AccountDetailsWorkflowFacade extends AbstractWorkflowFacade implements AccountDetailsWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
        @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        CorporateCard corporateCard = (CorporateCard) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            nameValuePairs.add(new TaskObjectNameValuePair("Corporate: ", corporateCard.getCorporate().getCorporateName(),
                    null));
            if (!corporateCard.getCardNumber().equals(corporateCard.getTmpCardNumber())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Card Number: ", corporateCard.getCardNumber(),
                        corporateCard.getTmpCardNumber()));
            }
            if (!corporateCard.getDescription().equals(corporateCard.getTmpDescription())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Card Alias: ", corporateCard.getDescription(),
                        corporateCard.getTmpDescription()));
            }
//            if (!corporateCard.getExpMonth().equals(corporateCard.getTmpExpMonth())) {
//                nameValuePairs.add(new TaskObjectNameValuePair("Card Expiry Month: ", corporateCard.getExpMonth(),
//                        corporateCard.getTmpExpMonth()));
//            }
//            if (!corporateCard.getExpYear().equals(corporateCard.getTmpExpYear())) {
//                nameValuePairs.add(new TaskObjectNameValuePair("Card Expiry Year: ", corporateCard.getExpYear(),
//                        corporateCard.getTmpExpYear()));
//            }

        } else {
            nameValuePairs.add(0, new TaskObjectNameValuePair("Corporate: ", corporateCard.getCorporate().getCorporateName(),
                    null));
            nameValuePairs.add(1, new TaskObjectNameValuePair("Card Number: ", corporateCard.getCardNumber(),
                    null));
            nameValuePairs.add(2, new TaskObjectNameValuePair("Card Alias: ", corporateCard.getDescription(),
                    null));
//            nameValuePairs.add(3, new TaskObjectNameValuePair("Card Expiration: ", corporateCard.getExpMonth() + "/" + corporateCard.getExpYear(),
//                    null));
        }

        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
