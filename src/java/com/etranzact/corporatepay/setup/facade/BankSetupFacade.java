/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class BankSetupFacade extends AbstractFacade<Bank> implements BankSetupFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @EJB
    private BankSetupWorkflowFacadeLocal bankSetupWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    public BankSetupFacade() {
        super(Bank.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Bank create(Bank entity) throws Exception {
        Bank create = super.create(entity);
        bankSetupWorkflowFacadeLocal.startWorkflow(Bank.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, null);
        return create;
    }

    @Override
    public Bank edit(Bank entity) throws Exception {
        Bank bank = find(entity.getId());
        bank.setTmpActive(entity.getActive());
        bank.setTmpBankEmail(entity.getBankEmail());
        bank.setTmpBankIp(entity.getBankIp());
        bank.setTmpBankName(entity.getBankName());
        bank.setTmpTssAccount(entity.getTssAccount());
        bank.setTmpEtzFee(entity.getEtzFee());
        bank.setTmpEnableESA(entity.getEnableESA());
        bank.setTmpSettlementMode(entity.getSettlementMode());
        if(entity.getMfbBank()) {
            bank.setTmpAffliateBank(entity.getAffliateBank());
            bank.setTmpAffliateBankAccount(entity.getAffliateBankAccount());
            bank.setTmpmfbBank(entity.getMfbBank());
        }
        bank.setFlagStatus(AppConstants.MODIFIED);
        Bank create = super.create(bank);
        bankSetupWorkflowFacadeLocal.startWorkflow(Bank.class, create.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return create;
    }

    @Override
    public void finalizeApprove(Bank t, ApprovalTask task) throws Exception {
        log.info("finalizin bank....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        Map map = new HashMap<>();
        map.put("u.role.id in ('003')", null);
        List<User> blincopies = userSetupFacadeLocal.findAll(map, true);
        String[] bcopies = new String[blincopies.size()];
        for (int i = 0; i < blincopies.size(); i++) {
            bcopies[i] = blincopies.get(i).getEmail();
        }
        map = new HashMap<>();
        map.put("u.role.administrativeRole=true", null);
        map.put("u.central=true", null);
        List<User> findAll = userSetupFacadeLocal.findAll(map, true);
         String[] recepients ;
          if(t.getBankEmail()!=null && t.getBankEmail().isEmpty()) {
             recepients = new String[findAll.size()]; 
          } else {
         recepients = new String[findAll.size() + 1];
          }
        int i = 0;
        for (; i < findAll.size(); i++) {
            recepients[i] = findAll.get(i).getEmail();
        }
        if(t.getBankEmail()!=null && t.getBankEmail().isEmpty())
        recepients[i] = t.getBankEmail();
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            MailUtil.getInstance().sendEditMail(t, recepients, t.getBankName(), bcopies);
            t.setBankEmail(t.getTmpBankEmail());
            t.setBankIp(t.getTmpBankIp());
            t.setBankName(t.getTmpBankName());
            t.setTssAccount(t.getTmpTssAccount());
            t.setEtzFee(t.getTmpEtzFee());
                    t.setEnableESA(t.getTmpEnableESA());
            if(t.getTmpmfbBank()!=null && t.getTmpmfbBank()) {
                t.setMfbBank(t.getTmpmfbBank());
                t.setAffliateBank(t.getTmpAffliateBank());
                t.setAffliateBankAccount(t.getTmpAffliateBankAccount());
            }
            t.setSettlementMode(t.getTmpSettlementMode());
            t.setTmpActive(null);
            t.setTmpBankEmail(null);
            t.setTmpBankIp(null);
            t.setTmpBankName(null);
            t.setTmpTssAccount(null);
            t.setTmpAffliateBank(null);
            t.setTmpAffliateBankAccount(null);
            t.setTmpmfbBank(null);
            t.setTmpEnableESA(null);
            t.setTmpSettlementMode(null);
        } else if (t.getFlagStatus().equals(AppConstants.CREATED)) {
            MailUtil.getInstance().sendCreationMail(t, recepients, t.getBankName(), bcopies);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);

    }

}
