/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.CommissionFee;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class BankCommissionFacade extends AbstractFacade<BankCommission> implements BankCommissionFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @EJB
    private CommissionWorkflowFacadeLocal commissionWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    public BankCommissionFacade() {
        super(BankCommission.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BankCommission create(BankCommission entity) throws Exception {
        BankCommission create = super.create(entity);
        commissionWorkflowFacadeLocal.startWorkflow(BankCommission.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, null);
        return null;
    }

    @Override
    public BankCommission edit(BankCommission entity) throws Exception {
          BankCommission create = super.create(entity);
        commissionWorkflowFacadeLocal.startWorkflow(BankCommission.class, create.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return null;

    }

    @Override
    public void finalizeApprove(BankCommission t, ApprovalTask task) throws Exception {
        log.info("finalizin Commission....");
        String flagStatus = t.getFlagStatus();
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            for (CommissionFee fee : t.getTmpCommissionFees()) {
                fee.setAmount(fee.getTmpAmount());
                fee.setAmountType(fee.getTmpAmountType());
                fee.setMaxRange(fee.getTmpMaxRange());
                fee.setMinRange(fee.getTmpMinRange());
                fee.setTmpAmount(null);
                fee.setTmpAmountType(null);
                fee.setTmpMaxRange(null);
                fee.setTmpMinRange(null);
            }
//            t.setBankCommissionFees(t.getTmpBankCommissionFees());
//            t.setTmpBankCommissionFees(null);
            t.setTmpActive(null);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
            Map map = new HashMap();
            map.put("u.role.id in ('005')", null);
            List<User> blincopies = userSetupFacadeLocal.findAll(map, true);
            String[] bcopies = new String[blincopies.size()];
            for (int i = 0; i < blincopies.size(); i++) {
                bcopies[i] = blincopies.get(i).getEmail();
            }
            map = new HashMap<>();
            map.put("u.role.administrativeRole=true", null);
            map.put("bank", getLoggedBank());
            userSetupFacadeLocal.setEntityNamePrefix("Bank");
            List<User> findAll = userSetupFacadeLocal.findAll(map, true);
            String[] recepients = new String[findAll.size()];
            for (int i = 0; i < findAll.size(); i++) {
                recepients[i] = findAll.get(i).getEmail();
            }
            if (flagStatus.equals(AppConstants.MODIFIED)) {
                MailUtil.getInstance().sendEditMail2(task, t, recepients, t.getStructureName(), bcopies);
            } else {
                MailUtil.getInstance().sendCreationMail2(task, t, recepients, t.getStructureName(), bcopies);
            }
        
    }

}
