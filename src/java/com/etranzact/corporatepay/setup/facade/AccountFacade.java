/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankApprovalGroup;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateApprovalGroup;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.web.ApprovalRouteBean;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class AccountFacade extends AbstractFacade<Account> implements AccountFacadeLocal {

    @PersistenceContext
    private EntityManager em;


    public AccountFacade() {
        super(Account.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Account edit(Account entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 

}
