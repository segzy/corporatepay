/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.Workflow;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface ApprovalRouteFacadeLocal extends AbstractFacadeLocal<ApprovalRoute>{
    
        boolean approvalRouteExist(Class clazz) throws Exception;
        
       
        List<Object> findRouteAliases(Class clazz) throws Exception;
        
           public List<ApprovalRoute> findRoute(Class clazz) throws Exception;
           
                   
           public List<ApprovalRoute> findRoute(Class clazz,Corporate corporate) throws Exception;
        
  

    
}
