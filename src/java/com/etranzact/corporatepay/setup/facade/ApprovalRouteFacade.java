/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.model.Workflow;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class ApprovalRouteFacade extends AbstractFacade<ApprovalRoute> implements ApprovalRouteFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public ApprovalRouteFacade() {
        super(ApprovalRoute.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ApprovalRoute edit(ApprovalRoute entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public boolean approvalRouteExist(Class clazz) throws Exception {
        Map<String, Object> filter = new HashMap<>();
        if (isBankUser()) {
            filter.put("bank", getLoggedBank());
            entityNamePrefix = "Bank";
        } else if (isCorporateUser()) {
            filter.put("corporate", getLoggedCorporate());
            entityNamePrefix = "Corporate";
        } else {
            filter.put("central", Boolean.TRUE);
        }
        String q = "select a from ApprovalType a where a.description='" + clazz.getSimpleName() + "'";
        log.info(q);
        ApprovalType approvalType = getEntityManager().createQuery(q, ApprovalType.class).getSingleResult();
        filter.put("approvalType", approvalType);
        String[] flagStatuses = new String[]{AppConstants.CREATED, AppConstants.MODIFIED, AppConstants.APPROVED};
        List<ApprovalRoute> findAll = findAll(filter, true, flagStatuses, new String[0]);
        return !findAll.isEmpty();
    }

    @Override
    public ApprovalRoute create(ApprovalRoute entity) throws Exception {
        Map<String, Object> filter = new HashMap<>();
        if (isBankUser()) {
            filter.put("bank", getLoggedBank());
            entityNamePrefix = "Bank";
        } else if (isCorporateUser()) {
            filter.put("corporate", getLoggedCorporate());
            entityNamePrefix = "Corporate";
        } else {
            filter.put("central", Boolean.TRUE);
        }
        filter.put("approvalType", entity.getApprovalType());
        filter.put("(a.approver= " + entity.getApprover().getId() + " or a.level=" + entity.getLevel() + ")", null);
        if (entity.getApprovalType().getAllowMultipleRoute()) {
            filter.put("routeAlias", entity.getRouteAlias());
        }
        if (entity.getId() != null) {
            filter.put("a.id != " + entity.getId(), null);
        }

        String[] flagStatuses = new String[]{AppConstants.CREATED, AppConstants.MODIFIED, AppConstants.APPROVED};
        List<ApprovalRoute> findAll = findAll(filter, true, flagStatuses, new String[0]);
        if (!findAll.isEmpty()) {
            throw new Exception("Approver or Level already exist");
        }
        return super.create(entity);
    }

    @Override
    public List<Object> findRouteAliases(Class clazz) throws Exception {
                    String q = "select a from ApprovalType a where a.description=:clazz";
        log.info(q);
        ApprovalType approvalType = getEntityManager().createQuery(q, ApprovalType.class).setParameter("clazz", clazz.getSimpleName()).getSingleResult();
        if (!approvalType.getAllowMultipleRoute()) {
            throw new Exception("Approval Type does not suppot multiple route");
        }
//        q = "select distinct(c.routeAlias) from CorporateApprovalRoute c where c.routeAlias is not null "
//                + "and c.corporate=" + getLoggedCorporate().getId() + " and c.approvalType= '" + approvalType.getId() + "'";
            q = "select distinct(c.routeAlias) from CorporateApprovalRoute c where c.routeAlias is not null "
                + "and c.corporate=:copId and c.approvalType= :apptypid";
        log.log(Level.INFO, "query..", q);
        List<Object> objects = getEntityManager().createQuery(q).setParameter("copId", getLoggedCorporate()).setParameter("apptypid", approvalType).getResultList();
        log.log(Level.INFO, "got {0} result", objects.size());
        return objects;
    }

    @Override
    public List<ApprovalRoute> findRoute(Class clazz) throws Exception {
            Map<String, Object> filter = new HashMap<>();
        if (isBankUser()) {
            filter.put("bank", getLoggedBank());
            entityNamePrefix = "Bank";
        } else if (isCorporateUser()) {
            filter.put("corporate", getLoggedCorporate());
            entityNamePrefix = "Corporate";
        } else {
            filter.put("central", Boolean.TRUE);
        }
        String q = "select a from ApprovalType a where a.description=:clazz";
        log.info(q);
        ApprovalType approvalType = getEntityManager().createQuery(q, ApprovalType.class).setParameter("clazz", clazz.getSimpleName()).getSingleResult();
        filter.put("approvalType", approvalType);
        String[] flagStatuses = new String[]{AppConstants.CREATED, AppConstants.MODIFIED, AppConstants.APPROVED};
        List<ApprovalRoute> findAll = findAll(filter, true, flagStatuses, new String[0]);
        return findAll;
    }
    
       @Override
    public List<ApprovalRoute> findRoute(Class clazz,Corporate corporate) throws Exception {
            Map<String, Object> filter = new HashMap<>();
            filter.put("corporate", corporate);
            entityNamePrefix = "Corporate";
        
//        String q = "select a from ApprovalType a where a.description='" + clazz.getSimpleName() + "'";
                    String q = "select a from ApprovalType a where a.description=:clazz";
        log.info(q);
        ApprovalType approvalType = getEntityManager().createQuery(q, ApprovalType.class).setParameter("clazz", clazz.getSimpleName()).getSingleResult();
        filter.put("approvalType", approvalType);
        String[] flagStatuses = new String[]{AppConstants.CREATED, AppConstants.MODIFIED, AppConstants.APPROVED};
        List<ApprovalRoute> findAll = findAll(filter, true, flagStatuses, new String[0]);
        return findAll;
    }

   
}
