/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Menu;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.model.Workflow;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WWorkflowFacade extends AbstractWorkflowFacade implements WWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        Workflow workflow = (Workflow) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();

        int i = 1;
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            nameValuePairs.add(new TaskObjectNameValuePair("New Approvers for " + workflow.getApprovalType().getDescription(), null,
                    null));
            List<ApprovalRoute> approvalRoutes = new ArrayList<>();
            approvalRoutes.addAll(workflow.getApprovalRoutes());

            i++;
            Comparator<ApprovalRoute> approvalRouteComparable = new Comparator<ApprovalRoute>() {

                @Override
                public int compare(ApprovalRoute o1, ApprovalRoute o2) {
                    return o1.getLevel() - o2.getLevel();
                }
            };
            Collections.sort(approvalRoutes, approvalRouteComparable);
            for (ApprovalRoute approvalRoute : approvalRoutes) {
                nameValuePairs.add(new TaskObjectNameValuePair("Approval Path " + approvalRoute.getLevel() + ":", approvalRoute.getApprover().getIdentity(),
                        null));
                i++;
            }
        }
        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
