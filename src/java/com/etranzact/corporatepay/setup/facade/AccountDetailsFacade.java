/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.core.facade.ParametersFacadeLocal;
import com.etranzact.corporatepay.core.web.TaskBean;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.Parameters;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.MailUtil;
import com.etz.security.util.Cryptographer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class AccountDetailsFacade extends AbstractFacade<CorporateCard> implements AccountDetailsFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private AccountDetailsWorkflowFacadeLocal accountDetailsWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;
    @EJB
    private ParametersFacadeLocal parametersFacadeLocal;
    private static final Cryptographer crpt = new Cryptographer();

    public AccountDetailsFacade() {
        super(CorporateCard.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CorporateCard create(CorporateCard entity) throws Exception {
        CorporateCard create = super.create(entity);
        if (isBankUser()) {
            accountDetailsWorkflowFacadeLocal.startWorkflowBank(Card.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, getLoggedBank(), null);
        } else if (isCorporateUser()) {
            accountDetailsWorkflowFacadeLocal.startWorkflowCorporate(Card.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, getLoggedCorporate(), null);
        }

        return create;
    }

    @Override
    public CorporateCard edit(CorporateCard entity) throws Exception {
        CorporateCard corporateCard = find(entity.getId());
        corporateCard.setTmpActive(entity.getActive());
        corporateCard.setTmpCardNumber(entity.getCardNumber());
        corporateCard.setTmpDescription(entity.getDescription());
        corporateCard.setTmpExpMonth(entity.getExpMonth());
        corporateCard.setTmpExpYear(entity.getExpYear());
        corporateCard.setFlagStatus(entity.getFlagStatus());
        CorporateCard create = super.create(corporateCard);
        if (isBankUser()) {
            accountDetailsWorkflowFacadeLocal.startWorkflowBank(Card.class, entity.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, getLoggedBank(), null);
        } else if (isCorporateUser()) {
            accountDetailsWorkflowFacadeLocal.startWorkflowCorporate(Card.class, entity.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, getLoggedCorporate(), null);
        }
        return create; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finalizeApprove(CorporateCard t, ApprovalTask task) throws Exception {
        log.info("finalizin corporate card....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
//        try {
//            Parameters parameter = parametersFacadeLocal.find("TOKEN_URL");
//            if (parameter != null) {
//                Map<String, Object> map = new HashMap<>();
//                Bank loggedBank = getLoggedBank();
//                String issuerCode = null;
//                if (loggedBank == null) {
//                    loggedBank = getLoggedCorporate().getBank();
//                }
//                if (loggedBank == null) {
//                    issuerCode = "700";
//                }
//                if (issuerCode == null) {
//                    issuerCode = loggedBank.getId();
//                }
//                map.put("issuerCode", issuerCode);
//                map.put("channelCode", "00");
//                map.put("requestType", "AUT");
//                map.put("responseCode", getLoggedUser().getTokenUserGroup());
//                map.put("requester", getLoggedUser().getTokenUserName());
//                map.put("tokenValue", task.getToken());
//                Map<String, Object> result = AppUtil.doHttpPost(parameter.getParamValue(), map);
//                String responseCode = result.get("responseCode").toString();
//                if(responseCode.equals("-1")) {
//                  throw new PersistenceException("Token Authentication Error"); 
//                }
//                
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(TaskBean.class.getName()).log(Level.SEVERE, null, ex);
//             throw new PersistenceException("Token Authentication Error");
//        }
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (isBankUser()) {
            accountDetailsWorkflowFacadeLocal.startWorkflowCorporate(Card.class, t.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, t.getCorporate(), null);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            Map map = new HashMap<>();
            map.put("central", true);
            map.put("u.role.id='003'", null);
            List<User> audit = userSetupFacadeLocal.findAll(map, true);
            map = new HashMap<>();
            map.put("u.role.finalAuthorizerRole=true or u.role.administrativeRole=true", null);
            map.put("corporate", t.getCorporate());
            userSetupFacadeLocal.setEntityNamePrefix("Corporate");
            List<User> findAll = userSetupFacadeLocal.findAll(map, true);
            findAll.addAll(audit);
            String[] recepients = new String[findAll.size()];
            for (int i = 0; i < findAll.size(); i++) {
                recepients[i] = findAll.get(i).getEmail();
            }
            map = new HashMap<>();
            userSetupFacadeLocal.setEntityNamePrefix("Bank");
            map.put("bank", t.getCorporate().getBank());
            map.put("u.role.bankRole=true and u.role.administrativeRole=true", null);
            findAll = userSetupFacadeLocal.findAll(map, true);
            String[] blindCopy = new String[findAll.size()];
            for (int i = 0; i < findAll.size(); i++) {
                blindCopy[i] = findAll.get(i).getEmail();
            }
            MailUtil.getInstance().sendEditMail(t, recepients, t.getCardNumber() + " ( for " + t.getCorporate().getCorporateName() + ")", blindCopy);
            t.setActive(t.getTmpActive());
            t.setCardNumber(t.getTmpCardNumber());
            t.setDescription(t.getTmpDescription());
            t.setExpMonth(t.getTmpExpMonth());
            t.setExpYear(t.getTmpExpYear());
            t.setTmpActive(null);
            t.setTmpCardNumber(null);
            t.setTmpDescription(null);
            t.setTmpExpYear(null);
            t.setTmpExpMonth(null);
        }
        if (t.getFlagStatus().equals(AppConstants.CREATED)) {
            String doMd5Hash = crpt.doMd5Hash(StringUtils.
                    join(new String[]{t.getCardNumber(), task.getCardDetail()}));
            t.setCardPIN(doMd5Hash);
            Map<String, Object> map = new HashMap<>();
            map.put("central", true);
            map.put("u.role.id='003'", null);
            List<User> audit = userSetupFacadeLocal.findAll(map, true);
            map = new HashMap<>();
            userSetupFacadeLocal.setEntityNamePrefix("Bank");
            map.put("bank", t.getCorporate().getBank());
            map.put("u.role.bankRole=true and u.role.administrativeRole=true", null);
            List<User> findAll = userSetupFacadeLocal.findAll(map, true);
            findAll.addAll(audit);
            map = new HashMap<>();
            userSetupFacadeLocal.setEntityNamePrefix("Corporate");
            map.put("corporate", t.getCorporate());
            map.put("u.role.finalAuthorizerRole=true", null);
            findAll.addAll(userSetupFacadeLocal.findAll(map, true));
            String[] recepients = new String[findAll.size()];
            for (int i = 0; i < findAll.size(); i++) {
                recepients[i] = findAll.get(i).getEmail();
            }
            MailUtil.getInstance().sendCreationMail(t, recepients, t.getCardNumber() + " ( for " + t.getCorporate().getCorporateName() + ")");
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
    }

}
