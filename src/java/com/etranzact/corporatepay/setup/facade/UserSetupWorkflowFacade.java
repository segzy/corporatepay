/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.payment.facade.AuditFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class UserSetupWorkflowFacade extends AbstractWorkflowFacade implements UserSetupWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        User user = (User) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new TaskObjectNameValuePair("Username: ", user.getUsername(), null));
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            if (!user.getFirstName().equals(user.getTmpFirstName())) {
                nameValuePairs.add(new TaskObjectNameValuePair("First Name: ", user.getFirstName(),
                        user.getTmpFirstName()));
            }  if (!user.getLastName().equals(user.getTmpLastName())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Last Name: ", user.getLastName(),
                        user.getTmpLastName()));
            }  if (!user.getEmail().equals(user.getTmpEmail())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Email: ", user.getEmail(),
                         user.getTmpEmail()));
            } if (!user.getRole().equals(user.getTmpRole())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Role: ", user.getRole()==null?"":user.getRole().getRoleName(),
                         user.getTmpRole()==null?"":user.getTmpRole().getRoleName()));
            }if (!user.getMobilePhone().equals(user.getTmpMobilePhone())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Mobile Phone: ", user.getMobilePhone(),
                         user.getTmpMobilePhone()));
            }if (!user.getSunday().equals(user.getTmpSunday())) {
              nameValuePairs.add(new TaskObjectNameValuePair("Sunday Access? ", user.getSunday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                     user.getTmpSunday().equals(AppConstants.TURNED_ON) ? "Yes" : "No"));  
            }if (!user.getMonday().equals(user.getTmpMonday())) {
              nameValuePairs.add(new TaskObjectNameValuePair("Monday Access? ", user.getMonday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                     user.getTmpMonday().equals(AppConstants.TURNED_ON) ? "Yes" : "No"));  
            }if (!user.getTuesday().equals(user.getTmpTuesday())) {
              nameValuePairs.add(new TaskObjectNameValuePair("Tuesday Access? ", user.getTuesday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                     user.getTmpTuesday().equals(AppConstants.TURNED_ON) ? "Yes" : "No"));  
            }if (!user.getWednesday().equals(user.getTmpWednesday())) {
              nameValuePairs.add(new TaskObjectNameValuePair("Wednesday Access? ", user.getWednesday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                     user.getTmpWednesday().equals(AppConstants.TURNED_ON) ? "Yes" : "No"));  
            }if (!user.getThursday().equals(user.getTmpThursday())) {
              nameValuePairs.add(new TaskObjectNameValuePair("Thursday Access? ", user.getThursday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                     user.getTmpThursday().equals(AppConstants.TURNED_ON) ? "Yes" : "No"));  
            }if (!user.getFriday().equals(user.getTmpFriday())) {
              nameValuePairs.add(new TaskObjectNameValuePair("Friday Access? ", user.getFriday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                     user.getTmpFriday().equals(AppConstants.TURNED_ON) ? "Yes" : "No"));  
            }if (!user.getSaturday().equals(user.getTmpSaturday())) {
              nameValuePairs.add(new TaskObjectNameValuePair("Saturday Access? ", user.getSaturday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                     user.getTmpSaturday().equals(AppConstants.TURNED_ON) ? "Yes" : "No"));  
            }
        } else {
            nameValuePairs.add(0,new TaskObjectNameValuePair("First Name: ", user.getFirstName(),
                    null));
            nameValuePairs.add(1,new TaskObjectNameValuePair("Last Name: ", user.getLastName(),
                    null));
            nameValuePairs.add(2,new TaskObjectNameValuePair("Email: ", user.getEmail(),
                    null));
            nameValuePairs.add(3,new TaskObjectNameValuePair("Role: ", user.getRole().getRoleName(),
                    null));
            nameValuePairs.add(4,new TaskObjectNameValuePair("Mobile Phone: ", user.getMobilePhone(),
                    null));
            nameValuePairs.add(5,new TaskObjectNameValuePair("Sunday Access? ", user.getSunday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                    null));
            nameValuePairs.add(6,new TaskObjectNameValuePair("Monday Access? ", user.getMonday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                    null));
            nameValuePairs.add(7,new TaskObjectNameValuePair("Tuesday Access? ", user.getTuesday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                    null));
            nameValuePairs.add(8,new TaskObjectNameValuePair("Wednesday Access? ", user.getWednesday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                    null));
            nameValuePairs.add(9,new TaskObjectNameValuePair("Thursday Access? ", user.getThursday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                    null));
            nameValuePairs.add(10,new TaskObjectNameValuePair("Friday Access? ", user.getFriday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                    null));
            nameValuePairs.add(11,new TaskObjectNameValuePair("Saturday Access? ", user.getSaturday().equals(AppConstants.TURNED_ON) ? "Yes" : "No",
                    null));
        }

        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
