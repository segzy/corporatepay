/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.MailUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class CorporateSetupFacade extends AbstractFacade<Corporate> implements CorporateSetupFacadeLocal {
    
    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private CorporateSetupWorkflowFacadeLocal corporateSetupWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;
    
    public CorporateSetupFacade() {
        super(Corporate.class);
    }
    
    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Corporate create(Corporate entity) throws Exception {
        Corporate create = super.create(entity);
        corporateSetupWorkflowFacadeLocal.startWorkflow(Corporate.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, null);
        return create;
    }
    
    @Override
    public Corporate save(Corporate entity) throws Exception {
        return super.create(entity);
    }
    
    @Override
    public Corporate edit(Corporate entity) throws Exception {
        Corporate corporate = find(entity.getId());
        corporate.setTmpActive(entity.getActive());
        corporate.setTmpCorporateName(entity.getCorporateName());
        corporate.setTmpContactEmail(entity.getContactEmail());
        corporate.setTmpContactPhone(entity.getContactPhone());
        corporate.setTmpContactState(entity.getContactState());
        corporate.setTmpContactStreet(entity.getContactStreet());
        corporate.setTmpContactTown(entity.getContactTown());
        corporate.setTmpBank(entity.getBank());
        corporate.setApproveCommission(entity.getApproveCommission());
        corporate.setCorporatePayServices(null);
        corporate.setTmpCorporatePayServices(entity.getCorporatePayServices());
        corporate.setTmpLogo(entity.getLogo());
        corporate.setTmpAllowFinalAuthCreation(entity.getAllowFinalAuthCreation());
        corporate.setTmpThirdPartyCommission(entity.getThirdPartyCommission());
        corporate.setTmpThirdPartyCommission2(entity.getThirdPartyCommission2());
        corporate.setTmpThirdPartyCommissionAccount(entity.getThirdPartyCommissionAccount());
        corporate.setTmpThirdPartyCommissionAccount2(entity.getThirdPartyCommissionAccount2());
        corporate.setTmpBankCommission(entity.getBankCommission());
        corporate.setTmpBankCommissionAccount(entity.getBankCommissionAccount());
        corporate.setTmpEtzCommission(entity.getEtzCommission());
        corporate.setTmpBankCommissionType(entity.getBankCommissionType());
        corporate.setTmpThirdPartyCommissionType(entity.getThirdPartyCommissionType());
        corporate.setTmpThirdPartyCommissionType2(entity.getThirdPartyCommissionType2());
        corporate.setTmpEnableESA(entity.getEnableESA());
        corporate.setTmpGradBankCommission(entity.getGradBankCommission());
        corporate.setTmpCommissionType(entity.getCommissionType());
        corporate.setFlagStatus(AppConstants.MODIFIED);
        Corporate create = super.create(corporate);
        corporateSetupWorkflowFacadeLocal.startWorkflow(Corporate.class, create.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return create;
    }
    
    @Override
    public void finalizeApprove(Corporate t, ApprovalTask task) throws Exception {
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            Map map = new HashMap<>();
            map.put("u.role.administrativeRole=true", null);
            map.put("corporate", t);
            userSetupFacadeLocal.setEntityNamePrefix("Corporate");
            List<User> findAll = userSetupFacadeLocal.findAll(map, true);
            String[] recepients = new String[findAll.size() + 1];
            recepients[0] = t.getContactEmail();
            for (int i = 1; i <= findAll.size(); i++) {
                recepients[i] = findAll.get(i - 1).getEmail();
            }
            MailUtil.getInstance().sendEditMail(t, recepients, t.getCorporateName());
            t.setCorporateName(t.getTmpCorporateName());
            t.setContactEmail(t.getTmpContactEmail());
            t.setContactPhone(t.getTmpContactPhone());
            t.setContactState(t.getTmpContactState());
            t.setContactStreet(t.getTmpContactStreet());
            t.setContactTown(t.getTmpContactTown());
            t.setCorporatePayServices(t.getTmpCorporatePayServices());
            t.setLogo(t.getTmpLogo());
            t.setAllowFinalAuthCreation(t.getTmpAllowFinalAuthCreation());
            t.setThirdPartyCommission(t.getTmpThirdPartyCommission());
            t.setThirdPartyCommission2(t.getTmpThirdPartyCommission2());
            t.setThirdPartyCommissionAccount(t.getTmpThirdPartyCommissionAccount());
            t.setThirdPartyCommissionAccount2(t.getTmpThirdPartyCommissionAccount2());
            t.setBankCommission(t.getTmpBankCommission());
            t.setBankCommissionAccount(t.getTmpBankCommissionAccount());
            t.setEtzCommission(t.getTmpEtzCommission());
            t.setBankCommissionType(t.getTmpBankCommissionType());
            t.setThirdPartyCommissionType(t.getTmpThirdPartyCommissionType());
            t.setThirdPartyCommissionType2(t.getTmpThirdPartyCommissionType2());
            t.setGradBankCommission(t.getTmpGradBankCommission());
            t.setEnableESA(t.getTmpEnableESA());
            t.setCommissionType(t.getTmpCommissionType());
            t.setTmpActive(null);
            t.setTmpCorporateName(null);
            t.setTmpContactEmail(null);
            t.setTmpContactPhone(null);
            t.setTmpContactState(null);
            t.setTmpContactStreet(null);
            t.setTmpContactTown(null);
            t.setTmpBank(null);
            t.setTmpCorporatePayServices(null);
            t.setTmpLogo(null);
            t.setTmpAllowFinalAuthCreation(null);
            t.setTmpThirdPartyCommission(null);
            t.setTmpThirdPartyCommission2(null);
            t.setTmpThirdPartyCommissionAccount(null);
            t.setTmpThirdPartyCommissionAccount2(null);
            t.setTmpGradBankCommission(null);
            t.setTmpBankCommission(null);
            t.setTmpBankCommissionAccount(null);
            t.setTmpEtzCommission(null);
            t.setTmpBankCommissionType(null);
            t.setTmpThirdPartyCommissionType(null);
            t.setTmpThirdPartyCommissionType2(null);
            t.setTmpEnableESA(null);
            t.setTmpCommissionType(null);
        } else if (t.getFlagStatus().equals(AppConstants.CREATED)) {
            
            MailUtil.getInstance().sendCreationMail(t, new String[]{t.getContactEmail()}, t.getCorporateName());
        }
        if (!t.getApproveCommission()) {
            Map map = new HashMap<>();
            map.put("u.role.administrativeRole=true", null);
            map.put("central", true);
            userSetupFacadeLocal.setEntityNamePrefix("");
            List<User> findAll = userSetupFacadeLocal.findAll(map, true);
            String[] administrators = new String[findAll.size()];
            int i = 0;
            for (User supportPerson : findAll) {
                administrators[i] = supportPerson.getEmail();
                i++;
            }
            map = new HashMap<>();
            map.put("u.role.id='0002'", null);
            List<User> support = userSetupFacadeLocal.findAll(map, true);
            String[] supportEmails = new String[support.size()];
            i = 0;
            for (User supportPerson : support) {
                supportEmails[i] = supportPerson.getEmail();
                i++;
            }
            log.info("sending etz commission exception to admins");
            for (String admin : administrators) {
                log.info("admin:: " + admin);                
            }
            log.info("copyin etz commission exception to supports");
            for (String admin : supportEmails) {
                log.info("support:: " + admin);                
            }
            MailUtil.getInstance().sendEtzCommissionExceptionMail(administrators, t.getCorporateName(), AppUtil.formatAsAmount(t.getEtzCommission().toString()), AppUtil.formatAsAmount(t.getBank().getEtzFee().toString()), t.getBank().getBankName(), supportEmails);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
    }
    
}
