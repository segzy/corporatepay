/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Event;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class EventFacade extends AbstractFacade<Event> implements EventFacadeLocal {

    @PersistenceContext
    private EntityManager em;


    public EventFacade() {
        super(Event.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Event edit(Event entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 @Override
    public List<Event> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        List<Event> events = super.findAll(min, max, filters, activeOnly, flagStatuses, fromDate, toDate, dateField, orderFields);
//        log.info("minmax events found " + events.size());
        for(Event event: events) {
//              log.info("events type " + event.getEventType());
            if(event.getEventType().equals(Payment.class.getSimpleName())) {
                event.setPayment(em.find(Payment.class, Long.parseLong(event.getEventObject().startsWith("08")?event.getEventObject().substring(3):event.getEventObject())));
            } else if(event.getEventType().equals(Transaction.class.getSimpleName())) {
                event.setTransaction(em.find(Transaction.class, new BigInteger(event.getEventObject().startsWith("08")?event.getEventObject().substring(3):event.getEventObject())));
            }
        }
        return events;
    }
    
 @Override
    public List<Event> findAll(Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception {
        List<Event> events = super.findAll(filters, activeOnly, flagStatuses, fromDate, toDate, dateField, orderFields);
            log.info("events found " + events.size());
        for(Event event: events) {
                          log.info("events type " + event.getEventType());
            if(event.getEventType().equals(Payment.class.getSimpleName())) {
                event.setPayment(em.find(Payment.class, Long.parseLong(event.getEventObject().startsWith("08")?event.getEventObject().substring(3):event.getEventObject())));
            } else if(event.getEventType().equals(Transaction.class.getSimpleName())) {
                event.setTransaction(em.find(Transaction.class, new BigInteger(event.getEventObject().startsWith("08")?event.getEventObject().substring(3):event.getEventObject())));
            }
        }
        return events;
    }

}
