/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.CommissionFee;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class CommissionWorflowFacade extends AbstractWorkflowFacade implements CommissionWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        Commission commission = (Commission) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        int i = 0;
        nameValuePairs.add(i, new TaskObjectNameValuePair("Graduated Fee Structure ", commission.getStructureName(),
                null));
        i++;
        nameValuePairs.add(i, new TaskObjectNameValuePair("Calculation Mode ", commission.getTmpCalculationMode() == 0 ? "Cummulative Calculation" : "Specific Range Calculation",
                null));
        i++;
        if (commission.getTmpEtzPercent() != null) {
            nameValuePairs.add(i, new TaskObjectNameValuePair("Etranzact Percentage ", String.valueOf(commission.getTmpEtzPercent().intValue()),
                    null));
            i++;
        }
        if (commission.getTmpThirdPartyPercent1() != null) {
            nameValuePairs.add(i, new TaskObjectNameValuePair("ThirdParty1 Percentage ", String.valueOf(commission.getThirdPartyPercent1().intValue()),
                    null));
            i++;
        }
        if (commission.getTmpThirdPartyPercent2() != null) {
            nameValuePairs.add(i, new TaskObjectNameValuePair("ThirdParty2 Percentage ", String.valueOf(commission.getThirdPartyPercent2().intValue()),
                    null));

            i++;
        }
        for (CommissionFee commissionFee : commission.getTmpCommissionFees()) {
            nameValuePairs.add(i, new TaskObjectNameValuePair("Transaction from "
                    + commissionFee.getTmpMinRange() + " to " + commissionFee.getTmpMaxRange(),
                    commissionFee.getTmpAmount() + " " + (commissionFee.getTmpAmountType().equals(AppConstants.AMOUNT_TYPE_FIXED) ? "Naira" : "Percent"),
                    null));
            i++;

        }
        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
