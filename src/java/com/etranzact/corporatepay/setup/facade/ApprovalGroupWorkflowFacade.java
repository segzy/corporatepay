/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class ApprovalGroupWorkflowFacade extends AbstractWorkflowFacade implements ApprovalGroupWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
        @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        ApprovalGroup approvalGroup = (ApprovalGroup) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
//        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
//            if (!approvalGroup.getGroupAlias().equals(approvalGroup.getTmpGroupAlias())) {
//                nameValuePairs.add(new TaskObjectNameValuePair("Group Alias: ", approvalGroup.getGroupAlias(),
//                        approvalGroup.getTmpGroupAlias()));
//            }
//        } else {
//            nameValuePairs.add(new TaskObjectNameValuePair("Group Alias: ", approvalGroup.getGroupAlias(),
//                    null));
//        }

        int i = 1;
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            for (User user : approvalGroup.getTmpUsers()) {
                nameValuePairs.add(new TaskObjectNameValuePair("Member " + i + ":", user.getFirstName() + " " + user.getLastName() + "(" + user.getUsername() + ")",
                        null));
                i++;
            }
        } else {
            for (User user : approvalGroup.getUsers()) {
                nameValuePairs.add(new TaskObjectNameValuePair("Member " + i + ":", user.getFirstName() + " " + user.getLastName() + "(" + user.getUsername() + ")",
                        null));
                i++;
            }
        }
        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
