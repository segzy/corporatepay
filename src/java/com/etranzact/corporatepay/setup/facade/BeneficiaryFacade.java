/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateBeneficiary;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class BeneficiaryFacade extends AbstractFacade<Beneficiary> implements BeneficiaryFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public BeneficiaryFacade() {
        super(Beneficiary.class);
    }

    @Override
    public Beneficiary create(Beneficiary entity) throws Exception {
        Account account = entity.getAccount();
        String accountName = entity.getAccount().getAccountName();
        if (account instanceof PocketMoneyAccount) {
            List<PocketMoneyAccount> resultList = em.createQuery("select a from PocketMoneyAccount a where a.accountNumber='" + account.getAccountNumber() + "'").getResultList();
            if (!(resultList.isEmpty())) {
                account = resultList.get(0);

            }
            ((PocketMoneyAccount) entity.getAccount()).setType(AppConstants.POCKET_MONEY_ACCOUNT_TYPE);
        } else if (account instanceof BankAccount) {
            Bank bank = ((BankAccount) account).getBank();
            List<PocketMoneyAccount> resultList = em.createQuery("select a from BankAccount a where a.accountNumber='" + account.getAccountNumber() + "' and a.bank.id='" + bank.getId() + "'").getResultList();
            if (!(resultList.isEmpty())) {
                account = resultList.get(0);
            }
        }
        if (account.getId() == null) {
            account = em.merge(account);
        }

        if (accountName == null) {
            account.setAccountName(account.getAccountNumber());
        }
        entity.setCreator(getLoggedUser().getId());
        if (isCorporateUser()) {
            CorporateBeneficiary corporateBeneficiary = new CorporateBeneficiary();
            entity.setAmount(BigDecimal.ZERO);
            BeanUtils.copyProperties(corporateBeneficiary, entity);
            corporateBeneficiary.setCorporate(getLoggedCorporate());
            log.info("account::: " + account.getId());
            corporateBeneficiary.setAccount(account);
            entity = super.create(corporateBeneficiary);
            entity.setAccount(account);
            log.info("created::: ");
        } else {
            entity = super.create(entity);
            entity.setAccount(account);
        }
        return entity;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Beneficiary edit(Beneficiary entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Beneficiary> findBeneficiaries(String query) throws Exception {
        boolean accountNumber = false;
        try {
            Long.parseLong(query);
            accountNumber = true;
        } catch (NumberFormatException e) {

        }
        Map<String, Object> map = new HashMap<>();

        if (isCorporateUser()) {
            setEntityNamePrefix("Corporate");
            map.put("corporate", getLoggedCorporate());
        } else {
            map.put("central", true);
            map.put("creator", getLoggedUser().getId());
        }
           // log.info("query::: " + query);
        if (accountNumber) {
            log.log(Level.INFO, "is account number");
            map.put("b.account.accountNumber like '%" + query + "%'", null);
        } else {
            log.log(Level.INFO, "is account name");
            map.put("b.account.accountName like '%" + query + "%'", null);
        }
        List<Beneficiary> foundBeneficiarys = findAll(map, true);
        //log.log(Level.INFO, "found beneficiary: {0}", foundBeneficiarys.size());
        return foundBeneficiarys;
    }

    @Override
    public boolean isExistBeneficiary(Account account) throws Exception {
        Map<String, Object> map = new HashMap<>();
        if (isCorporateUser()) {
            setEntityNamePrefix("Corporate");
            map.put("corporate", getLoggedCorporate());
        } else {
            map.put("central", true);
            map.put("creator", getLoggedUser().getId());
        }
        map.put("b.account.accountNumber = '" + account.getAccountNumber() + "'", null);
//      if(account instanceof BankAccount) {
//                map.put("b.account.bank = '" + ((BankAccount)account).getBank().getId() + "'" , null);
//        }
        List<Beneficiary> foundBeneficiarys = findAll(map, true);
        if (account instanceof BankAccount) {
            for (Beneficiary beneficiary : foundBeneficiarys) {
                if (beneficiary.getAccount() instanceof BankAccount) {
                    Bank selectedBank = ((BankAccount) account).getBank();
                    Bank benBank = ((BankAccount) beneficiary.getAccount()).getBank();
                    if (selectedBank.equals(benBank)) {
                        return true;
                    }
                }
            }
        }
        return !foundBeneficiarys.isEmpty();
    }

    @Override
    public Beneficiary findBeneficiary(Account account) throws Exception {
        Map<String, Object> map = new HashMap<>();
        if (isCorporateUser()) {
            setEntityNamePrefix("Corporate");
            map.put("corporate", getLoggedCorporate());
        } else {
            map.put("central", true);
            map.put("creator", getLoggedUser().getId());
        }
        log.info("account number: " + account.getAccountNumber());
        map.put("b.account.accountNumber = '" + account.getAccountNumber() + "'", null);
//      if(account instanceof BankAccount) {
//                map.put("b.account.bank = '" + ((BankAccount)account).getBank().getId() + "'" , null);
//        }
        List<Beneficiary> foundBeneficiarys = findAll(map, true);
        if (foundBeneficiarys.isEmpty()) {
            return null;
        }
//        else {
//            return foundBeneficiarys.get(0);
//        }
        if (account instanceof BankAccount) {
            for (Beneficiary beneficiary : foundBeneficiarys) {
                if (beneficiary.getAccount() instanceof BankAccount) {
                    Bank selectedBank = ((BankAccount) account).getBank();
                    Bank benBank = ((BankAccount) beneficiary.getAccount()).getBank();
                    if (selectedBank.equals(benBank)) {
                        return beneficiary;
                    }
                }
            }
            return null;
        } else {
            return foundBeneficiarys.get(0);
        }
    }

    @Override
    public List<Beneficiary> findBeneficaries(BeneficiaryGroup beneficiaryGroup) throws Exception {
        Map<String, Object> map = new HashMap<>();
        if (isCorporateUser()) {
            setEntityNamePrefix("Corporate");
            map.put("corporate", getLoggedCorporate());
        } else {
            map.put("central", true);
            map.put("creator", getLoggedUser().getId());
        }
        map.put("beneficiaryGroup", beneficiaryGroup);
        List<Beneficiary> foundBeneficiarys = findAll(map, true);
        return foundBeneficiarys; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Beneficiary save(Beneficiary beneficiary) throws Exception {
        return super.create(beneficiary);
    }

    @Override
    public List<Beneficiary> findBeneficiary(Corporate corporate) throws Exception {
        Map<String, Object> map = new HashMap<>();

        if (isCorporateUser()) {
            setEntityNamePrefix("Corporate");
            map.put("corporate", getLoggedCorporate());
        } else {
            map.put("central", true);
            map.put("creator", getLoggedUser().getId());
        }
        List<Beneficiary> foundBeneficiarys = findAll(map, true);
        //log.log(Level.INFO, "found beneficiary: {0}", foundBeneficiarys.size());
        return foundBeneficiarys;
    }

}
