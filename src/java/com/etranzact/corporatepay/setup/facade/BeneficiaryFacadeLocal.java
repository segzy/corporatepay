/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import com.etranzact.corporatepay.model.Corporate;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface BeneficiaryFacadeLocal extends AbstractFacadeLocal<Beneficiary> {

    List<Beneficiary> findBeneficiaries(String query) throws Exception;
    
     public Beneficiary findBeneficiary(Account account) throws Exception;
     
          public List<Beneficiary> findBeneficiary(Corporate corporate) throws Exception;
    
        Beneficiary save(Beneficiary beneficiary) throws Exception;
    
    boolean isExistBeneficiary(Account account) throws Exception;

    List<Beneficiary> findBeneficaries(BeneficiaryGroup beneficiaryGroup) throws Exception;

}
