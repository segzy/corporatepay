/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.core.facade.InMailFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.InMail;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.MailUtil;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class UserSetupFacade extends AbstractFacade<User> implements UserSetupFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private UserSetupWorkflowFacadeLocal userSetupWorkflowFacadeLocal;
    @EJB
    private InMailFacadeLocal inMailFacadeLocal;

    public UserSetupFacade() {
        super(User.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User create(User entity) throws Exception {

        entity.setPassword(AppUtil.randomPassword(8, null));
        entity.setSecurityKey(entity.getFirstName());
        //entity.setUsername(entity.getEmail());
        User create = super.create(entity);
        userSetupWorkflowFacadeLocal.startWorkflow(User.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, null);
        return create;
    }
    

    @Override
    public User findGlobalUser(String username) throws Exception {
//        Map<String, Object> map = new HashMap<>();
//        map.put("username", username);
//        List<User> findAll = super.findAll(map, false);
//        if (findAll.size() > 1) {
//            throw new Exception("Unexpected Result: more than one user found");
//        } else if (findAll.isEmpty()) {
//            return null;
//        }
         List<User> findAll = getEntityManager().createQuery("select u from User u where u.username='" + username + "'").getResultList();
        if (findAll.size() > 1) {
            throw new Exception("Unexpected Result: more than one user found");
        } else if (findAll.isEmpty()) {
            return null;
        }
        User user = findAll.get(0);
        log.log(Level.INFO, "user found {0}", user.getUsername());
        return user;
    }

    @Override
    public User edit(User entity) throws Exception {
        User dbUser = find(entity.getId());
        dbUser.setTmpActive(entity.getActive());
        dbUser.setTmpEmail(entity.getEmail());
        dbUser.setTmpFirstName(entity.getFirstName());
        dbUser.setTmpFriday(entity.getTTfriday() ? AppConstants.TURNED_ON : AppConstants.TURNED_OFF);
        dbUser.setTmpFromTimeAccess(entity.getFromTimeAccess());
        dbUser.setTmpLastName(entity.getLastName());
        dbUser.setTmpMobilePhone(entity.getMobilePhone());
        dbUser.setTmpMonday(entity.getTTmonday() ? AppConstants.TURNED_ON : AppConstants.TURNED_OFF);
        dbUser.setTmpRole(entity.getRole());
        dbUser.setTmpSaturday(entity.getTTsaturday() ? AppConstants.TURNED_ON : AppConstants.TURNED_OFF);
        dbUser.setTmpSunday(entity.getTTsunday() ? AppConstants.TURNED_ON : AppConstants.TURNED_OFF);
        dbUser.setTmpThursday(entity.getTTthursday() ? AppConstants.TURNED_ON : AppConstants.TURNED_OFF);
        dbUser.setTmpToTimeAccess(entity.getToTimeAccess());
        dbUser.setTmpTuesday(entity.getTTtuesday() ? AppConstants.TURNED_ON : AppConstants.TURNED_OFF);
        dbUser.setTmpWednesday(entity.getTTwednesday() ? AppConstants.TURNED_ON : AppConstants.TURNED_OFF);
        dbUser.setFlagStatus(AppConstants.MODIFIED);
        User create = super.create(dbUser);
        userSetupWorkflowFacadeLocal.startWorkflow(User.class, entity.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return create; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finalizeApprove(User t, ApprovalTask task) throws Exception {
        log.info("finalizin user....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if(task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            MailUtil.getInstance().sendEditMail(t, new String[]{t.getEmail()}, t.getUsername());
            t.setActive(t.getTmpActive());
            t.setEmail(t.getTmpEmail());
            t.setFirstName(t.getTmpFirstName());
            t.setFriday(t.getTmpFriday());
            t.setFromTimeAccess(t.getTmpFromTimeAccess());
            t.setLastName(t.getTmpLastName());
            t.setMobilePhone(t.getTmpMobilePhone());
            t.setMonday(t.getTmpMonday());
            t.setRole(t.getTmpRole());
            t.setSaturday(t.getTmpSaturday());
            t.setSunday(t.getTmpSunday());
            t.setThursday(t.getTmpThursday());
            t.setToTimeAccess(t.getTmpToTimeAccess());
            t.setTuesday(t.getTmpTuesday());
            t.setWednesday(t.getTmpWednesday());
            t.setTokenUserName(t.getTmpTokenUserName());
            t.setTokenSerialNumber(t.getTmpTokenSerialNumber());
            t.setTokenUserGroup(t.getTmpTokenUserGroup());
            t.setTmpActive(null);
            t.setTmpEmail(null);
            t.setTmpFirstName(null);
            t.setTmpFriday(null);
            t.setTmpFromTimeAccess(null);
            t.setTmpLastName(null);
            t.setTmpMobilePhone(null);
            t.setTmpMonday(null);
            t.setTmpRole(null);
            t.setTmpSaturday(null);
            t.setTmpSunday(null);
            t.setTmpThursday(null);
            t.setTmpToTimeAccess(null);
            t.setTmpTuesday(null);
            t.setTmpWednesday(null);
            t.setTmpTokenUserName(null);
            t.setTmpTokenUserGroup(null);
            t.setTmpTokenSerialNumber(null);
        } else if (t.getFlagStatus().equals(AppConstants.CREATED)) {
            t.setPassword(AppUtil.randomPassword(8, null));
            MailUtil.getInstance().sendWelcomeMail(t);
        }
        if(t.getTokenUserName()!=null) {
            t.setTokenActive("1");
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
        InMail inmail = new InMail();
        inmail.setClazz(User.class.getSimpleName());
        inmail.setMessageCode(AppConstants.NEW_USER_MESSAGE_CODE);
        inmail.setTargetObjectId(String.valueOf(t.getId()));
        inMailFacadeLocal.create(inmail);

    }

}
