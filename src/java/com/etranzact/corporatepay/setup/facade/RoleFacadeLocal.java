/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.Role;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface RoleFacadeLocal extends AbstractFacadeLocal<Role>{
    
        List<Role> findRoles() throws Exception;
    
     

    
}
