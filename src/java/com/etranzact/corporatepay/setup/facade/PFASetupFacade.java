/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.PFA;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class PFASetupFacade extends AbstractFacade<PFA> implements PFASetupFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @EJB
    private PFAWorkflowFacadeLocal pFAWorkflowFacadeLocal;

    public PFASetupFacade() {
        super(PFA.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PFA create(PFA entity) throws Exception {
        Set<BankAccount> accounts = new HashSet<>();
         for(BankAccount account: entity.getAccounts()) {
             account.setId(null);
            BankAccount merge = em.merge(account);            
            accounts.add(merge);
        }
         entity.setAccounts(accounts);
        PFA create = super.create(entity);
        //pFAWorkflowFacadeLocal.startWorkflow(PFA.class, create.getAuthCode(), AppConstants.REQUEST_CREATION_APPROVAL, null,null);
        return create;
    }

    @Override
    public PFA edit(PFA entity) throws Exception {
        PFA pfa = find(entity.getAuthCode());
        pfa.setTmpAuthName(entity.getAuthName());
        pfa.setTmpActive(entity.getActive());
        Set<BankAccount> accounts = new HashSet<>();
         for(BankAccount account: entity.getAccounts()) {
            BankAccount merge;
            if (account.getId() < 0) {
                account.setId(null);
                account.setTmpAccountName(account.getAccountName());
                account.setTmpAccountNumber(account.getAccountNumber());
                account.setTmpBank(account.getBank());
                merge =em.merge(account);
                accounts.add(merge);
            } else {
                merge = em.find(BankAccount.class, account.getId());
                merge.setTmpAccountName(account.getAccountName());
                merge.setTmpAccountNumber(account.getAccountNumber());
                merge.setTmpBank(account.getBank());
                merge.setTmpAccountType(account.getAccountType());
                accounts.add(merge);
            }
        }
        pfa.setTmpAccounts(accounts);
        pfa.setTmpUser(entity.getUser());
        pfa.setTmpContactEmail(entity.getContactEmail());
        BankAccount merge = em.merge(entity.getDefaultAccount());
        pfa.setDefaultAccount(merge);
        pfa.setFlagStatus(AppConstants.MODIFIED);
        PFA create = super.create(pfa);
        pFAWorkflowFacadeLocal.startWorkflow(PFA.class, create.getAuthCode(), AppConstants.REQUEST_MODIFIED_APPROVAL, null,null);
        return create;
    }
    
    @Override
       public PFA setDefaultAccount(PFA entity) throws Exception {
           return getEntityManager().merge(entity);
       }

    @Override
    public void finalizeApprove(PFA t, ApprovalTask task) throws Exception {
        log.info("finalizin pfa....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
              if(task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if(t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            t.setActive(t.getTmpActive());
              for (BankAccount account : t.getTmpAccounts()) {
                  account.setAccountName(account.getTmpAccountName());
                account.setAccountNumber(account.getTmpAccountNumber());
                account.setBank(account.getTmpBank());
                account.setAccountType(account.getTmpAccountType());
                account.setTmpAccountName(null);
                account.setTmpAccountNumber(null);
                account.setTmpBank(null);
                account.setTmpAccountType(null);
            }
            t.setAccounts(t.getTmpAccounts());
            t.setAuthName(t.getTmpAuthName());
            t.setContactEmail(t.getTmpContactEmail());
            t.setUser(t.getTmpUser());
            t.setTmpActive(null);
            t.setTmpAccounts(null);
            t.setTmpContactEmail(null);
            t.setTmpAuthName(null);
            t.setTmpUser(null);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE); 
        
    }

}
