/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.Event;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface EventFacadeLocal extends AbstractFacadeLocal<Event> {
    
        public List<Event> findAll(int min, int max, Map<String, Object> filters, boolean activeOnly, String[] flagStatuses, Date fromDate, Date toDate, String dateField, String... orderFields) throws Exception;

}
