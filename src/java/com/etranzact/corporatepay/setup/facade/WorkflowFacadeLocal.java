/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Workflow;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface WorkflowFacadeLocal extends AbstractFacadeLocal<Workflow>{
    
           
        void saveAllRoute(List<ApprovalRoute> approvalRoutes,ApprovalType approvalType) throws Exception;
        
        
    public void finalizeApprove(Workflow t, ApprovalTask task) throws Exception;

    
}
