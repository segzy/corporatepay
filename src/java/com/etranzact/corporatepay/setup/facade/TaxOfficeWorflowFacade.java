/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.model.Tax;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class TaxOfficeWorflowFacade extends AbstractWorkflowFacade implements TaxOfficeWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
          Tax tax = (Tax) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(0,new TaskObjectNameValuePair("Tax Name: ", tax.getAuthName() + "(" + tax.getAuthCode() + ")",
                null));
        int i=1;
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            for (Account account : tax.getTmpAccounts()) {
                if(account instanceof BankAccount) {
                    BankAccount bankAccount = (BankAccount) account;
                                                      String defaultBank = (tax.getDefaultAccount()!=null?tax.getDefaultAccount().equals(bankAccount)?"(Default)":"":"");
                 nameValuePairs.add(i,new TaskObjectNameValuePair("Account " + i + defaultBank + ":", "" + (bankAccount.getTmpBank()==null?bankAccount.getBank().getBankName(): bankAccount.getTmpBank().getBankName()) + " - " + (bankAccount.getTmpAccountName()==null?bankAccount.getAccountName():bankAccount.getTmpAccountName()) + "(" + (bankAccount.getTmpAccountNumber()==null?bankAccount.getAccountNumber():bankAccount.getTmpAccountNumber()) + ")" +  " )",
                        null));
                }
                  if(account instanceof PocketMoneyAccount) {
                    PocketMoneyAccount pocketMoneyAccount = (PocketMoneyAccount) account;
                nameValuePairs.add(i,new TaskObjectNameValuePair("Account " + i + ":", "" + pocketMoneyAccount.getAccountNumber()+ " )",
                        null));
                }
                i++;
            }
        } else {
                 for (Account account : tax.getAccounts()) {
                      if(account instanceof BankAccount) {
                    BankAccount bankAccount = (BankAccount) account;
                                                                   String defaultBank = (tax.getDefaultAccount()!=null?tax.getDefaultAccount().equals(bankAccount)?"(Default)":"":"");
                nameValuePairs.add(i,new TaskObjectNameValuePair("Account " + i + defaultBank + ":", "" + bankAccount.getBank().getBankName() + " - " + bankAccount.getAccountName() + "(" + bankAccount.getAccountNumber() + ")" +  " )",
                        null));
                }
                  if(account instanceof PocketMoneyAccount) {
                    PocketMoneyAccount pocketMoneyAccount = (PocketMoneyAccount) account;
                nameValuePairs.add(i,new TaskObjectNameValuePair("Account " + i + ":", "" + pocketMoneyAccount.getAccountNumber() + " )",
                        null));
                }
                i++;
            }
        }
        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }


}
