/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Role;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class RoleFacade extends AbstractFacade<Role> implements RoleFacadeLocal {

    @PersistenceContext
    private EntityManager em;


    public RoleFacade() {
        super(Role.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Role edit(Role entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Role> findRoles() throws Exception {
      
                  ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            Map<String,Object> filters = new HashMap<>();
               if (loggedInUser instanceof BankUser) {
            filters.put("r.accessLevel in ('" + AppConstants.ALL_ROLE_TYPE_ACCESS + "','" + 
                    AppConstants.BANK_ADMINISTRATOR_ACCESS + "','" + AppConstants.BANKADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS
                    + "','" + AppConstants.ADMINISTRATOR_BANKADMINISTRATOR_ACCESS + "')" , null);
        } else if(loggedInUser instanceof CorporateUser) {
               filters.put("r.accessLevel in ('" + AppConstants.ALL_ROLE_TYPE_ACCESS + "','" + 
                    AppConstants.CORPORATE_ADMINISTRATOR_ACCESS + "','" + AppConstants.BANKADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS
                    + "','" + AppConstants.ADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS + "')" , null);
        } else {
                 filters.put("r.accessLevel in ('" + AppConstants.ALL_ROLE_TYPE_ACCESS + "','" + 
                    AppConstants.ADMINISTRATOR_ACCESS + "','" + AppConstants.ADMINISTRATOR_BANKADMINISTRATOR_ACCESS
                    + "','" + AppConstants.ADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS + "')" , null);
        }
            return findAll(filters, true,new String[]{"roleName"});
    }



}
