/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.HoldingAccount;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class HoldingAccountFacade extends AbstractFacade<HoldingAccount> implements HoldingAccountFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private HoldingAccountWorkflowFacadeLocal holdingAccountWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    public HoldingAccountFacade() {
        super(HoldingAccount.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HoldingAccount create(HoldingAccount entity) throws Exception {
        HoldingAccount create = super.create(entity);
        holdingAccountWorkflowFacadeLocal.startWorkflowBank(HoldingAccount.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null, getLoggedBank(), null);
        return create;
    }

    @Override
    public HoldingAccount edit(HoldingAccount entity) throws Exception {
        HoldingAccount holdingAccount = find(entity.getId());
        holdingAccount.setTmpActive(entity.getActive());
        holdingAccount.setTmpCardNumber(entity.getCardNumber());
        holdingAccount.setFlagStatus(entity.getFlagStatus());
        HoldingAccount create = super.create(holdingAccount);
        holdingAccountWorkflowFacadeLocal.startWorkflow(HoldingAccount.class, entity.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return create; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void finalizeApprove(HoldingAccount t, ApprovalTask task) throws Exception {
        log.info("finalizin holding account....");
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
              if(task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        Map map = new HashMap<>();
        map.put("u.role.id in ('003','005')", null);
        List<User> blincopies = userSetupFacadeLocal.findAll(map, true);
        String[] bcopies = new String[blincopies.size()];
        for (int i = 0; i < blincopies.size(); i++) {
            bcopies[i] = blincopies.get(i).getEmail();
        }
        map = new HashMap<>();
        map.put("u.role.administrativeRole=true", null);
        map.put("bank", getLoggedBank());
        userSetupFacadeLocal.setEntityNamePrefix("Bank");
        List<User> findAll = userSetupFacadeLocal.findAll(map, true);
        String[] recepients = new String[findAll.size()];
        for (int i = 0; i < findAll.size(); i++) {
            recepients[i] = findAll.get(i).getEmail();
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {

            MailUtil.getInstance().sendEditMail(t, recepients, t.getCardNumber() + " (" + t.getCorporate().getCorporateName() + ")", bcopies);
            t.setActive(t.getTmpActive());
            t.setCardNumber(t.getTmpCardNumber());
            t.setTmpActive(null);
            t.setTmpCardNumber(null);
        } else if (t.getFlagStatus().equals(AppConstants.CREATED)) {
            MailUtil.getInstance().sendCreationMail(t, recepients, t.getCardNumber() + (t.getCorporate()!=null?" (" + t.getCorporate().getCorporateName() + ")":""), bcopies);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
    }

}
