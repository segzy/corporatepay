/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.PFA;
import com.etranzact.corporatepay.model.Tax;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface TaxOfficeSetupFacadeLocal extends AbstractFacadeLocal<Tax> {

    public Tax edit(Tax entity) throws Exception;
    
    public Tax setDefaultAccount(Tax entity) throws Exception;

    public void finalizeApprove(Tax t, ApprovalTask task) throws Exception;
   
}
