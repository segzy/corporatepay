/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class BeneficiaryGroupFacade extends AbstractFacade<BeneficiaryGroup> implements BeneficiaryGroupFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public BeneficiaryGroupFacade() {
        super(BeneficiaryGroup.class);
    }

        @Override
    public BeneficiaryGroup create(BeneficiaryGroup entity) throws Exception {
        entity.setCreator(getLoggedUser().getId());
        if (isCorporateUser()) {
            entity.setCorporate(getLoggedCorporate());
        }
           entity = super.create(entity);
        return entity;
    }


    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected BeneficiaryGroup edit(BeneficiaryGroup entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
