/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.HoldingAccount;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class HoldingAccountWorkflowFacade extends AbstractWorkflowFacade implements HoldingAccountWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
        @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        HoldingAccount holdingAccount = (HoldingAccount) target;
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            if (!holdingAccount.getCardNumber().equals(holdingAccount.getTmpCardNumber())) {
                nameValuePairs.add(0,new TaskObjectNameValuePair("Card Number: ", holdingAccount.getCardNumber(),
                        holdingAccount.getTmpCardNumber()));
            }
        } else {
            nameValuePairs.add(0,new TaskObjectNameValuePair("Card Number: ", holdingAccount.getCardNumber(),
                    null));
        }
        nameValuePairs.add(1,new TaskObjectNameValuePair("Account for: ", holdingAccount.getCorporate() == null ? "Global" : holdingAccount.getCorporate().getCorporateName(),
                null));
        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
