/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporatePayService;
import com.etranzact.corporatepay.util.AppConstants;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class CorporatepayServiceFacade extends AbstractFacade<CorporatePayService> implements CorporatepayServiceFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    
    public CorporatepayServiceFacade() {
        super(CorporatePayService.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected CorporatePayService edit(CorporatePayService entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
