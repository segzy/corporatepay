/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.MailUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class ApprovalGroupFacade extends AbstractFacade<ApprovalGroup> implements ApprovalGroupFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private ApprovalGroupWorkflowFacadeLocal approvalGroupWorkflowFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    public ApprovalGroupFacade() {
        super(ApprovalGroup.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ApprovalGroup create(ApprovalGroup entity) throws Exception {
        ApprovalGroup create = super.create(entity);
        //       approvalGroupWorkflowFacadeLocal.startWorkflow(ApprovalGroup.class, create.getId(), AppConstants.REQUEST_CREATION_APPROVAL, null);
        return create;
    }

    @Override
    public ApprovalGroup edit(ApprovalGroup entity) throws Exception {
        ApprovalGroup appGroup = find(entity.getId());
        Set<User> users = new HashSet<>();
        for (User user : entity.getUsers()) {
            users.add(em.find(User.class, user.getId()));
        }
        appGroup.setGroupAlias(entity.getGroupAlias());
        appGroup.setFlagStatus(entity.getFlagStatus());
        appGroup.setTmpGroupAlias(entity.getGroupAlias());
        appGroup.setTmpUsers(users);
        appGroup.setTmpActive(entity.getActive());
        approvalGroupWorkflowFacadeLocal.startWorkflow(ApprovalGroup.class, appGroup.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
        return appGroup;
    }

    @Override
    public void finalizeApprove(ApprovalGroup t, ApprovalTask task) throws Exception {
        log.info("finalizin Approval Group....");
        String flagStatus = t.getFlagStatus();
        String status = task.getApprovalAction().equals(AppConstants.REJECTED)
                ? t.getFlagStatus().equals(AppConstants.CREATED)
                        ? AppConstants.CREATED_REJECTED : AppConstants.MODIFIED_REJECTED : task.getApprovalAction();
        if (task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(status);
            return;
        }
        if (t.getFlagStatus().equals(AppConstants.MODIFIED)) {
            t.setGroupAlias(t.getGroupAlias());
            t.setUsers(t.getTmpUsers());
            t.setTmpGroupAlias(null);
            t.setTmpUsers(null);
            t.setTmpActive(null);
        }
        t.setFlagStatus(status);
        t.setActive(Boolean.TRUE);
        Set<User> users = t.getUsers();
        List<User> listUsers = new ArrayList<>();
        listUsers.addAll(users);
        String[] recepients = new String[listUsers.size()];
        for (int i = 0; i < listUsers.size(); i++) {
            recepients[i] = listUsers.get(i).getEmail();
        }
        if (flagStatus.equals(AppConstants.MODIFIED)) {
            MailUtil.getInstance().sendEditMail2(task, t, recepients, t.getGroupAlias());
        } else {
            MailUtil.getInstance().sendCreationMail2(task, t, recepients, t.getGroupAlias());
        }
    }

}
