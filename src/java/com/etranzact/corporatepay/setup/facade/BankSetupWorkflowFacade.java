/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractWorkflowFacade;
import com.etranzact.corporatepay.core.facade.AuditLogFacadeLocal;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.TaskObjectNameValuePair;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class BankSetupWorkflowFacade extends AbstractWorkflowFacade implements BankSetupWorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private AuditLogFacadeLocal auditLogFacadeLocal;

    @Override
    public List<TaskObjectNameValuePair> getTaskObject(String requestType) {
        Bank bank = (Bank) target;
        setAuditLogFacadeLocal(auditLogFacadeLocal);
        List<TaskObjectNameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new TaskObjectNameValuePair("Bank Code: ", bank.getId(), null));
        if (requestType.equals(AppConstants.REQUEST_MODIFIED_APPROVAL)) {
            if (!bank.getBankName().equals(bank.getTmpBankName())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Bank Name: ", bank.getBankName(),
                        bank.getTmpBankName()));
            }
            if (!bank.getBankEmail().equals(bank.getTmpBankEmail())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Bank Email: ", bank.getBankEmail(),
                        bank.getTmpBankEmail()));
            }
//            if (!bank.getBankIp().equals(bank.getTmpBankIp())) {
//                nameValuePairs.add(new TaskObjectNameValuePair("Preferred Access IP: ", bank.getBankIp(),
//                        bank.getTmpBankIp()));
//            }
//            if (!bank.getTssAccount().equals(bank.getTmpTssAccount())) {
//                nameValuePairs.add(new TaskObjectNameValuePair("TSS Account: ", bank.getTssAccount(),
//                        bank.getTmpTssAccount()));
//            }
            if (!bank.getEtzFee().equals(bank.getTmpEtzFee())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Platform Provider Commission: ", "NGN " + AppUtil.formatAsAmount(bank.getEtzFee().toString()),
                        "NGN " + AppUtil.formatAsAmount(bank.getTmpEtzFee().toString())));
            }
            if (bank.getTmpmfbBank() != null) {
                if (!bank.getMfbBank().equals(bank.getTmpmfbBank())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Bank Type: ", bank.getMfbBank() ? "MFB" : "Regular",
                            bank.getTmpmfbBank() ? "MFB" : "Regular"));
                }
                if (!bank.getTmpAffliateBank().equals(bank.getAffliateBank())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Affliate Bank: ", bank.getAffliateBank().getBankName(),
                            bank.getTmpAffliateBank().getBankName()));
                }
                if (!bank.getTmpAffliateBankAccount().equals(bank.getAffliateBankAccount())) {
                    nameValuePairs.add(new TaskObjectNameValuePair("Affliate Bank Account: ", bank.getAffliateBankAccount(),
                            bank.getTmpAffliateBankAccount()));
                }
            }
            if (bank.getTmpEnableESA() != null && !bank.getEnableESA().equals(bank.getTmpEnableESA())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Enable Token Operations: ", bank.getEnableESA().toString(),
                        bank.getTmpEnableESA().toString()));
            }
              if (bank.getTmpSettlementMode() != null && !bank.getSettlementMode().equals(bank.getTmpSettlementMode())) {
                nameValuePairs.add(new TaskObjectNameValuePair("Settlement Type: ", bank.getSettlementMode(),
                        bank.getTmpSettlementMode()));
            }

        } else {

            nameValuePairs.add(0, new TaskObjectNameValuePair("Bank Name: ", bank.getBankName(),
                    null));
            nameValuePairs.add(1, new TaskObjectNameValuePair("Bank Email: ", bank.getBankEmail(),
                    null));
            nameValuePairs.add(2, new TaskObjectNameValuePair("Platform Provider Commission: ", "NGN " + AppUtil.formatAsAmount(bank.getEtzFee().toString()),
                    null));
            nameValuePairs.add(3, new TaskObjectNameValuePair("Bank Type: ", bank.getMfbBank() ? "MFB" : "Regular",
                    null));
            nameValuePairs.add(4, new TaskObjectNameValuePair("Enable Token Operations: ", bank.getEnableESA().toString(),
                    null));
            nameValuePairs.add(5, new TaskObjectNameValuePair("Settlement Type: ", "0".equals(bank.getSettlementMode()) ? "Normal" : "Specialized",
                    null));
            if (bank.getMfbBank()) {
                nameValuePairs.add(6, new TaskObjectNameValuePair("Affliate Bank: ", bank.getAffliateBank().getBankName(),
                        null));
                nameValuePairs.add(7, new TaskObjectNameValuePair("Affliate Bank Account: ", bank.getAffliateBankAccount(),
                        null));
            }
        }

        return nameValuePairs;
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

}
