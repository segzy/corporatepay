/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Commission;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface CommissionFacadeLocal extends AbstractFacadeLocal<Commission>{
    
      public  Commission save(Commission entity) throws Exception;

     public Commission edit(Commission entity) throws Exception;
       
    public void finalizeApprove(Commission t, ApprovalTask task) throws Exception;
    
}
