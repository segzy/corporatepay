/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.model.HoldingAccount;
import com.etranzact.corporatepay.model.User;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface HoldingAccountFacadeLocal extends AbstractFacadeLocal<HoldingAccount> {

    public HoldingAccount edit(HoldingAccount entity) throws Exception;

    public void finalizeApprove(HoldingAccount t, ApprovalTask task) throws Exception;

}
