/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.PFA;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface PFASetupFacadeLocal extends AbstractFacadeLocal<PFA> {

    public PFA edit(PFA entity) throws Exception;
    
    public PFA setDefaultAccount(PFA entity) throws Exception;

    public void finalizeApprove(PFA t, ApprovalTask task) throws Exception;
   
}
