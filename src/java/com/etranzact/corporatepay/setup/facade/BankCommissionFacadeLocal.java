/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.BankCommission;
import javax.ejb.Local;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface BankCommissionFacadeLocal extends AbstractFacadeLocal<BankCommission>{

       BankCommission edit(BankCommission entity) throws Exception;
       
    public void finalizeApprove(BankCommission t, ApprovalTask task) throws Exception;
    
}
