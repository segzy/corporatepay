/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacadeLocal;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.Corporate;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Local
public interface CorporateSetupFacadeLocal extends AbstractFacadeLocal<Corporate> {

    public Corporate edit(Corporate entity) throws Exception;

    public void finalizeApprove(Corporate t, ApprovalTask task) throws Exception;
    
    
    public Corporate save(Corporate entity) throws Exception;
    
 

}
