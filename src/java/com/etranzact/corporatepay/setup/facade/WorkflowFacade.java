/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalTask;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.model.Workflow;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class WorkflowFacade extends AbstractFacade<Workflow> implements WorkflowFacadeLocal {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private WWorkflowFacadeLocal wWorkflowFacadeLocal;

    public WorkflowFacade() {
        super(Workflow.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return em; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Workflow edit(Workflow entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveAllRoute(List<ApprovalRoute> approvalRoutes, ApprovalType approvalType) throws Exception {
        Map<String, Object> filter = new HashMap<>();
        if (isBankUser()) {
            filter.put("bank", getLoggedBank());
            entityNamePrefix = "Bank";
        } else if (isCorporateUser()) {
            filter.put("corporate", getLoggedCorporate());
            entityNamePrefix = "Corporate";
        } else {
            filter.put("central", Boolean.TRUE);
        }
        filter.put("approvalType", approvalType);
        List<Workflow> findAll = findAll(filter, true);
        Workflow workflow;
        if (findAll.isEmpty()) {
            workflow = new Workflow();
            for (ApprovalRoute approvalRoute : approvalRoutes) {
                if (approvalRoute.getId() < 0) {
                    approvalRoute.setId(null);
                    workflow.getApprovalRoutes().add(em.merge(approvalRoute));
                } else {
                    workflow.getApprovalRoutes().add(em.find(ApprovalRoute.class, approvalRoute.getId()));
                }
               
            }
            workflow.setApprovalType(approvalType);
            workflow.setFlagStatus(AppConstants.CREATED);
            workflow.setActive(Boolean.TRUE);
        } else if (findAll.size() > 1) {
            throw new PersistenceException("unexpected result");
        } else {
            workflow = findAll.get(0);
            if(workflow.getFlagStatus().equals(AppConstants.CREATED)) {
                throw new PersistenceException("Cannot Save Changes: Workflow currently undergoing an approval process");
            }
            for (ApprovalRoute approvalRoute : approvalRoutes) {
                if (approvalRoute.getId() < 0) {
                    approvalRoute.setId(null);
                     workflow.getApprovalRoutes().add(em.merge(approvalRoute));
                } else {
                    workflow.getApprovalRoutes().add(em.find(ApprovalRoute.class, approvalRoute.getId()));
                }
               
            }
        }
        Workflow create = super.create(workflow);
        wWorkflowFacadeLocal.startWorkflow(Workflow.class, create.getId(), AppConstants.REQUEST_MODIFIED_APPROVAL, null, null);
    }

    @Override
    public void finalizeApprove(Workflow t, ApprovalTask task) throws Exception {
              if(task.getApprovalAction().equals(AppConstants.REJECTED)) {
            t.setFlagStatus(AppConstants.REJECTED);
            return;
        }
        t.setFlagStatus(AppConstants.APPROVED);
        for(ApprovalRoute approvalRoute: t.getApprovalRoutes()) {
            approvalRoute.setFlagStatus(AppConstants.APPROVED);
        }
    }

}
