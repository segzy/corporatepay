/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.facade;

import com.etranzact.corporatepay.core.facade.AbstractFacade;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Beneficiary;
import com.etranzact.corporatepay.model.BeneficiaryGroup;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateBeneficiary;
import com.etranzact.corporatepay.model.PocketMoneyAccount;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Stateless
public class CorporateBeneficiaryFacade extends AbstractFacade<CorporateBeneficiary> implements CorporateBeneficiaryFacadeLocal {

    @PersistenceContext
    private EntityManager em;

    public CorporateBeneficiaryFacade() {
        super(CorporateBeneficiary.class);
    }

    @Override
    protected EntityManager getEntityManager() {
      return em;
    }

    @Override
    protected CorporateBeneficiary edit(CorporateBeneficiary entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

}
