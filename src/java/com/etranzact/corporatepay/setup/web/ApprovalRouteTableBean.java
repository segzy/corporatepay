/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Menu;
import com.etranzact.corporatepay.model.MenuItem;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApproverFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.SortOrder;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "approvalRouteTableBean")
@SessionScoped
public class ApprovalRouteTableBean extends AbstractTableBean<ApprovalRoute> {

    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;
   
    @PostConstruct
    @Override
    public void init() {
        super.init();
    }

    public ApprovalRouteTableBean() {
        super(ApprovalRoute.class);
    }

    @Override
    protected ApprovalRouteFacadeLocal getFacade() {
        return approvalRouteFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties tableProperties = new TableProperties();
//        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
                    ApprovalRouteBean approvalRouteBean = (ApprovalRouteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("approvalRouteBean");
        ApprovalType approvalType = approvalRouteBean.getApprovalType();
//        log.log(Level.INFO, "approvalType || approvalType || approvalType {0}", approvalType);
        if (isBankUser()) {
//            BankUser bankUser = (BankUser) loggedInUser;
            tableProperties.getAdditionalFilter().put("bank",getLoggedBank());
            approvalRouteFacadeLocal.setEntityNamePrefix("Bank");
        } else if (isCorporateUser()) {
//            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            tableProperties.getAdditionalFilter().put("corporate", getLoggedCorporate());
            approvalRouteFacadeLocal.setEntityNamePrefix("Corporate");
        }
        tableProperties.getAdditionalFilter().put("approvalType", approvalType);
        return tableProperties;
    }

 


    @Override
    public ApprovalRoute getTableRowData(String rowKey) {
        List<ApprovalRoute> wrappedData = (List<ApprovalRoute>) getLazyDataModel().getWrappedData();
        for (ApprovalRoute t : wrappedData) {
            if (Long.parseLong(rowKey) == t.getId()) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(ApprovalRoute t) {
        return t.getId();//To change body of generated methods, choose Tools | Templates.
    }


    

}
