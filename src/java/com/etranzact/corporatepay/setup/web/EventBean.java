/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Event;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.payment.facade.PaymentFacadeLocal;
import com.etranzact.corporatepay.payment.facade.TransactionFacadeLocal;
import com.etranzact.corporatepay.payment.web.TransactionTableBean;
import com.etranzact.corporatepay.setup.facade.EventFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "eventBean")
@SessionScoped
public class EventBean extends AbstractBean<Event> {

    @EJB
    private EventFacadeLocal eventFacadeLocal;
    @EJB
    private TransactionFacadeLocal transactionFacadeLocal;
    @EJB
    private PaymentFacadeLocal paymentFacadeLocal;
    private Transaction transaction;
    private Payment payment;

    @Override
    protected EventFacadeLocal getFacade() {
        return eventFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public EventBean() {
        super(Event.class);
    }

    public void initEvent() {
        transaction = null;
        if (selectedItem != null) {
            if (selectedItem.getPayment() != null) {
                selectedItem.setPayment(null);
            }
            selectedItem = null;
        }
    }

    /**
     * @param selectedItemm the selectedItem to set
     */
    @Override
    public void setSelectedItem(Event selectedItemm) {
        try {
            super.setSelectedItem(selectedItemm);
            if(selectedItem!=null) {
            String eventObject = selectedItem.getEventObject().startsWith("08")? selectedItem.getEventObject().substring(2) : selectedItem.getEventObject();
            payment = paymentFacadeLocal.find(Long.valueOf(eventObject));
            selectedItem.setPayment(payment);
            }
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void markAsFailed() {
        FacesMessage m = new FacesMessage();
        try {
            transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_FAILED);
            transactionFacadeLocal.create(transaction);
            Map<String, Object> map = new HashMap<>();
            map.put("eventObject='" + "08" + transaction.getTransactionId().toString() + "'", null);
            List<Event> findAll = getFacade().findAll(map, false);
            for (Event e : findAll) {
                e.setResolved(Boolean.TRUE);
                eventFacadeLocal.create(e);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Transaction has being marked failed");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            initEvent();
        }
    }

    public void markAsSuccessful() {
        FacesMessage m = new FacesMessage();
        try {
            transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PAID);
            transaction.setPaymentDate(new Date());
            transactionFacadeLocal.create(transaction);
            Map<String, Object> map = new HashMap<>();
            map.put("eventObject='" + "08" + transaction.getTransactionId().toString() + "'", null);
            List<Event> findAll = getFacade().findAll(map, false);
            for (Event e : findAll) {
                e.setResolved(Boolean.TRUE);
                eventFacadeLocal.create(e);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Transaction has being marked successful");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            initEvent();
        }
    }
    
    public void forceSettleTransaction(boolean commission) {
       FacesMessage m = new FacesMessage();
        try {
            if(commission) {
              transaction.setForcedCommissionSettlement(commission);
            } else {
            transaction.setForcedSettlement(Boolean.TRUE);
            }
            transactionFacadeLocal.create(transaction);
            Map<String, Object> map = new HashMap<>();
            map.put("eventObject='" + "08" + transaction.getTransactionId().toString() + "'", null);
            List<Event> findAll = getFacade().findAll(map, false);
            for (Event e : findAll) {
                e.setResolved(Boolean.TRUE);
                eventFacadeLocal.create(e);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Transaction " + (commission? "(Commission) has being marked for force settling":" has being marked for force settling"));
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            initEvent();
        }  
    }
    
       public void exemptSettleTransaction(boolean commission) {
       FacesMessage m = new FacesMessage();
        try {
            if(commission) {
              transaction.setExemptedCommissionSettlement(commission);
            } else {
            transaction.setExemptedCommissionSettlement(Boolean.TRUE);
            }
            transactionFacadeLocal.create(transaction);
            Map<String, Object> map = new HashMap<>();
            map.put("eventObject='" + "08" + transaction.getTransactionId().toString() + "'", null);
            List<Event> findAll = getFacade().findAll(map, false);
            for (Event e : findAll) {
                e.setResolved(Boolean.TRUE);
                eventFacadeLocal.create(e);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Transaction " + (commission? "(Commission) has being exempted from force settling":" has being exempted from force settling"));
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            initEvent();
        }  
    }
    

    public void markAsAuthorized() {
        FacesMessage m = new FacesMessage();
        try {
            paymentFacadeLocal.finalizeApprove(payment);
             Map<String, Object> map = new HashMap<>();
            map.put("eventObject='" + "08" + payment.getId().toString() + "'", null);
            List<Event> findAll = getFacade().findAll(map, false);
            log.info("events " + findAll.size());
            for (Event e : findAll) {
                e.setResolved(Boolean.TRUE);
                eventFacadeLocal.create(e);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Payment has being marked Authorized");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            initEvent();
        }
    }

    public void markForProcessfing() {
        FacesMessage m = new FacesMessage();
        try {
            transaction.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
            transactionFacadeLocal.create(transaction);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Transaction has being marked for Processing");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            initEvent();
        }
    }

    public void markAllAsFailed() {
        FacesMessage m = new FacesMessage();
        try {
            TransactionTableBean transactionTableBean = (TransactionTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionTableBean");
            List<Transaction> allTransactions = transactionTableBean.getAllList();
            for (Transaction transactn : allTransactions) {
                if (!transactn.isStatusUnknown()) {
                    continue;
                }
                transactn.setTrsnsactionStatus(AppConstants.TRANSACTION_FAILED);
                transactionFacadeLocal.create(transactn);
                Map<String, Object> map = new HashMap<>();
                map.put("eventObject='" + "08" + transactn.getTransactionId().toString() + "'", null);
                List<Event> findAll = getFacade().findAll(map, false);
                for (Event e : findAll) {
                    e.setResolved(Boolean.TRUE);
                    eventFacadeLocal.create(e);
                }
            }

            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(allTransactions.size() + "Transaction" + "(s) has being marked failed");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void markAllAsSuccessful() {
        FacesMessage m = new FacesMessage();
        try {
            TransactionTableBean transactionTableBean = (TransactionTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionTableBean");
            List<Transaction> allTransactions = transactionTableBean.getAllList();
            for (Transaction transactn : allTransactions) {
                if (!transactn.isStatusUnknown()) {
                    continue;
                }
                transactn.setTrsnsactionStatus(AppConstants.TRANSACTION_PAID);
                transactionFacadeLocal.create(transactn);
                Map<String, Object> map = new HashMap<>();
                map.put("eventObject='" + "08" + transactn.getTransactionId().toString() + "'", null);
                List<Event> findAll = getFacade().findAll(map, false);
                for (Event e : findAll) {
                    e.setResolved(Boolean.TRUE);
                    eventFacadeLocal.create(e);
                }
            }

            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(allTransactions.size() + "Transaction" + "(s) has being marked Sucessful");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void markAllForProcessfing() {
        FacesMessage m = new FacesMessage();
        try {
            TransactionTableBean transactionTableBean = (TransactionTableBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("transactionTableBean");
            List<Transaction> allTransactions = transactionTableBean.getAllList();
            for (Transaction transactn : allTransactions) {
                if (!transactn.isStatusUnknown()) {
                    continue;
                }
                transactn.setTrsnsactionStatus(AppConstants.TRANSACTION_PENDING_PAYMENT);
                transactionFacadeLocal.create(transactn);
            }

            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(allTransactions.size() + "Transaction" + "(s) has being marked for Processing");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(EventBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            initEvent();
        }
    }

    /**
     * @return the transaction
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * @param transaction the transaction to set
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
    


}
