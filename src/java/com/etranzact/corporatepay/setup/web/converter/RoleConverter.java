/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Role;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.RoleFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("roleConverter")
public class RoleConverter implements Converter {
    
    @Inject
    private RoleFacadeLocal roleFacadeLocal;

    private static final Logger log = Logger.getLogger(RoleConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            List<Role> roles = null;
            try{
               roles=roleFacadeLocal.findRoles();
            } catch (Exception ex) {
                Logger.getLogger(RoleConverter.class.getName()).log(Level.SEVERE, null, ex);
                roles = new ArrayList<>();
            }
            for (Role role : roles) {
                if (value.equalsIgnoreCase(role.getId())) {
                    return role;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Role) {
            Role role = (Role) value;
            if( role.getId()==null) return "";
            return role.getId();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }

}
