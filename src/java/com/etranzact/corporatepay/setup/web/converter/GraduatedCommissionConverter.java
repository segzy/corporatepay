/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.setup.facade.BankCommissionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CommissionFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("gradBankCommissionConverter")
public class GraduatedCommissionConverter implements Converter {

    private static final Logger log = Logger.getLogger(GraduatedCommissionConverter.class.getName());

    @Inject
    private CommissionFacadeLocal commissionFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Long id = Long.valueOf(value);
            List<Commission> bankCommissions;
            try {
                bankCommissions = commissionFacadeLocal.findAll();
            } catch (Exception ex) {
                Logger.getLogger(GraduatedCommissionConverter.class.getName()).log(Level.SEVERE, null, ex);
                bankCommissions = new ArrayList<>();
            }
            for (Commission bankCommission : bankCommissions) {
                if (id.equals(bankCommission.getId())) {
                    return bankCommission;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Commission) {
            Commission bankCommission = (Commission) value;
            if ( bankCommission.getId() == null) {
                return "";
            }
            return bankCommission.getId().toString();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }

}
