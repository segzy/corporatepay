/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.CorporatePayService;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporatepayServiceFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("cpservicesConverter")
public class CPServiceConverter implements Converter {

    private static final Logger log = Logger.getLogger(CorporateConverter.class.getName());

    @Inject
    private CorporatepayServiceFacadeLocal corporatepayServiceFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("defaultService", Boolean.FALSE);
            List<CorporatePayService> corporatePayServices;
            try {
                corporatePayServices = corporatepayServiceFacadeLocal.findAll(map, false);
            } catch (Exception ex) {
                Logger.getLogger(CPServiceConverter.class.getName()).log(Level.SEVERE, null, ex);
                corporatePayServices = new ArrayList<>();
            }
            for (CorporatePayService corporatePayService : corporatePayServices) {
                if (value.equals(corporatePayService.getServiceCode())) {
                    return corporatePayService;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof CorporatePayService) {
            CorporatePayService corporatePayService = (CorporatePayService) value;
            if (corporatePayService.getServiceCode() == null) {
                return "";
            }
            return corporatePayService.getServiceCode();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
