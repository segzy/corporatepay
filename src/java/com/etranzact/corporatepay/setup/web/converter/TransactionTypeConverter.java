/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Role;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.RoleFacadeLocal;
import com.etranzact.corporatepay.setup.facade.TransactionTypeFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("transactionTypeConverter")
public class TransactionTypeConverter implements Converter {
    
    @Inject
    private TransactionTypeFacadeLocal transactionTypeFacadeLocal;

    private static final Logger log = Logger.getLogger(TransactionTypeConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            List<TransactionType> transactionTypes = null;
            try{
               transactionTypes= transactionTypeFacadeLocal.findAll(new HashMap<String, Object>(), false);
            } catch (Exception ex) {
                Logger.getLogger(TransactionTypeConverter.class.getName()).log(Level.SEVERE, null, ex);
                transactionTypes = new ArrayList<>();
            }
            for (TransactionType transactionType : transactionTypes) {
                if (value.equalsIgnoreCase(transactionType.getId())) {
                    return transactionType;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof TransactionType) {
            TransactionType transactionType = (TransactionType) value;
            if( transactionType.getId()==null) return "";
            return transactionType.getId();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }

}
