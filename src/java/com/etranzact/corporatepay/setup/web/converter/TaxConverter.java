/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.PFA;
import com.etranzact.corporatepay.model.Tax;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.PFASetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.TaxOfficeSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("taxConverter")
public class TaxConverter implements Converter {

    private static final Logger log = Logger.getLogger(TaxConverter.class.getName());

    @Inject
    private TaxOfficeSetupFacadeLocal taxOfficeSetupFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Map<String, Object> map = new HashMap<>();
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            map.put("corporate", ((CorporateUser) loggedInUser).getCorporate());
            List<Tax> taxs;
            try {
                taxs = taxOfficeSetupFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(TaxConverter.class.getName()).log(Level.SEVERE, null, ex);
                taxs = new ArrayList<>();
            }
            for (Tax tax : taxs) {
                if (value.equals(tax.getAuthCode())) {
                    return tax;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Tax) {
            Tax tax = (Tax) value;
            return tax.getAuthCode();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
