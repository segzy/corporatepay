/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.setup.web.ApprovalRouteBean;
import com.etranzact.corporatepay.setup.web.ApprovalRouteTableBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Oluwasegun.Idowu
 */
@FacesConverter("approverConverter")
public class ApproverConverter implements Converter {

    private static final Logger log = Logger.getLogger(ApproverConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Long id = Long.valueOf(value);
            ApprovalRouteBean approvalRouteBean = (ApprovalRouteBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("approvalRouteBean");
            for (Approver approver : approvalRouteBean.getApprovers()) {
           //     log.log(Level.INFO, "Approver {0}", approver);
                if (id.equals(approver.getId())) {
                 //   log.log(Level.INFO, "Found Approver Id{0}", id);
                    return approver;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Approver) {
            Approver app = (Approver) value;
          //  log.log(Level.INFO, "Found Approver Id{0}", app.getId());
            if(app.getId()==null) return "";
            return app.getId().toString();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }

}
