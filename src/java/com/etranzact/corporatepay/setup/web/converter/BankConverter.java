/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("bankConverter")
public class BankConverter implements Converter {

    private static final Logger log = Logger.getLogger(CorporateConverter.class.getName());

    @Inject
    private BankSetupFacadeLocal bankFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("flagStatus", AppConstants.APPROVED);
            List<Bank> banks;
            try {
                banks = bankFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(BankConverter.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
            for (Bank bank : banks) {
                if (value.equals(bank.getId())) {
                    return bank;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof Bank) {
            Bank bank = (Bank) value;
            if (bank.getId() == null) {
                return "";
            }
            return bank.getId();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }
}
