/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web.converter;

import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Oluwasegun.Idowu
 */
@Named
@ApplicationScoped
@FacesConverter("userConverter")
public class UserConverter implements Converter {

    private static final Logger log = Logger.getLogger(UserConverter.class.getName());

    @Inject
    private UserSetupFacadeLocal userSetupFacadeLocal;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value.trim().equals("")) {
            return null;
        } else {
            Long id = Long.valueOf(value);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            Map<String, Object> map = new HashMap<>();
            if (loggedInUser instanceof BankUser) {
                BankUser bankUser = (BankUser) loggedInUser;
                map.put("bank", bankUser.getBank());
                userSetupFacadeLocal.setEntityNamePrefix("Bank");
            } else if (loggedInUser instanceof CorporateUser) {
                CorporateUser corporateUser = (CorporateUser) loggedInUser;
                map.put("corporate", corporateUser.getCorporate());
                userSetupFacadeLocal.setEntityNamePrefix("Corporate");
            } else {
                map.put("central", true);
            }
            List<User> users;
            try {
                users = userSetupFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(UserConverter.class.getName()).log(Level.SEVERE, null, ex);
                users = new ArrayList<>();
            }
            for (User user : users) {
                if (id.equals(user.getId())) {
                    return user;
                }
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value instanceof User) {
            User user = (User) value;
            if (user.getId() == null) {
                return "";
            }
            return user.getId().toString();
        } else {
            return "";
        }//To change body of generated methods, choose Tools | Templates.
    }

}
