/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "corporateSetupTableBean")
@SessionScoped
public class CorporateSetupTableBean extends AbstractTableBean<Corporate> {

    @EJB
    private CorporateSetupFacadeLocal corporateSetupFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;
    private Bank bankFilter;
    private List<Bank> banks;
    private String corpNameFilter = "";

    public CorporateSetupTableBean() {
        super(Corporate.class);
    }

    @Override
    protected CorporateSetupFacadeLocal getFacade() {
        return corporateSetupFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
//        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (isBankUser()) {
            properties.getAdditionalFilter().put("bank",getLoggedBank());
        }
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!corpNameFilter.isEmpty()) {
            properties.getAdditionalFilter().put("corporateName", corpNameFilter);
        }
         if (bankFilter!=null) {
            properties.getAdditionalFilter().put("bank", bankFilter);
        }
        return properties;
    }

    @Override
    public Corporate getTableRowData(String rowKey) {
        List<Corporate> wrappedData = (List<Corporate>) getLazyDataModel().getWrappedData();
        for (Corporate t : wrappedData) {
            if (rowKey.equals(String.valueOf(t.getId()))) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Corporate t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the bankFilter
     */
    public Bank getBankFilter() {
        return bankFilter;
    }

    /**
     * @param bankFilter the bankFilter to set
     */
    public void setBankFilter(Bank bankFilter) {
        this.bankFilter = bankFilter;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null) {
            try {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                banks = bankSetupFacadeLocal.findAll(map, true, new String[]{"bankName"});
            } catch (Exception ex) {
                Logger.getLogger(CorporateSetupTableBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the corpNameFilter
     */
    public String getCorpNameFilter() {
        return corpNameFilter;
    }

    /**
     * @param corpNameFilter the corpNameFilter to set
     */
    public void setCorpNameFilter(String corpNameFilter) {
        this.corpNameFilter = corpNameFilter;
    }

   

}
