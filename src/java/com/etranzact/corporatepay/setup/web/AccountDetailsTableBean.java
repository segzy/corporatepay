/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.setup.facade.AccountDetailsFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "accountDetailsTableBean")
@SessionScoped
public class AccountDetailsTableBean extends AbstractTableBean<CorporateCard> {

    @EJB
    private AccountDetailsFacadeLocal accountDetailsFacadeLocal;

    @EJB
    private CorporateSetupFacadeLocal corporateSetupFacadeLocal;
    private Corporate corporateFilter;
    private List<Corporate> corporates;
    private String cardAliasFilter = "";

    public AccountDetailsTableBean() {
        super(CorporateCard.class);
    }

    @Override
    protected AccountDetailsFacadeLocal getFacade() {
        return accountDetailsFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();

        if (isCorporateUser()) {
            properties.getAdditionalFilter().put("corporate", getLoggedCorporate());
        }
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (isBankUser()) {
            if (getCorporateFilter() != null) {
                properties.getAdditionalFilter().put("corporate", getCorporateFilter());
            }
            properties.getAdditionalFilter().put("corporate.bank", getLoggedBank());
        }

        if (!cardAliasFilter.isEmpty()) {
            properties.getAdditionalFilter().put("description", getCardAliasFilter());
        }
        return properties;
    }

    @Override
    public CorporateCard getTableRowData(String rowKey) {
        List<CorporateCard> wrappedData = (List<CorporateCard>) getLazyDataModel().getWrappedData();
        for (CorporateCard t : wrappedData) {
            if (rowKey.equals(String.valueOf(t.getId()))) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(CorporateCard t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the corporateFilter
     */
    public Corporate getCorporateFilter() {
        return corporateFilter;
    }

    /**
     * @param corporateFilter the corporateFilter to set
     */
    public void setCorporateFilter(Corporate corporateFilter) {
        this.corporateFilter = corporateFilter;
    }
    
        /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
               if(corporates==null && isBankUser()) {
            try {
                Map<String, Object> hashMap = new HashMap<>();
                log.log(Level.INFO, "{0}", getLoggedBank());
                hashMap.put("bank", getLoggedBank());
               corporates =  corporateSetupFacadeLocal.findAll(hashMap, true, new String[]{"corporateName"});
               log.log(Level.INFO, "corporates size:: {0}", corporates.size());
            } catch (Exception ex) {
                Logger.getLogger(AccountDetailsBean.class.getName()).log(Level.SEVERE, null, ex);
                corporates = new ArrayList<>();
                log.log(Level.INFO, "corporates size:: {0}", corporates.size());
            }
        }
        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }

    /**
     * @return the cardAliasFilter
     */
    public String getCardAliasFilter() {
        return cardAliasFilter;
    }

    /**
     * @param cardAliasFilter the cardAliasFilter to set
     */
    public void setCardAliasFilter(String cardAliasFilter) {
        this.cardAliasFilter = cardAliasFilter;
    }

}
