/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.Card;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateCard;
import com.etranzact.corporatepay.setup.facade.AccountDetailsFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "accountDetailsBean")
@SessionScoped
public class AccountDetailsBean extends AbstractBean<CorporateCard> {

    private Bank bank;
    private Corporate corporate;
    private List<Bank> banks;
    private List<Corporate> corporates;

    @EJB
    private AccountDetailsFacadeLocal accountDetailsFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateSetupFacadeLocal;

    public void onDialogClose(CloseEvent event) {
        this.log.info("dialog close event.....");
        reset();
    }

    @Override
    protected AccountDetailsFacadeLocal getFacade() {
        return accountDetailsFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void reset() {
        super.reset();
        bank = null;
        corporate = null;
    }

    public AccountDetailsBean() {
        super(CorporateCard.class);
    }

    public void doAccountBalance() {
        FacesMessage m = new FacesMessage();
        try {
            String reference = "08" + System.currentTimeMillis();
            log.info("reference " + reference);
            log.info("cardNumber " + selectedItem.getCardNumber());
            log.info("expYear " + expYear);
            log.info("expMonth " + expMonth);
            Double balance = balanceCheck("08" + System.currentTimeMillis(), selectedItem.getCardNumber(), pin, expYear, expMonth);
            String balanceStr = "";
            if (balance != null) {
                balanceStr = AppUtil.formatAsAmount(balance.toString());
            }
//            String balanceStr = "NGN 0";
            if (balance == null) {
                balanceStr = "Unavailable Balance or Incorrect details";
            } else {
                balanceStr = "NGN " + balanceStr;
            }
            selectedItem.setAccountBalance(balanceStr);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Current Account Balance (" + balanceStr + ")");
            FacesContext.getCurrentInstance().addMessage(null, m);
            expMonth = "";
            expYear = "";
            pin = "";
        } catch (Exception ex) {
            Logger.getLogger(AccountDetailsBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
                expMonth = "";
            expYear = "";
            pin = "";
             reset();
        } 
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {

            corporate = corporate == null ? getLoggedCorporate() : corporate;
            if (!corporate.getDisAllowOtherAccount()) {
                bank = bank == null ? getLoggedBank() : bank;
                if (bank.getId().equals("700")) {
                    if (!(selectedItem.getCardNumber().substring(0, 3).equals(bank.getId())
                            || selectedItem.getCardNumber().startsWith("7"))) {
                        throw new Exception("Card is not valid");
                    }
                } else {
                    if (!selectedItem.getCardNumber().substring(0, 3).equals(bank.getId())) {
                        throw new Exception("Card is not valid");
                    }
                }
            }
            selectedItem.setCorporate(corporate);
            if (selectedItem.getId() == null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                Map<String, Object> map = new HashMap<>();
                log.log(Level.INFO, "corporate {0}", corporate);
                map.put("corporate", corporate);
                map.put("c.cardNumber='" + selectedItem.getCardNumber() + "' or c.description='" + selectedItem.getDescription() + "'", null);
//                accountDetailsFacadeLocal.setEntityNamePrefix("Corporate");
                List<CorporateCard> cards = accountDetailsFacadeLocal.findAll(map, false);
                if (!cards.isEmpty()) {
                    throw new Exception("Card Number or Alias already exist");
                }
                accountDetailsFacadeLocal.create(selectedItem);
            } else {
                if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED)
                        || selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED)
                        || selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {
                    throw new Exception("Cannot Save Account Details: Account Details currently undergoing an approval process");
                }
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                accountDetailsFacadeLocal.edit(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Account Details Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
            selectedItem = new CorporateCard();
            setBank(null);
            setCorporate(null);
        } catch (Exception ex) {
            Logger.getLogger(AccountDetailsBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        Object obj = selectEvent.getObject();
        if (getLoggedCorporate() != null) {
            try {
                //bank = getLoggedCorporate().getBank();
                bank = bankSetupFacadeLocal.find(selectedItem.getCardNumber().substring(0, 3));
            } catch (Exception ex) {
                Logger.getLogger(AccountDetailsBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (obj instanceof CorporateCard) {
            selectedItem = (CorporateCard) obj;
            corporate = selectedItem.getCorporate();
        }
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null && isCorporateUser()) {
            try {
                banks = bankSetupFacadeLocal.findAll(new HashMap<String, Object>(), true, new String[]{"bankName"});
            } catch (Exception ex) {
                Logger.getLogger(AccountDetailsBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = new ArrayList<>();
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        if (corporates == null && isBankUser()) {
            try {
                Map<String, Object> hashMap = new HashMap<>();
                hashMap.put("bank", getLoggedBank());
                corporates = corporateSetupFacadeLocal.findAll(hashMap, true, new String[]{"corporateName"});
            } catch (Exception ex) {
                Logger.getLogger(AccountDetailsBean.class.getName()).log(Level.SEVERE, null, ex);
                corporates = new ArrayList<>();
            }
        }
        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }

}
