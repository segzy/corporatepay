/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.setup.facade.TransactionTypeFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "transactionTypeTableBean")
@SessionScoped
public class TransactionTypeTableBean extends AbstractTableBean<TransactionType> {

    @EJB
    private TransactionTypeFacadeLocal transactionTypeFacadeLocal;

    public TransactionTypeTableBean() {
        super(TransactionType.class);
    }

    @Override
    protected TransactionTypeFacadeLocal getFacade() {
        return transactionTypeFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties tableProperties = new TableProperties();
        tableProperties.setFlagStatuses(new String[0]);
        return tableProperties;
    }

      @Override
    public TransactionType getTableRowData(String rowKey) {
        List<TransactionType> wrappedData = (List<TransactionType>) getLazyDataModel().getWrappedData();
        for (TransactionType t : wrappedData) {
            if (rowKey.equalsIgnoreCase(t.getId())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(TransactionType t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }


    
}
