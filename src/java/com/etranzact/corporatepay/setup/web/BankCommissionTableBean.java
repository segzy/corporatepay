/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.setup.facade.CommissionFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "bankCommissionTableBean")
@SessionScoped
public class BankCommissionTableBean extends AbstractTableBean<Commission> {

    @EJB
    private CommissionFacadeLocal commissionFacadeLocal;
    private String structureNameFilter = "";

    public BankCommissionTableBean() {
        super(Commission.class);
    }

    @Override
    protected CommissionFacadeLocal getFacade() {
        return commissionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!structureNameFilter.isEmpty()) {
            properties.getAdditionalFilter().put("structureName", getStructureNameFilter());
        }
        return properties;
    }

    @Override
    public Commission getTableRowData(String rowKey) {
        List<Commission> wrappedData = (List<Commission>) getLazyDataModel().getWrappedData();
        for (Commission t : wrappedData) {
            if (rowKey.equals(String.valueOf(t.getId()))) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Commission t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the structureNameFilter
     */
    public String getStructureNameFilter() {
        return structureNameFilter;
    }

    /**
     * @param structureNameFilter the structureNameFilter to set
     */
    public void setStructureNameFilter(String structureNameFilter) {
        this.structureNameFilter = structureNameFilter;
    }

   

}
