/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.facade.UserFacadeLocal;
import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.BankApprovalGroup;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateApprovalGroup;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.ApprovalGroupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.print.attribute.HashAttributeSet;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "approvalGroupBean")
@SessionScoped
public class ApprovalGroupBean extends AbstractBean<ApprovalGroup> {

    private boolean showUsers;

    public ApprovalGroupBean() {
        super(ApprovalGroup.class);
    }
    private List<User> users;
    private User user;

    @EJB
    private ApprovalGroupFacadeLocal approvalGroupFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;

    @Override
    protected ApprovalGroupFacadeLocal getFacade() {
        return approvalGroupFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("savingg...........");
            selectedItem.setFlagStatus(AppConstants.MODIFIED);
            approvalGroupFacadeLocal.edit(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Approval Group Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
                selectedItem = null;
            users = new ArrayList<>();
            selectedItem = prepObject(selectedItem);
        } catch (Exception ex) {
            Logger.getLogger(ApprovalGroupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } 

    }

    public void addUser() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("new Group...........");
            selectedItem.getUsers().add(user);
            users.remove(user);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("New Memeber Added");
            FacesContext.getCurrentInstance().addMessage(null, m);
            
        } catch (Exception ex) {
            Logger.getLogger(ApprovalGroupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            user = null;
        }

    }
    
    public void removeUser(User u) {
        this.user  = u; 
              FacesMessage m = new FacesMessage();
        try {
            log.info("new Group...........");
            selectedItem.getUsers().remove(user);
            users.add(user);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("New Member Removed");
            FacesContext.getCurrentInstance().addMessage(null, m);
            
        } catch (Exception ex) {
            Logger.getLogger(ApprovalGroupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            user = null;
        } 
    }

    public void newGroup() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("new Group...........");
            String alias = selectedItem.getGroupAlias();
            selectedItem = null;
            selectedItem = prepObject(selectedItem);
            selectedItem.setGroupAlias(alias);
            selectedItem.setFlagStatus(AppConstants.CREATED);
            approvalGroupFacadeLocal.create(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("New Group Added Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(ApprovalGroupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new ApprovalGroup();
        }

    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        selectedItem = (ApprovalGroup) selectEvent.getObject();
         
        try {
            if(!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED) || 
                    selectedItem.getFlagStatus().equals(AppConstants.CREATED) ||  
                     selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED) 
                    || selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {
                throw new Exception("Cannot modify approval group, currently undergoing approval");
            }
            users = findAllUsers();
            selectedItem = approvalGroupFacadeLocal.find(selectedItem.getId());
            users.removeAll(selectedItem.getUsers());
            setShowUsers(true); 
        } catch (Exception ex) {
            Logger.getLogger(ApprovalRouteTableBean.class.getName()).log(Level.SEVERE, null, ex);
            selectedItem.setUsers(new HashSet<User>());
                     FacesMessage m = new FacesMessage();
                m.setSeverity(FacesMessage.SEVERITY_ERROR);
                m.setSummary(ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, m);
                  setShowUsers(false); 
        }
    }

    @Override
    public ApprovalGroup getSelectedItem() {
        super.getSelectedItem();
        selectedItem = prepObject(selectedItem);
        return selectedItem;
    }

    private ApprovalGroup prepObject(ApprovalGroup approvalGroup) {
        if (approvalGroup == null) {
//            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            if (isBankUser()) {
                approvalGroup = new BankApprovalGroup();
                ((BankApprovalGroup) approvalGroup).setBank(getLoggedBank());
            } else if (isCorporateUser()) {
                approvalGroup = new CorporateApprovalGroup();
                ((CorporateApprovalGroup) approvalGroup).setCorporate(getLoggedCorporate());
            } else {
                approvalGroup = new ApprovalGroup();
            }
        }
        return approvalGroup;
    }

    /**
     * @return the showUsers
     */
    public boolean isShowUsers() {
        return showUsers;
    }

    /**
     * @param showUsers the showUsers to set
     */
    public void setShowUsers(boolean showUsers) {
        this.showUsers = showUsers;
    }

    /**
     * @return the users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
    
    private List<User> findAllUsers() {
                   try {
//                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//                User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
                Map<String, Object> map = new HashMap<>();
                if (isBankUser()) {
//                    BankUser bankUser = (BankUser) loggedInUser;
                    map.put("bank", getLoggedBank());
                    userSetupFacadeLocal.setEntityNamePrefix("Bank");
                } else if (isCorporateUser()) {
//                    CorporateUser corporateUser = (CorporateUser) loggedInUser;
                    map.put("corporate", getLoggedCorporate());
                    userSetupFacadeLocal.setEntityNamePrefix("Corporate");
                } else {
                    map.put("central", true); 
                }
                return  userSetupFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(ApprovalGroupBean.class.getName()).log(Level.SEVERE, null, ex);
                return new ArrayList<>();
            }
        
    }

}
