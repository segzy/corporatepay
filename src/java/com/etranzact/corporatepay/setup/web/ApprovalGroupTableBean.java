/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.ApprovalGroup;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.ApprovalGroupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "approvalGroupTableBean")
@SessionScoped
public class ApprovalGroupTableBean extends AbstractTableBean<ApprovalGroup> {

    @EJB
    private ApprovalGroupFacadeLocal approvalGroupFacadeLocal;

    public ApprovalGroupTableBean() {
        super(ApprovalGroup.class);
    }

    @Override
    protected ApprovalGroupFacadeLocal getFacade() {
        return approvalGroupFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties tableProperties = new TableProperties();
//        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        if (isBankUser()) {
//            BankUser bankUser = (BankUser) loggedInUser;
            tableProperties.getAdditionalFilter().put("bank", getLoggedBank());
            approvalGroupFacadeLocal.setEntityNamePrefix("Bank");
        } else if (isCorporateUser()) {
//            CorporateUser corporateUser = (CorporateUser) loggedInUser;
            tableProperties.getAdditionalFilter().put("corporate",getLoggedCorporate());
            approvalGroupFacadeLocal.setEntityNamePrefix("Corporate");
        } 
        return tableProperties;
    }

    @Override
    public ApprovalGroup getTableRowData(String rowKey) {
        List<ApprovalGroup> wrappedData = (List<ApprovalGroup>) getLazyDataModel().getWrappedData();
        for (ApprovalGroup t : wrappedData) {
            if (Long.parseLong(rowKey) == t.getId()) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(ApprovalGroup t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

}
