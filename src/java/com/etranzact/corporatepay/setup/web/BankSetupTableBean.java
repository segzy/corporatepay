/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "bankSetupTableBean")
@SessionScoped
public class BankSetupTableBean extends AbstractTableBean<Bank> {

    @EJB
    private BankSetupFacadeLocal bankFacadeLocal;
    private String bankNameFilter="";

    public BankSetupTableBean() {
        super(Bank.class);
    }

    @Override
    protected BankSetupFacadeLocal getFacade() {
        return bankFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!bankNameFilter.isEmpty()) {
            properties.getAdditionalFilter().put("bankName", bankNameFilter);
        }
        if(isBankUser()) {
          properties.getAdditionalFilter().put("mfbBank=true",null);  
        }
        
        return properties;
    }

    @Override
    public Bank getTableRowData(String rowKey) {
        List<Bank> wrappedData = (List<Bank>) getLazyDataModel().getWrappedData();
        for (Bank t : wrappedData) {
            if (rowKey.equals(t.getId())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Bank t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the bankNameFilter
     */
    public String getBankNameFilter() {
        return bankNameFilter;
    }

    /**
     * @param bankNameFilter the bankNameFilter to set
     */
    public void setBankNameFilter(String bankNameFilter) {
        this.bankNameFilter = bankNameFilter;
    }

}
