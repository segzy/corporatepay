/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.CommissionFee;
import com.etranzact.corporatepay.setup.facade.CommissionFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "bankCommissionBean")
@SessionScoped
public class BankCommissionBean extends AbstractBean<Commission> {

    public BankCommissionBean() {
        super(Commission.class);
    }

    @EJB
    private CommissionFacadeLocal commissionFacadeLocal;
    private CommissionFee commissionFee = new CommissionFee();
    private long tempId = 0l;

    @Override
    protected CommissionFacadeLocal getFacade() {
        return commissionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void addGraduatedStructureFee() {
        FacesMessage m = new FacesMessage();
        try {
            CommissionFee cf = commissionFee;
            log.info("addinggggg..........." + tempId);
            tempId = tempId - 1;
            log.info("adding..........." + tempId);
            cf.setId(tempId);
            selectedItem.getCommissionFees().add(cf);

            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Commission Added");
            FacesContext.getCurrentInstance().addMessage(null, m);

        } catch (Exception ex) {
            Logger.getLogger(BankCommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            commissionFee = new CommissionFee();
            commissionFee.setMinRange(getMaxRangeSetUp(selectedItem.getCommissionFees()));
        }
    }

    public void saveGraduatedStructureName() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("savingg...........");
            BigDecimal perc = selectedItem.getEtzPercent();
            perc.add(selectedItem.getThirdPartyPercent1() == null ? BigDecimal.ZERO : selectedItem.getThirdPartyPercent1());
            perc.add(selectedItem.getThirdPartyPercent2() == null ? BigDecimal.ZERO : selectedItem.getThirdPartyPercent2());
            if(perc.compareTo(new BigDecimal("100"))>=0) {
                throw new Exception("Charge Split is invalid: please check the values");
            }
            if (selectedItem.getId() == null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                selectedItem.setBank(getLoggedBank());
                commissionFacadeLocal.save(selectedItem);
                m.setSummary("Structure Saved Successfully but still requires the addition of graduated fees");
            } else {
                if (selectedItem.getFlagStatus().equals(AppConstants.MODIFIED)) {
                    throw new Exception("Cannot Save Commission, It is currently in approval process");
                }
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                commissionFacadeLocal.edit(selectedItem);
                m.setSummary("Commission Saved Successfully and Sent for Approval");
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);

            FacesContext.getCurrentInstance().addMessage(null, m);

        } catch (Exception ex) {
            Logger.getLogger(BankCommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new Commission();
            commissionFee = new CommissionFee();
        }
    }

    public void onDialogClose(CloseEvent event) {
        this.selectedItem = new Commission();
        this.commissionFee = new CommissionFee();
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("savingg...........");
            selectedItem.setFlagStatus(AppConstants.MODIFIED);
            commissionFacadeLocal.edit(selectedItem);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Commission Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);

        } catch (Exception ex) {
            Logger.getLogger(BankCommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new BankCommission();
        }

    }

    public void selectedCommission(Commission commission) {
        log.info("commmmmmmmmmmmmmmmmmm iiii  " + commission);
        selectedItem = commission;
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        commissionFee.setMinRange(getMaxRangeSetUp(selectedItem.getCommissionFees()));
    }

    /**
     * @return the commissionFee
     */
    public CommissionFee getCommissionFee() {
        return commissionFee;
    }

    /**
     * @param commissionFee the commissionFee to set
     */
    public void setCommissionFee(CommissionFee commissionFee) {
        log.log(Level.INFO, "setting commissionFee: {0}", commissionFee);
        log.log(Level.INFO, "selectedItem: {0}", selectedItem);
        if (commissionFee == null) {
            commissionFee = new CommissionFee();
            commissionFee.setMinRange(getMaxRangeSetUp(selectedItem.getCommissionFees()));
        }
        this.commissionFee = commissionFee;
    }

    private String getMaxRangeSetUp(Set<CommissionFee> commissionFees) {
        if (commissionFees.isEmpty()) {
            return "0.0";
        } else {
//            Comparator<CommissionFee> comparable = new Comparator<CommissionFee>() {
//
//                @Override
//                public int compare(CommissionFee o1, CommissionFee o2) {
//                    
//                    int val= o2.getId().intValue() - o1.getId().intValue();
//                    return val;
//                }
//            };
//            List<CommissionFee> comFees = new ArrayList<>();
//            comFees.addAll(commissionFees);
//            Collections.sort(comFees, comparable);
            int i = 0;
            BigDecimal highestMaxRange = null;
            String toString = null;
            for (CommissionFee cFee : commissionFees) {
                if (i == 0) {
                    highestMaxRange = new BigDecimal(cFee.getMaxRange());
                } else {
                    if (highestMaxRange.compareTo(new BigDecimal(cFee.getMaxRange())) < 0) {
                        highestMaxRange = new BigDecimal(cFee.getMaxRange());
                    }
                }
                toString = highestMaxRange.add(new BigDecimal("0.01")).toString();
                i++;
            }
            return toString == null ? "0.0" : toString;

        }
    }

}
