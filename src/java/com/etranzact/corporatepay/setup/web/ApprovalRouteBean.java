/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.ApprovalRoute;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.Approver;
import com.etranzact.corporatepay.model.BankApprovalRoute;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateApprovalRoute;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.model.Workflow;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.ApproverFacadeLocal;
import com.etranzact.corporatepay.setup.facade.WorkflowFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "approvalRouteBean")
@SessionScoped
public class ApprovalRouteBean extends AbstractBean<ApprovalRoute> {

    private boolean showAppRoute;
    private List<ApprovalRoute> pathsSetUp;
    

    public ApprovalRouteBean() {
        super(ApprovalRoute.class);
    }

    private ApprovalType approvalType;
    @EJB
    private ApproverFacadeLocal approverFacadeLocal;
    private List<Approver> approvers;

    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;

    @EJB
    private WorkflowFacadeLocal workflowFacadeLocal;

    private long tempId = -1l;
    private boolean newApprovalRoute;

    @Override
    protected ApprovalRouteFacadeLocal getFacade() {
        return approvalRouteFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public List<String> completeAlias(String query) {
        try {
            Map map = new HashMap();
            if(isBankUser() || isCorporateUser()) {
                 map.put("corporate", getLoggedCorporate());
            } else {
                map.put("central", true);
            }
            List<ApprovalRoute> approvalRoutes = approvalRouteFacadeLocal.findAll(map, true);
            List<String> results = new ArrayList<>();
            for (ApprovalRoute approvalRoute : approvalRoutes) {
                if (approvalRoute.getRouteAlias() == null || approvalRoute.getRouteAlias().isEmpty()) {
                    continue;
                }
                if (results.contains(approvalRoute.getRouteAlias())) {
                    continue;
                }
                results.add(approvalRoute.getRouteAlias());
            }
            return results;
        } catch (Exception ex) {
            Logger.getLogger(ApprovalRouteBean.class.getName()).log(Level.SEVERE, null, ex);
            return new ArrayList<>();
        }
    }

    public void saveAllModifications() {
        FacesMessage m = new FacesMessage();
        try {
            boolean modificationValid = true;
            for (ApprovalRoute route : pathsSetUp) {
                if (route.getFlagStatus().equals(AppConstants.CREATED)) {
                    modificationValid = false;
                    break;
                }
                if (!modificationValid) {
                    throw new Exception("You have not added a new approval path");
                }
            }
            log.log(Level.INFO, "approvalType:::::: {0}", approvalType);
            workflowFacadeLocal.saveAllRoute(pathsSetUp, pathsSetUp.get(0).getApprovalType());
            setPathsSetUp(null);
            newApprovalRoute = false;
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Approval Route Saved successfully and Sent for Approval ");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(ApprovalRouteBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            approvalType = selectedItem.getApprovalType();
            if (!pathsSetUp.isEmpty()  && !isNewApprovalRoute() &&
                    !approvalType.getDescription().equals(Workflow.class.getSimpleName())) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                selectedItem.setId(tempId);
                for(ApprovalRoute approvalRoute: getPathsSetUp()) {
                    if(approvalRoute.getLevel().equals(selectedItem.getLevel()) || 
                         approvalRoute.getApprover().getId().equals(selectedItem.getApprover().getId())   ) {
                        throw new Exception("Approver or Level already exist");
                    }
                }
                pathsSetUp.add(selectedItem);
                selectedItem = null;
                tempId -= 1;
                selectedItem = prepObject(selectedItem);
                selectedItem.setApprovalType(approvalType);
                m.setSeverity(FacesMessage.SEVERITY_INFO);
                m.setSummary("Approval Path Added Successfully");
                FacesContext.getCurrentInstance().addMessage(null, m);
                return;
            }
            log.info("savingg...........");
            log.log(Level.INFO, "saving for ...........{0}", selectedItem.getApprovalType());
            log.log(Level.INFO, "saving for ...........{0}", selectedItem.getApprover());
            approvalType = selectedItem.getApprovalType();
            selectedItem.setFlagStatus(AppConstants.APPROVED);
            ApprovalRoute create = approvalRouteFacadeLocal.create(selectedItem);
            pathsSetUp.add(create);
            selectedItem = null;
            selectedItem = prepObject(selectedItem);
            selectedItem.setApprovalType(approvalType);
            approvers = approverFacadeLocal.findApproversExcluding();
            setNewApprovalRoute(true);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Approval Path Saved Successfully");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(ApprovalRouteBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        Object obj = selectEvent.getObject();
        showAppRoute = true;
        if (obj instanceof ApprovalType) {
            approvalType = (ApprovalType) selectEvent.getObject();
            try {
                approvers = approverFacadeLocal.findApproversExcluding();
            } catch (Exception ex) {
                Logger.getLogger(ApprovalRouteTableBean.class.getName()).log(Level.SEVERE, null, ex);
                approvers = new ArrayList<>();
            }
            
            selectedItem = null;
            tempId = -1l;
            pathsSetUp = pathsSetUp();
        } else {
            try {
                approvers = approverFacadeLocal.findApprovers();
                super.onRowSelect(selectEvent);
            } catch (Exception ex) {
                Logger.getLogger(ApprovalRouteTableBean.class.getName()).log(Level.SEVERE, null, ex);
                approvers = new ArrayList<>();
            }
        }
        selectedItem = prepObject(selectedItem);
        selectedItem.setApprovalType(approvalType);
    }

    private List<ApprovalRoute> pathsSetUp() {
        try {
            Map<String, Object> filter = new HashMap<>();
            if (isBankUser()) {
                filter.put("bank", getLoggedBank());
            } else if (isCorporateUser()) {
                filter.put("corporate", getLoggedCorporate());
            } else {
                filter.put("central", Boolean.TRUE);
            }
            filter.put("approvalType", approvalType);
            String[] flagStatuses = new String[]{AppConstants.APPROVED,AppConstants.CREATED};
            pathsSetUp = approvalRouteFacadeLocal.findAll(filter, true, flagStatuses, new String[0]);
            setNewApprovalRoute(pathsSetUp.isEmpty());
            return pathsSetUp;
        } catch (Exception ex) {
            Logger.getLogger(ApprovalRouteBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public ApprovalRoute getSelectedItem() {
        selectedItem = prepObject(selectedItem);
        return selectedItem;
    }

    /**
     * @return the approvalType
     */
    public ApprovalType getApprovalType() {
        return approvalType;
    }

    /**
     * @param approvalType the approvalType to set
     */
    public void setApprovalType(ApprovalType approvalType) {
        this.approvalType = approvalType;
    }

    /**
     * @return the approvers
     */
    public List<Approver> getApprovers() {
        return approvers;
    }

    /**
     * @param approvers the approvers to set
     */
    public void setApprovers(List<Approver> approvers) {
        this.approvers = approvers;
    }

    /**
     * @return the showAppRoute
     */
    public boolean isShowAppRoute() {
        return showAppRoute;
    }

    /**
     * @param showAppRoute the showAppRoute to set
     */
    public void setShowAppRoute(boolean showAppRoute) {
        this.showAppRoute = showAppRoute;
    }

    private ApprovalRoute prepObject(ApprovalRoute approvalRoute) {
        if (approvalRoute == null) {
//            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            if (isBankUser()) {
                approvalRoute = new BankApprovalRoute();
                ((BankApprovalRoute) approvalRoute).setBank(getLoggedBank());
            } else if (isCorporateUser()) {
                approvalRoute = new CorporateApprovalRoute();
                ((CorporateApprovalRoute) approvalRoute).setCorporate(getLoggedCorporate());
            } else {
                approvalRoute = new ApprovalRoute();
            }
        }
        return approvalRoute;
    }

    /**
     * @return the pathsSetUp
     */
    public List<ApprovalRoute> getPathsSetUp() {
        return pathsSetUp;
    }

    /**
     * @param pathsSetUp the pathsSetUp to set
     */
    public void setPathsSetUp(List<ApprovalRoute> pathsSetUp) {
        this.pathsSetUp = pathsSetUp;
    }

    /**
     * @return the newApprovalRoute
     */
    public boolean isNewApprovalRoute() {
        return newApprovalRoute;
    }

    /**
     * @param newApprovalRoute the newApprovalRoute to set
     */
    public void setNewApprovalRoute(boolean newApprovalRoute) {
        this.newApprovalRoute = newApprovalRoute;
    }

    
}
