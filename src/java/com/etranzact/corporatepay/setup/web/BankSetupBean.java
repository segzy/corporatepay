/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankHoldingAccount;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupWorkflowFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectBoolean;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "bankSetupBean")
@SessionScoped
public class BankSetupBean extends AbstractBean<Bank> {

    @EJB
    private BankSetupFacadeLocal bankSetUpFacadeLocal;
    private List<Bank> banks;

    @Override
    protected BankSetupFacadeLocal getFacade() {
        return getBankSetUpFacadeLocal(); //To change body of generated methods, choose Tools | Templates.
    }

    public BankSetupBean() {
        super(Bank.class);
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        if (selectedItem.getMfbBank()) {
            try {
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("mfbBank=false or mfbBank is null", null);
                banks = bankSetUpFacadeLocal.findAll(new HashMap<String, Object>(), false, new String[]{"bankName"});
            } catch (Exception ex) {
                Logger.getLogger(BankSetupBean.class.getName()).log(Level.SEVERE, null, ex);
                banks = null;
            }
        } else {
            banks = null;
        }
        log.info(pin);
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            if(isBankUser()) {
                selectedItem.setMfbBank(Boolean.TRUE);
                selectedItem.setId(AppUtil.randomPassword(6, "1234567890"));
            }
            selectedItem.setBankName(selectedItem.getBankName().toUpperCase());
            selectedItem.setTimeOutPreference(15);
            if (selectedItem.getFlagStatus() == null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                Map<String, Object> map = new HashMap<>();
                map.put("id", selectedItem.getId());
                List<Bank> banks = getBankSetUpFacadeLocal().findAll(map, false);
                if (!banks.isEmpty()) {
                    throw new Exception("bank code already exist");
                }
                getBankSetUpFacadeLocal().create(selectedItem);
            } else {
                if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED)
                        || selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED)
                        || selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {
                    throw new Exception("Cannot Save Bank: Bank currently undergoing an approval process");
                }
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                getBankSetUpFacadeLocal().edit(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Bank Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(BankSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new Bank();
        }

    }

    /**
     * @return the bankSetUpFacadeLocal
     */
    public BankSetupFacadeLocal getBankSetUpFacadeLocal() {
        return bankSetUpFacadeLocal;
    }

    /**
     * @param bankSetUpFacadeLocal the bankSetUpFacadeLocal to set
     */
    public void setBankSetUpFacadeLocal(BankSetupFacadeLocal bankSetUpFacadeLocal) {
        this.bankSetUpFacadeLocal = bankSetUpFacadeLocal;
    }

    public void handleMFBChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        Object input = ((UISelectBoolean) e.getComponent()).getValue();
        if (Boolean.valueOf(String.valueOf(input))) {
            try {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("mfbBank=false or mfbBank is null", null);
                banks = bankSetUpFacadeLocal.findAll(new HashMap<String, Object>(), false, new String[]{"bankName"});
            } catch (Exception ex) {
                Logger.getLogger(BankSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            banks = null;
        }
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        if (banks == null && isBankUser()) {
            try {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("mfbBank=false or mfbBank is null", null);
                banks = bankSetUpFacadeLocal.findAll(new HashMap<String, Object>(), false, new String[]{"bankName"});
            } catch (Exception ex) {
                Logger.getLogger(BankSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

}
