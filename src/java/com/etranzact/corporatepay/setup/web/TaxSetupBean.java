/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Account;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankAccount;
import com.etranzact.corporatepay.model.Tax;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.TaxOfficeSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.setup.web.converter.UserConverter;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "taxSetupBean")
@SessionScoped
public class TaxSetupBean extends AbstractBean<Tax> {

    @EJB
    private TaxOfficeSetupFacadeLocal taxSetUpFacadeLocal;
    @EJB
    private UserSetupFacadeLocal userSetupFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankSetupFacadeLocal;
    private BankAccount bankAccount;
    private List<Bank> banks;
    private long mirrorId;
    private List<User> users;
    private boolean defaultAccount;

    @Override
    protected TaxOfficeSetupFacadeLocal getFacade() {
        return taxSetUpFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public TaxSetupBean() {
        super(Tax.class);
    }

    public void save(boolean clear) {
        FacesMessage m = new FacesMessage();
        try {
            selectedItem.setAuthName(selectedItem.getAuthName().toUpperCase());
            if (selectedItem.getFlagStatus() == null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                Map<String, Object> map = new HashMap<>();
                map.put("authCode", selectedItem.getAuthCode());
                map.put("corporate", getLoggedCorporate());
                List<Tax> taxs = getFacade().findAll(map, false);
                if (!taxs.isEmpty()) {
                    throw new Exception("Tax code already exist");
                }
                selectedItem.setCorporate(getLoggedCorporate());
                getFacade().create(selectedItem);
            } else {
                if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED) || selectedItem.
                        getFlagStatus().equals(AppConstants.MODIFIED_REJECTED) 
                        || selectedItem.getFlagStatus().equals(AppConstants.CREATED)
                        || selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {
                    throw new Exception("Cannot Save Tax: Tax currently undergoing an approval process");
                }
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                getFacade().edit(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Tax Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
            if (clear) {
                selectedItem = new Tax();
            } else {
                selectedItem = taxSetUpFacadeLocal.find(selectedItem.getAuthCode());
            }
        } catch (Exception ex) {
            Logger.getLogger(TaxSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {

        }

    }

    public void doAccount(Tax tax) {
        try {
            selectedItem = tax;
            selectedItem = taxSetUpFacadeLocal.find(selectedItem.getAuthCode());
            for (BankAccount account : selectedItem.getAccounts()) {
               if(account.equals(selectedItem.getDefaultAccount())) {
                   account.setDefaultBank(true);
               }
            }
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            String path = "/setup/taxoffice/account";
            bankAccount = new BankAccount();
            bankAccount.setAccountName(selectedItem.getAuthName());
            if (banks == null) {
                banks = bankSetupFacadeLocal.findAll(new HashMap<String, Object>(), true, new String[]{"bankName"});
            }
            externalContext.redirect(externalContext.getRequestContextPath() + path);
        } catch (Exception ex) {
            Logger.getLogger(TaxSetupBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void defaultBank(BankAccount bankAccount) {
        FacesMessage m = new FacesMessage();
        try {
            selectedItem.setDefaultAccount(bankAccount);
           selectedItem= taxSetUpFacadeLocal.setDefaultAccount(selectedItem);
              for (BankAccount account : selectedItem.getAccounts()) {
               if(account.equals(selectedItem.getDefaultAccount())) {
                   account.setDefaultBank(true);
               }
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Account set as default");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(TaxSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void backTaxSetup() {
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            String path = "/setup/taxoffice";
            externalContext.redirect(externalContext.getRequestContextPath() + path);
        } catch (Exception ex) {
            Logger.getLogger(TaxSetupBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addAccount() {
        FacesMessage m = new FacesMessage();
        try {
            if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED)
                    || selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED) || selectedItem.getFlagStatus().equals(AppConstants.CREATED)) ) {
                throw new Exception("Cannot Add Account: Tax currently undergoing an approval process");
            }
            boolean accountModified = false;
            if (defaultAccount) {
                selectedItem.setDefaultAccount(bankAccount);
                  bankAccount.setDefaultBank(defaultAccount);
            }
            if (bankAccount.getId() == null) {
                mirrorId = mirrorId - 1;
                bankAccount.setId(mirrorId);
                 bankAccount.setDefaultBank(true);
                selectedItem.getAccounts().add(bankAccount);
            } else {
                boolean contains = selectedItem.getAccounts().contains(bankAccount);
                if (contains) {
                    selectedItem.getAccounts().remove(bankAccount);
                    selectedItem.getAccounts().add(bankAccount);
                }
                accountModified = true;
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(accountModified ? "Account Modified" : "Account Added");
            bankAccount = new BankAccount();
            bankAccount.setAccountName(selectedItem.getAuthName());
            FacesContext.getCurrentInstance().addMessage(null, m);
            defaultAccount = false;
        } catch (Exception ex) {
            Logger.getLogger(BankCommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    public void removeAccount() {
        FacesMessage m = new FacesMessage();
        try {
            selectedItem.getAccounts().remove(bankAccount);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Account Removed");
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(BankCommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    /**
     * @return the bankAccount
     */
    public BankAccount getBankAccount() {
        return bankAccount;
    }

    /**
     * @param bankAccount the bankAccount to set
     */
    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * @return the users
     */
    public List<User> getUsers() {
        if (users == null) {
            Map<String, Object> map = new HashMap<>();
            map.put("corporate", getLoggedCorporate());
            map.put("u.role.roleName like '%tax%'", null);
            userSetupFacadeLocal.setEntityNamePrefix("Corporate");
            try {
                users = userSetupFacadeLocal.findAll(map, true);
            } catch (Exception ex) {
                Logger.getLogger(UserConverter.class.getName()).log(Level.SEVERE, null, ex);
                users = new ArrayList<>();
            }
        }
        return users;
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        Object obj = selectEvent.getObject();
        if (obj instanceof Tax) {
            selectedItem = (Tax) obj;
            log.log(Level.INFO, "tax selected ........{0}", selectedItem);
        } else if (obj instanceof Account) {
            bankAccount = (BankAccount) obj;
        }
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * @return the banks
     */
    public List<Bank> getBanks() {
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the defaultAccount
     */
    public boolean isDefaultAccount() {
        return defaultAccount;
    }

    /**
     * @param defaultAccount the defaultAccount to set
     */
    public void setDefaultAccount(boolean defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

}
