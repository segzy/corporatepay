/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.PFA;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.PFASetupFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "pfaSetupTableBean")
@SessionScoped
public class PFASetupTableBean extends AbstractTableBean<PFA> {

    @EJB
    private PFASetupFacadeLocal pFASetupFacadeLocal;
    private String pfaNameFilter="";

    public PFASetupTableBean() {
        super(PFA.class);
    }

    @Override
    protected PFASetupFacadeLocal getFacade() {
        return pFASetupFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!pfaNameFilter.isEmpty()) {
            properties.getAdditionalFilter().put("authName", getPfaNameFilter());
        }
        return properties;
    }

    @Override
    public PFA getTableRowData(String rowKey) {
        List<PFA> wrappedData = (List<PFA>) getLazyDataModel().getWrappedData();
        for (PFA t : wrappedData) {
            if (rowKey.equals(t.getAuthCode())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(PFA t) {
        return t.getAuthCode(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the pfaNameFilter
     */
    public String getPfaNameFilter() {
        return pfaNameFilter;
    }

    /**
     * @param pfaNameFilter the pfaNameFilter to set
     */
    public void setPfaNameFilter(String pfaNameFilter) {
        this.pfaNameFilter = pfaNameFilter;
    }


}
