/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.CommissionFee;
import com.etranzact.corporatepay.model.MenuItem;
import com.etranzact.corporatepay.model.TransactionType;
import com.etranzact.corporatepay.setup.facade.CommissionFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "commissionBean")
@SessionScoped
public class CommissionBean extends AbstractBean<Commission> {

    private boolean showFees;
    private long mirrorId;

    public CommissionBean() {
        super(Commission.class);
    }
    private CommissionFee commissionFee;
    private TransactionType transactionType;
    private boolean ommitCheck;

    @EJB
    private CommissionFacadeLocal commissionFacadeLocal;

    @Override
    protected CommissionFacadeLocal getFacade() {
        return commissionFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("savingg...........");
            selectedItem.setCentral(Boolean.TRUE);
            selectedItem.setBank(null);
            Set<CommissionFee> commissionFees1 = selectedItem.getCommissionFees();
            List<CommissionFee> commissionFees = new ArrayList<>();
            commissionFees.addAll(commissionFees1);
            Comparator<CommissionFee> comparable = new Comparator<CommissionFee>() {

                @Override
                public int compare(CommissionFee o1, CommissionFee o2) {
                    return new BigDecimal(o1.getMaxRange()).compareTo(new BigDecimal(o2.getMaxRange()));
                }
            };

            Collections.sort(commissionFees, comparable);
            for (int i = 0; i < commissionFees.size(); i++) {
                if (i != 0) {
                    BigDecimal previousTo = new BigDecimal(commissionFees.get(i - 1).getMaxRange());
                    log.log(Level.INFO, "previous:: {0}", previousTo);
                    BigDecimal from = new BigDecimal(commissionFees.get(i).getMinRange());
                    log.log(Level.INFO, "from:: {0}", from);
                    if (!from.subtract(previousTo).equals(BigDecimal.ONE)) {
                        commissionFee = new CommissionFee();
                        commissionFee.setMinRange(previousTo.add(BigDecimal.ONE).toString());
                        ommitCheck = true;
                        throw new Exception(from + " 'from transaction' Commission Fee is missing");
                    }
                }
            }
            if (selectedItem.getId() == null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                commissionFacadeLocal.create(selectedItem);
            } else {
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                commissionFacadeLocal.edit(selectedItem);
            }

            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Commission Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);

        } catch (Exception ex) {
            Logger.getLogger(CommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {
            selectedItem = new Commission();
            transactionType = new TransactionType();
            mirrorId = 0;
        }

    }

    public void addFee() {
        FacesMessage m = new FacesMessage();
        try {
            Set<CommissionFee> commissionFees = selectedItem.getCommissionFees();

            if (commissionFee.getId() == null) {
                if (!ommitCheck) {
                    for (CommissionFee fee : commissionFees) {
                        if (new BigDecimal(fee.getMaxRange()).compareTo(new BigDecimal(commissionFee.getMaxRange())) >= 0) {
                            throw new Exception("invalid commission fee; Range or Similar range exist");
                        }
                    }
                }
            }
            if (new BigDecimal(commissionFee.getMinRange()).compareTo(new BigDecimal(commissionFee.getMaxRange())) >= 0) {
                throw new Exception("invalid commission fee; Transaction From: cannot be equal or greater than Transaction To:");
            }
            BigDecimal max = null;
            if (commissionFee.getId() == null) {
                mirrorId = mirrorId - 1;
                commissionFee.setId(mirrorId);
                max = new BigDecimal(commissionFee.getMaxRange());
                selectedItem.getCommissionFees().add(commissionFee);
            } else {
                boolean contains = selectedItem.getCommissionFees().contains(commissionFee);
                if (contains) {
                    selectedItem.getCommissionFees().remove(commissionFee);
                    selectedItem.getCommissionFees().add(commissionFee);
                }
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Commission Fee Added");
            commissionFee = new CommissionFee();
            if (max != null) {
                commissionFee.setMinRange(max.add(BigDecimal.ONE).toString());
            }
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception ex) {
            Logger.getLogger(CommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
            commissionFee.setMaxRange(null);
        }

    }

    public void removeFee(CommissionFee commissionF) {
        commissionFee = commissionF;
        FacesMessage m = new FacesMessage();
        try {
            selectedItem.getCommissionFees().remove(commissionFee);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Commission Fee Removed");
            FacesContext.getCurrentInstance().addMessage(null, m);
            commissionFee = new CommissionFee();
            commissionFee.setMinRange(commissionF.getMinRange());
            commissionFee.setMaxRange(commissionF.getMaxRange());
            ommitCheck = true;
        } catch (Exception ex) {
            Logger.getLogger(CommissionBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        Object obj = selectEvent.getObject();
        if (obj instanceof TransactionType) {
            try {
                commissionFee = new CommissionFee();

                transactionType = (TransactionType) obj;
                Map<String, Object> map = new HashMap<>();
                map.put("transactionType", transactionType);
                map.put("central", true);
                map.put("c.bank is null", null);
                List<Commission> comms = commissionFacadeLocal.findAll(map, false);
                if (comms.size() > 1) {
                    throw new Exception("Unexpected outcome, "
                            + "there cannot be more than one commission setup per transaction type");
                } else if (comms.size() == 1) {
                    selectedItem = comms.get(0);
                    selectedItem = commissionFacadeLocal.find(selectedItem.getId());
                    if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED)
                            || selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED) 
                            || selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {

                        throw new Exception("Cannot modify commission setup, currently undergoing approval");
                    }
                    Comparator<CommissionFee> comparable = new Comparator<CommissionFee>() {

                        @Override
                        public int compare(CommissionFee o1, CommissionFee o2) {
                            return new BigDecimal(o1.getMaxRange()).compareTo(new BigDecimal(o2.getMaxRange()));
                        }
                    };
                    List<CommissionFee> commissionFees = new ArrayList<>();
                    commissionFees.addAll(selectedItem.getCommissionFees());
                    Collections.sort(commissionFees, comparable);
                    commissionFee.setMinRange(commissionFees.isEmpty()?"1":new BigDecimal(commissionFees.get(commissionFees.size() - 1).getMaxRange()).add(BigDecimal.ONE).toString());
                } else {
                    selectedItem = new Commission();
                    commissionFee.setMinRange("1");
                }
                showFees = true;
            } catch (Exception ex) {
                Logger.getLogger(CommissionBean.class.getName()).log(Level.SEVERE, null, ex);
                FacesMessage m = new FacesMessage();
                m.setSeverity(FacesMessage.SEVERITY_ERROR);
                m.setSummary(ex.getMessage());
                FacesContext.getCurrentInstance().addMessage(null, m);
                setShowFees(false);
            }

        } else if (obj instanceof CommissionFee) {
            commissionFee = (CommissionFee) obj;

        }
    }

    /**
     * @return the showFees
     */
    public boolean isShowFees() {
        return showFees;
    }

    /**
     * @param showFees the showFees to set
     */
    public void setShowFees(boolean showFees) {
        this.showFees = showFees;
    }

    /**
     * @return the commissionFee
     */
    public CommissionFee getCommissionFee() {
        return commissionFee;
    }

    /**
     * @param commissionFee the commissionFee to set
     */
    public void setCommissionFee(CommissionFee commissionFee) {
        this.commissionFee = commissionFee;
    }

    /**
     * @return the transactionType
     */
    public TransactionType getTransactionType() {
        return transactionType;
    }

    /**
     * @param transactionType the transactionType to set
     */
    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

}
