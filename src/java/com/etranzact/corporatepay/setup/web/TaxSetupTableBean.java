/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Tax;
import com.etranzact.corporatepay.setup.facade.TaxOfficeSetupFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "taxSetupTableBean")
@SessionScoped
public class TaxSetupTableBean extends AbstractTableBean<Tax> {

    @EJB
    private TaxOfficeSetupFacadeLocal taxSetupFacadeLocal;
    private String taxNameFilter="";

    public TaxSetupTableBean() {
        super(Tax.class);
    }

    @Override
    protected TaxOfficeSetupFacadeLocal getFacade() {
        return taxSetupFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
        if (statusFilter != null && !statusFilter.isEmpty()) {
            properties.setFlagStatuses(statusFilter.split(","));
        }
        if (!taxNameFilter.isEmpty()) {
            properties.getAdditionalFilter().put("authName", getTaxNameFilter());
        }
        return properties;
    }

    @Override
    public Tax getTableRowData(String rowKey) {
        List<Tax> wrappedData = (List<Tax>) getLazyDataModel().getWrappedData();
        for (Tax t : wrappedData) {
            if (rowKey.equals(t.getAuthCode())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Tax t) {
        return t.getAuthCode(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the taxNameFilter
     */
    public String getTaxNameFilter() {
        return taxNameFilter;
    }

    /**
     * @param taxNameFilter the taxNameFilter to set
     */
    public void setTaxNameFilter(String taxNameFilter) {
        this.taxNameFilter = taxNameFilter;
    }

    

}
