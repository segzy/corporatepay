/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankCommission;
import com.etranzact.corporatepay.model.Commission;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporatePayService;
import com.etranzact.corporatepay.setup.facade.CommissionFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporatepayServiceFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import com.etranzact.corporatepay.util.AppUtil;
import com.etranzact.corporatepay.util.ConfigurationUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "corporateSetupBean")
@SessionScoped
public class CorporateSetupBean extends AbstractBean<Corporate> {

    @EJB
    private CorporateSetupFacadeLocal corporateSetupFacadeLocal;

    @EJB
    private CorporatepayServiceFacadeLocal corporatepayServiceFacadeLocal;
    
    @EJB
    private CommissionFacadeLocal commissionFacadeLocal;

    private List<CorporatePayService> corporatePayServices;
    private List<Commission> gradBankCommissions;
 

    @Override
    protected CorporateSetupFacadeLocal getFacade() {
        return getCorporateSetupFacadeLocal(); //To change body of generated methods, choose Tools | Templates.
    }
    
       public void handleCommissionChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        String commType = (String) ((UISelectOne) e.getComponent()).getValue();
        selectedItem.setCommissionType(commType);

    }

    public CorporateSetupBean() {
        super(Corporate.class);
    }

    public void omitCommission(Corporate entity) {
        FacesMessage m = new FacesMessage();
        try {
            String msg = entity.getCorporateName() + (entity.getApplyCommission() ? " Omitted from Commission Successfully" : " Placed on Commission Successfully");
            entity.setApplyCommission(!entity.getApplyCommission());
            getCorporateSetupFacadeLocal().save(entity);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(msg);
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception e) {
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void allowOtherAccount(Corporate entity) {
        FacesMessage m = new FacesMessage();
        try {
            String msg = entity.getCorporateName() + (entity.getDisAllowOtherAccount() ? " allowed to use other bank's account" : " disallowed from using other bank's account");
            entity.setDisAllowOtherAccount(!entity.getDisAllowOtherAccount());
            getCorporateSetupFacadeLocal().save(entity);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary(msg);
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception e) {
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }
    
    public void allowCommission(Corporate entity) {
               FacesMessage m = new FacesMessage();
        try {
            entity.setApproveCommission(Boolean.TRUE);
            getCorporateSetupFacadeLocal().save(entity);
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Commision (NGN " + AppUtil.formatAsAmount(entity.getEtzCommission().toString()) + ") has been approved for " + entity.getCorporateName() );
            FacesContext.getCurrentInstance().addMessage(null, m);
        } catch (Exception e) {
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } 
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
            log.info("cooooperate....");
            selectedItem.setCorporateName(selectedItem.getCorporateName().toUpperCase());
            if(selectedItem.getEtzCommission()!=null) {
                if(selectedItem.getEtzCommission().compareTo(getLoggedBank().getEtzFee())<0) {
//                        throw new Exception("Corporate code already exist for you bank");
                    selectedItem.setApproveCommission(Boolean.FALSE);
                }
            }
            if (selectedItem.getId() == null) {
                Map<String, Object> map = new HashMap<>();
                map.put("corporateName", selectedItem.getCorporateName());
                map.put("bank", getLoggedBank());
                List<Corporate> corporates = getCorporateSetupFacadeLocal().findAll(map, false);
                if (!corporates.isEmpty()) {
                    throw new Exception("Corporate code already exist for you bank");
                }
                selectedItem.setBank(getLoggedBank());
                selectedItem.setFlagStatus(AppConstants.CREATED);
                getCorporateSetupFacadeLocal().create(selectedItem);
            } else {
                if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED)
                        || selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED)
                        || selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {
                    throw new Exception("Cannot Save Corporate: Corporate currently undergoing an approval process");
                }
                log.log(Level.INFO, "cooooperate.... {0}", selectedItem.getCorporatePayServices().size());
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                getCorporateSetupFacadeLocal().edit(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Corporate Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
            selectedItem = new Corporate();
        } catch (Exception ex) {
            Logger.getLogger(CorporateSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } finally {

        }

    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        try {
            selectedItem = getCorporateSetupFacadeLocal().find(selectedItem.getId());
        } catch (Exception ex) {
            Logger.getLogger(CorporateSetupBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleLogoUpload(FileUploadEvent event) {
        try {
            UploadedFile f = event.getFile();
            byte[] b = new byte[f.getInputstream().available()];
            f.getInputstream().read(b);
            f.getInputstream().close();
            selectedItem.setLogo(b);
            FacesMessage msg = new FacesMessage("File Upload", f.getFileName() + " uploaded successfully.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        } catch (IOException ex) {
            Logger.getLogger(CorporateSetupBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the corporatePayServices
     */
    public List<CorporatePayService> getCorporatePayServices() {
        if (corporatePayServices == null) {
            try {
                Map<String, Object> map = new HashMap<>();
                map.put("defaultService", Boolean.FALSE);
                corporatePayServices = corporatepayServiceFacadeLocal.findAll(map, false);
                // corporatePayServices.addAll(cps);

            } catch (Exception ex) {
                Logger.getLogger(CorporateSetupBean.class.getName()).log(Level.SEVERE, null, ex);
                return new ArrayList<>();
            }
        }
        return corporatePayServices;
    }

    /**
     * @param corporatePayServices the corporatePayServices to set
     */
    public void setCorporatePayServices(List<CorporatePayService> corporatePayServices) {
        this.corporatePayServices = corporatePayServices;
    }

    /**
     * @return the corporateSetupFacadeLocal
     */
    public CorporateSetupFacadeLocal getCorporateSetupFacadeLocal() {
        return corporateSetupFacadeLocal;
    }

    /**
     * @param corporateSetupFacadeLocal the corporateSetupFacadeLocal to set
     */
    public void setCorporateSetupFacadeLocal(CorporateSetupFacadeLocal corporateSetupFacadeLocal) {
        this.corporateSetupFacadeLocal = corporateSetupFacadeLocal;
    }

    /**
     * @return the gradBankCommissions
     */
    public List<Commission> getGradBankCommissions() {
        if(gradBankCommissions==null) {
            Map<String,Object> m = new HashMap<>();
            m.put("bank", getLoggedBank());
            try {
                gradBankCommissions = commissionFacadeLocal.findAll(m, true);
            } catch (Exception ex) {
                Logger.getLogger(CorporateSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return gradBankCommissions;
    }

    /**
     * @param gradBankCommissions the gradBankCommissions to set
     */
    public void setGradBankCommissions(List<Commission> gradBankCommissions) {
        this.gradBankCommissions = gradBankCommissions;
    }
    
    

}
