/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Event;
import com.etranzact.corporatepay.model.Payment;
import com.etranzact.corporatepay.model.Transaction;
import com.etranzact.corporatepay.setup.facade.EventFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "transEventTableBean")
@SessionScoped
public class TransEventTableBean extends AbstractTableBean<Event> {

    @EJB
    private EventFacadeLocal eventFacadeLocal;
    private String eventTypeFilter;
    private String eventObjectFilter="";
     private String corporateFilter="";
         private String userFilter="";

    public TransEventTableBean() {
        super(Event.class);
    }
    
        @PostConstruct
    @Override
    public void init() {
        dateField = "createdDate";
        super.init();
    }

    @Override
        public List<SelectItem> getStatusOptions() {
        List<SelectItem> options = new ArrayList<>();
        options.add(new SelectItem(Payment.class.getSimpleName(), "Authorizations"));
        options.add(new SelectItem(Transaction.class.getSimpleName(), Transaction.class.getSimpleName()+ "s") );
        return options;
    }
        
    @Override
    protected EventFacadeLocal getFacade() {
        return eventFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties properties = new TableProperties();
             properties.getAdditionalFilter().put("eventType", Transaction.class.getSimpleName());
         if (!getEventObjectFilter().isEmpty()) {
            properties.getAdditionalFilter().put("eventObject", getEventObjectFilter());
        }
            if (!corporateFilter.isEmpty()) {
            properties.getAdditionalFilter().put("corporate.corporateName", getCorporateFilter());
        }
                        if (!userFilter.isEmpty()) {
            properties.getAdditionalFilter().put("loggedUser", getUserFilter());
        }
         properties.setFlagStatuses(new String[0]);
        return properties;
    }

    @Override
    public Event getTableRowData(String rowKey) {
        List<Event> wrappedData = (List<Event>) getLazyDataModel().getWrappedData();
        for (Event t : wrappedData) {
            if (rowKey.equals(String.valueOf(t.getId()))) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(Event t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the eventTypeFilter
     */
    public String getEventTypeFilter() {
        return eventTypeFilter;
    }

    /**
     * @param eventTypeFilter the eventTypeFilter to set
     */
    public void setEventTypeFilter(String eventTypeFilter) {
        this.eventTypeFilter = eventTypeFilter;
    }

    /**
     * @return the eventObjectFilter
     */
    public String getEventObjectFilter() {
        return eventObjectFilter;
    }

    /**
     * @param eventObjectFilter the eventObjectFilter to set
     */
    public void setEventObjectFilter(String eventObjectFilter) {
        this.eventObjectFilter = eventObjectFilter;
    }

    /**
     * @return the corporateFilter
     */
    public String getCorporateFilter() {
        return corporateFilter;
    }

    /**
     * @param corporateFilter the corporateFilter to set
     */
    public void setCorporateFilter(String corporateFilter) {
        this.corporateFilter = corporateFilter;
    }

    /**
     * @return the userFilter
     */
    public String getUserFilter() {
        return userFilter;
    }

    /**
     * @param userFilter the userFilter to set
     */
    public void setUserFilter(String userFilter) {
        this.userFilter = userFilter;
    }



    

}
