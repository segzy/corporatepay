/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.Role;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.ApprovalRouteFacadeLocal;
import com.etranzact.corporatepay.setup.facade.BankSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.RoleFacadeLocal;
import com.etranzact.corporatepay.setup.facade.UserSetupFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectOne;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.apache.commons.beanutils.PropertyUtils;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "userSetupBean")
@SessionScoped
public class UserSetupBean extends AbstractBean<User> {

    private List<Role> roles;
    private List<Bank> banks;
    private List<Corporate> corporates;
    private Bank bank;
    private Corporate corporate;

    @EJB
    private UserSetupFacadeLocal userFacadeLocal;
    @EJB
    private ApprovalRouteFacadeLocal approvalRouteFacadeLocal;
    @EJB
    private BankSetupFacadeLocal bankFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateFacadeLocal;
    @EJB
    private RoleFacadeLocal roleFacadeLocal;
    private boolean showBank;
    private boolean showCorporate;
//    private Long id;

    @Override
    protected UserSetupFacadeLocal getFacade() {
        return getUserFacadeLocal(); //To change body of generated methods, choose Tools | Templates.
    }

    public UserSetupBean() {
        super(User.class);
    }
    

    @Override
    public void reset() {
        super.reset();
        bank = null;
        corporate=null;
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
//            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//            User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
            bank = bank == null ? isBankUser() ? getLoggedBank() : bank : bank;
            corporate = corporate == null ? isCorporateUser() ? getLoggedCorporate() : corporate : corporate;
            Boolean tmonday = selectedItem.getTmonday();
            Boolean ttuesday = selectedItem.getTtuesday();
                 Boolean twednesday = selectedItem.getTwednesday();
            if (bank == null && selectedItem.getRole().getBankRole()) {
                throw new Exception("Bank must be selected");
            }
            if (corporate == null && selectedItem.getRole().getMerchantRole()) {
                throw new Exception("Corporate must be selected");
            }
            if (bank != null && selectedItem.getId() == null && corporate == null) {
                BankUser bankUser = new BankUser();
                PropertyUtils.copyProperties(bankUser, selectedItem);
                selectedItem = bankUser;
                selectedItem.setCentral(Boolean.FALSE);
                selectedItem.setBankCode(bank.getId());
                ((BankUser) selectedItem).setBank(bank);
            } else if (corporate != null && selectedItem.getId() == null) {
                if(!corporate.getAllowFinalAuthCreation() && isCorporateUser() && selectedItem.getRole().getId().equals("0009")) {
                      throw new Exception("Operation Forbiden: Only your bank can create this user"); 
                }
                CorporateUser corporateUser = new CorporateUser();
                PropertyUtils.copyProperties(corporateUser, selectedItem);
                selectedItem = corporateUser;
                selectedItem.setCentral(Boolean.FALSE);
                selectedItem.setBankCode(corporate.getBank().getId());
                ((CorporateUser) selectedItem).setCorporate(corporate);
            }
                    log.log(Level.INFO, "monday:::: {0}", tmonday);
            log.log(Level.INFO, "tuesday:::: {0}", ttuesday);
                log.log(Level.INFO, "wednesday:::: {0}", twednesday);
            selectedItem.setTuesday(ttuesday?AppConstants.TURNED_ON:AppConstants.TURNED_OFF);
            selectedItem.setWednesday(twednesday?AppConstants.TURNED_ON:AppConstants.TURNED_OFF);
            log.log(Level.INFO, "monday:::: {0}", selectedItem.getTmonday());
            log.log(Level.INFO, "tuesday:::: {0}", selectedItem.getTtuesday());
                log.log(Level.INFO, "wednesday:::: {0}", selectedItem.getTwednesday());
            if (selectedItem.getId() == null) {
                selectedItem.setFlagStatus(AppConstants.CREATED);
                getUserFacadeLocal().create(selectedItem);
            } else {
                if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED) 
                        || selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED) 
                        ||  selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {
                    throw new Exception("Cannot Save User: User currently undergoing an approval process");
                }
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                getUserFacadeLocal().edit(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("User Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
                selectedItem = new User();
            setBank(null);
            showBank = false;
            setCorporate(null);
            showCorporate = false;
        } catch (Exception ex) {
            Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        } 
//        finally {
//            selectedItem = new User();
//            setBank(null);
//            showBank = false;
//            setCorporate(null);
//            showCorporate = false;
//        }

    }

    public void handleBankChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        bank = (Bank) ((UISelectOne) e.getComponent()).getValue();

    }

    public void handleCorporateChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        corporate = (Corporate) ((UISelectOne) e.getComponent()).getValue();
    }

    public void handleUsernameChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        FacesMessage m = new FacesMessage();
        try {
            Object value = ((UIInput) e.getComponent()).getValue();

            if (userFacadeLocal.findGlobalUser(String.valueOf(value)) != null) {
                log.log(Level.INFO, "event:: username exist.....");
                throw new Exception("Username already in use");
            }
        } catch (Exception ex) {
            //    Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    public void handleRoleChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        Object input = ((UISelectOne) e.getComponent()).getValue();
        Role role = (Role) input;
        setShowBank(role.getBankRole() && loggedInUser.getRole().getAdministrativeRole() && !isBankUser());
        setShowCorporate(role.getMerchantRole() && loggedInUser.getRole().getBankRole() && !isCorporateUser());
        if (isShowBank()) {
            try {
                banks = bankFacadeLocal.findAll(new HashMap(), true);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (isShowCorporate()) {
            try {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("bank", ((BankUser) loggedInUser).getBank());
                corporates = corporateFacadeLocal.findAll(map, true);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        try {
            super.onRowSelect(selectEvent);
            bank = selectedItem instanceof BankUser ? ((BankUser) selectedItem).getBank() : null;
            showBank = selectedItem instanceof BankUser;
            if (showBank) {
                banks = bankFacadeLocal.findAll(new HashMap(), true);
            }
            corporate = selectedItem instanceof CorporateUser ? ((CorporateUser) selectedItem).getCorporate() : null;
            showCorporate = selectedItem instanceof CorporateUser;
            if(bank==null)
            bank = getLoggedBank();
            bank = bank==null? corporate.getBank(): bank;
            if (showCorporate) {
                Map<String, Object> map = new HashMap<>();
                map.put("flagStatus", AppConstants.APPROVED);
                map.put("bank", bank);
                corporates = corporateFacadeLocal.findAll(map, true);
            }
//            log.log(Level.INFO, "select item {0}", selectedItem.getId());
//            log.log(Level.INFO, "select item {0}", selectedItem.getFlagStatus());
//            log.log(Level.INFO, "select item {0}", selectedItem.getFirstName());
        } catch (Exception ex) {
            Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the roles
     */
    public List<Role> getRoles() {
        try {
            roles = getRoleFacadeLocal().findRoles();
        } catch (Exception ex) {
            Logger.getLogger(UserSetupBean.class.getName()).log(Level.SEVERE, null, ex);
            roles = new ArrayList<>();
        }
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    /**
     * @return the showBank
     */
    public boolean isShowBank() {
        return showBank;
    }

    /**
     * @param showBank the showBank to set
     */
    public void setShowBank(boolean showBank) {
        this.showBank = showBank;
    }

    /**
     * @return the showCorporate
     */
    public boolean isShowCorporate() {
        return showCorporate;
    }

    /**
     * @param showCorporate the showCorporate to set
     */
    public void setShowCorporate(boolean showCorporate) {
        this.showCorporate = showCorporate;
    }

    /**
     * @return the bank
     */
    public List<Bank> getBanks() {
        return banks;
    }

    /**
     * @param banks the bank to set
     */
    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }

    /**
     * @return the userFacadeLocal
     */
    public UserSetupFacadeLocal getUserFacadeLocal() {
        return userFacadeLocal;
    }

    /**
     * @param userFacadeLocal the userFacadeLocal to set
     */
    public void setUserFacadeLocal(UserSetupFacadeLocal userFacadeLocal) {
        this.userFacadeLocal = userFacadeLocal;
    }

    /**
     * @return the approvalRouteFacadeLocal
     */
    public ApprovalRouteFacadeLocal getApprovalRouteFacadeLocal() {
        return approvalRouteFacadeLocal;
    }

    /**
     * @param approvalRouteFacadeLocal the approvalRouteFacadeLocal to set
     */
    public void setApprovalRouteFacadeLocal(ApprovalRouteFacadeLocal approvalRouteFacadeLocal) {
        this.approvalRouteFacadeLocal = approvalRouteFacadeLocal;
    }

    /**
     * @return the bankFacadeLocal
     */
    public BankSetupFacadeLocal getBankFacadeLocal() {
        return bankFacadeLocal;
    }

    /**
     * @param bankFacadeLocal the bankFacadeLocal to set
     */
    public void setBankFacadeLocal(BankSetupFacadeLocal bankFacadeLocal) {
        this.bankFacadeLocal = bankFacadeLocal;
    }

    /**
     * @return the corporateFacadeLocal
     */
    public CorporateSetupFacadeLocal getCorporateFacadeLocal() {
        return corporateFacadeLocal;
    }

    /**
     * @param corporateFacadeLocal the corporateFacadeLocal to set
     */
    public void setCorporateFacadeLocal(CorporateSetupFacadeLocal corporateFacadeLocal) {
        this.corporateFacadeLocal = corporateFacadeLocal;
    }

    /**
     * @return the roleFacadeLocal
     */
    public RoleFacadeLocal getRoleFacadeLocal() {
        return roleFacadeLocal;
    }

    /**
     * @param roleFacadeLocal the roleFacadeLocal to set
     */
    public void setRoleFacadeLocal(RoleFacadeLocal roleFacadeLocal) {
        this.roleFacadeLocal = roleFacadeLocal;
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

//    private void copyProperties(BankUser bankUser, User selectedItem) {
//        bankUser.setActive(selectedItem.getActive()); 
//        bankUser.setCentral(Boolean.FALSE);
//        bankUser.setChangePassword(selectedItem.getChangePassword());
//        bankUser.setEmail(selectedItem.getEmail());
//        bankUser.setFirstName(selectedItem.getFirstName());
//        bankUser.setFromTimeAccess(selectedItem.);
//    }
//
//    private void copyProperties(CorporateUser corporateUser, User selectedItem) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
