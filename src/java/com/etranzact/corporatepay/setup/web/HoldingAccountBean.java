/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.Bank;
import com.etranzact.corporatepay.model.BankHoldingAccount;
import com.etranzact.corporatepay.model.Corporate;
import com.etranzact.corporatepay.model.HoldingAccount;
import com.etranzact.corporatepay.setup.facade.CorporateSetupFacadeLocal;
import com.etranzact.corporatepay.setup.facade.HoldingAccountFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UISelectBoolean;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "holdingAccountBean")
@SessionScoped
public class HoldingAccountBean extends AbstractBean<HoldingAccount> {

    private Bank bank;
    private Corporate corporate;
    private Corporate currCorporate;
    private List<Corporate> corporates;
    private boolean global = true;

    @EJB
    private HoldingAccountFacadeLocal holdingAccountFacadeLocal;
    @EJB
    private CorporateSetupFacadeLocal corporateSetupFacadeLocal;

    @Override
    protected HoldingAccountFacadeLocal getFacade() {
        return holdingAccountFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    public HoldingAccountBean() {
        super(HoldingAccount.class);
    }

    @Override
    public void reset() {
        getSelectedItem();
    }

    @Override
    public void onRowSelect(SelectEvent selectEvent) {
        super.onRowSelect(selectEvent);
        currCorporate = ((BankHoldingAccount) selectedItem).getCorporate();
        corporate = currCorporate;
        if (currCorporate != null) {
            global = false;
        } else {
            global = true;
        }
    }

    public void save() {
        FacesMessage m = new FacesMessage();
        try {
        
            if (getLoggedBank().getId().equals("700")) {
                    if (!(selectedItem.getCardNumber().substring(0, 3).equals(getLoggedBank().getId()) ||
                            selectedItem.getCardNumber().startsWith("7"))) {
                    throw new Exception("Card is not valid");
                }
            } else {
                if (!selectedItem.getCardNumber().substring(0, 3).equals(getLoggedBank().getId())) {
                    throw new Exception("Card is not valid");
                }
            }
            Map<String, Object> map = new HashMap<>();
            if (((BankHoldingAccount) selectedItem).getCorporate() != null) {
                map.put("h.corporate=" + ((BankHoldingAccount) selectedItem).getCorporate().getId(), null);
            } else {
                map.put("h.corporate is null", null);
            }
            holdingAccountFacadeLocal.setEntityNamePrefix("Bank");
            List<HoldingAccount> findAll = holdingAccountFacadeLocal.findAll(map, new String[]{});
            log.log(Level.INFO, "holding account:: {0}", findAll.size());
            log.log(Level.INFO, "other {0}", (selectedItem.getId() == null || !corporate.equals(currCorporate)));
            if (!findAll.isEmpty() && (selectedItem.getId() == null || !corporate.equals(currCorporate))) {
                throw new Exception("Holding Account Already exist");
            }
            if (selectedItem.getId() == null) {
                    selectedItem.setFlagStatus(AppConstants.CREATED);
                holdingAccountFacadeLocal.create(selectedItem);
            } else {
                if (!(selectedItem.getFlagStatus().equals(AppConstants.APPROVED)
                        || selectedItem.getFlagStatus().equals(AppConstants.MODIFIED_REJECTED)
                        || selectedItem.getFlagStatus().equals(AppConstants.CREATED_REJECTED))) {
                    throw new Exception("Cannot Save Holding Account: Holding Account currently undergoing an approval process");
                }
                selectedItem.setFlagStatus(AppConstants.MODIFIED);
                holdingAccountFacadeLocal.edit(selectedItem);
            }
            m.setSeverity(FacesMessage.SEVERITY_INFO);
            m.setSummary("Holding Account Saved Successfully and Sent for Approval");
            FacesContext.getCurrentInstance().addMessage(null, m);
            selectedItem = new BankHoldingAccount();
            ((BankHoldingAccount) selectedItem).setBank(getLoggedBank());
            ((BankHoldingAccount) selectedItem).setCorporate(null);
            setBank(null);
            setCorporate(null);
        } catch (Exception ex) {
            Logger.getLogger(HoldingAccountBean.class.getName()).log(Level.SEVERE, null, ex);
            m.setSeverity(FacesMessage.SEVERITY_ERROR);
            m.setSummary(ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, m);
        }

    }

    public void handleGlobalChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        Object input = ((UISelectBoolean) e.getComponent()).getValue();
        if (Boolean.valueOf(String.valueOf(input))) {
            corporate = null;
            ((BankHoldingAccount) selectedItem).setCorporate(corporate);
        }
    }

    public void handleCorporateChange(AjaxBehaviorEvent e) throws AbortProcessingException {
        Object input = ((UISelectOne) e.getComponent()).getValue();
        corporate = (Corporate) input;
        log.log(Level.INFO, "input ... {0}", input);
        ((BankHoldingAccount) selectedItem).setCorporate(corporate);
    }

    /**
     * @return the bank
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * @param bank the bank to set
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @return the corporate
     */
    public Corporate getCorporate() {
        return corporate;
    }

    /**
     * @param corporate the corporate to set
     */
    public void setCorporate(Corporate corporate) {
        this.corporate = corporate;
    }

    /**
     * @return the corporates
     */
    public List<Corporate> getCorporates() {
        if (corporates == null && isBankUser()) {
            try {
                Map<String, Object> hashMap = new HashMap<>();
                hashMap.put("bank", getLoggedBank());
                corporates = corporateSetupFacadeLocal.findAll(hashMap, true, new String[]{"corporateName"});
            } catch (Exception ex) {
                Logger.getLogger(HoldingAccountBean.class.getName()).log(Level.SEVERE, null, ex);
                corporates = new ArrayList<>();
            }
        }
        return corporates;
    }

    /**
     * @param corporates the corporates to set
     */
    public void setCorporates(List<Corporate> corporates) {
        this.corporates = corporates;
    }

    /**
     * @return the global
     */
    public boolean isGlobal() {
        return global;
    }

    /**
     * @param global the global to set
     */
    public void setGlobal(boolean global) {
        this.global = global;
    }

    /**
     * @return the selectedItem
     */
    @Override
    public HoldingAccount getSelectedItem() {
        if (selectedItem == null) {
            this.selectedItem = new BankHoldingAccount();
            this.selectedItem.setCorporate(null);
        }
        ((BankHoldingAccount) selectedItem).setBank(getLoggedBank());

        return selectedItem;
    }

}
