/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etranzact.corporatepay.setup.web;

import com.etranzact.corporatepay.core.web.*;
import com.etranzact.corporatepay.model.ApprovalType;
import com.etranzact.corporatepay.model.BankUser;
import com.etranzact.corporatepay.model.CorporateUser;
import com.etranzact.corporatepay.model.User;
import com.etranzact.corporatepay.setup.facade.ApprovalTypeFacadeLocal;
import com.etranzact.corporatepay.util.AppConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author oluwasegun.idowu
 */
@ManagedBean(name = "approvalTypeTableBean")
@SessionScoped
public class ApprovalTypeTableBean extends AbstractTableBean<ApprovalType> {

    @EJB
    private ApprovalTypeFacadeLocal approvalTypeFacadeLocal;

    public ApprovalTypeTableBean() {
        super(ApprovalType.class);
    }

    @Override
    protected ApprovalTypeFacadeLocal getFacade() {
        return approvalTypeFacadeLocal; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected TableProperties getTableProperties() {
        TableProperties tableProperties = new TableProperties();
        tableProperties.setFlagStatuses(new String[0]);
//        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//        User loggedInUser = (User) externalContext.getSessionMap().get(AppConstants.LOGIN_USER);
        Map<String,Object> filters = new HashMap<>();
        if (isBankUser()) {
            filters.put("a.accessLevel in ('" + AppConstants.ALL_ROLE_TYPE_ACCESS + "','" + 
                    AppConstants.BANK_ADMINISTRATOR_ACCESS + "','" + AppConstants.BANKADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS
                    + "','" + AppConstants.ADMINISTRATOR_BANKADMINISTRATOR_ACCESS + "')" , null);
        } else if(isCorporateUser()) {
               filters.put("a.accessLevel in ('" + AppConstants.ALL_ROLE_TYPE_ACCESS + "','" + 
                    AppConstants.CORPORATE_ADMINISTRATOR_ACCESS + "','" + AppConstants.BANKADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS
                    + "','" + AppConstants.ADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS + "')" , null);
        } else {
                 filters.put("a.accessLevel in ('" + AppConstants.ALL_ROLE_TYPE_ACCESS + "','" + 
                    AppConstants.ADMINISTRATOR_ACCESS + "','" + AppConstants.ADMINISTRATOR_BANKADMINISTRATOR_ACCESS
                    + "','" + AppConstants.ADMINISTRATOR_CORPORATEADMINISTRATOR_ACCESS + "')" , null);
        }
        tableProperties.setAdditionalFilter(filters);
        return tableProperties;
    }

      @Override
    public ApprovalType getTableRowData(String rowKey) {
        List<ApprovalType> wrappedData = (List<ApprovalType>) getLazyDataModel().getWrappedData();
        for (ApprovalType t : wrappedData) {
            if (rowKey.equalsIgnoreCase(t.getId())) {
                return t;
            }
        }
        return null;
    }

    @Override
    public Object getTableKey(ApprovalType t) {
        return t.getId(); //To change body of generated methods, choose Tools | Templates.
    }


    
}
